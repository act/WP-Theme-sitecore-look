<?php
/**
 * Footer used by the theme
 * Contains bottom of page + html tags closure
 *
 * Author: Eric Mathieu
 * Last modification: 31/01/2017
 */
?> 


<?php //Get the page ID containing the ready for more content (can be a parent page)
$id_page_more=get_the_ID(); //by default, we'll display the content of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('moreinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('moreinherit',$ancestor) == 0) 
		{ 
			$id_page_more=$ancestor; //get id of parent page
			break;
		}
	}
}

 //Get the page ID containing the header and footer pictures (can be a parent page)
$id_page_picture=get_the_ID(); //by default, we'll display the pictures of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('backgroundimagesinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('backgroundimagesinherit',$ancestor) == 0) 
		{ 
			$id_page_picture=$ancestor; //get id of parent page
			break;
		}
	}
}
?>

<?php if (get_field('moretitle1',$id_page_more)<>"") { //Display the more fields only if there is a content in the first title field. If not, we consider that there is nothing to display?>
<div class="img-bg-section prefooter" style="background-image: url('<?php echo get_field('imagefooter',$id_page_picture)['url'];?>')" id="Prefooter1">
	<div class="wrapper">
		<div class="row-small m-pad">
			<div class="pane base6 t-base8">
				<article class="row pad-around pad-around-large bg-grey-transparent row-margin">
					<h2 class="row-margin-small text-transform-uppercase text-weight-medium text-size-25">Ready for more?</h2>
					<div class="row-normal row-margin-small">
						<?php if (get_field('moretitle1',$id_page_more)<>"") {?>
							<div class="pane base6 t-base6 m-margin-bottom">
								<a href='<?php echo get_field('morelink1',$id_page_more);?>' class='text-color-white link-decoration-none prefooter-icon-<?php echo get_field('moreicon1',$id_page_more);?> prefooter-invisible-cell' >
									<span class="display-tablecell pad-left">
										<strong class="text-weight-medium link-decoration-border-dark"><?php echo get_field('moretitle1',$id_page_more);?></strong>
									</span>
								</a>
							</div>
						<?php } ?>
						<?php if (get_field('moretitle2',$id_page_more)<>"") {?>
							<div class="pane base6 t-base6 m-margin-bottom">
								<a href='<?php echo get_field('morelink2',$id_page_more);?>' class='text-color-white link-decoration-none prefooter-icon-<?php echo get_field('moreicon2',$id_page_more);?> prefooter-invisible-cell' >
									<span class="display-tablecell pad-left">
										<strong class="text-weight-medium link-decoration-border-dark"><?php echo get_field('moretitle2',$id_page_more);?></strong>
									</span>
								</a>
							</div>
						<?php } ?>
					</div>
					<div class="row-normal">
						<?php if (get_field('moretitle3',$id_page_more)<>"") {?>
							<div class="pane base6 t-base6 m-margin-bottom">
								<a href='<?php echo get_field('morelink3',$id_page_more);?>' class='text-color-white link-decoration-none prefooter-icon-<?php echo get_field('moreicon3',$id_page_more);?> prefooter-invisible-cell' >
									<span class="display-tablecell pad-left">
										<strong class="text-weight-medium link-decoration-border-dark"><?php echo get_field('moretitle3',$id_page_more);?></strong>
									</span>
								</a>
							</div>
						<?php } ?>
						<?php if (get_field('moretitle4',$id_page_more)<>"") {?>
							<div class="pane base6 t-base6 m-margin-bottom">
								<a href='<?php echo get_field('morelink4',$id_page_more);?>' class='text-color-white link-decoration-none prefooter-icon-<?php echo get_field('moreicon4',$id_page_more);?> prefooter-invisible-cell' >
									<span class="display-tablecell pad-left">
										<strong class="text-weight-medium link-decoration-border-dark"><?php echo get_field('moretitle4',$id_page_more);?></strong>
									</span>
								</a>
							</div>
						<?php } ?>
					</div>
				</article>
			</div>
		</div>
	</div>
</div>
<?php } ?>

</div><!-- /wrapper-wide -->

</div><!-- /footer-push -->
    <footer class="main-footer" id="#MainFooter">
        <!-- /wrapper -->

        <div class="footer-links">

            <div class="wrapper m-pad">

                <div class="row-normal">
                    <div class="pane base9">
                        <ul>
                            <li>
								<a href='http://www.reading.ac.uk/internal/finance/formsguidesandpolicies/fcs-TheUniversitysCharitableStatus.aspx' target='_blank' >Charitable status </a>                                    </li>
                            <li>
								<a href='http://www.reading.ac.uk/15/about/about-accessibility.aspx' target='_blank' >Accessibility</a>                                    </li>
                            <li>
								<a href='http://www.reading.ac.uk/15/about/about-privacy.aspx' target='_blank' >Privacy policy</a>                                    </li>
                            <li>
								<a href='http://www.reading.ac.uk/15/about/about-privacy.aspx#cookies' target='_blank' >Cookies</a>                                    </li>
                            <li>
								<a href='http://www.reading.ac.uk/about/terms-of-use.aspx' target='_blank' >Terms of use</a>                                    </li>
                            <li>
								<a href='http://www.reading.ac.uk/sitemap.aspx' target='_blank' title='This opens the sitemap page in a new window' >Sitemap</a>                                    </li>
                        </ul>

                    </div>

                    <div class="pane base3">
                        <p class="copy">&copy;
                            <a href='http://www.reading.ac.uk/15/about/about-copyright.aspx' target='_blank' >University of Reading</a>
                        </p>
                    </div>

                </div>

            </div>
            <!-- /wrapper -->

        </div>
    </footer>


    <input type="hidden" class="require-js" value="<?php echo get_template_directory_uri(); ?>/js/common/cleanupForms.js" />

    <script src="<?php echo get_template_directory_uri(); ?>/js/bundled.js"></script>

    <!--[if lte IE 9]>
        <script src="/assets/js/vendor/selectivizr-min.js"></script>
    <![endif]-->

    <script src="<?php echo get_template_directory_uri(); ?>/js/sitebundled.js?v=COtDGKsC_NVFFfqT3bLx9jTpkcA8wNTgvARiyOSy1rM1"></script>
	
    
	<?php wp_footer(); ?>
    

</body>
</html>