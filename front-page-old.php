<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */



  get_header('home');
  
  
  
//Get the page ID containing the sideboxes (can be a parent page)
$id_page_boxes=get_the_ID(); //by default, we'll display the sideboxes of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('boxinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('boxinherit',$ancestor) == 0) 
		{ 
			$id_page_boxes=$ancestor; //get id of parent page
			break;
		}
	}
}

?>


<div class="row bg-light-grey">
    <div class="wrapper paddingtop">
      
        <div class="row-medium masonry-area">     
				
				<?php
				while (have_posts()) :
					the_post(); ?>
					<div class="pane base8 t-base6 pane-around">
						<article class="bg-white pad-around">
						
							
							<?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
							<small><?php the_time('F jS, Y'); ?></small><br>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(151,151)  ); ?></a>
							<?php the_excerpt();
							?>
						</article> 
					</div><?php
				endwhile; 	
				if (get_field('include_page')!="") {echo file_get_contents(get_field('include_page')); }
							
				
				?>
				
			
		<?php if (get_field('box1content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box1content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box2content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box2content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box3content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box3content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box4content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box4content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
			<?php
				get_sidebar();
			?>
        </div>


    </div>
</div>
<?php 
	get_footer();
 ?>
