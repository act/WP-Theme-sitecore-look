<?php
/**
 * University of Reading Site Core theme functions and definitions.
 *
 */


// Add default posts and comments RSS feed links to head.
add_theme_support( 'automatic-feed-links' );

//Add styles for tinyMCE editor
function wpdocs_theme_add_editor_styles() {
    add_editor_style( './css/custom-editor-style.css' );
}
add_action( 'admin_init', 'wpdocs_theme_add_editor_styles' );


//Enable support for Post Thumbnails on posts and pages.
add_theme_support( 'post-thumbnails', array( 'post' ) );

//This theme has one menu location, on the left */
function add_menu()
{
    register_nav_menu( 'primary', __( 'Primary Menu', 'UniversityOfReading' ) );
}
add_action('init', 'add_menu');

/*
 * Switch default core markup for search form, comment form, and comments
 * to output valid HTML5.
 */
add_theme_support( 'html5', array(
	'search-form',
	'comment-form',
	'comment-list',
	'gallery',
	'caption',
) );

add_theme_support( 'title-tag' );
 
 
 




//Get the list of menus */
function get_menu_by_location( $location ) {
    if( empty($location) ) return false;

    $locations = get_nav_menu_locations();
    if( ! isset( $locations[$location] ) ) return false;

    $menu_obj = get_term( $locations[$location], 'nav_menu' );

    return $menu_obj;
}

/************************************************
// function to read the menu items and sort them in an array */
function wp_get_menu_array($current_menu) {
 
    $array_menu = wp_get_nav_menu_items($current_menu);
    $menu = array();
    foreach ($array_menu as $m) {
        if (empty($m->menu_item_parent)) {
            $menu[$m->ID] = array();
            $menu[$m->ID]['ID']      =   $m->ID;
            $menu[$m->ID]['title']       =   $m->title;
            $menu[$m->ID]['url']         =   $m->url;
            $menu[$m->ID]['children']    =   array();
        }
    }
    $submenu = array();
    foreach ($array_menu as $m) {
        if ($m->menu_item_parent) {
            $submenu[$m->ID] = array();
            $submenu[$m->ID]['ID']       =   $m->ID;
            $submenu[$m->ID]['title']    =   $m->title;
            $submenu[$m->ID]['url']  =   $m->url;
            $menu[$m->menu_item_parent]['children'][$m->ID] = $submenu[$m->ID];
        }
    }
    return $menu;
     
}

//$menu = wp_get_menu_array('research-menu');


/************************************************
/* Filter to limit the size of the page titles*/
add_filter('the_title', 'truncate_long_title');
function truncate_long_title($title)
{
    if (strlen($title) > 70) {
        $title = substr($title, 0, 70).'...';
    }
    return $title;
}


/************************************************
/* max size of the pictures / elements in articles (may not be actually taken into account by this theme, but requested for standardisation) */
if ( ! isset( $content_width ) ) {
	$content_width = 730;
}


/************************************************
/* Functions to highlight search results: */
function search_excerpt_highlight() {
    $excerpt = get_the_excerpt();
    $keys = preg_quote(implode('|', explode(' ', get_search_query())));
    $excerpt = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $excerpt);

    echo '<p>' . $excerpt . '</p>';
}
function search_title_highlight() {
    $title = get_the_title();
    $keys = preg_quote(implode('|', explode(' ', get_search_query())));
    $title = preg_replace('/(' . $keys .')/iu', '<strong class="search-highlight">\0</strong>', $title);

    echo $title;
}


/************************************************
 * Register a sidebar widget area.
 *
 * copied from Twenty Fourteen 1.0
 */
function uor_widgets_init() {
	register_sidebar( array(
		'name'          => __( '1/3rd side bar', 'UoR_research' ),
		'id'            => 'sidebar-1',
		'description'   => __( 'Main sidebar that generally appears on the right.', 'UoR_research' ),
		'before_widget' => '<div class="pane base4 t-base6 pane-around"> <article class="bg-white pad-around">',
		'after_widget'  => '</article></div>',
		'before_title'  => '<h2 class="headline nopaddingbottom">',
		'after_title'   => '</h2>',
	) );
	register_sidebar( array(
		'name'          => __( '2/3rd widget area', 'UoR_research' ),
		'id'            => 'sidebar-2',
		'description'   => __( 'Larger area that will appear under the article', 'UoR_research' ),
		'before_widget' => '<div class="pane base8 t-base6 pane-around"> <article class="bg-white pad-around">',
		'after_widget'  => '</article></div>',
		'before_title'  => '<h2 class="headline nopaddingbottom">',
		'after_title'   => '</h2>',
	) );
}
add_action( 'widgets_init', 'uor_widgets_init' );

/************************************************
//Unregister some useless widgets */
function my_widgets_init() {
    unregister_widget( 'WP_Widget_Calendar' );
    unregister_widget( 'WP_Widget_Archives' );
    unregister_widget( 'WP_Widget_Meta' );
    unregister_widget( 'WP_Widget_Recent_Comments' );
    unregister_widget( 'WP_Widget_Text' );
    unregister_widget( 'WP_Widget_Pages' );
    unregister_widget( 'WP_Widget_Recent_Posts' );
    unregister_widget( 'WP_Widget_Tag_Cloud' );
}
//add_action('widgets_init', 'my_widgets_init');



//***********************************************
//Replace [split] shortcode by closing the current box and creating a new one
function split_func( $atts ) {
	$chain = '</article></div><div class="pane base8 t-base6 pane-around"><article class="bg-white pad-around">';
	if(isset($atts['width'])){
		if ($atts['width']=="small")
			$chain = '</article></div><div class="pane base4 t-base6 pane-around"><article class="bg-white pad-around">';
	}
	return $chain;
}
add_shortcode( 'split', 'split_func' );


//***********************************************
// This is PHP function to convert a user-supplied URL to just the domain name,
// which I use as the link text. (https://gist.github.com/davejamesmiller/1965937)
// Remember you still need to use htmlspecialchars() or similar to escape the
// result.
function url_to_domain($url)
{
    $host = @parse_url($url, PHP_URL_HOST);

    // If the URL can't be parsed, use the original URL
    // Change to "return false" if you don't want that
    if (!$host)
        $host = $url;

    // The "www." prefix isn't really needed if you're just using
    // this to display the domain to the user
    if (substr($host, 0, 4) == "www.")
        $host = substr($host, 4);

    return $host;
}

//***********************************************
//Function to check that the provided URL is from the University domain. If not, it is not loaded.
function include_external_page ($pageURL){
	if (substr(url_to_domain($pageURL), -13)!='reading.ac.uk'){
		echo "This page is trying to load an unauthorized external ressource...";
	}
	else {
		echo file_get_contents($pageURL); 
	}
}

/************************************************
 * Hide the Add New User page
 */
function custom_menu_page_removing() {
    remove_submenu_page( 'users.php','user-new.php' );
}
add_action( 'admin_menu', 'custom_menu_page_removing' );

// Hide button to add user on users list page
function this_screen() {
    $current_screen = get_current_screen();
    if( $current_screen->id === "users" ) {
		echo '<style type="text/css">
		.page-title-action { display:none; }
		</style>';
    }
}
add_action( 'current_screen', 'this_screen' );

?>