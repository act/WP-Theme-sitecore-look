<?php
/**
 * Header used by the theme
 * Contains header tag + menus
 *
 * This theme is a copy of the Site Core theme designed for the University of Reading. 
 * All css and javascript are copy of AE.
 */
 

$menu_obj = get_menu_by_location("primary"); 

//$menu = wp_get_menu_array('auto-menu-from-pages');
$menu = wp_get_menu_array($menu_obj->name);

?><!DOCTYPE html>
<html class="no-js" lang="en">
<!--<![endif]-->
<head>
	<?php wp_head(); ?>
    <title><?php bloginfo('name'); ?></title>
    <meta name="keywords" content=".">
    <meta name="description" content="The University of Reading is defined and driven forward by its research.">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width,initial-scale=1.0,minimum-scale=1.0"/>
	
    <link href="<?php echo get_template_directory_uri(); ?>/css/uorResearch.css" rel="stylesheet"/>
	<!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
	<script src="<?php echo get_template_directory_uri(); ?>/js/uorResearch.js"></script>  Removed on 07/02/2017 by E. Mathieu because useless -->
	<script src="<?php echo get_template_directory_uri(); ?>/js/modernizr-2.6.2-respond-1.1.0.min.js"></script>  
</head>

<body class="bg-light-grey cookie-visible">

    <!--[if lt IE 7]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->
    <a href="#skipnav" class="skip">Skip to main content</a>

    <div class="footer-push">

        
<meta name="VIcurrentDateTime" content="636192978659658614" />

        
<input type="hidden" id="lastUpdatedCookieDate" value="" />
<header class="cookie" id="cookie" aria-hidden="true">

    <div class="wrapper m-pad">
        <div class="pane base12">
            <p class="headline text-color-white">University of Reading Cookie Policy</p>
            <p>
                We use cookies on reading.ac.uk to improve your experience. Find out more about our
<a href="http://www.reading.ac.uk/15/about/about-privacy.aspx#cookies" target="_blank" class="text-color-white link-decoration-border">cookie policy</a>. By continuing to use our site you accept these terms, and are happy for us to use cookies to improve your browsing experience.
            </p>

            <p>
                <a id="dismissCookie" href="#cookie" class="button button-transparent chevron-right">Continue using the University of Reading website</a>
            </p>
        </div>
    </div>
</header>


        
<!-- Google Tag Manager -->
<noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TC325M"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'//www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TC325M');</script>
<!-- End Google Tag Manager -->
        <nav class="row bg-light-grey mobile-hidden tablet-hidden" id="SuperHeader">

        <div class="wrapper nopaddingbottom">

            <div class="pad-row display-table full-width">
                <div class="base7 display-tablecell">
                    <ul class="pad-none display-table courtesy-nav" role="navigation">

                            <li class="display-tablecell text-align-center pad-sides  ">                             
                                <a href='http://www.reading.ac.uk/A-Z/az-academic.aspx' class='text-size-13 text-color-headings text-weight-medium' target='_blank' title='Visit our school and departmental sites' >Schools & departments</a>
                            </li>
                            <li class="display-tablecell text-align-center pad-sides border-left ">                             
                                <a href='http://www.henley.ac.uk' class='text-size-13 text-color-headings text-weight-medium' target='_blank' title='Find out more about our triple-accredited Henley Business School' >Henley Business School</a>
                            </li>
                            <li class="display-tablecell text-align-center pad-sides border-left ">                             
                                <a href='http://www.reading.edu.my' class='text-size-13 text-color-headings text-weight-medium' target='_blank' title='Discover our Malaysia campus' >University of Reading&nbsp;Malaysia</a>
                            </li>
                    </ul>
                </div>
                <div class="base5 display-tablecell">
                    <ul class="pad-none display-table meta-nav pull-right" role="navigation">

                            <li class="display-tablecell text-align-center pad-sides  ">

                                <a href='http://www.reading.ac.uk/alumni' class='text-size-13 text-color-headings text-weight-medium' target='_blank' >Alumni</a>
                            </li>
                            <li class="display-tablecell text-align-center pad-sides border-left ">

                                <a href='http://student.reading.ac.uk/essentials' class='text-size-13 text-color-headings text-weight-medium' target='_blank' >Essentials</a>
                            </li>
                            <li class="display-tablecell text-align-center pad-sides border-left ">

                                <a href='http://www.reading.ac.uk/staff' class='text-size-13 text-color-headings text-weight-medium' target='_blank' >Staff</a>
                            </li>
                    </ul>
                </div>
            </div>
        </div>
    </nav>
    <header id="SuperHeaderMainHeader" class="main-header bg-white t-bg-standout text-color-black">

        <div class="wrapper nopaddingbottom">

            <div class="row-normal m-margin-none">
                <div class="pane m-base2 desktop-hidden">
                    <a href="#navigation" class="icon-menu">Show/Hide navigation</a>
                </div>
                <div class="pane base7 m-base8 t-text-align-center">
<a href='/' class='home-link mobile-hidden tablet-hidden' ><img src='<?php echo get_template_directory_uri(); ?>/images/logo-reading.png?h=40&amp;w=116&amp;la=en' alt='University of Reading' /></a>                    <a href='/' class='home-link desktop-hidden' ></a>
                </div>
                    <!--Desktop search-->
    <form class="search-form pane base5 text-align-right mobile-hidden tablet-hidden" action="" id="DesktopHeaderSearchForm">
        <div class="text-input display-table pad-none text-nowrap">
            <div class="display-tablecell">
                <input type="search" title="Search term" class="raise-on-focus text-input text-input-naked  shadow-none" placeholder="Search in" id="DesktopSearchTerm">
            </div>
            <div class="display-tablecell">
                <select title="Select a category" class="raise-on-focus select-input shadow-none partial-border-left border-none " id="DesktopSearchCat">
                    <option value="http://www.reading.ac.uk/allsearchresults.aspx">Everything</option>
                        <option value="http://www.reading.ac.uk/staffsearchresults.aspx" data-id="b9b9b7e1-24c6-4213-8499-f747655aeb8a">Staff</option>
                        <option value="http://www.reading.ac.uk/searchresults.aspx" data-id="c0bdcdd1-810e-4a0d-98fe-93e020af1813">Courses</option>
                        <option value="http://www.reading.ac.uk/searchresults.aspx" data-id="54f11334-1dd7-4f46-87b4-4f7bb5852a0d">Content</option>
                </select>
            </div>

            <button type="submit" title="Start search" class="raise-on-focus display-tablecell button partial-border-left button-white"><i class="button-search-icon ">Search</i></button>

        </div>
    </form>
    <!--mobile search-->
    <div class="desktop-hidden pane m-base2 t-text-right">
        <button type="button" title="Open search" class="focus-light button-search-icon-white-large display-inline-block btnSearch" id="MobileSearchOpen">Open Search form</button>
    </div>
    <form class="search-form-mobile bg-standout" id="MobileSearchForm">
        <div class="row t-pad-around">
            <input type="search" title="Search term" class="focus-light pane m-base9 t-base10 text-input text-weight-regular text-input-standout-dark" id="MobileSearchTerm">
            <div class="display-inline-block pull-left m-pad-row-xsmall t-pull-left-large">
                <button type="submit" title="Start search" class="focus-light button-search-icon-white-large" id="MobileSearchSubmit">Search</button>
            </div>
            <div class="pane m-base3 t-base2 t-text-align-center">
                <button type="button" title="Cancel search" class="focus-light button button-naked pad-sides-small text-color-white m-button-inline" id="MobileSearchClose">Cancel</button>
            </div>
        </div>
        <fieldset class="row-none bg-light-grey pad-sides text-color-headings">
            <legend class="visuallyhidden">Mobile search categories</legend>
            <div class="pane m-base12">
                <input type="radio" name="search filter" class="visuallyhidden raise-on-focus" id="SearchForEverything" checked="checked" value="http://www.reading.ac.uk/allsearchresults.aspx">
                <label for="SearchForEverything" class="button button-block button-light row-no-margin pad-row">Everything</label>
            </div>
                <div class="pane m-base4">
                    <input type="radio" name="search filter" class="visuallyhidden raise-on-focus" id="SearchForStaff"
                           value="http://www.reading.ac.uk/staffsearchresults.aspx" data-id="b9b9b7e1-24c6-4213-8499-f747655aeb8a">
                    <label for="SearchForStaff" class="button button-block button-light row-no-margin pad-row">Staff</label>
                </div>
                <div class="pane m-base4">
                    <input type="radio" name="search filter" class="visuallyhidden raise-on-focus" id="SearchForCourses"
                           value="http://www.reading.ac.uk/searchresults.aspx" data-id="c0bdcdd1-810e-4a0d-98fe-93e020af1813">
                    <label for="SearchForCourses" class="button button-block button-light row-no-margin pad-row">Courses</label>
                </div>
                <div class="pane m-base4">
                    <input type="radio" name="search filter" class="visuallyhidden raise-on-focus" id="SearchForContent"
                           value="http://www.reading.ac.uk/searchresults.aspx" data-id="54f11334-1dd7-4f46-87b4-4f7bb5852a0d">
                    <label for="SearchForContent" class="button button-block button-light row-no-margin pad-row">Content</label>
                </div>
        </fieldset>
    </form>
    <input type="hidden" id="ProductType" value="DiscoverReading" />

            </div>

            <div class="row-normal">
                    <!-- Desktop Navigation -->
                    <nav class="mobile-hidden tablet-hidden">
                        <ul class="display-block no-indent row-no-margin text-weight-medium navigation-items">
                                <li class="current display-inline-block pad-around-small border-top margin-top">
                                    <a href="http://www.reading.ac.uk/ready-to-study.aspx" class="text-color-black hover-color-standout pane">Study &amp; Life</a>
                                </li>
                                <li class="current display-inline-block pad-around-small border-top margin-top">
                                    <a href="http://www.reading.ac.uk/research.aspx" class="text-color-black hover-color-standout pane">Research</a>
                                </li>
                                <li class="current display-inline-block pad-around-small border-top margin-top">
                                    <a href="http://www.reading.ac.uk/about.aspx" class="text-color-black hover-color-standout pane">About Us</a>
                                </li>
                        </ul>
                    </nav>


            </div>

        </div>

        <!-- /wrapper -->
    </header>
    <div>
            <nav class="mainnavigation navigation desktop-hidden" id="navigation">

                <!-- Mobile Navigation -->


                <div class="navigation-mask"></div>
                <div class="navigation-items">
                    <ul class="display-block no-indent row-no-margin text-weight-medium margin-top">
                        <li class="current">
                            <a href="/">Home</a>
                        </li>
                            <li class="">
                                <a href="http://reading.ac.uk/ready-to-study.aspx">Study & Life</a>
                                <i class="chevron-down-white"></i>
                                    <ul class="navigation-lvl2">
                                            <li>
                                                <a href="http://reading.ac.uk/ready-to-study/study.aspx">Study</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/ready-to-study/student-life.aspx">Student life</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/ready-to-study/accommodation.aspx">Accommodation</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/ready-to-study/visiting-and-open-days.aspx">Visiting us and Open Days</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/ready-to-study/international-and-eu.aspx">International & EU</a>
                                            </li>
                                    </ul>
                            </li>
                            <li class="">
                                <a href="<?php get_home_url(); ?>">Research</a>
                                <i class="chevron-down-white"></i>
                                    <ul class="navigation-lvl2">
										<?php 
										// loop on the menu items to build the menu
										foreach ($menu as $key => $menu_item){
											echo "<li><a href=\"".$menu_item['url']."\" class=\"text-nowrap\" role=\"menuitem\" aria-haspopup=\"true\" tabindex=\"0\">".$menu_item['title']."</a></li>";
										}
										?>   
                                    </ul>
                            </li>
                            <li class="">
                                <a href="http://reading.ac.uk/about.aspx">About Us</a>
                                <i class="chevron-down-white"></i>
                                    <ul class="navigation-lvl2">
                                            <li>
                                                <a href="http://reading.ac.uk/about/working-with-business.aspx">Working with business</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/about/strategy.aspx">Strategy</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/about/working-with-the-community.aspx">Working with the community</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/about/governance.aspx">Governance</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/about/visit-us.aspx">Visit us</a>
                                            </li>
                                            <li>
                                                <a href="http://reading.ac.uk/about/contact-us.aspx">Contact us</a>
                                            </li>
                                    </ul>
                            </li>
                    </ul>

                    <ul class="no-border pad-row mobile-meta-nav">
                            <li>
                                <a href='http://www.reading.ac.uk/alumni' class='prospectus-login-icon' target='_blank' >Alumni</a>
                            </li>
                            <li>
                                <a href='http://student.reading.ac.uk/essentials' class='prospectus-login-icon' target='_blank' >Essentials</a>
                            </li>
                            <li>
                                <a href='http://www.reading.ac.uk/staff' class='prospectus-login-icon' target='_blank' >Staff</a>
                            </li>
                    </ul>

                    <ul class="bg-grey mobile-courtesy-nav">
                            <li>
                                <a href='http://www.reading.ac.uk/A-Z/az-academic.aspx' target='_blank' title='Visit our school and departmental sites' >Schools & departments</a>
                            </li>
                            <li>
                                <a href='http://www.henley.ac.uk' target='_blank' title='Find out more about our triple-accredited Henley Business School' >Henley Business School</a>
                            </li>
                            <li>
                                <a href='http://www.reading.edu.my' target='_blank' title='Discover our Malaysia campus' >University of Reading&nbsp;Malaysia</a>
                            </li>
                    </ul>
                </div>
            </nav>
    </div>

    <div role="main" class="main-content">
        <a id="skipnav" tabindex="-1"></a>

            
    <!-- DESKTOP 3rd LEVEL NAVIGATION -->

    <div class="wrapper nopaddingbottom relative" id="#DesktopSubMenu">
		<nav class="navigation-lvl2 mobile-hidden tablet-hidden" role="navigation">
			<ul class="bg-white text-weight-medium text-size-17 root-level" role="menubar" id="SectionNavigation">
				<?php 
				// loop on the menu items to build the menu
				foreach ($menu as $key => $menu_item){
					echo "<li class=\"navigation-top-item\"><a href=\"".$menu_item['url']."\" class=\"text-nowrap\" role=\"menuitem\" aria-haspopup=\"true\" tabindex=\"0\">".$menu_item['title']."</a>";
						if ($menu_item['children']) {
							echo "<ul class=\"navigation-lvl3\" role=\"menu\" aria-hidden=\"true\">";
							foreach ($menu_item['children'] as $key => $submenu_item){
								echo "<li> <a role=\"menuitem\" tabindex=\"-1\" href=\"".$submenu_item['url']."\">".$submenu_item['title']."</a></li>";
							}
							echo "</ul>";
						}
					echo "</li>";
				}
				?>   
			</ul>
		</nav>
    </div>
	
    <!-- MOBILE 3RD LEVEL NAVIGATION -->
    <nav class="desktop-hidden navigation subnavigation" id="#MobileSubMenu">
        <ul id="SectionSubNavigation" class="display-block no-indent row-no-margin text-weight-medium bg-black">
        </ul>
    </nav>


<input type="hidden" class="require-js" value="<?php echo get_template_directory_uri(); ?>/js/masonryLayout.js" />

<input type="hidden" class="require-js" value="<?php echo get_template_directory_uri(); ?>/js/appSubjectsSidebar.js" />

<?php //Get the page ID containing the header and footer pictures (can be a parent page)
$id_page_picture=get_the_ID(); //by default, we'll display the pictures of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('backgroundimagesinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('backgroundimagesinherit',$ancestor) == 0) 
		{ 
			$id_page_picture=$ancestor; //get id of parent page
			break;
		}
	}
}
?>

<div class="main-heading" style="background-image: url('<?php echo get_field('imageheader',$id_page_picture)['url'];?>')">
<div class="wrapper">
    <div class="section-header page" role="banner">
        <div class="wrapper section-content">
            <h1 class="text-color-white m-pad row-margin-small text-transform-uppercase text-weight-medium text-size-40 m-text-size-20">
                <span class="text-bg-standout"><?php 
					bloginfo('name'); ?><span class="text-weight-light"></span></span>
            </h1>
				<p class="m-pad">
				<a href='<?php bloginfo('url');?>' class='button button-primary chevron-right m-button-inline' ><?php bloginfo('description');?></a>
				</p> 
        </div>
    </div>
    </div>
    </div>