<?php
/**
 * The Header for our theme
 *
 * Displays all of the <head> section and everything up till <div id="main">
 *
 */



  get_header();
  
  
  
//Get the page ID containing the sideboxes (can be a parent page)
$id_page_boxes=get_the_ID(); //by default, we'll display the sideboxes of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('boxinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('boxinherit',$ancestor) == 0) 
		{ 
			$id_page_boxes=$ancestor; //get id of parent page
			break;
		}
	}
}

?>


<div class="row bg-light-grey">
    <div class="wrapper paddingtop">
      
        <div class="row-medium masonry-area">
			<?php if (is_front_page() && is_home() ) 
			{ //Special display for front page set as "Your Latest Posts"
				while (have_posts()) :
					the_post(); ?>
					<div class="pane base8 t-base6 pane-around">
						<article class="bg-white pad-around">
							<?php the_title( sprintf( '<h3><a href="%s" rel="bookmark">', esc_url( get_permalink() ) ), '</a></h3>' ); ?>
							<small><?php the_time('F jS, Y'); ?></small><br>
							<a href="<?php the_permalink(); ?>"><?php the_post_thumbnail( array(151,151)  ); ?></a>
							<?php the_excerpt();?>
						</article> 
					</div><?php
				endwhile; 	
				if (get_field('include_page')!="") {echo file_get_contents(get_field('include_page')); }
			} //end special display for front page when "latest posts " selected
			else { //normal page/post display
			    if ( get_page_template_slug( get_the_ID() ) == "page-templates/nosidebar.php"){
				   echo "<div class=\"pane base12 t-base6 pane-around clearfix\">";
				}
				else echo "<div class=\"pane base8 t-base6 pane-around clearfix\">";
			?>          
				<article class="bg-white pad-around">
				<?php
				while (have_posts()) :
					the_post();
					the_post_thumbnail( array(200,200)  );
					the_content();

				endwhile; 	
				if (get_field('include_page')!="") {include_external_page (get_field('include_page')); }

				?>
				</article>
				
			</div>
			<?php } //End normal page display?>
			
		<?php if (get_field('box1content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box1content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box2content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box2content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box3content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box3content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box4content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box4content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
			<?php
				get_sidebar();
			?>
        </div>


    </div>
</div>
<?php 
	get_footer();
 ?>
