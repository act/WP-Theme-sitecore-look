function footerPush() {
    var i;
    if ($(".main-content, .quicklinks").removeAttr("style"), typeof window.Sitecore == "undefined" && $(window).height() >= $(".main-content").height()) {
        var t = function() {
                var n = $(".main-footer").outerHeight();
                return $(".main-footer").siblings(".discover-home-prefooter, .prefooter").each(function() {
                    n += $(this).outerHeight()
                }), n
            }(),
            r = $(".main-header").outerHeight() + $("#SuperHeader").outerHeight(),
            n = "";
        n = "<style type='text/css'>";
        n += ".footer-push { min-height: 100%; margin-bottom: -" + t + "px;}";
        n += ".footer-push:after { height: " + t + "px}";
        n += "<\/style>";
        $(n).appendTo("head");
        i = $(".footer-push").outerHeight() - t - r;
        $(".main-content").outerHeight() < i && ($(".main-content").outerHeight(i).css("position", "relative"), $(".quicklinks").length && ($(".quicklinks").css({
            position: "absolute",
            right: "0",
            left: "0",
            bottom: "0"
        }), $(".main-content").css({
            paddingBottom: $(".quicklinks").outerHeight(),
            boxSizing: "border-box"
        })))
    }
}

function getParameterByName(n) {
    n = n.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var i = new RegExp("[\\?&]" + n + "=([^&#]*)"),
        t = i.exec(location.search);
    return t === null ? "" : decodeURIComponent(t[1].replace(/\+/g, " "))
}

function bindExtraInfo() {
    $(".extra-info").hide();
    $(".extra-info-open").on("click", function() {
        $("#" + $(this).attr("data-id")).before('<div class="overlay-mask extra-info-mask">').fadeIn(function() {
            target = $(this);
            $("html,body").animate({
                scrollTop: target.offset().top - 130
            }, 300)
        });
        $(".extra-info-mask").unbind("click").on("click", function() {
            $(".extra-info-mask").remove();
            $(".extra-info").fadeOut()
        });
        $(".extra-info-mask").off("mouseover").on("mouseover", function() {
            clearTimeout(globalTimer);
            globalTimer = setTimeout(function() {
                $(".extra-info-mask").remove();
                $(".extra-info").fadeOut()
            }, 3e3)
        });
        $("#" + $(this).attr("data-id")).on("mouseover", function() {
            clearTimeout(globalTimer)
        })
    })
}

function unbindExtraInfo() {
    $(".extra-info-open").unbind("click");
    $(".extra-info-mask").unbind("click");
    $(".extra-info-mask").remove();
    $(".extra-info").show()
}

function onWindowResize(n) {
    function u() {
        var u = $(window).width();
        u < 768 && !t && typeof n.onMobile == "function" && (n.onMobile(), t = !0, i = !1, r = !1);
        u < 1024 && !i && u >= 768 && typeof n.onTablet == "function" && (n.onTablet(), t = !1, i = !0, r = !1);
        u >= 1024 && !r && typeof n.onDesktop == "function" && (n.onDesktop(), t = !1, i = !1, r = !0)
    }
    var t = !1,
        i = !1,
        r = !1;
    u();
    $(window).resize(function() {
        u()
    })
}

function isPageEditor() {
    return typeof window.Sitecore == "undefined" ? !1 : typeof window.Sitecore.PageModes == "undefined" || window.Sitecore.PageModes == null ? !1 : window.Sitecore.PageModes.PageEditor != null
}

function getParameterByName(n) {
    n = n.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var i = new RegExp("[\\?&]" + n + "=([^&#]*)"),
        t = i.exec(location.search);
    return t === null ? "" : decodeURIComponent(t[1].replace(/\+/g, " "))
}
var lockScroll, globalTimer, remindersCollapse, Menu, Navigation, CookieBar, mobileOS, startCarousel, onYouTubeIframeAPIReady, tag, firstScriptTag, ShowMore, convertTables, resetEllipsis, appOverlay, appTooltip, openSearch, appAccordion, Masonary, CostCalculator, requirejs, require, define;
(function(n) {
    n.fn.appSticky = function(t) {
        function u() {
            i.height() !== i.parent().outerHeight() && i.parent().height(i.outerHeight())
        }

        function r() {
            n("#personalizationHeader").length ? i.addClass("sticky-home") : i.removeClass("sticky-home");
            t === "top" ? i.parent().offset().top >= n(window).scrollTop() ? i.hasClass("sticky-fixed") && (n("#personalizationHeader").length ? i.removeClass("sticky-fixed") : i.removeClass("sticky-home").removeClass("sticky-fixed")) : i.hasClass("sticky-fixed") || (n("#personalizationHeader").length ? i.removeClass("sticky-fixed") : i.removeClass("sticky-home").addClass("sticky-fixed")) : t === "bottom" && (i.parent().offset().top + i.parent().height() <= n(window).scrollTop() + n(window).height() ? i.addClass("sticky-home").removeClass("sticky-fixed") : i.addClass("sticky-fixed").removeClass("sticky-home"))
        }
        var i = this,
            f, e = n('<div class="sticky-container">');
        i.wrap('<div class="sticky-container">');
        n(window).on("load", function() {
            i.parent().css({
                display: "block",
                width: "100%",
                height: i.outerHeight()
            })
        });
        n(window).on("scroll", function() {
            n(window).innerWidth() >= 320 && r()
        });
        n(window).on("resize", function() {
            f = setTimeout(function() {
                n(window).innerWidth() >= 320 ? r() : (i.removeClass("sticky-fixed", "sticky-home"), i.parent().removeAttr("style"));
                u()
            }, 100)
        });
        return n(window).innerWidth() >= 320 ? (u(), i.addClass("sticky-home"), r()) : i.parent().removeAttr("style"), this
    }
})(jQuery),
function(n) {
    function t(n, t, i, r) {
        if ("addEventListener" in n) try {
            n.addEventListener(t, i, r)
        } catch (u) {
            if (typeof i == "object" && i.handleEvent) n.addEventListener(t, function(n) {
                i.handleEvent.call(i, n)
            }, r);
            else throw u;
        } else "attachEvent" in n && (typeof i == "object" && i.handleEvent ? n.attachEvent("on" + t, function() {
            i.handleEvent.call(i)
        }) : n.attachEvent("on" + t, i))
    }

    function i(n, t, i, r) {
        if ("removeEventListener" in n) try {
            n.removeEventListener(t, i, r)
        } catch (u) {
            if (typeof i == "object" && i.handleEvent) n.removeEventListener(t, function(n) {
                i.handleEvent.call(i, n)
            }, r);
            else throw u;
        } else "detachEvent" in n && (typeof i == "object" && i.handleEvent ? n.detachEvent("on" + t, function() {
            i.handleEvent.call(i)
        }) : n.detachEvent("on" + t, i))
    }(function(n) {
        function c() {
            t.setAttribute("content", a);
            i = !0
        }

        function v() {
            t.setAttribute("content", l);
            i = !1
        }

        function y(t) {
            r = t.accelerationIncludingGravity;
            f = Math.abs(r.x);
            e = Math.abs(r.y);
            o = Math.abs(r.z);
            (!n.orientation || n.orientation === 180) && (f > 7 || (o > 6 && e < 8 || o < 8 && e > 6) && f > 5) ? i && v(): i || c()
        }
        var s = navigator.userAgent,
            u;
        if (/iPhone|iPad|iPod/.test(navigator.platform) && /OS [1-5]_[0-9_]* like Mac OS X/i.test(s) && s.indexOf("AppleWebKit") > -1 && (u = n.document, u.querySelector)) {
            var t = u.querySelector("meta[name=viewport]"),
                h = t && t.getAttribute("content"),
                l = h + ",maximum-scale=1",
                a = h + ",maximum-scale=10",
                i = !0,
                f, e, o, r;
            t && (n.addEventListener("orientationchange", c, !1), n.addEventListener("devicemotion", y, !1))
        }
    })(this);
    window.MBP = window.MBP || {};
    MBP.viewportmeta = n.querySelector && n.querySelector('meta[name="viewport"]');
    MBP.ua = navigator.userAgent;
    MBP.scaleFix = function() {
        MBP.viewportmeta && /iPhone|iPad|iPod/.test(MBP.ua) && !/Opera Mini/.test(MBP.ua) && (MBP.viewportmeta.content = "width=device-width, minimum-scale=1.0, maximum-scale=1.0", n.addEventListener("gesturestart", MBP.gestureStart, !1))
    };
    MBP.gestureStart = function() {
        MBP.viewportmeta.content = "width=device-width, minimum-scale=0.25, maximum-scale=1.6"
    };
    MBP.BODY_SCROLL_TOP = !1;
    MBP.getScrollTop = function() {
        var i = window,
            t = n;
        return i.pageYOffset || t.compatMode === "CSS1Compat" && t.documentElement.scrollTop || t.body.scrollTop || 0
    };
    MBP.hideUrlBar = function() {
        var n = window;
        location.hash || MBP.BODY_SCROLL_TOP === !1 || n.scrollTo(0, MBP.BODY_SCROLL_TOP === 1 ? 0 : 1)
    };
    MBP.hideUrlBarOnLoad = function() {
        var n = window,
            i = n.document,
            t;
        n.navigator.standalone || location.hash || !n.addEventListener || (window.scrollTo(0, 1), MBP.BODY_SCROLL_TOP = 1, t = setInterval(function() {
            i.body && (clearInterval(t), MBP.BODY_SCROLL_TOP = MBP.getScrollTop(), MBP.hideUrlBar())
        }, 15), n.addEventListener("load", function() {
            setTimeout(function() {
                MBP.getScrollTop() < 20 && MBP.hideUrlBar()
            }, 0)
        }, !1))
    };
    MBP.fastButton = function(n, t, i) {
        if (this.handler = t, this.pressedClass = typeof i == "undefined" ? "pressed" : i, MBP.listenForGhostClicks(), n.length && n.length > 1)
            for (var r in n) this.addClickEvent(n[r]);
        else this.addClickEvent(n)
    };
    MBP.fastButton.prototype.handleEvent = function(n) {
        n = n || window.event;
        switch (n.type) {
            case "touchstart":
                this.onTouchStart(n);
                break;
            case "touchmove":
                this.onTouchMove(n);
                break;
            case "touchend":
                this.onClick(n);
                break;
            case "click":
                this.onClick(n)
        }
    };
    MBP.fastButton.prototype.onTouchStart = function(t) {
        var i = t.target || t.srcElement;
        t.stopPropagation();
        i.addEventListener("touchend", this, !1);
        n.body.addEventListener("touchmove", this, !1);
        this.startX = t.touches[0].clientX;
        this.startY = t.touches[0].clientY;
        i.className += " " + this.pressedClass
    };
    MBP.fastButton.prototype.onTouchMove = function(n) {
        (Math.abs(n.touches[0].clientX - this.startX) > 10 || Math.abs(n.touches[0].clientY - this.startY) > 10) && this.reset(n)
    };
    MBP.fastButton.prototype.onClick = function(n) {
        var t, i;
        n = n || window.event;
        t = n.target || n.srcElement;
        n.stopPropagation && n.stopPropagation();
        this.reset(n);
        this.handler.apply(n.currentTarget, [n]);
        n.type == "touchend" && MBP.preventGhostClick(this.startX, this.startY);
        i = new RegExp(" ?" + this.pressedClass, "gi");
        t.className = t.className.replace(i, "")
    };
    MBP.fastButton.prototype.reset = function(t) {
        var r = t.target || t.srcElement,
            u;
        i(r, "touchend", this, !1);
        i(n.body, "touchmove", this, !1);
        u = new RegExp(" ?" + this.pressedClass, "gi");
        r.className = r.className.replace(u, "")
    };
    MBP.fastButton.prototype.addClickEvent = function(n) {
        t(n, "touchstart", this, !1);
        t(n, "click", this, !1)
    };
    MBP.preventGhostClick = function(n, t) {
        MBP.coords.push(n, t);
        window.setTimeout(function() {
            MBP.coords.splice(0, 2)
        }, 2500)
    };
    MBP.ghostClickHandler = function(n) {
        var t, i, r, u;
        if (!MBP.hadTouchEvent && MBP.dodgyAndroid) {
            n.stopPropagation();
            n.preventDefault();
            return
        }
        for (t = 0, i = MBP.coords.length; t < i; t += 2) r = MBP.coords[t], u = MBP.coords[t + 1], Math.abs(n.clientX - r) < 25 && Math.abs(n.clientY - u) < 25 && (n.stopPropagation(), n.preventDefault())
    };
    MBP.dodgyAndroid = "ontouchstart" in window && navigator.userAgent.indexOf("Android 2.3") != -1;
    MBP.listenForGhostClicks = function() {
        var i = !1;
        return function() {
            i || (n.addEventListener && n.addEventListener("click", MBP.ghostClickHandler, !0), t(n.documentElement, "touchstart", function() {
                MBP.hadTouchEvent = !0
            }, !1), i = !0)
        }
    }();
    MBP.coords = [];
    MBP.autogrow = function(n, t) {
        function r() {
            var n = this.scrollHeight,
                t = this.clientHeight;
            n > t && (this.style.height = n + 3 * i + "px")
        }
        var u = t ? t : 12,
            i = n.currentStyle ? n.currentStyle.lineHeight : getComputedStyle(n, null).lineHeight;
        i = i.indexOf("px") == -1 ? u : parseInt(i, 10);
        n.style.overflow = "hidden";
        n.addEventListener ? n.addEventListener("input", r, !1) : n.attachEvent("onpropertychange", r)
    };
    MBP.enableActive = function() {
        n.addEventListener("touchstart", function() {}, !1)
    };
    MBP.preventScrolling = function() {
        n.addEventListener("touchmove", function(n) {
            n.target.type !== "range" && n.preventDefault()
        }, !1)
    };
    MBP.preventZoom = function() {
        if (MBP.viewportmeta && navigator.platform.match(/iPad|iPhone|iPod/i))
            for (var i = n.querySelectorAll("input, select, textarea"), r = "width=device-width,initial-scale=1,maximum-scale=", t = 0, u = i.length, f = function() {
                    MBP.viewportmeta.content = r + "1"
                }, e = function() {
                    MBP.viewportmeta.content = r + "10"
                }; t < u; t++) i[t].onfocus = f, i[t].onblur = e
    };
    MBP.startupImage = function() {
        var i, e, u, f, t, r;
        u = window.devicePixelRatio;
        f = n.getElementsByTagName("head")[0];
        navigator.platform === "iPad" ? (i = u === 2 ? "img/startup/startup-tablet-portrait-retina.png" : "img/startup/startup-tablet-portrait.png", e = u === 2 ? "img/startup/startup-tablet-landscape-retina.png" : "img/startup/startup-tablet-landscape.png", t = n.createElement("link"), t.setAttribute("rel", "apple-touch-startup-image"), t.setAttribute("media", "screen and (orientation: portrait)"), t.setAttribute("href", i), f.appendChild(t), r = n.createElement("link"), r.setAttribute("rel", "apple-touch-startup-image"), r.setAttribute("media", "screen and (orientation: landscape)"), r.setAttribute("href", e), f.appendChild(r)) : (i = u === 2 ? "img/startup/startup-retina.png" : "img/startup/startup.png", i = screen.height === 568 ? "img/startup/startup-retina-4in.png" : i, t = n.createElement("link"), t.setAttribute("rel", "apple-touch-startup-image"), t.setAttribute("href", i), f.appendChild(t));
        navigator.platform.match(/iPhone|iPod/i) && screen.height === 568 && navigator.userAgent.match(/\bOS 6_/) && MBP.viewportmeta && (MBP.viewportmeta.content = MBP.viewportmeta.content.replace(/\bwidth\s*=\s*320\b/, "width=320.1").replace(/\bwidth\s*=\s*device-width\b/, ""))
    }
}(document),
function() {
    "use strict";

    function n(t, r) {
        function h(n, t) {
            return function() {
                return n.apply(t, arguments)
            }
        }
        var o, f, e, u, s;
        if (r = r || {}, this.trackingClick = !1, this.trackingClickStart = 0, this.targetElement = null, this.touchStartX = 0, this.touchStartY = 0, this.lastTouchIdentifier = 0, this.touchBoundary = r.touchBoundary || 10, this.layer = t, this.tapDelay = r.tapDelay || 200, this.tapTimeout = r.tapTimeout || 700, !n.notNeeded(t)) {
            for (f = ["onMouse", "onClick", "onTouchStart", "onTouchMove", "onTouchEnd", "onTouchCancel"], e = this, u = 0, s = f.length; u < s; u++) e[f[u]] = h(e[f[u]], e);
            i && (t.addEventListener("mouseover", this.onMouse, !0), t.addEventListener("mousedown", this.onMouse, !0), t.addEventListener("mouseup", this.onMouse, !0));
            t.addEventListener("click", this.onClick, !0);
            t.addEventListener("touchstart", this.onTouchStart, !1);
            t.addEventListener("touchmove", this.onTouchMove, !1);
            t.addEventListener("touchend", this.onTouchEnd, !1);
            t.addEventListener("touchcancel", this.onTouchCancel, !1);
            Event.prototype.stopImmediatePropagation || (t.removeEventListener = function(n, i, r) {
                var u = Node.prototype.removeEventListener;
                n === "click" ? u.call(t, n, i.hijacked || i, r) : u.call(t, n, i, r)
            }, t.addEventListener = function(n, i, r) {
                var u = Node.prototype.addEventListener;
                n === "click" ? u.call(t, n, i.hijacked || (i.hijacked = function(n) {
                    n.propagationStopped || i(n)
                }), r) : u.call(t, n, i, r)
            });
            typeof t.onclick == "function" && (o = t.onclick, t.addEventListener("click", function(n) {
                o(n)
            }, !1), t.onclick = null)
        }
    }
    var r = navigator.userAgent.indexOf("Windows Phone") >= 0,
        i = navigator.userAgent.indexOf("Android") > 0 && !r,
        t = /iP(ad|hone|od)/.test(navigator.userAgent) && !r,
        u = t && /OS 4_\d(_\d)?/.test(navigator.userAgent),
        f = t && /OS [6-7]_\d/.test(navigator.userAgent),
        e = navigator.userAgent.indexOf("BB10") > 0;
    if (n.prototype.needsClick = function(n) {
            switch (n.nodeName.toLowerCase()) {
                case "button":
                case "select":
                case "textarea":
                    if (n.disabled) return !0;
                    break;
                case "input":
                    if (t && n.type === "file" || n.disabled) return !0;
                    break;
                case "label":
                case "iframe":
                case "video":
                    return !0
            }
            return /\bneedsclick\b/.test(n.className)
        }, n.prototype.needsFocus = function(n) {
            switch (n.nodeName.toLowerCase()) {
                case "textarea":
                    return !0;
                case "select":
                    return !i;
                case "input":
                    switch (n.type) {
                        case "button":
                        case "checkbox":
                        case "file":
                        case "image":
                        case "radio":
                        case "submit":
                            return !1
                    }
                    return !n.disabled && !n.readOnly;
                default:
                    return /\bneedsfocus\b/.test(n.className)
            }
        }, n.prototype.sendClick = function(n, t) {
            var r, i;
            document.activeElement && document.activeElement !== n && document.activeElement.blur();
            i = t.changedTouches[0];
            r = document.createEvent("MouseEvents");
            r.initMouseEvent(this.determineEventType(n), !0, !0, window, 1, i.screenX, i.screenY, i.clientX, i.clientY, !1, !1, !1, !1, 0, null);
            r.forwardedTouchEvent = !0;
            n.dispatchEvent(r)
        }, n.prototype.determineEventType = function(n) {
            return i && n.tagName.toLowerCase() === "select" ? "mousedown" : "click"
        }, n.prototype.focus = function(n) {
            var i;
            t && n.setSelectionRange && n.type.indexOf("date") !== 0 && n.type !== "time" && n.type !== "month" ? (i = n.value.length, n.setSelectionRange(i, i)) : n.focus()
        }, n.prototype.updateScrollParent = function(n) {
            var i, t;
            if (i = n.fastClickScrollParent, !i || !i.contains(n)) {
                t = n;
                do {
                    if (t.scrollHeight > t.offsetHeight) {
                        i = t;
                        n.fastClickScrollParent = t;
                        break
                    }
                    t = t.parentElement
                } while (t)
            }
            i && (i.fastClickLastScrollTop = i.scrollTop)
        }, n.prototype.getTargetElementFromEventTarget = function(n) {
            return n.nodeType === Node.TEXT_NODE ? n.parentNode : n
        }, n.prototype.onTouchStart = function(n) {
            var r, i, f;
            if (n.targetTouches.length > 1) return !0;
            if (r = this.getTargetElementFromEventTarget(n.target), i = n.targetTouches[0], t) {
                if (f = window.getSelection(), f.rangeCount && !f.isCollapsed) return !0;
                if (!u) {
                    if (i.identifier && i.identifier === this.lastTouchIdentifier) return n.preventDefault(), !1;
                    this.lastTouchIdentifier = i.identifier;
                    this.updateScrollParent(r)
                }
            }
            return this.trackingClick = !0, this.trackingClickStart = n.timeStamp, this.targetElement = r, this.touchStartX = i.pageX, this.touchStartY = i.pageY, n.timeStamp - this.lastClickTime < this.tapDelay && n.preventDefault(), !0
        }, n.prototype.touchHasMoved = function(n) {
            var t = n.changedTouches[0],
                i = this.touchBoundary;
            return Math.abs(t.pageX - this.touchStartX) > i || Math.abs(t.pageY - this.touchStartY) > i ? !0 : !1
        }, n.prototype.onTouchMove = function(n) {
            return this.trackingClick ? ((this.targetElement !== this.getTargetElementFromEventTarget(n.target) || this.touchHasMoved(n)) && (this.trackingClick = !1, this.targetElement = null), !0) : !0
        }, n.prototype.findControl = function(n) {
            return n.control !== undefined ? n.control : n.htmlFor ? document.getElementById(n.htmlFor) : n.querySelector("button, input:not([type=hidden]), keygen, meter, output, progress, select, textarea")
        }, n.prototype.onTouchEnd = function(n) {
            var s, c, e, o, h, r = this.targetElement;
            if (!this.trackingClick) return !0;
            if (n.timeStamp - this.lastClickTime < this.tapDelay) return this.cancelNextClick = !0, !0;
            if (n.timeStamp - this.trackingClickStart > this.tapTimeout) return !0;
            if (this.cancelNextClick = !1, this.lastClickTime = n.timeStamp, c = this.trackingClickStart, this.trackingClick = !1, this.trackingClickStart = 0, f && (h = n.changedTouches[0], r = document.elementFromPoint(h.pageX - window.pageXOffset, h.pageY - window.pageYOffset) || r, r.fastClickScrollParent = this.targetElement.fastClickScrollParent), e = r.tagName.toLowerCase(), e === "label") {
                if (s = this.findControl(r), s) {
                    if (this.focus(r), i) return !1;
                    r = s
                }
            } else if (this.needsFocus(r)) return n.timeStamp - c > 100 || t && window.top !== window && e === "input" ? (this.targetElement = null, !1) : (this.focus(r), this.sendClick(r, n), t && e === "select" || (this.targetElement = null, n.preventDefault()), !1);
            return t && !u && (o = r.fastClickScrollParent, o && o.fastClickLastScrollTop !== o.scrollTop) ? !0 : (this.needsClick(r) || (n.preventDefault(), this.sendClick(r, n)), !1)
        }, n.prototype.onTouchCancel = function() {
            this.trackingClick = !1;
            this.targetElement = null
        }, n.prototype.onMouse = function(n) {
            return this.targetElement ? n.forwardedTouchEvent ? !0 : n.cancelable ? !this.needsClick(this.targetElement) || this.cancelNextClick ? (n.stopImmediatePropagation ? n.stopImmediatePropagation() : n.propagationStopped = !0, n.stopPropagation(), n.preventDefault(), !1) : !0 : !0 : !0
        }, n.prototype.onClick = function(n) {
            var t;
            return this.trackingClick ? (this.targetElement = null, this.trackingClick = !1, !0) : n.target.type === "submit" && n.detail === 0 ? !0 : (t = this.onMouse(n), t || (this.targetElement = null), t)
        }, n.prototype.destroy = function() {
            var n = this.layer;
            i && (n.removeEventListener("mouseover", this.onMouse, !0), n.removeEventListener("mousedown", this.onMouse, !0), n.removeEventListener("mouseup", this.onMouse, !0));
            n.removeEventListener("click", this.onClick, !0);
            n.removeEventListener("touchstart", this.onTouchStart, !1);
            n.removeEventListener("touchmove", this.onTouchMove, !1);
            n.removeEventListener("touchend", this.onTouchEnd, !1);
            n.removeEventListener("touchcancel", this.onTouchCancel, !1)
        }, n.notNeeded = function(n) {
            var t, r, u, f;
            if (typeof window.ontouchstart == "undefined") return !0;
            if (r = +(/Chrome\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1], r)
                if (i) {
                    if (t = document.querySelector("meta[name=viewport]"), t && (t.content.indexOf("user-scalable=no") !== -1 || r > 31 && document.documentElement.scrollWidth <= window.outerWidth)) return !0
                } else return !0;
            return e && (u = navigator.userAgent.match(/Version\/([0-9]*)\.([0-9]*)/), u[1] >= 10 && u[2] >= 3 && (t = document.querySelector("meta[name=viewport]"), t && (t.content.indexOf("user-scalable=no") !== -1 || document.documentElement.scrollWidth <= window.outerWidth))) ? !0 : n.style.msTouchAction === "none" || n.style.touchAction === "manipulation" ? !0 : (f = +(/Firefox\/([0-9]+)/.exec(navigator.userAgent) || [, 0])[1], f >= 27 && (t = document.querySelector("meta[name=viewport]"), t && (t.content.indexOf("user-scalable=no") !== -1 || document.documentElement.scrollWidth <= window.outerWidth))) ? !0 : n.style.touchAction === "none" || n.style.touchAction === "manipulation" ? !0 : !1
        }, n.attach = function(t, i) {
            return new n(t, i)
        }, typeof define == "function" && typeof define.amd == "object" && define.amd ? define(function() {
            return n
        }) : typeof module != "undefined" && module.exports ? (module.exports = n.attach, module.exports.FastClick = n) : window.FastClick = n, navigator.userAgent.match(/Trident\/7\./)) $("body").on("mousewheel", function() {
        event.preventDefault();
        var n = event.wheelDelta,
            t = window.pageYOffset;
        window.scrollTo(0, t - n)
    })
}();
var appSearch = function(n) {
        var u = {
                container: "#CoursesSearch",
                resultsContainer: ".search-results",
                resultElements: "label",
                counterContainer: ".selected-counter",
                searchField: ".text-input",
                buttonCancel: ".button-cancel",
                buttonReview: ".button-review",
                buttonSearch: ".button-search",
                buttonFinish: ".button-finish",
                buttonABC: ".icon-ss-abc",
                buttonQuery: ".icon-ss-search",
                panelEmpty: ".panel-empty",
                panelReview: ".panel-review",
                panelQuery: ".panel-query",
                panelButtonsReview: ".panel-buttons-review",
                panelButtonsFinished: ".panel-buttons-finished",
                panelAlphabet: ".panel-alphabet",
                panelAlphabetScroll: ".alphabet"
            },
            t = n.container ? $(n.container) : $(u.container),
            i = n.results ? t.find(n.results) : t.find(u.resultsContainer),
            c = n.elem ? i.find(n.elem) : i.find(u.resultElements),
            v = n.counter ? t.find(n.counter) : t.find(u.counterContainer),
            s = n.search ? t.find(n.search) : t.find(u.searchField),
            kt = n.cancel ? t.find(n.cancel) : t.find(u.buttonCancel),
            dt = n.buttonReview ? t.find(n.buttonReview) : n.buttonReview === null ? null : t.find(u.buttonReview),
            gt = n.buttonSearch ? t.find(n.buttonSearch) : n.buttonSearch === null ? null : t.find(u.buttonSearch),
            ni = n.buttonFinish ? t.find(n.buttonFinish) : n.buttonFinish === null ? null : t.find(u.buttonFinish),
            ti = n.buttonABC ? t.find(n.buttonABC) : t.find(u.buttonABC),
            ii = n.buttonQuery ? t.find(n.buttonQuery) : t.find(u.buttonQuery),
            g = n.panelEmpty ? t.find(n.panelEmpty) : t.find(u.panelEmpty),
            w = n.panelReview ? t.find(n.panelReview) : t.find(u.panelReview),
            y = n.panelQuery ? t.find(n.panelQuery) : t.find(u.panelQuery),
            nt = n.panelButtonsReview ? t.find(n.panelButtonsReview) : t.find(u.panelButtonsReview),
            tt = n.panelButtonsFinished ? t.find(n.panelButtonsFinished) : t.find(u.panelButtonsFinished),
            h = n.panelAlphabet ? t.find(n.panelAlphabet) : t.find(u.panelAlphabet),
            ct = n.panelAlphabetScroll ? t.find(n.panelAlphabetScroll) : t.find(u.panelAlphabetScroll),
            lt = n.alphabetIgnore ? n.alphabetIgnore : null,
            p = {},
            b = [],
            k = "desktop",
            et = n.focussed || !1,
            f, e = n.maxSelections,
            it = n.minSelections,
            at = n.globalCallback || !1,
            r = "all",
            o = {},
            ri = function(n) {
                typeof n != "undefined" && (n.externalFilter && (r = n.externalFilter), n.maxSelections && (e = n.maxSelections, ut()), n.minSelections && (it = n.minSelections, ut()), n.resetSearch && ut());
                f = i.find(":checked").length;
                t.find(".filtered-search-results").length || fi();
                ot();
                ft();
                e && !nt.find(".maxCounterContainer").length && (nt.prepend('<span class="pull-left maxCounterContainer m-bg-white m-pad-around-small" aria-live="polite" aria-role="region" id="MaxCounter" aria-atomic="true">Maximum selections left:<span class="maxCounter">' + (e - f) + "<\/span><\/span>"), vt());
                e && !tt.find(".maxCounterContainer").length && (tt.prepend('<span class="pull-left maxCounterContainer m-bg-white m-pad-around-small" aria-live="polite" aria-role="region" id="MaxCounter" aria-atomic="true">Maximum selections left:<span class="maxCounter">' + (e - f) + "<\/span><\/span>"), vt());
                f === e && e > 0 ? (i.find("input:not(:checked)").prop("disabled", !0), t.find(".maxCounterContainer").addClass("text-color-standout m-bg-white m-pad-around-small")) : i.find("input").prop("disabled", !1);
                it && !f ? $(v).parent().prop("disabled", !0) : $(v).parent().prop("disabled", !1);
                wi();
                t.find(".panel-review-scroll").length && ht();
                ui()
            },
            ui = function() {
                var n = {
                        tab: 9,
                        enter: 13,
                        esc: 27,
                        space: 32,
                        end: 35,
                        home: 36,
                        left: 37,
                        up: 38,
                        right: 39,
                        down: 40
                    },
                    t, i = h.find("a"),
                    r = function(n) {
                        var u = i.filter(".active"),
                            f = u.index(t),
                            r = f + n;
                        t.attr("tabindex", "-1");
                        r < 0 ? r = u.length - 1 : r > u.length - 1 && (r = 0);
                        t = u.eq(r).attr("tabindex", "0");
                        t.focus()
                    };
                i.on("keydown", function(t) {
                    switch (t.keyCode) {
                        case n.tab:
                        case n.esc:
                        case n.home:
                        case n.end:
                            return !0;
                        case n.right:
                        case n.down:
                            r(1);
                            break;
                        case n.left:
                        case n.up:
                            r(-1);
                            break;
                        case n.enter:
                        case n:
                            n: {
                                return !0
                            }
                    }
                    return !1
                });
                i.attr("tabindex", "-1");
                i.eq(2).attr("tabindex", "0");
                t = i.filter("[tabindex=0]")
            },
            vt = function() {
                i.find("input").attr("aria-controls", "MaxCounter")
            },
            fi = function() {
                p.all = [];
                o.all = $();
                c.each(function() {
                    var i = $(this).parent(),
                        r = $(this),
                        u = $(this).siblings("input"),
                        n = u.attr("data-filter"),
                        t = {
                            txt: r.children(".name").text(),
                            helper: si(r)
                        };
                    o[n] && o[n].length ? ($.merge(o[n], i), p[n].push(t)) : (p[n] = [], p[n].push(t), o[n] = i);
                    $.merge(o.all, $(this).parent());
                    p.all.push(t)
                })
            },
            l = function(n) {
                var i = n.type ? n.type : "content",
                    t = n.term ? n.term : "";
                i === "attribute" && (d(), s.val(""), r = t);
                r && !t.length && rt(r);
                i === "content" && ei(t);
                (i !== "content" || t.length !== 0 || r && r !== "all") && (i !== "attribute" || t.length !== 0) || hi()
            },
            rt = function(n, t) {
                if (n !== r) {
                    r = n;
                    for (var u in o)
                        if (u === n) {
                            o[u].show();
                            break
                        } else o[u].hide();
                    st();
                    s.val().length >= 3 && l({
                        type: "content",
                        term: s.val()
                    });
                    t === "uncheck all" && (i.find(":checked").prop("checked", !1), a())
                }
                s.val().length >= 3 && (l({
                    type: "content",
                    term: s.val()
                }), a())
            },
            ei = function(n) {
                var t, f, u, e, s, h;
                if ($(g).hide(), $(i).show(), n) {
                    for (u = 0, e = c.length; u < e; u++) t = c.eq(u), f = t.siblings("input"), s = t.children(".name").eq(0).text(), h = function() {
                        return r && f.attr("data-filter") === r ? !0 : r === "all" ? !0 : void 0
                    }, s.toLowerCase().indexOf(n.toLowerCase()) >= 0 && h() ? (ci(t), oi(t, n), i.scrollTop(0)) : li(t);
                    i.find("label:visible").length || ($(i).hide(), $(g).show())
                } else o[r].show()
            },
            oi = function(n, t) {
                var r = n.children(".name").eq(0),
                    i = r.text(),
                    f = i.toLowerCase(),
                    e = t.toLowerCase(),
                    u = f.indexOf(e),
                    o = u + t.length;
                t = i.substring(u, o);
                r.html(i.split(t).join('<span class="text-bg-standout" style="padding:0">' + t + "<\/span>"))
            },
            si = function(n) {
                return lt === "bachelorType" ? n.siblings("input").attr("data-coursename") : null
            },
            hi = function() {
                o.all.show();
                r = "all";
                st()
            },
            ci = function(n) {
                n.parent().show()
            },
            li = function(n) {
                n.parent().hide()
            },
            ot = function() {
                rt(r);
                st()
            },
            st = function() {
                var t = 0,
                    u = i.scrollTop(),
                    n;
                b.splice(0, b.length);
                n = p[r];
                h.find("a").each(function() {
                    var e = $(this),
                        f = e.text().toLowerCase(),
                        l = "",
                        a = 0,
                        i, s, h, c, v;
                    for (e.removeClass("active").removeAttr("data-id"), i = a, s = n.length; i < s; i++) h = 0, c = n[i], lt === "bachelorType" && (h = c.txt.indexOf(n[i].helper)), v = c.txt.charAt(h).toLowerCase(), f === v && f !== l && (b.push({
                        txt: f,
                        pos: o[r].eq(i).position().top + u,
                        elem: o[r].eq(i)
                    }), e.addClass("active").attr("data-id", t), l = f, t++, a = i, i = s)
                })
            },
            yt = function() {
                i.is(":visible") ? f = i.find(":checked").length : w.is(":visible") && (f = w.find(":checked").length);
                ai();
                vi()
            },
            ai = function() {
                !f && it ? (v.html(""), v.parent().prop("disabled", !0)) : v.parent().prop("disabled", !1)
            },
            vi = function() {
                t.find(".maxCounter").text(e - f);
                v.text(f === 0 ? "" : f)
            },
            a = function() {
                if (yt(), f && f === e ? (i.is(":visible") ? i.find("input:not(:checked)").prop("disabled", !0) : w.is(":visible") && i.find("input:not(:checked)").prop("disabled", !0), t.find(".maxCounterContainer").addClass("text-color-standout")) : (i.find("input").prop("disabled", !1), t.find(".maxCounterContainer").removeClass("text-color-standout")), at) {
                    var n = [];
                    return i.find(":checked").next().each(function() {
                        var i = $(this),
                            t = {};
                        i.find("*").each(function() {
                            var n = $(this).attr("class").split(" ")[0];
                            t[n] = $(this).text()
                        });
                        n.push(t)
                    }), at(n)
                }
            },
            ut = function(n) {
                r = n ? n : r;
                i.find("input").prop("disabled", !1).prop("checked", !1);
                l({
                    content: "",
                    type: "content"
                });
                s.val("");
                rt(r);
                d();
                a()
            },
            yi = function(n) {
                if (n >= 0) {
                    var t = b[n].pos;
                    $(i).animate({
                        scrollTop: t - 20
                    }, 200);
                    b[n].elem.find("input").focus()
                }
            },
            d = function() {
                for (var n = 0; n < c.length; n++) c.eq(n).find(".name").html(c.eq(n).find(".name").text());
                $(g).hide()
            },
            pi = function(n) {
                n.is(":checked") && i.find("input:not(:checked)").prop("disabled", !1);
                a()
            },
            ht = function() {
                d();
                $(s).val("");
                $(i).hide();
                $(y).hide();
                $(h).hide();
                $(nt).hide();
                $(w).fadeIn();
                $(tt).fadeIn();
                a();
                t.addClass("focus")
            },
            pt = function() {
                $(w).hide();
                $(tt).hide();
                $(i).fadeIn(function() {
                    ot()
                });
                $(nt).fadeIn();
                $(y).fadeIn();
                k !== "mobile" && $(h).fadeIn();
                l({
                    type: "content",
                    term: ""
                });
                a();
                t.removeClass("focus")
            },
            wt = function() {
                return e && f > e ? !0 : !1
            },
            bt = function() {
                g.hide();
                i.show()
            },
            wi = function() {
                $(i).find("input").off("change").on("change", function() {
                    pi($(this))
                });
                $(h).find("a").off("click").on("click", function() {
                    bt();
                    d();
                    s.val("");
                    l({
                        term: "",
                        type: "content"
                    });
                    var n = $(this).attr("data-id");
                    return yi(n), !1
                });
                $(s).off("keyup").on("keyup", function(n) {
                    var t = $(this).val();
                    return t.length >= 3 ? l({
                        term: t,
                        type: "content"
                    }) : l({
                        term: "",
                        type: "content"
                    }), t.length === 0 && (bt(), d(), $(s).val("")), n.which === 13 ? (n.preventDefault(), i.find("input:visible").eq(0).focus(), !1) : void 0
                });
                $(s).off("keydown").on("keydown", function(n) {
                    if (n.which === 13) return n.preventDefault(), !1
                });
                $(s).off("focus").on("focus", function() {
                    t.addClass("focus");
                    $("html").addClass("m-locked");
                    et = !0
                });
                $(kt).off("click").on("click", function() {
                    t.removeClass("focus");
                    $("html").removeClass("m-locked");
                    et = !1;
                    ft()
                });
                $(ni).off("click").on("click", function() {
                    t.removeClass("focus");
                    $("html").removeClass("m-locked")
                });
                $(dt).off("click").on("click", function() {
                    return f > 0 || n.limit !== 0 ? wt() ? alert("Error: You can only set " + n.limit + " items") : ht() : n.required && alert("Error: No items selected"), !1
                });
                $(gt).off("click").on("click", function() {
                    return pt(), !1
                });
                $(ti).off("click").on("click", function() {
                    return ki(), !1
                });
                $(ii).off("click").on("click", function() {
                    return bi(), !1
                });
                c.siblings("input").off("focus").on("focus", function() {
                    var r = $(this),
                        u = r.position().top,
                        t = r.parent().outerHeight(),
                        n = i.scrollTop(),
                        f = i.outerHeight();
                    n + u + t > n + f ? i.scrollTop(n + t) : n + u < n && i.scrollTop(n - t)
                })
            },
            bi = function() {
                $(h).hide();
                $(y).show()
            },
            ki = function() {
                ft();
                $(y).hide();
                $(h).show()
            },
            di = function() {
                $(y).show();
                $(h).show()
            },
            gi = function() {
                $(y).show();
                $(h).hide()
            },
            ft = function() {
                var n = $(window).width(),
                    t;
                n >= 767 ? k !== "desktop" && (ct.removeAttr("style"), di(), k = "desktop") : k !== "mobile" ? (gi(), k = "mobile") : (t = 0, t = et ? n - 80 : n - 110, ct.css("width", t))
            };
        $(window).on("resize", function() {
            ft()
        });
        return {
            init: ri,
            recalculatePositions: ot,
            count: yt,
            filterList: rt,
            updateSelected: a,
            resetSearch: ut,
            limitReached: wt,
            showReview: ht,
            showSearch: pt,
            maximumSelections: function() {
                return e !== 0 && f >= e ? !0 : !1
            },
            isRequired: it ? !0 : !1
        }
    },
    createStages = function(n) {
        var i = 1,
            f = n.stages,
            r = n.collapsible,
            c = n.stagesGraphic,
            a = n.showSectionButtons,
            t = {
                stage1: function() {},
                stage2: function() {},
                stage3: function() {},
                stage4: function() {}
            },
            l = function(n) {
                r.each(function() {
                    $(this).is(":visible") && $(this).slideUp().removeClass("open")
                });
                n ? ($(f[0]).removeClass("available"), i = n, u(i)) : u(i);
                o(i)
            },
            v = function(n) {
                r.each(function(t) {
                    var i = $(this);
                    t + 1 === n ? i.addClass("open").slideDown() : i.hide()
                });
                e(n)
            },
            e = function(n) {
                switch (n) {
                    case 1:
                        t.stage1();
                        break;
                    case 2:
                        t.stage2();
                        break;
                    case 3:
                        t.stage3();
                        break;
                    case 4:
                        t.stage4()
                }
            },
            o = function(n) {
                $(c).each(function(t) {
                    var i = $(this),
                        r = t + 1;
                    r === n ? (i.removeClass("complete"), i.addClass("active")) : (i.removeClass("active"), r < n ? i.addClass("complete") : i.removeClass("complete"))
                })
            },
            s = function(n) {
                f.each(function(t) {
                    var i = $(this),
                        r = t + 1;
                    i.removeClass("active");
                    r < n && t !== 0 ? i.addClass("available") : r > n ? i.removeClass("available") : r === n && i.removeClass("available").addClass("active")
                })
            },
            h = function(n) {
                f.each(function(t) {
                    var r = $(this),
                        i = t + 1;
                    if (i < n && t !== 0) r.unbind("click").on("click", function() {
                        u(i)
                    });
                    else i >= n && r.unbind("click")
                })
            },
            u = function(n) {
                if ($("html").removeClass("m-locked"), $("#CoursesSearch, #HobbiesSearch").removeClass("focus"), $(".main-content").removeAttr("style"), r.is(":visible")) r.each(function() {
                    $(this).is(":visible") && $(this).slideUp("slow", function() {
                        $(this).removeClass("open");
                        var t = $("#form-section-" + n);
                        s(n);
                        t.find(".collapsible-zone").addClass("open").slideDown(function() {
                            $("body").animate({
                                scrollTop: t.position().top - 100
                            });
                            h(n);
                            e(n);
                            o(n)
                        })
                    })
                });
                else {
                    var t = $("#form-section-" + n);
                    s(n);
                    t.find(".collapsible-zone").addClass("open").slideDown(function() {
                        $("body").animate({
                            scrollTop: t.position().top - 100
                        });
                        h(n);
                        e(n);
                        o(n)
                    })
                }
            };
        return {
            init: l,
            stageBehaviours: t,
            goToStage: u
        }
    },
    appValidations = function(n) {
        function o(n) {
            var t = new RegExp("<\/?\\w+((\\s+\\w+(\\s*=\\s*(?:\".*?\"|'.*?'|[^'\">\\s]+))?)+\\s*|\\s*)/?>"),
                i = t.exec(n);
            if (i) return !0
        }

        function s(n) {
            if (n === "") return !0
        }

        function h(n) {
            return isNaN(parseInt(n)) && n !== ""
        }

        function c(n) {
            return !/^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(n) && n !== ""
        }

        function l(t, i) {
            for (var r = 0; r < n.length; r++)
                if (i.attr("id") !== n[r].id.substring(1) && t === $(n[r].id).val() && t !== "") return !0
        }

        function a(n) {
            if (n.match(/^\s/)) return !0
        }

        function v(n, t) {
            if (parseInt(n) < parseInt(t.attr("min"))) return !0
        }

        function y(n, t) {
            if (parseInt(n) > parseInt(t.attr("max"))) return !0
        }

        function p(n, t) {
            var i = t.attr("placeholder");
            return n = moment(n, ["DDMMYYYY", "DDMMYY", "YYYYMMDD", "YYMMDD", "DD/MM/YYYY"]), n.isValid() && n.isBefore(moment()) && t.val().length >= t.attr("min") ? (t.attr("type") === "date" && t.attr("type", "text"), t.val(n.format(i)), !1) : !0
        }

        function w(n, r, u, f, o) {
            t = !0;
            i(f, r, u, o);
            $.ajax({
                type: r.validations[f].method,
                url: r.validations[f].url,
                data: n,
                success: function(n) {
                    n.Success === !1 ? (t = !1, i(f, r, u)) : (t = !1, i(null, r, u), e())
                }
            })
        }

        function b(n) {
            var i = n.find("input[type=checkbox], input[type=radio]"),
                t = !1;
            return i.each(function() {
                $(this).is(":checked") && (t = !0)
            }), t ? void 0 : !0
        }

        function k(n, t) {
            var r = t.parent(),
                u = t.attr("maxlength"),
                i = u - n.length;
            if (i === 0) return r.attr("data-chars", i + " characters left"), !0;
            r.attr("data-chars", i + " characters left")
        }

        function i(n, i, r, u) {
            var f = null;
            return (u.addClass("validated"), t) ? (f = i.validations[n].whileChecking ? i.validations[n].whileChecking : "Checking", r.removeClass("validation-error validation-success validation-success"), setTimeout(function() {
                r.text(f);
                u.attr("aria-invalid", !1);
                r.removeClass("validation-error validation-success validation-success validation-no-message");
                r.html(f);
                r.addClass("validation-waiting");
                f || r.addClass("validation-no-message")
            }, 100), !1) : n !== null ? (f = i.validations[n].ifTrue, r.removeClass("validation-success validation-waiting"), setTimeout(function() {
                u.attr("aria-invalid", !0);
                r.attr("role", "alert");
                r.html('<span class="visuallyhidden" aria-visible="true">.Error:<\/span> ' + f + ".");
                r.removeClass("validation-success validation-waiting validation-no-message");
                r.addClass("validation-error");
                f || r.addClass("validation-no-message")
            }, 100), i.isValid = !1, !1) : (f = i.success, r.removeClass("validation-error validation-waiting"), r.removeAttr("role"), setTimeout(function() {
                r.removeClass("validation-error validation-waiting validation-no-message");
                i.isRequired || $(i.id).val() !== "" ? (r.html(f), u.attr("aria-invalid", !1), r.addClass("validation-success")) : (r.text(""), u.attr("aria-invalid", !1), r.removeClass("validation-success"), u.removeClass("validated"));
                f || r.addClass("validation-no-message");
                f === null && (r.removeClass("validation-error validation-waiting validation-no-message validation-success"), u.removeClass("validated"))
            }, 300), i.isValid = !0, !0)
        }

        function r(n, r) {
            var u, nt, tt;
            n = n.data ? n.data : n;
            var f = $(n.id).val() ? $(n.id).val() : $(n.id).text(),
                d = $(n.id),
                e = n.validations.length,
                k = null,
                g = $("label[for=" + n.id.substring(1) + "] .validation-message");
            for (n.isRequired = n.isRequired === undefined ? !0 : n.isRequired, f === d.attr("placeholder") && (f = ""), u = 0; u < e; u++) {
                nt = typeof n.validations[u] != "undefined" ? n.validations[u].checkIf : "FA";
                tt = n.validations[u];
                switch (nt) {
                    case "isInjection":
                        o(f) && (k = u, u = e);
                        break;
                    case "isEmpty":
                        s(f) && (n.isRequired || r === "all required") && (k = u, u = e);
                        break;
                    case "isValidEmail":
                        c(f) && (k = u, u = e);
                        break;
                    case "isDuplicate":
                        l(f, d) && (k = u, u = e);
                        break;
                    case "isUnderMinimum":
                        v(f, d) && (k = u, u = e);
                        break;
                    case "isOverMaximum":
                        y(f, d) && (k = u, u = e);
                        break;
                    case "hasCheckedItems":
                        b(d) && (k = u, u = e);
                        break;
                    case "isValidName":
                        a(f) && (k = u, u = e);
                        break;
                    case "isNotNumber":
                        h(f) && (k = u, u = e);
                        break;
                    case "isValidDate":
                        p(f, d) && (k = u, u = e);
                        break;
                    case "isAvailable":
                        w(f, n, g, u, d)
                }
            }
            t || i(k, n, g, d)
        }

        function e() {
            for (var t = 0; t < n.length; t++)
                if (!n[t].isValid) return $(n[t].id).focus(), !1;
            return u && u(), !0
        }
        var t = !1,
            u = null,
            d = function(n) {
                var r, o;
                n = n.data ? n.data : n;
                var s = $(n.id).val() ? $(n.id).val() : $(n.id).text(),
                    u = $(n.id),
                    f = n.validations.length,
                    e = null,
                    h = $("label[for=" + n.id.substring(1) + "] .validation-message");
                for (r = 0; r < f; r++) {
                    o = n.validations[r].checkIf;
                    switch (o) {
                        case "hasMaxChars":
                            k(s, u) && (e = r, r = f)
                    }
                }
                t || i(e, n, h, u)
            },
            g = function(i, f) {
                if (u = i, !t) {
                    for (var o = 0; o < n.length; o++) n[o].isValid && f !== "all required" || r(n[o], f);
                    return e()
                }
            },
            nt = function() {
                for (var t = 0; t < n.length; t++) $(n[t].id).removeClass(".validation-input"), $("[for=" + n[t].id.substring(1) + "]").remove(), $(n[t].id).parent().hasClass("char-counter") && $(n[t].id).parent().removeClass("char-counter");
                f()
            },
            f = function() {
                for (var t = 0; t < n.length; t++) {
                    var u = $("label[for=" + n[t].id.substring(1) + "]"),
                        f = "label",
                        i = $(n[t].id);
                    if (i.is("input") || i.is("select") && i.not("textarea")) {
                        i.addClass("validationInput");
                        u.length || $(n[t].id).before("<" + f + ' for="' + n[t].id.substring(1) + '"><span class="validation-message"><\/span><\/label>');
                        u.children(".validation-message").length || u.append('<span class="validation-message"><\/span>');
                        i.on("blur", n[t], r)
                    } else if (i.is("textarea")) {
                        u.length || $(n[t].id).before("<" + f + ' for="' + n[t].id.substring(1) + '"><span class="validation-message validation-line">');
                        u.children(".validation-message").length || u.append('<span class="validation-message validation-line"><\/span>');
                        i.parent().is(".char-counter") || i.wrap('<div class="char-counter row-margin">');
                        i.on("keyup", n[t], d);
                        i.on("blur", n[t], r)
                    } else {
                        u.length || $(n[t].id).before("<" + f + ' for="' + n[t].id.substring(1) + '"><span class="validation-message validation-line">');
                        u.children(".validation-message").length || u.append('<span class="validation-message validation-line"><\/span>');
                        i.find("[type=radio],[type=checkbox]").on("change", n[t], r)
                    }
                }
            };
        return f(), {
            init: f,
            validateAll: g,
            removeValidations: nt
        }
    };
(function() {
    /*Modernizr.load({
        test: Modernizr.input.placeholder,
        nope: ["/assets/css/placeholder_polyfill.min.css", "/assets/js/vendor/placeholder_polyfill.jquery.min.combo.js"]
    });*/
    typeof FastClick != "undefined" && "addEventListener" in document && document.addEventListener("DOMContentLoaded", function() {
        FastClick.attach(document.body)
    }, !1);
    $(".main-header").length > 0 && typeof window.Sitecore == "undefined" && jQuery().appSticky && $(".main-header").appSticky("top");
    footerPush()
})();
lockScroll = function(n) {
        $(n).on("DOMMouseScroll mousewheel", function(n) {
            var t = $(this),
                r = this.scrollTop,
                u = this.scrollHeight,
                o = t.height(),
                i = n.originalEvent.wheelDelta,
                f = i > 0,
                e = function() {
                    return n.stopPropagation(), n.preventDefault(), n.returnValue = !1, !1
                };
            return !f && -i > u - o - r ? (t.scrollTop(u), e()) : f && i > r ? (t.scrollTop(0), e()) : void 0
        })
    },
    function(n) {
        n.fn.customSelect = function(t) {
            var r = n(this).attr("class") || "",
                i;
            n(this).attr("class", null);
            i = {
                replacedClass: "replaced select-input",
                customSelectClass: "custom-select " + r,
                activeClass: "active",
                wrapperElement: '<div class="custom-select-container" />'
            };
            t && n.extend(i, t);
            this.each(function() {
                var t = n(this),
                    u, r;
                t.addClass(i.replacedClass);
                t.wrap(i.wrapperElement);
                u = function() {
                    val = n("option:selected", this).text();
                    r.find("span span").text(val)
                };
                t.change(u);
                t.keyup(u);
                r = n('<span class="' + i.customSelectClass + '" aria-hidden="true"><span><span>' + n("option:selected", this).text() + "<\/span><\/span><\/span>");
                t.after(r);
                t.bind({
                    focus: function() {
                        r.addClass(i.activeClass)
                    },
                    blur: function() {
                        r.removeClass(i.activeClass)
                    }
                })
            })
        }
    }(jQuery);
$(document).ready(function() {
    $(".select-input").customSelect()
});
$(document).ready(function() {
    $(".extra-info-open").length > 0 && onWindowResize({
        onMobile: unbindExtraInfo,
        onTablet: bindExtraInfo,
        onDesktop: bindExtraInfo
    })
});
remindersCollapse = function() {
    var n = $(".m-border-rounded").find(".icon-add").parent(),
        t = $(".m-border-rounded").find(".icon-add");
    if ($(window).innerWidth() <= 767) {
        if (n.find(".section-content:visible").length > 2) {
            n.find(".section-content").hide();
            n.find(".icon-add").removeClass("icon-close-right");
            t.unbind("click").on("click", function() {
                $(this).next(".section-content").slideToggle();
                $(this).toggleClass("icon-close-right")
            })
        }
    } else t.unbind("click"), n.find(".section-content").show(), t.removeClass("icon-close-right")
};
$(document).ready(function() {
    var n;
    $(window).on("resize", function() {
        n = setTimeout(remindersCollapse, 100)
    });
    remindersCollapse();
    $("#Reminders .section-content").click(function(n) {
        n.stopPropagation()
    });
    $("#YourApplicationsTitle").click(function() {
        var n = $("#YourApplicationsContent"),
            i = $(this),
            t = n.parent();
        t.css("height") === "0px" && t.css("overflow") === "hidden" && (n.unwrap(), n.css("display", "none"));
        n.slideToggle(function() {
            $(this).css("display") === "none" && ($(this).wrap('<div style="height:0; overflow:hidden">'), $(this).css("display", "block"));
            i.toggleClass("icon-add").toggleClass("icon-dash")
        })
    });
    $("html").unbind("click").on("click", function() {
        $("#SearchIcon, #UserIcon").removeClass("open")
    });
    $("#SearchIcon").click(function(n) {
        n.stopPropagation();
        $(this).toggleClass("open");
        $("#UserIcon").removeClass("open");
        $(this).find(".flyout").click(function(n) {
            n.stopPropagation()
        })
    })
});
Menu = function(n) {
        var t = $(n.trigger),
            u = function() {
                f()
            },
            f = function() {
                t.on("click", function() {
                    var n = $($(this).attr("href")),
                        t = n.find(".menu-items");
                    return n.hasClass("open") ? r(n, t) : e(n, t), !1
                })
            },
            e = function(n, t) {
                n.addClass("open");
                t.slideDown(function() {
                    i("user-icon-close vertical-middle")
                });
                t.attr("aria-hidden", !1);
                $(document).one("click", function() {
                    r(n, t)
                })
            },
            i = function(n) {
                t.find("i").length ? t.find("i").attr("class", n) : t.attr("class", n)
            },
            r = function(n, t) {
                n.removeClass("open");
                t.slideUp(function() {
                    i("user-icon vertical-middle")
                });
                t.attr("aria-hidden", !0)
            };
        return {
            init: u
        }
    },
    function() {
        var n = new Menu({
            trigger: ".menu .menu-parent"
        });
        n.init()
    }();
Navigation = function(n) {
        var f = $(n.trigger) || null,
            t = $("body"),
            i = $(f.attr("href")),
            r = i.find("ul"),
            h = $(n.showMore),
            u = n.subnavigtion || !1,
            e;
        $(r[0]).children().each(function() {
            !$(this).is(".active") && $(this).children(".chevron-down-white").length && $(this).children("ul").hide()
        });
        r.find(".active").children(".chevron-down-white").addClass("chevron-up-white");
        u && (e = $(".subnavigation"), e.find(".closed").each(function() {
            $(this).children("ul").hide()
        }));
        var c = function() {
                a();
                u && onWindowResize({
                    onMobile: l,
                    onTablet: o,
                    onDesktop: o
                })
            },
            o = function() {
                var n = $(".subnavigation"),
                    t = $(".main-heading:first");
                t.length > 0 && n.removeClass("negate-padding m-margin-bottom").prependTo($(".main-content"))
            },
            l = function() {
                var t = $(".subnavigation"),
                    n = $(".main-heading:first");
                n.length > 0 && n.after(t)
            },
            a = function() {
                if (!u) f.on("click", function() {
                    return i.hasClass("open") ? s(i, r) : v(i, r), !1
                });
                h.on("click", function(n) {
                    n.stopPropagation();
                    $(this).siblings("ul").slideToggle();
                    $(this).parent().toggleClass("active");
                    $(this).toggleClass("chevron-up-white")
                })
            },
            v = function(n, i) {
                var e, r, u, f;
                n.addClass("open");
                e = n.find(".navigation-items");
                i.attr("aria-hidden", !1);
                t.addClass("bodyOpen");
                $(document, ".navigation-mask").one("click", function() {
                    s(n, i)
                });
                t.on("touchmove", function(n) {
                    n.preventDefault()
                });
                f = n.find(".navigation-items");
                f.off("touchstart").on("touchstart", function(n) {
                    u = n.originalEvent.touches[0].pageY;
                    r = 0
                });
                f.off("touchmove").on("touchmove", function(n) {
                    var t = n.originalEvent.touches[0].pageY;
                    r = u - t;
                    u = t;
                    $(this).scrollTop($(this).scrollTop() + r)
                })
            },
            s = function(n, i) {
                n.removeClass("open");
                i.attr("aria-hidden", !0);
                t.removeClass("bodyOpen");
                t.off("touchmove")
            };
        return {
            init: c
        }
    },
    function() {
        var t = new Navigation({
                trigger: ".icon-menu",
                showMore: ".mainnavigation .chevron-down-white, .mainnavigation .chevron-up-white"
            }),
            n;
        t.init();
        n = new Navigation({
            trigger: ".icon-menu",
            showMore: ".subnavigation .chevron-down-white, .subnavigation .chevron-up-white",
            subnavigtion: !0
        });
        n.init()
    }();
CookieBar = function(n) {
        var r = $(n.target),
            e = $(n.btn),
            t = "cookiebar",
            o = n.edited,
            s = function() {
                var r;
                h();
                var e = new Date,
                    s = e.toGMTString(),
                    n = new Date(o);
                u(t) === null ? (f(), i(t, s, 365)) : (r = new Date(u(t)), (n > r || typeof window.Sitecore != "undefined") && (f(), i(t, n.toGMTString(), 365)))
            },
            h = function() {
                e.on("click", function() {
                    return c(), !1
                })
            },
            u = function(n) {
                for (var r = n + "=", u = document.cookie.split(";"), t, i = 0; i < u.length; i++) {
                    for (t = u[i]; t.charAt(0) == " ";) t = t.substring(1, t.length);
                    if (t.indexOf(r) === 0) return t.substring(r.length, t.length)
                }
                return null
            },
            i = function(n, t, i) {
                var u = "",
                    r;
                i ? (r = new Date, r.setTime(r.getTime() + i * 864e5), u = "; expires=" + r.toGMTString()) : u = "";
                document.cookie = n + "=" + t + u + "; path=/"
            },
            a = function(n) {
                i(n, "", -1)
            },
            c = function() {
                r.slideUp("slow", function() {
                    $(window).off(".affix");
                    $(".main-header").removeClass("affix affix-top affix-bottom").removeData("bs.affix")
                }).attr("aria-hidden", !0)
            },
            f = function() {
                $("body").addClass("cookie-visible");
                r.show().attr("aria-hidden", !1)
            };
        return {
            init: s
        }
    },
    function() {
        var n = new CookieBar({
            target: "#cookie",
            btn: "#dismissCookie",
            edited: $("#lastUpdatedCookieDate").val()
        });
        n.init()
    }();
mobileOS = !1,
    function(n) {
        (/(android|bb\d+|meego).+mobile|android|ipad|playbook|silk|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(n) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(n.substr(0, 4))) && (mobileOS = !0)
    }(navigator.userAgent || navigator.vendor || window.opera, "http://detectmobilebrowser.com/mobile");
$(document).ready(function() {
    if ($(".image-carousel").length) {
        var n = $(".image-carousel");
        n.children().length > 1 && n.each(function() {
            var t = $(this),
                i = $('<div class="slick-navbar m-pad">'),
                n = $('<div class="wrapper nopaddingbottom">'),
                r = $('<div class="slick-controls">');
            $(this).slick({
                dots: !0,
                autoplay: !1,
                accessibility: !0,
                adaptiveHeight: !1,
                autoplaySpeed: 5e3,
                pauseOnHover: !0,
                speed: 300
            });
            n.append(t.find(".slick-list").siblings());
            r.append(n.find(".slick-next, .slick-prev"));
            t.append(i);
            i.append(n);
            n.append(r)
        })
    }
});
startCarousel = function() {
    var t = !1,
        n = $("#MainSlider"),
        s = $('<div class="slick-navbar m-pad">'),
        i = $('<div class="wrapper nopaddingbottom">'),
        h = $('<div class="slick-controls">'),
        e = $('<button type="button" class="slick-pause focus-light" data-role="none"><span class="visuallyhidden">Play/Pause<\/span><\/button>'),
        r = [],
        u = !0,
        f = function() {
            n.slick("play");
            e.addClass("slick-pause").removeClass("slick-play")
        },
        o = function() {
            n.slick("pause");
            e.addClass("slick-play").removeClass("slick-pause")
        },
        a, v;
    mobileOS ? n.find("iframe").each(function() {
        var t = $(this).attr("src").match(/(youtu(?:\.be|be\.com)\/(?:.*v(?:\/|=)|(?:.*\/)?)([\w'-]+))/i)[2],
            i = $('<a target="_blank">'),
            n;
        i.attr("href", "http://www.youtube.com/watch?v=" + t);
        n = $("<img>");
        n.addClass("img-scale");
        n.attr("src", "http://img.youtube.com/vi/" + t + "/maxresdefault.jpg");
        $(this).after(n);
        n.wrap(i);
        n.after('<svg style="width:85px; height:60px; position:absolute; top:50%; left:50%; margin-left:-42px; margin-top:-30px; cursor:pointer;"><path fill-rule="evenodd" clip-rule="evenodd" fill="#1F1F1F" class="ytp-large-play-button-svg" d="M84.15,26.4v6.35c0,2.833-0.15,5.967-0.45,9.4c-0.133,1.7-0.267,3.117-0.4,4.25l-0.15,0.95c-0.167,0.767-0.367,1.517-0.6,2.25c-0.667,2.367-1.533,4.083-2.6,5.15c-1.367,1.4-2.967,2.383-4.8,2.95c-0.633,0.2-1.316,0.333-2.05,0.4c-0.767,0.1-1.3,0.167-1.6,0.2c-4.9,0.367-11.283,0.617-19.15,0.75c-2.434,0.034-4.883,0.067-7.35,0.1h-2.95C38.417,59.117,34.5,59.067,30.3,59c-8.433-0.167-14.05-0.383-16.85-0.65c-0.067-0.033-0.667-0.117-1.8-0.25c-0.9-0.133-1.683-0.283-2.35-0.45c-2.066-0.533-3.783-1.5-5.15-2.9c-1.033-1.067-1.9-2.783-2.6-5.15C1.317,48.867,1.133,48.117,1,47.35L0.8,46.4c-0.133-1.133-0.267-2.55-0.4-4.25C0.133,38.717,0,35.583,0,32.75V26.4c0-2.833,0.133-5.95,0.4-9.35l0.4-4.25c0.167-0.966,0.417-2.05,0.75-3.25c0.7-2.333,1.567-4.033,2.6-5.1c1.367-1.434,2.967-2.434,4.8-3c0.633-0.167,1.333-0.3,2.1-0.4c0.4-0.066,0.917-0.133,1.55-0.2c4.9-0.333,11.283-0.567,19.15-0.7C35.65,0.05,39.083,0,42.05,0L45,0.05c2.467,0,4.933,0.034,7.4,0.1c7.833,0.133,14.2,0.367,19.1,0.7c0.3,0.033,0.833,0.1,1.6,0.2c0.733,0.1,1.417,0.233,2.05,0.4c1.833,0.566,3.434,1.566,4.8,3c1.066,1.066,1.933,2.767,2.6,5.1c0.367,1.2,0.617,2.284,0.75,3.25l0.4,4.25C84,20.45,84.15,23.567,84.15,26.4z M33.3,41.4L56,29.6L33.3,17.75V41.4z"><\/path><polygon fill-rule="evenodd" clip-rule="evenodd" fill="#FFFFFF" points="33.3,41.4 33.3,17.75 56,29.6"><\/polygon><\/svg>');
        playButton = $(this).find("svg");
        $(this).remove()
    }) : n.find(".main-heading-large").each(function(n) {
        var i = $(this).find("iframe"),
            u;
        i.length && (i.attr("id") || i.attr("id", "Player" + n), u = new YT.Player(i.attr("id")), u.addEventListener("onStateChange", function(n) {
            n.data === 1 || n.data === 3 ? (o(), t = !0) : (f(), t = !1)
        }), i.attr("has-video", r.length), r.push({
            player: u
        }))
    });
    var y = function() {
            var t = n.find(".slick-list");
            t.scrollLeft(0)
        },
        c = function(n) {
            var t = n.find(".main-heading-large, .slide"),
                i, r;
            $(window).width() >= 768 ? (i = [], r = 0, t.css("height", "auto"), $.each(t, function(n, t) {
                i.push($(t).height())
            }), r = Math.max.apply(Math, i), t.css("height", r)) : t.css("height", "auto")
        },
        l = function() {
            $(window).width() < 768 && (n.offset().top + n.height() - 150 < $(window).scrollTop() ? o() : u === !0 && f())
        };
    if (n.children(".main-heading-large, .slide").length > 1) {
        setTimeout(function() {
            c(n)
        }, 100);
        $(window).on("resize", function() {
            setTimeout(function() {
                c(n)
            }, 100)
        });
        n.slick({
            dots: !0,
            autoplay: !0,
            accessibility: !0,
            adaptiveHeight: !0,
            autoplaySpeed: autoplaySpeed,
            pauseOnHover: !1,
            speed: 300
        });
        i.append(n.find(".slick-list").siblings());
        h.append(i.find(".slick-next, .slick-prev"));
        h.find(".slick-prev").after(e);
        n.append(s);
        s.append(i);
        i.append(h);
        $(".slick-navbar").find("button").addClass("focus-light");
        e.on("click", function() {
            n.slick("setOption", "pauseOnHover", !1);
            $(this).hasClass("slick-play") ? (u = !0, f()) : (u = !1, o())
        });
        $(n).on("mouseover", function() {
            o()
        });
        $(n).on("mouseleave", function() {
            !t && u && f()
        });
        $(s, i).on("mouseover", function() {
            return !t && u && f(), !1
        });
        n.on("keyup", function(t) {
            if (t.keyCode === 27) {
                n.find(":focus").blur();
                var i = n.next().next().text().trim().length ? n.next().next() : n.parent().next();
                i.attr("tabindex", "0");
                i.focus();
                i.on("blur", function() {
                    $(this).removeAttr("tabindex")
                })
            }
        });
        a = n.find(".video-16x9").parent();
        v = n.find(".slick-cloned .video-16x9").parent();
        n.on("beforeChange", function(n) {
            var i = $(n.target).find(".slick-active").find("iframe").attr("has-video");
            i >= 0 && r[i].player.pauseVideo && r[i].player.getPlayerState() === 1 && r[i].player.pauseVideo();
            t = !1;
            a.show();
            l()
        });
        $(document).on("scroll", function() {
            n.find(":focus").blur()
        });
        n.on("afterChange", function() {
            n.find(".slick-track").find(":focus").length && (n.find(".slick-current").focus(), y());
            v.hide()
        });
        $(window).on("scroll", function() {
            l()
        })
    }
};
$("#MainSlider").length > 0 && ($("#MainSlider iframe").length > 0 ? (onYouTubeIframeAPIReady = function() {
    editMode !== 1 && startCarousel()
}, tag = document.createElement("script"), tag.src = "https://www.youtube.com/iframe_api", firstScriptTag = document.getElementsByTagName("script")[0], firstScriptTag.parentNode.insertBefore(tag, firstScriptTag)) : $(document).ready(function() {
    editMode !== 1 && startCarousel()
}));
$(document).ready(function() {
    if ($("#ApplicantId, #DateBirth").length === 2 && !$("html").hasClass("lt-ie9")) {
        var n = function() {
                var n = $("#DateBirth"),
                    t = n.attr("placeholder");
                n.on("touchend", function() {
                    n.attr("type", "date")
                });
                n.on("blur", function() {
                    var t = n.val();
                    n.attr("type", "password");
                    n.val(t)
                })
            },
            t = function() {
                var n = $("#ApplicantId");
                n.attr("type", "number");
                n.attr("pattern", "[0-9]*");
                n.attr("inputmode", "numeric");
                n.attr("min", "0");
                n.attr("max", "99999999")
            },
            i = function() {
                var n = $("#ApplicantId");
                n.attr("type", "text");
                n.removeAttr("pattern");
                n.removeAttr("inputmode");
                n.removeAttr("min");
                n.removeAttr("max")
            },
            r = function() {
                var n = $("#DateBirth");
                n.attr("type", "password")
            };
        onWindowResize({
            onMobile: n,
            onTablet: n,
            onDesktop: r
        });
        onWindowResize({
            onMobile: t,
            onTablet: t,
            onDesktop: i
        })
    }
});
ShowMore = function(n) {
        var r = $(n.btn),
            i = null,
            t = null,
            u = "Close",
            f = function() {
                e()
            },
            e = function() {
                r.each(function() {
                    $(this).attr("data-orig", $(this).find("strong").html())
                });
                r.on("click", function() {
                    return i = $(this), t = $(i.attr("href")), t.hasClass("visuallyhidden") ? o() : s(i.attr("data-orig")), !1
                })
            },
            o = function() {
                t.removeClass("visuallyhidden").show();
                $(".masonry-area").masonry();
                t.removeClass("visuallyhidden").hide();
                t.css("display", "none").removeClass("visuallyhidden").slideDown(function() {
                    t.attr("aria-hidden", !1);
                    i.removeClass("chevron-down").addClass("chevron-up").addClass("text-transform-uppercase");
                    i.find("strong").html(u)
                })
            },
            s = function(n) {
                t.removeClass("visuallyhidden").hide();
                $(".masonry-area").masonry();
                t.removeClass("visuallyhidden").show();
                t.slideUp(function() {
                    t.attr("aria-hidden", !0).addClass("visuallyhidden");
                    i.removeClass("chevron-up").addClass("chevron-down").removeClass("text-transform-uppercase");
                    i.find("strong").html(n)
                })
            };
        return {
            init: f
        }
    },
    function() {
        var n = new ShowMore({
            btn: ".show-more"
        });
        n.init()
    }();
$(document).ready(function() {
    if (typeof appMenu != "undefined") var n = new appMenu({
        menuContainer: $("#SectionNavigation"),
        parents: ".navigation-top-item > a",
        items: ".navigation-lvl3 > li > a",
        vmenu: !1
    })
});
convertTables = function() {
        var n = !1,
            t = function() {
                $("table").length > 0 && onWindowResize({
                    onMobile: i
                })
            },
            i = function() {
                n || ($("table").each(function(n, t) {
                    if (!$(this).hasClass("table-template-inbox")) {
                        var r = [],
                            i = $(t).clone();
                        i.addClass("desktop-hidden tablet-hidden").find("th").each(function(n, t) {
                            r.push($(t).text())
                        });
                        i.find("tbody tr").each(function(n, t) {
                            var u = [],
                                e = "",
                                i, f;
                            for ($(t).find("td").each(function(n, t) {
                                    u.push($(t).html())
                                }), i = 0; i < u.length; i++) f = "", i + 1 === u.length && (f = "nopaddingbottom"), e += '<p class="' + f + '"><strong class="display-block row-margin-small">' + r[i] + "<\/strong>" + u[i] + "<\/p>";
                            $(t).html("<td>" + e + "<\/td>")
                        });
                        $(t).addClass("mobile-hidden").before(i)
                    }
                }), n = !0)
            };
        return {
            init: t
        }
    },
    function() {
        var n = new convertTables;
        n.init()
    }();
$(document).ready(function() {
        if (($(".template-search-standout-dark").length || $(".template-search-primary").length) && ($('.search-form input[type="search"], .search-form-mobile input[type="search"]').val($(".headline strong").text().substring($(".headline strong").text().indexOf("'") + 1, $(".headline strong").text().length - 1)), typeof selectedCategory != "undefined" ? ($(".search-form select").children().each(function() {
                $(this).attr("selected", !1);
                var n = $(this).text().toLowerCase(),
                    t = selectedCategory.toLowerCase();
                n === t && $(this).attr("selected", !0)
            }), $(".search-form-mobile input[type=radio]").each(function() {
                $(this).attr("checked", !1);
                var n = $(this).siblings("label").text().toLowerCase(),
                    t = selectedCategory.toLowerCase();
                n === t && $(this).attr("checked", !0)
            })) : ($(".search-form select").children().eq(0).attr("selected", !0), $(".search-form-mobile input[type=radio]").eq(0).attr("checked", !0)), $(".search-form select").change()), $(".search-form-mobile").length) {
            $("#MobileSearchOpen").on("click", function() {
                $(".search-form-mobile").fadeIn()
            });
            $("#MobileSearchClose").on("click", function() {
                $(".search-form-mobile").fadeOut()
            })
        }
    }),
    function() {
        $(".cb-checkbox").on("change", function() {
            var n = $(this),
                t;
            n.is(":checked") ? (t = "input:checkbox[name='" + n.attr("name") + "']", $(t).prop("checked", !1), n.prop("checked", !0)) : n.prop("checked", !1)
        })
    }();
$(function() {
    $(".main-content a[href*=#]:not([href=#])").click(function() {
        if (location.pathname.replace(/^\//, "") == this.pathname.replace(/^\//, "") && location.hostname == this.hostname && !$(this).hasClass("no-scroll")) {
            var n = $(this.hash);
            if (n = n.length ? n : $("[name=" + this.hash.slice(1) + "]"), n.length) return $("html,body").animate({
                scrollTop: n.offset().top - 130
            }, 1e3), !1
        }
    });
    $(".main-content *").on("focus", function() {
        function t() {
            var t = $(document).scrollTop(),
                i = n.offset().top - 150;
            i < t && $("html,body").scrollTop(n.offset().top - 150)
        }
        var n = $(this);
        setTimeout(t, 1)
    })
});
$(document).ready(function() {
    (function() {
        function n() {
            $(".masonry-area").each(function() {
                var n = $(this),
                    t = n.innerWidth() / 12;
                n.masonry({
                    columnWidth: t
                });
                n.find(".show-more").on("click", function() {
                    setTimeout(function() {
                        n.masonry()
                    }, 600)
                })
            })
        }
        $(window).load(function() {
            n()
        });
        $(window).resize(function() {
            n()
        })
    })()
});
resetEllipsis = function() {
    $(".rows-x-5, .max-rows-x-5").length && !$(".scpm").length && $(".rows-x-5, .max-rows-x-5").dotdotdot({
        watch: !0,
        after: $('<span class="ellipsis text-weight-heavy">...<\/span>'),
        ellipsis: ""
    });
    $(".max-rows-x-3").length && !$(".scpm").length && $(".max-rows-x-3").dotdotdot({
        watch: !0,
        after: $('<span class="ellipsis text-weight-heavy">...<\/span>'),
        wrap: "letter",
        ellipsis: ""
    });
    $(".max-rows-x-1").length && !$(".scpm").length && $(".max-rows-x-1").dotdotdot({
        watch: !0,
        after: $('<span class="ellipsis text-weight-heavy">...<\/span>'),
        wrap: "letter",
        ellipsis: ""
    })
};
$(window).load(function() {
    resetEllipsis()
});
$(document).ready(function() {
    $(".text-resize-script").each(function() {
        var t = $(this),
            r = t.find(".scChromeData").length ? !0 : !1,
            u = r ? t.find(".scChromeData").next() : t,
            s = r ? u.parent() : null,
            e = u.text().replace(/  +|\r\n|\r|\n/g, ""),
            o, f, i, n, h;
        for (r ? u.text(e) : t.children().text(e), o = e.length, f = t.attr("class"), f = f.replace(/( m-| )text-size.{3}/g, ""), r ? s.attr("class", f) : t.attr("class", f), i = [
                [0, "text-size-40 t-text-size-30"],
                [40, "text-size-35 t-text-size-25"],
                [65, "text-size-25 t-text-size-20"],
                [80, "text-size-20 m-text-size-20"]
            ], n = 0, h = i.length; n < h; n++) o > i[n][0] && (!i[n + 1] || o <= i[n + 1][0]) && (r ? (u.css("display", "inline"), s.addClass(i[n][1]), t.removeAttr("class")) : u.addClass(i[n][1]))
    })
});
appOverlay = function(n) {
        var t = ["a", "input", "select", "textarea", "button", "iframe"],
            s = $(n.trigger),
            h = $(".close-overlay, .icon-close-white, .icon-close, .overlay-mask"),
            i = $(n.target) || "",
            r = "",
            c = n.style || "",
            f = n.backgroundLocked || !1,
            e = ".main-content, .sticky-container, footer, .cookie",
            l = function() {
                a();
                y()
            },
            a = function() {
                f && $("html").addClass("locked")
            },
            v = function() {
                f && $("html").removeClass("locked")
            },
            y = function() {
                n.auto && o();
                s.on("click", function() {
                    return r = $(this), i = $("#" + r.attr("data-id")), o(), !1
                });
                h.on("click", function() {
                    return u(), !1
                })
            },
            p = function() {
                $(document).on("keyup", function(n) {
                    n.keyCode == 27 && u()
                })
            },
            o = function() {
                w(i);
                i.appendTo("body").addClass(c).fadeIn(300).attr("aria-hidden", !1);
                p();
                $(e).attr("aria-hidden", !0)
            },
            u = function() {
                b();
                v();
                i.fadeOut(300).attr("aria-hidden", !0);
                r.length > 0 && r.focus();
                $(e).attr("aria-hidden", !1)
            },
            w = function(n) {
                for (var r, i = 0; i < t.length; i++) $(t[i]).attr("tabindex", -1);
                for ($("[tabindex=0]").attr("tabindex", -1).addClass("recoverTabIndex"), r = 0; r < t.length; r++) n.find(t[r]).removeAttr("tabindex")
            },
            b = function() {
                for (var n = 0; n < t.length; n++) $(t[n]).removeAttr("tabindex");
                $(".recoverTabIndex").attr("tabindex", 0).removeClass("recoverTabIndex")
            };
        return {
            init: l,
            overlayClose: u
        }
    },
    function() {
        var n = new appOverlay({
                trigger: ".overlay-trigger"
            }),
            t;
        n.init();
        t = new lockScroll(".overlay-scroll")
    }();
appTooltip = function(n) {
        var i = ["a", "input", "select", "textarea", "button"],
            a = $(n.trigger),
            v = $(".icon-close, .tooltip-mask, .tooltip-dismiss"),
            t = $(n.target) || "",
            r = "",
            u = "",
            e = 0,
            o = 0,
            f = "";
        this.moved = !1;
        var s = ".main-content, .sticky-container, footer, .cookie",
            y = function() {
                p()
            },
            p = function() {
                n.auto && t.length > 0 && (r = $(".tooltip-chrome", t), u = $(".tooltip-scroll", t), c());
                a.off("click").on("click", function() {
                    return f = $(this), t = $("#" + f.attr("data-id")), r = $(".tooltip-chrome", t), u = $(".tooltip-scroll", t), c(), !1
                });
                v.off("click").on("click", function() {
                    return l($(this)), !1
                });
                $(window).on("resize orientationchange", function() {
                    r !== "" && h()
                })
            },
            h = function() {
                if (t.removeClass("overflow"), r.css("marginTop", 0), u.css("height", "auto"), o = $(window).height(), e = r.height() + 20, e > o) {
                    t.addClass("overflow");
                    var n = t.find(".tooltip-header").height();
                    u.css({
                        height: o - n - 120
                    })
                } else r.css("marginTop", -(e / 2))
            },
            w = function() {
                $(document).on("keyup", function(n) {
                    n.keyCode == 27 && l()
                })
            },
            c = function() {
                var r = t.find(".tooltip-header"),
                    n = t.attr("id") + "title",
                    u = t.find("tooltip-scroll"),
                    i = t.attr("id") + "description";
                r.attr("id", n);
                u.attr("id", i);
                t.attr("aria-labelledby", n);
                t.attr("aria-describedby", i);
                b(t);
                t.appendTo("body").fadeIn(300).attr("aria-hidden", !1);
                h();
                w();
                $(s).attr("aria-hidden", !0)
            },
            l = function(n) {
                k();
                t = t.length ? t : n.closest(".tooltip");
                t.fadeOut(300).attr("aria-hidden", !0);
                f.length > 0 && f.focus();
                $(s).attr("aria-hidden", !1)
            },
            b = function(n) {
                for (var r, t = 0; t < i.length; t++) $(i[t]).attr("tabindex", -1);
                for (r = 0; r < i.length; r++) n.find(i[r]).removeAttr("tabindex")
            },
            k = function() {
                for (var n = 0; n < i.length; n++) $(i[n]).removeAttr("tabindex")
            };
        return {
            init: y
        }
    },
    function() {
        var n = new appTooltip({
                trigger: ".icon-tooltip, .tooltip-trigger"
            }),
            t;
        n.init();
        t = new lockScroll(".tooltip-scroll")
    }();
openSearch = openSearch || {};
openSearch.search = function() {
    this.downarrow = "notpressed";
    this.searchTextBox = $("#txtSearchTerm");
    this.searchButton = $("#btnSearch")
};
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "")
};
openSearch.search.prototype = function() {
        var t = function() {
                i.call(this)
            },
            i = function() {
                var t = this;
                this.searchButton.on("click", function() {
                    n.call(t)
                });
                this.searchTextBox.on("keypress", function() {
                    r.call(t, event)
                })
            },
            n = function() {
                var i = this,
                    n, t;
                return $("#searchResultsPage").val() == undefined ? !1 : (n = $("#txtSearchTerm").val(), !u.call(i, n)) ? !1 : (t = $("#searchResultsPage").val() + "?SearchCategory=" + $("input[name=searchCategory]:checked").val() + "&SearchTerm=" + $("#txtSearchTerm").val(), location.href = t, !0)
            },
            r = function(t) {
                var i = this,
                    r = t.which ? t.which : t.keyCode;
                r == "40" && (t.returnValue = !1, t.cancel = !0, downarrow = "pressed");
                r == "13" && (t.returnValue = !1, t.cancel = !0, i.downarrow == "notpressed" ? n.call(i) : i.downarrow = "notpressed")
            },
            u = function(n) {
                var t = this;
                return f.call(t, n) == "" ? !1 : !0
            },
            e = function(n) {
                n = n.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var i = "[\\?&]" + n + "=([^&#]*)",
                    r = new RegExp(i),
                    t = r.exec(window.location.href);
                return t == null ? "" : decodeURIComponent(t[1].replace(/\+/g, " "))
            },
            f = function(n) {
                return n = n.replace(/\r\n/g, ""), n.trim()
            };
        return {
            setup: t
        }
    }(),
    function() {
        var n = new openSearch.search;
        n.setup()
    }();
$(document).ready(function() {
    var n = getParameterByName("SearchCategory"),
        t = getParameterByName("SearchTerm");
    n != null && n.trim() != "" ? $('input[name="searchCategory"][value="' + n + '"]').prop("checked", !0) : $('input[name="searchCategory"]:first').attr("checked", !0);
    t != null && t.trim() != "" && $("#txtSearchTerm").val(t)
});
appAccordion = function(n) {
    for (var t = {
            container: n.accordionContainer,
            panels: n.accordionContainer.find(n.accordionPanels),
            triggers: n.accordionContainer.find(n.accordionTriggers),
            keys: {
                tab: 9,
                enter: 13,
                esc: 27,
                space: 32,
                pageup: 33,
                pagedown: 34,
                end: 35,
                home: 36,
                left: 37,
                up: 38,
                right: 39,
                down: 40
            }
        }, r = n.accordionType ? n.accordionType : "accordion", l = n.defaultOpen !== undefined ? n.defaultOpen : -1, a = n.singleOpen ? n.singleOpen : !0, e = n.iconOpen ? n.iconOpen : "icon-add-accordion", o = n.iconClose ? n.iconClose : "icon-dash-accordion", s = n.visiblePanelClass ? n.visiblePanelClass : "fade-panel-visible", h = n.hiddenPanelClass ? n.hiddenPanelClass : "fade-panel-hidden", nt = function() {
            t.triggers.keydown(function(n) {
                return v($(this), n)
            });
            t.triggers.keypress(function(n) {
                return y($(this), n)
            });
            t.triggers.click(function(n) {
                return p($(this), n)
            });
            t.triggers.focus(function(n) {
                return w($(this), n)
            });
            t.triggers.blur(function(n) {
                return b($(this), n)
            });
            t.panels.keydown(function(n) {
                return k($(this), n)
            });
            t.panels.keypress(function(n) {
                return d($(this), n)
            });
            t.panels.click(function(n) {
                return g($(this), n)
            })
        }(), v = function(n, i) {
            var r, e, o;
            if (i.altKey) return !0;
            switch (i.keyCode) {
                case t.keys.enter:
                case t.keys.space:
                    return n.attr("aria-expanded") !== "true" ? f($("#" + n.attr("aria-controls")), n) : u($("#" + n.attr("aria-controls")), n), i.stopPropagation(), !1;
                case t.keys.left:
                case t.keys.up:
                    return r = t.triggers.index(n), e = r - 1, c(t.triggers.eq(r), t.triggers.eq(e)), i.stopPropagation(), !1;
                case t.keys.right:
                case t.keys.down:
                    return r = t.triggers.index(n), o = r + 1 !== t.triggers.length ? r + 1 : 0, c(t.triggers.eq(r), t.triggers.eq(o)), i.stopPropagation(), !1;
                case t.keys.home:
                    return this.switchTabs($tab, this.$tabs.first()), i.stopPropagation(), !1;
                case t.keys.end:
                    return this.switchTabs($tab, this.$tabs.last()), i.stopPropagation(), !1
            }
        }, y = function(n, t) {
            if (t.preventDefault(), t.altKey) return !0;
            switch (t.keyCode) {
                case this.keys.enter:
                case this.keys.space:
                case this.keys.left:
                case this.keys.up:
                case this.keys.right:
                case this.keys.down:
                case this.keys.home:
                case this.keys.end:
                    return t.stopPropagation(), !1;
                case this.keys.pageup:
                case this.keys.pagedown:
                    return t.ctrlKey ? (t.stopPropagation(), !1) : !0
            }
            return !0
        }, p = function(n, t) {
            t.preventDefault();
            n.attr("aria-expanded") !== "true" ? f($("#" + n.attr("aria-controls")), n) : r !== "tabs" && u($("#" + n.attr("aria-controls")), n)
        }, w = function() {}, b = function() {}, k = function() {}, d = function() {}, g = function() {}, c = function(n, t) {
            n.attr("tabindex", "-1").attr("aria-selected", "false");
            t.attr("aria-selected", "true");
            t.attr("tabindex", "0");
            t.focus();
            r === "tabs" && f($("#" + t.attr("aria-controls")), t)
        }, f = function(n, i) {
            r === "accordion" ? (n.slideDown(function() {
                n.prev().addClass("bg-grey");
                n.parent().removeClass("force-hover-cursor");
                i.removeClass(e).addClass(o + " force-hover-cursor");
                n.attr("aria-hidden", !1);
                i.attr("aria-selected", !0);
                i.attr("aria-expanded", !0)
            }), a && t.panels.each(function(i) {
                $(this).html() != n.html() && u($(this), t.triggers.eq(i))
            })) : r === "tabs" && (n.addClass(s).removeClass(h), n.parent().height(n.height()), n.attr("aria-hidden", !1), i.attr("aria-selected", !0).attr("aria-expanded", !0).addClass("active"), t.panels.each(function(i) {
                $(this).html() !== n.html() && u($(this), t.triggers.eq(i))
            }))
        }, u = function(n, t) {
            r === "accordion" ? n.slideUp(function() {
                n.prev().removeClass("bg-grey");
                n.parent().addClass("force-hover-cursor");
                t.removeClass(o, "force-hover-cursor").addClass(e);
                n.attr("aria-hidden", !0);
                t.attr("aria-selected", !1);
                t.attr("aria-expanded", !1)
            }) : r === "tabs" && (n.addClass(h).removeClass(s), n.attr("aria-hidden", !0), t.attr("aria-selected", !1).attr("aria-expanded", !1).removeClass("active"))
        }, i = 0; i < t.panels.length; i++) i !== l ? u(t.panels.eq(i), t.triggers.eq(i)) : f(t.panels.eq(i), t.triggers.eq(i))
};
Masonary = function(n) {
    function kt() {
        $(window).off("scroll")
    }

    function dt() {
        r.layout()
    }

    function gt() {
        r.remove(r.getItemElements())
    }

    function ni(n) {
        r.appended(n)
    }

    function ti() {
        s = 1;
        a = !1;
        f = !1;
        v.addClass("visuallyhidden")
    }
    var t = n,
        u = $(t.target),
        ut = $(t.target),
        r = null,
        ft = u.children(t.items),
        et = t.infinity,
        i = t.filters || [],
        o = [],
        h = [],
        c = [],
        l = !1,
        k = $(t.loading),
        v = $(t.loadingComplete),
        f = !1,
        d = t.maxWidth,
        e = !1,
        g = t.maxPages || !1,
        nt = 1,
        ot = t.url,
        st = t.loadMoreHandler,
        y = t.preload || !1,
        ht = t.preloadPage,
        ct = t.preloadID,
        s = 1,
        a = !1,
        ii = t.staticDesign || !1,
        lt = function() {
            w();
            $(window).on("resize", function() {
                w()
            });
            ft.length ? (y && (s = ht, vt(s)), et && at(), i !== null && yt()) : p()
        },
        at = function() {
            $(window).scroll(function() {
                var n = $(window).scrollTop() + $(window).height() + 250 + $(".quicklinks").height(),
                    t = $(document).height();
                !a && !f && n > t && (f = !0, k.removeClass("visuallyhidden"), s++, window.setTimeout(function() {
                    st(s, function(n) {
                        if (e)
                            if (n !== null) {
                                var i = $.parseHTML(n),
                                    t = [];
                                i !== null ? ($.each(i, function(n, i) {
                                    t.push(i)
                                }), u.append(t), r.appended(t)) : (f = !0, a = !0)
                            } else f = !0, a = !0;
                        else u.append(n);
                        it();
                        f = !1;
                        k.addClass("visuallyhidden");
                        y && (bt(ct), y = !1);
                        n === "" && p();
                        g !== !1 && (nt >= g && (f = !0, v.removeClass("visuallyhidden")), nt += 1);
                        setTimeout(function() {
                            r.layout()
                        }, 500)
                    })
                }, 1e3))
            })
        },
        vt = function() {
            window.setTimeout(function() {
                $.ajax({
                    url: ot + "&page=" + s
                }).done(function() {})
            }, 1e3)
        },
        p = function() {
            f = !0;
            v.removeClass("visuallyhidden")
        },
        yt = function() {
            var n = 0,
                r = i.length,
                t, u;
            if (i[0] && i[0].constructor === Array)
                for (n = 0, r = i.length; n < r; n++)
                    for (t = 0, u = i[n].length; t < u; t++) $(i[n][t]).on("change", tt);
            for (n = 0; n < r; n++) $(i[n]).on("change", tt)
        },
        tt = function() {
            var f, n = 0,
                s = i.length,
                a = function() {
                    var n = $(this);
                    c.push("." + n.attr("id"))
                },
                v = function() {
                    var n = $(this);
                    h.push("." + n.attr("id"))
                },
                t, l;
            if (i[0] && i[0].constructor === Array) {
                for (h = [], c = []; n < s; n++) t = $(i[n].toString()), l = t.filter(":checked"), l.length && (t.filter(":checked").each(a), t.filter(":not(:checked)").each(v));
                h.length ? (u.children().show().addClass("pane"), u.children(h.toString()).each(function() {
                    for (var t = $(this), n = 0; n < c.length; n++) $(this).hasClass(c[n].substring(1)) || $(this).hide().removeClass("pane")
                }), e && r.layout()) : (b(), e && r.layout())
            } else {
                for (o = []; n < s; n++) f = $(i[n]), f.is(":checked") && o.push(f.attr("id"));
                it()
            }
        },
        it = function() {
            for (var t = "", n = 0; n < o.length; n++) t += "." + o[n], n !== o.length - 1 && (t += ", ");
            t === "" ? (b(), e && r.layout()) : (u.children().css({
                visibility: "hidden",
                display: "none"
            }).removeClass("pane"), u.children(t).css({
                visibility: "visible",
                display: "block"
            }).addClass("pane"), e && r.layout())
        },
        w = function() {
            var n = $(window).width();
            n >= d && !l && (e = !0, pt());
            n < d && l && (e = !1, rt())
        },
        pt = function() {
            b();
            wt();
            r = new Masonry(t.target, {
                itemSelector: ".pane",
                columnWidth: ".pane"
            });
            ut.imagesLoaded(function() {
                r.layout()
            });
            l = !0
        },
        b = function() {
            u.children("div").css({
                visibility: "visible",
                display: "block"
            }).addClass("pane")
        },
        wt = function() {
            var n, t;
            for (o = [], n = 0; n < i.length; n++) t = $(i[n]), t.removeAttr("checked")
        },
        bt = function(n) {
            var t = $("#" + n);
            $("html, body").animate({
                scrollTop: t.offset().top
            }, 200)
        },
        rt = function() {
            r.destroy();
            l = !1
        };
    return {
        init: lt,
        destroy: rt,
        checkSize: w,
        isComplex: function() {
            return e
        },
        unbindScrollEvent: kt,
        refreshLayout: dt,
        removeAllItems: gt,
        appendNewItems: ni,
        resetPageCounter: ti,
        showLoadComplete: p
    }
};
$(document).ready(function() {
    $("#DesktopHeaderSearchForm").on("submit", function(n) {
        n.preventDefault();
        var i = $("#DesktopHeaderSearchForm").find("option:selected").attr("data-id"),
            r = $("#ProductType").val(),
            t = $("#DesktopSearchTerm").val(),
            u = $("#DesktopHeaderSearchForm").find("select").val(),
            f = u + "?SearchCategory=" + i + "&SearchTerm=" + t + "&ProductType=" + r;
        t.length > 0 && t.split(" ").join("").length > 0 && (location.href = f)
    });
    $("#MobileSearchForm").on("submit", function(n) {
        n.preventDefault();
        var i = $(this),
            r = i.find(":checked").attr("data-id"),
            u = $("#ProductType").val(),
            t = $("#MobileSearchTerm").val(),
            f = i.find(":checked").val(),
            e = f + "?SearchCategory=" + r + "&SearchTerm=" + t + "&ProductType=" + u;
        t.length > 0 && t.split(" ").join("").length > 0 && (location.href = e)
    })
});
$(document).ready(function() {
    $(function() {
        $(document).ajaxError(function(n, t) {
            t.status === 401 && (window.location.href = t.responseText)
        })
    })
});
CostCalculator = function() {
        var t = [],
            p = $("#TuitionCalculate"),
            ut = $("#TuitionReset"),
            o = $("#TuitionToggle"),
            ft = $(".filterTuitionStatus"),
            et = $(".filterTuitionType"),
            w = $("#CostAccommodationMultiplier").val(),
            g = $("#CostLivingMultiplier").val(),
            nt = $("#ComparisonItem").val(),
            i, r, s, n = {
                campus: "Campus"
            },
            h = $("#costSelfCatered, #costCatered"),
            f = $("#costPremium, #costStandard"),
            c = $("#costBathroomPrivate, #costBathroomShared"),
            ot = $("#costPrivateRental"),
            b = $("#costSpendBottom, #costSpendMiddle, #costSpendTop"),
            l = $("#CostCalculate"),
            st = $("#CostReset"),
            e = $("#costToggle"),
            a = $("#costResults"),
            ht = function() {
                var n = $("#FoodTravelSocialCosts").val(),
                    t = $("#CalculatorCosts").val();
                $.ajax({
                    url: "/api/AccommodationFeesCalculator/CalculateFees",
                    type: "POST",
                    data: JSON.stringify({
                        foodTravelSocialIds: n,
                        calculatorFeeRanges: t
                    }),
                    contentType: "application/json; charset=utf-8",
                    success: function(n) {
                        i = n.costBandSelfCatered;
                        r = n.costBandCatered;
                        s = n.costBands;
                        $("#costSpendBottom").attr("value", i[0]).next().text("£" + i[0]);
                        $("#costSpendMiddle").attr("value", i[1]).next().text("£" + i[1]);
                        $("#costSpendTop").attr("value", i[2]).next().text("£" + i[2])
                    }
                });
                dt();
                gt();
                ni();
                ti();
                kt();
                vt();
                yt();
                pt();
                wt();
                bt();
                lt();
                ct()
            },
            tt = new appValidations([{
                id: "#costPrivateRental",
                isRequired: !1,
                validations: [{
                    checkIf: "isInjection",
                    ifTrue: "Error message"
                }, {
                    checkIf: "isUnderMinimum",
                    ifTrue: "Minimum value must be at least 50"
                }, {
                    checkIf: "isOverMaximum",
                    ifTrue: "Maximum value must be less than 500"
                }, {
                    checkIf: "isNotNumber",
                    ifTrue: "This field requires a number"
                }, ],
                success: null
            }]),
            ct = function() {
                e.each(function() {
                    $(this).hasClass("icon-add") && ($(".pnl-costs").hide(), footerPush(), $(this).removeClass("icon-dash"))
                });
                e.off("click").on("click", function() {
                    return e.hasClass("icon-dash") ? $(".pnl-costs").slideUp(function() {
                        e.addClass("icon-add").removeClass("icon-dash");
                        footerPush()
                    }) : $(".pnl-costs").slideDown(function() {
                        e.removeClass("icon-add").addClass("icon-dash");
                        footerPush()
                    }), !1
                })
            },
            lt = function() {
                l.off("click").on("click", function(n) {
                    n.preventDefault();
                    a.hasClass("visuallyhidden") && (u(), a.removeClass("visuallyhidden").hide().slideDown(function() {
                        footerPush();
                        $(this).attr("aria-hidden", "false");
                        var n = document.getElementById("costResults").offsetTop;
                        $("html,body").animate({
                            scrollTop: n - 200
                        }, 300)
                    }))
                });
                st.off("click").on("click", function() {
                    var t, i;
                    h.removeAttr("disabled");
                    f.removeAttr("disabled");
                    c.removeAttr("disabled");
                    $("#costPrivateRentalWarning").hide();
                    for (t in n) delete n[t];
                    n.campus = "Campus";
                    l.attr("disabled", "disabled").addClass("slvzr-disabled");
                    i = document.getElementById("costToggle").offsetTop;
                    $("html,body").animate({
                        scrollTop: i - 200
                    }, 300, function() {
                        tt.validateAll();
                        $("#costToggle").focus();
                        footerPush()
                    });
                    k()
                })
            },
            k = function() {
                a.slideUp(function() {
                    footerPush();
                    $(this).css("display", "block").addClass("visuallyhidden").attr("aria-hidden", "true")
                })
            },
            at = function() {
                return n.campus === "Campus" && (n.catered === "" || typeof n.catered == "undefined" || n.level === "" || typeof n.level == "undefined" || n.bathroom === "" || typeof n.bathroom == "undefined" || n.spend === "" || typeof n.spend == "undefined") ? !1 : n.campus === "Off Campus" && (isNaN(parseInt(n.privateRental)) || n.privateRental < 1 || n.spend === "" || typeof n.spend == "undefined") ? !1 : !0
            },
            u = function() {
                function t() {
                    var t, i, f, h, c, a, v, y, p;
                    if (at()) {
                        for (l.removeAttr("disabled").removeClass("slvzr-disabled"), t = "", i = 0, f = s.length; i < f; i++) {
                            var r = s[i].name.toLowerCase(),
                                b = n.catered.toLowerCase(),
                                d = n.level.toLowerCase(),
                                nt = n.bathroom.toLowerCase();
                            r.indexOf(b) >= 0 && r.indexOf(d) >= 0 && r.indexOf(nt) >= 0 && (t = s[i])
                        }
                        if (t !== "") {
                            var e = t.lower * w,
                                o = t.upper * w,
                                u = g * parseInt(n.spend);
                            $(".costs-campus").text(n.campus);
                            $(".costs-catered").text(n.catered).show();
                            $(".costs-level").text(n.level).show();
                            $(".costs-bathroom").text(n.bathroom + " bathroom").show();
                            $(".costs-spend").text(n.spend);
                            o === 0 ? (h = (e + u).formatMoney(0, ".", ","), $(".costs-total").text("£" + h)) : (c = (e + u).formatMoney(0, ".", ","), a = (o + u).formatMoney(0, ".", ","), $(".costs-total").html("£" + c + ' <span class="text-size-20">to<\/span> £' + a))
                        }
                        n.campus === "Off Campus" && (v = n.privateRental * w, y = g * parseInt(n.spend), $(".costs-campus").text(n.campus), $(".costs-catered").hide(), $(".costs-level").hide(), $(".costs-bathroom").hide(), $(".costs-spend").text(n.spend), p = (v + y).formatMoney(0, ".", ","), $(".costs-total").text("£" + p))
                    } else l.attr("disabled", "disabled").addClass("slvzr-disabled"), k()
                }
                a.hasClass("visuallyhidden") ? t() : k()
            };
        Number.prototype.formatMoney = function(n, t, i) {
            var f = this,
                e = isNaN(e = Math.abs(n)) ? 2 : n,
                s = t === undefined ? "." : t,
                o = i === undefined ? "," : i,
                h = f < 0 ? "-" : "",
                r = parseInt(f = Math.abs(+f || 0).toFixed(e)) + "",
                u = (u = r.length) > 3 ? u % 3 : 0;
            return r = Math.round(r / 10) * 10 + "", h + (u ? r.substr(0, u) + o : "") + r.substr(u).replace(/(\d{3})(?=\d)/g, "$1" + o) + (e ? s + Math.abs(f - r).toFixed(e).slice(2) : "")
        };
        var vt = function() {
                h.off("change").on("change", function() {
                    n.catered = $(this).val();
                    $(this).val() === "Catered" ? ($("#costPremium").attr("disabled", "disabled").removeAttr("checked").addClass("disabled"), $("#costSpendBottom").attr("value", r[0]).next().text("£" + r[0]), $("#costSpendMiddle").attr("value", r[1]).next().text("£" + r[1]), $("#costSpendTop").attr("value", r[2]).next().text("£" + r[2])) : ($("#costPremium").removeAttr("disabled").removeClass("disabled"), $("#costSpendBottom").attr("value", i[0]).next().text("£" + i[0]), $("#costSpendMiddle").attr("value", i[1]).next().text("£" + i[1]), $("#costSpendTop").attr("value", i[2]).next().text("£" + i[2]));
                    f.trigger("change");
                    b.trigger("change");
                    u()
                })
            },
            yt = function() {
                f.off("change").on("change", function() {
                    n.level = "";
                    n.level = $(this).closest("fieldset").find("input:checked").val();
                    u()
                })
            },
            pt = function() {
                c.off("change").on("change", function() {
                    n.bathroom = $(this).val();
                    u()
                })
            },
            wt = function() {
                ot.off("change").on("change", function() {
                    var t = $(this).val();
                    t !== "" && t !== "0" ? (h.removeAttr("checked").attr("disabled", "true"), f.removeAttr("checked").attr("disabled", "true"), c.removeAttr("checked").attr("disabled", "true"), n.privateRental = $(this).val(), n.campus = "Off Campus", n.bathroom = "", n.catered = "", n.level = "", $("#costSpendBottom").attr("value", i[0]).next().text("£" + i[0]), $("#costSpendMiddle").attr("value", i[1]).next().text("£" + i[1]), $("#costSpendTop").attr("value", i[2]).next().text("£" + i[2]), b.trigger("change"), $("#costPrivateRentalWarning").show()) : (h.removeAttr("disabled"), f.removeAttr("disabled"), c.removeAttr("disabled"), n.privateRental = 0, n.campus = "Campus", $("#costPrivateRentalWarning").hide());
                    tt.validateAll(function() {
                        u()
                    })
                })
            },
            bt = function() {
                b.off("change").on("change", function() {
                    $(this).is(":checked") && (n.spend = $(this).val(), u())
                })
            },
            kt = function() {
                o.off("click").on("click", function() {
                    return o.hasClass("icon-dash") ? $(".pnl-tuition").slideUp(function() {
                        footerPush();
                        o.addClass("icon-add").removeClass("icon-dash")
                    }) : $(".pnl-tuition").slideDown(function() {
                        footerPush();
                        o.removeClass("icon-add").addClass("icon-dash")
                    }), !1
                })
            },
            v, dt = function() {
                v = new appSearch({
                    container: "#CoursesSearch",
                    minSelections: 1,
                    maxSelections: null,
                    alphabetIgnore: "bachelorType",
                    globalCallback: ri
                });
                v.init()
            },
            gt = function() {
                ft.off("change").on("change", function() {
                    t.status = $(this).val();
                    t.id = $(this).attr("data-id");
                    y()
                })
            },
            ni = function() {
                et.off("change").on("change", function() {
                    t.type = $(this).val();
                    y();
                    v.filterList($(this).attr("data-id"), "uncheck all")
                })
            },
            ti = function() {
                p.off("click").on("click", function(n) {
                    n.preventDefault();
                    $("#TuitionResults").show();
                    var t = $("#TuitionResults").position().top;
                    $("#TuitionResults").hide();
                    $("html,body").animate({
                        scrollTop: t - 200
                    }, 300, function() {
                        $("#TuitionResults").slideDown(footerPush)
                    })
                });
                ut.off("click").on("click", function() {
                    var n, i;
                    d();
                    v.resetSearch("all");
                    for (n in t) delete t[n];
                    i = document.getElementById("TuitionToggle").offsetTop;
                    $("html,body").animate({
                        scrollTop: i - 200
                    }, 300, function() {
                        $("#TuitionToggle").focus();
                        footerPush()
                    })
                })
            },
            ii = function() {
                return typeof t.course == "undefined" || t.course === null ? !1 : typeof t.type == "undefined" ? !1 : typeof t.status == "undefined" ? !1 : typeof t.id == "undefined" ? !1 : !0
            },
            it = function() {
                $(".tuition-course-name").html(t.course.name);
                $(".tuition-course-meta").html(t.course.meta);
                $(".tuition-course-url").attr("href", t.course.url)
            },
            y = function() {
                $("#TuitionResults").slideUp(footerPush);
                ii() ? ($(".tuition-course-type").html(t.type), $(".tuition-course-status").html(t.status + " student"), it(), t.type === "Postgraduate" ? ($(".tuition-course-fee-post-section").hide(), t.id !== nt ? t.course.cost !== "0" && t.course.cost ? ($(".tuition-course-fee-pre").html("&pound;" + t.course.cost), $(".tuition-course-fee-pre-section").show()) : $(".tuition-course-fee-pre-section").hide() : t.course.internationalcost !== "0" && t.course.internationalcost ? ($(".tuition-course-fee-pre").html("&pound;" + t.course.internationalcost), $(".tuition-course-fee-pre-section").show()) : $(".tuition-course-fee-pre-section").hide()) : t.id !== nt ? t.course.cost !== "0" && t.course.cost ? ($(".tuition-course-fee-pre").html("&pound;0"), $(".tuition-course-fee-post").html("&pound;" + t.course.cost), $(".tuition-course-fee-post-section").show()) : $(".tuition-course-fee-post-section").hide() : (t.course.internationalcost !== "0" && t.course.internationalcost ? ($(".tuition-course-fee-pre").html("&pound;" + t.course.internationalcost), $(".tuition-course-fee-pre-section").show()) : $(".tuition-course-fee-pre-section").hide(), $(".tuition-course-fee-post-section").hide()), p.removeAttr("disabled").removeClass("slvzr-disabled")) : ($("#TuitionResults").slideUp(footerPush), p.attr("disabled", "disabled").addClass("slvzr-disabled"))
            },
            rt = function(n) {
                t.course = n;
                y();
                it();
                $("#TuitionCourse").slideDown(footerPush)
            },
            d = function() {
                t.course = null;
                y();
                $("#TuitionCourse").slideUp(footerPush)
            },
            ri = function(n) {
                n.length ? rt(n[0]) : d()
            };
        return {
            init: ht,
            tuitionCallbackAdd: rt,
            tuitionCallbackRemove: d
        }
    },
    function(n) {
        function l(n) {
            return "[object Function]" === ht.call(n)
        }

        function a(n) {
            return "[object Array]" === ht.call(n)
        }

        function f(n, t) {
            if (n)
                for (var i = 0; i < n.length && (!n[i] || !t(n[i], i, n)); i += 1);
        }

        function tt(n, t) {
            if (n)
                for (var i = n.length - 1; - 1 < i && (!n[i] || !t(n[i], i, n)); i -= 1);
        }

        function r(n, t) {
            return pt.call(n, t)
        }

        function i(n, t) {
            return r(n, t) && n[t]
        }

        function h(n, t) {
            for (var i in n)
                if (r(n, i) && t(n[i], i)) break
        }

        function it(n, t, i, u) {
            return t && h(t, function(t, f) {
                (i || !r(n, f)) && (u && "object" == typeof t && t && !a(t) && !l(t) && !(t instanceof RegExp) ? (n[f] || (n[f] = {}), it(n[f], t, i, u)) : n[f] = t)
            }), n
        }

        function u(n, t) {
            return function() {
                return t.apply(n, arguments)
            }
        }

        function et(n) {
            throw n;
        }

        function ot(t) {
            if (!t) return t;
            var i = n;
            return f(t.split("."), function(n) {
                i = i[n]
            }), i
        }

        function c(n, t, i, r) {
            return t = Error(t + "\nhttp://requirejs.org/docs/errors.html#" + n), t.requireType = n, t.requireModules = r, i && (t.originalError = i), t
        }

        function lt(e) {
            function ut(n, t, r) {
                var e, u, f, o, s, a, h, y, t = t && t.split("/"),
                    c = v.map,
                    l = c && c["*"];
                if (n) {
                    for (n = n.split("/"), u = n.length - 1, v.nodeIdCompat && g.test(n[u]) && (n[u] = n[u].replace(g, "")), "." === n[0].charAt(0) && t && (u = t.slice(0, t.length - 1), n = u.concat(n)), u = n, f = 0; f < u.length; f++)(o = u[f], "." === o) ? (u.splice(f, 1), f -= 1) : ".." === o && !(0 === f || 1 == f && ".." === u[2] || ".." === u[f - 1]) && 0 < f && (u.splice(f - 1, 2), f -= 2);
                    n = n.join("/")
                }
                if (r && c && (t || l)) {
                    u = n.split("/");
                    f = u.length;
                    n: for (; 0 < f; f -= 1) {
                        if (s = u.slice(0, f).join("/"), t)
                            for (o = t.length; 0 < o; o -= 1)
                                if ((r = i(c, t.slice(0, o).join("/"))) && (r = i(r, s))) {
                                    e = r;
                                    a = f;
                                    break n
                                }!h && l && i(l, s) && (h = i(l, s), y = f)
                    }!e && h && (e = h, a = y);
                    e && (u.splice(0, a, e), n = u.join("/"))
                }
                return (e = i(v.pkgs, n)) ? e : n
            }

            function ei(n) {
                o && f(document.getElementsByTagName("script"), function(t) {
                    if (t.getAttribute("data-requiremodule") === n && t.getAttribute("data-requirecontext") === s.contextName) return t.parentNode.removeChild(t), !0
                })
            }

            function vt(n) {
                var t = i(v.paths, n);
                if (t && a(t) && 1 < t.length) return t.shift(), s.require.undef(n), s.makeRequire(null, {
                    skipMap: !0
                })([n]), !0
            }

            function oi(n) {
                var i, t = n ? n.indexOf("!") : -1;
                return -1 < t && (i = n.substring(0, t), n = n.substring(t + 1, n.length)), [i, n]
            }

            function k(n, t, r, u) {
                var c, o, f = null,
                    h = t ? t.name : null,
                    a = n,
                    l = !0,
                    e = "";
                return n || (l = !1, n = "_@r" + (li += 1)), n = oi(n), f = n[0], n = n[1], f && (f = ut(f, h, u), o = i(w, f)), n && (f ? e = o && o.normalize ? o.normalize(n, function(n) {
                    return ut(n, h, u)
                }) : -1 === n.indexOf("!") ? ut(n, h, u) : n : (e = ut(n, h, u), n = oi(e), f = n[0], e = n[1], r = !0, c = s.nameToUrl(e))), r = f && !o && !r ? "_unnormalized" + (ai += 1) : "", {
                    prefix: f,
                    name: e,
                    parentMap: t,
                    unnormalized: !!r,
                    url: c,
                    originalName: a,
                    isDefine: l,
                    id: (f ? f + "!" + e : e) + r
                }
            }

            function st(n) {
                var r = n.id,
                    t = i(y, r);
                return t || (t = y[r] = new s.Module(n)), t
            }

            function lt(n, t, u) {
                var e = n.id,
                    f = i(y, e);
                if (r(w, e) && (!f || f.defineEmitComplete)) "defined" === t && u(w[e]);
                else if (f = st(n), f.error && "error" === t) u(f.error);
                else f.on(t, u)
            }

            function d(n, r) {
                var e = n.requireModules,
                    u = !1;
                if (r) r(n);
                else if (f(e, function(t) {
                        (t = i(y, t)) && (t.error = n, t.events.error && (u = !0, t.emit("error", n)))
                    }), !u) t.onError(n)
            }

            function pt() {
                nt.length && (wt.apply(rt, [rt.length, 0].concat(nt)), nt = [])
            }

            function kt(n) {
                delete y[n];
                delete ri[n]
            }

            function si(n, t, r) {
                var u = n.map.id;
                n.error ? n.emit("error", n.error) : (t[u] = !0, f(n.depMaps, function(u, f) {
                    var e = u.id,
                        o = i(y, e);
                    !o || n.depMatched[f] || r[e] || (i(t, e) ? (n.defineDep(f, w[e]), n.check()) : si(o, t, r))
                }), r[u] = !0)
            }

            function dt() {
                var n, u, i = (n = 1e3 * v.waitSeconds) && s.startTime + n < (new Date).getTime(),
                    t = [],
                    e = [],
                    r = !1,
                    l = !0;
                if (!ni) {
                    if (ni = !0, h(ri, function(n) {
                            var f = n.map,
                                o = f.id;
                            if (n.enabled && (f.isDefine || e.push(n), !n.error))
                                if (!n.inited && i) vt(o) ? r = u = !0 : (t.push(o), ei(o));
                                else if (!n.inited && n.fetched && f.isDefine && (r = !0, !f.prefix)) return l = !1
                        }), i && t.length) return n = c("timeout", "Load timeout for modules: " + t, null, t), n.contextName = s.contextName, d(n);
                    l && f(e, function(n) {
                        si(n, {}, {})
                    });
                    (!i || u) && r && (o || ct) && !ii && (ii = setTimeout(function() {
                        ii = 0;
                        dt()
                    }, 50));
                    ni = !1
                }
            }

            function gt(n) {
                r(w, n[0]) || st(k(n[0], null, !0)).init(n[1], n[2])
            }

            function hi(n) {
                var n = n.currentTarget || n.srcElement,
                    t = s.onScriptLoad;
                return n.detachEvent && !ft ? n.detachEvent("onreadystatechange", t) : n.removeEventListener("load", t, !1), t = s.onScriptError, (!n.detachEvent || ft) && n.removeEventListener("error", t, !1), {
                    node: n,
                    id: n && n.getAttribute("data-requiremodule")
                }
            }

            function ci() {
                var n;
                for (pt(); rt.length;) {
                    if (n = rt.shift(), null === n[0]) return d(c("mismatch", "Mismatched anonymous define() module: " + n[n.length - 1]));
                    gt(n)
                }
            }
            var ni, ti, s, ht, ii, v = {
                    waitSeconds: 7,
                    baseUrl: "./",
                    paths: {},
                    bundles: {},
                    pkgs: {},
                    shim: {},
                    config: {}
                },
                y = {},
                ri = {},
                ui = {},
                rt = [],
                w = {},
                at = {},
                fi = {},
                li = 1,
                ai = 1;
            return ht = {
                require: function(n) {
                    return n.require ? n.require : n.require = s.makeRequire(n.map)
                },
                exports: function(n) {
                    return n.usingExports = !0, n.map.isDefine ? n.exports ? w[n.map.id] = n.exports : n.exports = w[n.map.id] = {} : void 0
                },
                module: function(n) {
                    return n.module ? n.module : n.module = {
                        id: n.map.id,
                        uri: n.map.url,
                        config: function() {
                            return i(v.config, n.map.id) || {}
                        },
                        exports: n.exports || (n.exports = {})
                    }
                }
            }, ti = function(n) {
                this.events = i(ui, n.id) || {};
                this.map = n;
                this.shim = i(v.shim, n.id);
                this.depExports = [];
                this.depMaps = [];
                this.depMatched = [];
                this.pluginMaps = {};
                this.depCount = 0
            }, ti.prototype = {
                init: function(n, t, i, r) {
                    if (r = r || {}, !this.inited) {
                        if (this.factory = t, i) this.on("error", i);
                        else this.events.error && (i = u(this, function(n) {
                            this.emit("error", n)
                        }));
                        this.depMaps = n && n.slice(0);
                        this.errback = i;
                        this.inited = !0;
                        this.ignore = r.ignore;
                        r.enabled || this.enabled ? this.enable() : this.check()
                    }
                },
                defineDep: function(n, t) {
                    this.depMatched[n] || (this.depMatched[n] = !0, this.depCount -= 1, this.depExports[n] = t)
                },
                fetch: function() {
                    if (!this.fetched) {
                        this.fetched = !0;
                        s.startTime = (new Date).getTime();
                        var n = this.map;
                        if (this.shim) s.makeRequire(this.map, {
                            enableBuildCallback: !0
                        })(this.shim.deps || [], u(this, function() {
                            return n.prefix ? this.callPlugin() : this.load()
                        }));
                        else return n.prefix ? this.callPlugin() : this.load()
                    }
                },
                load: function() {
                    var n = this.map.url;
                    at[n] || (at[n] = !0, s.load(this.map.id, n))
                },
                check: function() {
                    var i, r, u, n, f;
                    if (this.enabled && !this.enabling)
                        if (u = this.map.id, r = this.depExports, n = this.exports, f = this.factory, this.inited) {
                            if (this.error) this.emit("error", this.error);
                            else if (!this.defining) {
                                if (this.defining = !0, 1 > this.depCount && !this.defined) {
                                    if (l(f)) {
                                        if (this.events.error && this.map.isDefine || t.onError !== et) try {
                                            n = s.execCb(u, f, r, n)
                                        } catch (e) {
                                            i = e
                                        } else n = s.execCb(u, f, r, n);
                                        if (this.map.isDefine && void 0 === n && ((r = this.module) ? n = r.exports : this.usingExports && (n = this.exports)), i) return i.requireMap = this.map, i.requireModules = this.map.isDefine ? [this.map.id] : null, i.requireType = this.map.isDefine ? "define" : "require", d(this.error = i)
                                    } else n = f;
                                    if (this.exports = n, this.map.isDefine && !this.ignore && (w[u] = n, t.onResourceLoad)) t.onResourceLoad(s, this.map, this.depMaps);
                                    kt(u);
                                    this.defined = !0
                                }
                                this.defining = !1;
                                this.defined && !this.defineEmitted && (this.defineEmitted = !0, this.emit("defined", this.exports), this.defineEmitComplete = !0)
                            }
                        } else this.fetch()
                },
                callPlugin: function() {
                    var n = this.map,
                        f = n.id,
                        e = k(n.prefix);
                    this.depMaps.push(e);
                    lt(e, "defined", u(this, function(e) {
                        var o, l = i(fi, this.map.id);
                        var a = this.map.name,
                            w = this.map.parentMap ? this.map.parentMap.name : null,
                            p = s.makeRequire(n.parentMap, {
                                enableBuildCallback: !0
                            });
                        if (this.map.unnormalized) {
                            if (e.normalize && (a = e.normalize(a, function(n) {
                                    return ut(n, w, !0)
                                }) || ""), e = k(n.prefix + "!" + a, this.map.parentMap), lt(e, "defined", u(this, function(n) {
                                    this.init([], function() {
                                        return n
                                    }, null, {
                                        enabled: !0,
                                        ignore: !0
                                    })
                                })), l = i(y, e.id)) {
                                if (this.depMaps.push(e), this.events.error) l.on("error", u(this, function(n) {
                                    this.emit("error", n)
                                }));
                                l.enable()
                            }
                        } else l ? (this.map.url = s.nameToUrl(l), this.load()) : (o = u(this, function(n) {
                            this.init([], function() {
                                return n
                            }, null, {
                                enabled: !0
                            })
                        }), o.error = u(this, function(n) {
                            this.inited = !0;
                            this.error = n;
                            n.requireModules = [f];
                            h(y, function(n) {
                                0 === n.map.id.indexOf(f + "_unnormalized") && kt(n.map.id)
                            });
                            d(n)
                        }), o.fromText = u(this, function(i, u) {
                            var e = n.name,
                                h = k(e),
                                l = b;
                            u && (i = u);
                            l && (b = !1);
                            st(h);
                            r(v.config, f) && (v.config[e] = v.config[f]);
                            try {
                                t.exec(i)
                            } catch (a) {
                                return d(c("fromtexteval", "fromText eval for " + f + " failed: " + a, a, [f]))
                            }
                            l && (b = !0);
                            this.depMaps.push(h);
                            s.completeLoad(e);
                            p([e], o)
                        }), e.load(n.name, p, o, v))
                    }));
                    s.enable(e, this);
                    this.pluginMaps[e.id] = e
                },
                enable: function() {
                    ri[this.map.id] = this;
                    this.enabling = this.enabled = !0;
                    f(this.depMaps, u(this, function(n, t) {
                        var f, e;
                        if ("string" == typeof n) {
                            if (n = k(n, this.map.isDefine ? this.map : this.map.parentMap, !1, !this.skipMap), this.depMaps[t] = n, f = i(ht, n.id)) {
                                this.depExports[t] = f(this);
                                return
                            }
                            this.depCount += 1;
                            lt(n, "defined", u(this, function(n) {
                                this.defineDep(t, n);
                                this.check()
                            }));
                            this.errback && lt(n, "error", u(this, this.errback))
                        }
                        f = n.id;
                        e = y[f];
                        r(ht, f) || !e || e.enabled || s.enable(n, this)
                    }));
                    h(this.pluginMaps, u(this, function(n) {
                        var t = i(y, n.id);
                        t && !t.enabled && s.enable(n, this)
                    }));
                    this.enabling = !1;
                    this.check()
                },
                on: function(n, t) {
                    var i = this.events[n];
                    i || (i = this.events[n] = []);
                    i.push(t)
                },
                emit: function(n, t) {
                    f(this.events[n], function(n) {
                        n(t)
                    });
                    "error" === n && delete this.events[n]
                }
            }, s = {
                config: v,
                contextName: e,
                registry: y,
                defined: w,
                urlFetched: at,
                defQueue: rt,
                Module: ti,
                makeModuleMap: k,
                nextTick: t.nextTick,
                onError: d,
                configure: function(n) {
                    n.baseUrl && "/" !== n.baseUrl.charAt(n.baseUrl.length - 1) && (n.baseUrl += "/");
                    var t = v.shim,
                        i = {
                            paths: !0,
                            bundles: !0,
                            config: !0,
                            map: !0
                        };
                    h(n, function(n, t) {
                        i[t] ? (v[t] || (v[t] = {}), it(v[t], n, !0, !0)) : v[t] = n
                    });
                    n.bundles && h(n.bundles, function(n, t) {
                        f(n, function(n) {
                            n !== t && (fi[n] = t)
                        })
                    });
                    n.shim && (h(n.shim, function(n, i) {
                        a(n) && (n = {
                            deps: n
                        });
                        (n.exports || n.init) && !n.exportsFn && (n.exportsFn = s.makeShimExports(n));
                        t[i] = n
                    }), v.shim = t);
                    n.packages && f(n.packages, function(n) {
                        var t, n = "string" == typeof n ? {
                            name: n
                        } : n;
                        t = n.name;
                        n.location && (v.paths[t] = n.location);
                        v.pkgs[t] = n.name + "/" + (n.main || "main").replace(yt, "").replace(g, "")
                    });
                    h(y, function(n, t) {
                        n.inited || n.map.unnormalized || (n.map = k(t))
                    });
                    (n.deps || n.callback) && s.require(n.deps || [], n.callback)
                },
                makeShimExports: function(t) {
                    return function() {
                        var i;
                        return t.init && (i = t.init.apply(n, arguments)), i || t.exports && ot(t.exports)
                    }
                },
                makeRequire: function(n, u) {
                    function f(i, o, h) {
                        var a, v;
                        return (u.enableBuildCallback && o && l(o) && (o.__requireJsBuild = !0), "string" == typeof i) ? l(o) ? d(c("requireargs", "Invalid require call"), h) : n && r(ht, i) ? ht[i](y[n.id]) : t.get ? t.get(s, i, n, f) : (a = k(i, n, !1, !0), a = a.id, r(w, a) ? w[a] : d(c("notloaded", 'Module name "' + a + '" has not been loaded yet for context: ' + e + (n ? "" : ". Use require([])")))) : (ci(), s.nextTick(function() {
                            ci();
                            v = st(k(null, n));
                            v.skipMap = u.skipMap;
                            v.init(i, o, h, {
                                enabled: !0
                            });
                            dt()
                        }), f)
                    }
                    return u = u || {}, it(f, {
                        isBrowser: o,
                        toUrl: function(t) {
                            var r, i = t.lastIndexOf("."),
                                u = t.split("/")[0];
                            return -1 !== i && (!("." === u || ".." === u) || 1 < i) && (r = t.substring(i, t.length), t = t.substring(0, i)), s.nameToUrl(ut(t, n && n.id, !0), r, !0)
                        },
                        defined: function(t) {
                            return r(w, k(t, n, !1, !0).id)
                        },
                        specified: function(t) {
                            return t = k(t, n, !1, !0).id, r(w, t) || r(y, t)
                        }
                    }), n || (f.undef = function(t) {
                        pt();
                        var u = k(t, n, !0),
                            r = i(y, t);
                        ei(t);
                        delete w[t];
                        delete at[u.url];
                        delete ui[t];
                        tt(rt, function(n, i) {
                            n[0] === t && rt.splice(i, 1)
                        });
                        r && (r.events.defined && (ui[t] = r.events), kt(t))
                    }), f
                },
                enable: function(n) {
                    i(y, n.id) && st(n).enable()
                },
                completeLoad: function(n) {
                    var u, t, f = i(v.shim, n) || {},
                        e = f.exports;
                    for (pt(); rt.length;) {
                        if (t = rt.shift(), null === t[0]) {
                            if (t[0] = n, u) break;
                            u = !0
                        } else t[0] === n && (u = !0);
                        gt(t)
                    }
                    if (t = i(y, n), !u && !r(w, n) && t && !t.inited) {
                        if (v.enforceDefine && (!e || !ot(e))) return vt(n) ? void 0 : d(c("nodefine", "No define call for " + n, null, [n]));
                        gt([n, f.deps || [], f.exportsFn])
                    }
                    dt()
                },
                nameToUrl: function(n, r, u) {
                    var f, o, e;
                    if ((f = i(v.pkgs, n)) && (n = f), f = i(fi, n)) return s.nameToUrl(f, r, u);
                    if (t.jsExtRegExp.test(n)) f = n + (r || "");
                    else {
                        for (f = v.paths, n = n.split("/"), o = n.length; 0 < o; o -= 1)
                            if (e = n.slice(0, o).join("/"), e = i(f, e)) {
                                a(e) && (e = e[0]);
                                n.splice(0, o, e);
                                break
                            }
                        f = n.join("/");
                        f += r || (/^data\:|\?/.test(f) || u ? "" : ".js");
                        f = ("/" === f.charAt(0) || f.match(/^[\w\+\.\-]+:/) ? "" : v.baseUrl) + f
                    }
                    return v.urlArgs ? f + ((-1 === f.indexOf("?") ? "?" : "&") + v.urlArgs) : f
                },
                load: function(n, i) {
                    t.load(s, n, i)
                },
                execCb: function(n, t, i, r) {
                    return t.apply(r, i)
                },
                onScriptLoad: function(n) {
                    ("load" === n.type || bt.test((n.currentTarget || n.srcElement).readyState)) && (p = null, n = hi(n), s.completeLoad(n.id))
                },
                onScriptError: function(n) {
                    var t = hi(n);
                    if (!vt(t.id)) return d(c("scripterror", "Script error for: " + t.id, n, [t.id]))
                }
            }, s.require = s.makeRequire(), s
        }
        var t, v, y, k, rt, d, p, ut, e, st, at = /(\/\*([\s\S]*?)\*\/|([^:]|^)\/\/(.*)$)/mg,
            vt = /[^.]\s*require\s*\(\s*["']([^'"\s]+)["']\s*\)/g,
            g = /\.js$/,
            yt = /^\.\//;
        v = Object.prototype;
        var ht = v.toString,
            pt = v.hasOwnProperty,
            wt = Array.prototype.splice,
            o = !!("undefined" != typeof window && "undefined" != typeof navigator && window.document),
            ct = !o && "undefined" != typeof importScripts,
            bt = o && "PLAYSTATION 3" === navigator.platform ? /^complete$/ : /^(complete|loaded)$/,
            ft = "undefined" != typeof opera && "[object Opera]" === opera.toString(),
            w = {},
            s = {},
            nt = [],
            b = !1;
        if ("undefined" == typeof define) {
            if ("undefined" != typeof requirejs) {
                if (l(requirejs)) return;
                s = requirejs;
                requirejs = void 0
            }
            "undefined" == typeof require || l(require) || (s = require, require = void 0);
            t = requirejs = function(n, r, u, f) {
                var e, o = "_";
                return a(n) || "string" == typeof n || (e = n, a(r) ? (n = r, r = u, u = f) : n = []), e && e.context && (o = e.context), (f = i(w, o)) || (f = w[o] = t.s.newContext(o)), e && f.configure(e), f.require(n, r, u)
            };
            t.config = function(n) {
                return t(n)
            };
            t.nextTick = "undefined" != typeof setTimeout ? function(n) {
                setTimeout(n, 4)
            } : function(n) {
                n()
            };
            require || (require = t);
            t.version = "2.1.15";
            t.jsExtRegExp = /^\/|:|\?|\.js$/;
            t.isBrowser = o;
            v = t.s = {
                contexts: w,
                newContext: lt
            };
            t({});
            f(["toUrl", "undef", "defined", "specified"], function(n) {
                t[n] = function() {
                    var t = w._;
                    return t.require[n].apply(t, arguments)
                }
            });
            o && (y = v.head = document.getElementsByTagName("head")[0], k = document.getElementsByTagName("base")[0]) && (y = v.head = k.parentNode);
            t.onError = et;
            t.createNode = function(n) {
                var t = n.xhtml ? document.createElementNS("http://www.w3.org/1999/xhtml", "html:script") : document.createElement("script");
                return t.type = n.scriptType || "text/javascript", t.charset = "utf-8", t.async = !0, t
            };
            t.load = function(n, i, r) {
                var u = n && n.config || {};
                if (o) return u = t.createNode(u, i, r), u.setAttribute("data-requirecontext", n.contextName), u.setAttribute("data-requiremodule", i), u.attachEvent && !(u.attachEvent.toString && 0 > u.attachEvent.toString().indexOf("[native code")) && !ft ? (b = !0, u.attachEvent("onreadystatechange", n.onScriptLoad)) : (u.addEventListener("load", n.onScriptLoad, !1), u.addEventListener("error", n.onScriptError, !1)), u.src = r, ut = u, k ? y.insertBefore(u, k) : y.appendChild(u), ut = null, u;
                if (ct) try {
                    importScripts(r);
                    n.completeLoad(i)
                } catch (f) {
                    n.onError(c("importscripts", "importScripts failed for " + i + " at " + r, f, [i]))
                }
            };
            o && !s.skipDataMain && tt(document.getElementsByTagName("script"), function(n) {
                return y || (y = n.parentNode), (rt = n.getAttribute("data-main")) ? (e = rt, s.baseUrl || (d = e.split("/"), e = d.pop(), st = d.length ? d.join("/") + "/" : "./", s.baseUrl = st), e = e.replace(g, ""), t.jsExtRegExp.test(e) && (e = rt), s.deps = s.deps ? s.deps.concat(e) : [e], !0) : void 0
            });
            define = function(n, t, i) {
                var r, u;
                "string" != typeof n && (i = t, t = n, n = null);
                a(t) || (i = t, t = null);
                !t && l(i) && (t = [], i.length && (i.toString().replace(at, "").replace(vt, function(n, i) {
                    t.push(i)
                }), t = (1 === i.length ? ["require"] : ["require", "exports", "module"]).concat(t)));
                b && ((r = ut) || (p && "interactive" === p.readyState || tt(document.getElementsByTagName("script"), function(n) {
                    if ("interactive" === n.readyState) return p = n
                }), r = p), r && (n || (n = r.getAttribute("data-requiremodule")), u = w[r.getAttribute("data-requirecontext")]));
                (u ? u.defQueue : nt).push([n, t, i])
            };
            define.amd = {
                jQuery: !0
            };
            t.exec = function(b) {
                return eval(b)
            };
            t(s)
        }
    }(this);
require.config({
    baseUrl: "/wordpress/wp-content/themes/UoR_Research/js/",
    paths: {
        jquery: "./jquery-1.11.1.min",
        jqueryUI: "./jquery-ui.min",
        includeModule: "./common/includeModule",
        smartSearch: "./smartSearch",
        stepValidationHelper: "./stepValidationHelper",
        openSearch: "./openSearch",
        pageEditorHelper: "./pageEditorHelper"
    }
});
require(["requireStartup"])