var openSearch;
openSearch = openSearch || {};
openSearch.search = function() {
    this.downarrow = "notpressed";
    this.searchTextBox = $("#txtSearchTerm");
    this.searchButton = $("#btnSearch")
};
String.prototype.trim = function() {
    return this.replace(/^\s+|\s+$/g, "")
};
openSearch.search.prototype = function() {
        var t = function() {
                i.call(this)
            },
            i = function() {
                var t = this;
                this.searchButton.on("click", function() {
                    n.call(t)
                });
                this.searchTextBox.on("keypress", function() {
                    r.call(t, event)
                })
            },
            n = function() {
                var i = this,
                    n, t;
                return $("#searchResultsPage").val() == undefined ? !1 : (n = $("#txtSearchTerm").val(), !u.call(i, n)) ? !1 : (t = $("#searchResultsPage").val() + "?SearchCategory=" + $("input[name=searchCategory]:checked").val() + "&SearchTerm=" + $("#txtSearchTerm").val(), location.href = t, !0)
            },
            r = function(t) {
                var i = this,
                    r = t.which ? t.which : t.keyCode;
                r == "40" && (t.returnValue = !1, t.cancel = !0, downarrow = "pressed");
                r == "13" && (t.returnValue = !1, t.cancel = !0, i.downarrow == "notpressed" ? n.call(i) : i.downarrow = "notpressed")
            },
            u = function(n) {
                var t = this;
                return f.call(t, n) == "" ? !1 : !0
            },
            e = function(n) {
                n = n.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var i = "[\\?&]" + n + "=([^&#]*)",
                    r = new RegExp(i),
                    t = r.exec(window.location.href);
                return t == null ? "" : decodeURIComponent(t[1].replace(/\+/g, " "))
            },
            f = function(n) {
                return n = n.replace(/\r\n/g, ""), n.trim()
            };
        return {
            setup: t
        }
    }(),
    function() {
        var n = new openSearch.search;
        n.setup()
    }();
$(document).ready(function() {
    var n = $('DesktopSearchTerm').val(),
        t = $('DesktopSearchCat').val();
    n != null && n.trim() != "" ? $('input[name="searchCategory"][value="' + n + '"]').prop("checked", !0) : $('input[name="searchCategory"]:first').attr("checked", !0);
    t != null && t.trim() != "" && $("#txtSearchTerm").val(t)
});

$(document).ready(function() {
    $("#DesktopHeaderSearchForm").on("submit", function(n) {
        n.preventDefault();
        var i = $("#DesktopHeaderSearchForm").find("option:selected").attr("data-id"),
            r = $("#ProductType").val(),
            t = $("#DesktopSearchTerm").val(),
            u = $("#DesktopHeaderSearchForm").find("select").val(),
            f = u + "?SearchCategory=" + i + "&SearchTerm=" + t + "&ProductType=" + r;
        t.length > 0 && t.split(" ").join("").length > 0 && (location.href = f)
    });
    $("#MobileSearchForm").on("submit", function(n) {
        n.preventDefault();
        var i = $(this),
            r = i.find(":checked").attr("data-id"),
            u = $("#ProductType").val(),
            t = $("#MobileSearchTerm").val(),
            f = i.find(":checked").val(),
            e = f + "?SearchCategory=" + r + "&SearchTerm=" + t + "&ProductType=" + u;
        t.length > 0 && t.split(" ").join("").length > 0 && (location.href = e)
    })
});