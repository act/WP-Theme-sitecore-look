
/* subjectSidebar functions */

var subjectsSidebar = function (param) {

    var interactiveElements = ['a', 'input', 'select', 'textarea', 'button', 'iframe'];
    var triggerOpen = $(param.trigger);
    var triggerClose = $(param.close);
    var target = $(param.target) || "";
    var trigger = "";
    var globalItems = ".main-content, .sticky-container, footer, .cookie";


    var init = function () {

        if ($('#undergradulateList').length > 0 || $('#postgradulateList').length > 0) {



            var accUndergrad = new appAccordion({
                accordionContainer: $('#undergradulateList'),
                accordionPanels: '.hiddenContent',
                accordionTriggers: '.panel-trigger',
                singleOpen: true,
                accordionType: 'accordion',
                defaultOpen: -1
            });

            var accPostgrad = new appAccordion({
                accordionContainer: $('#postgraduateList'),
                accordionPanels: '.hiddenContent',
                accordionTriggers: '.panel-trigger',
                singleOpen: true,
                accordionType: 'accordion',
                defaultOpen: -1
            });


            var accThirdList = new appAccordion({
                accordionContainer: $('#thirdlist'),
                accordionPanels: '.hiddenContent',
                accordionTriggers: '.panel-trigger',
                singleOpen: true,
                accordionType: 'accordion',
                defaultOpen: -1
            });


            // i need this timeout, or the tab height is incorrect
            window.setTimeout(function () {

                var panelTabs = new appAccordion({
                    accordionContainer: $('#TabPanels'),
                    accordionPanels: '.fade-panel',
                    accordionTriggers: '.fade-trigger',
                    accordionType: 'tabs',
                    defaultOpen: 0
                });

            }, 1000);






        }

        bindings();

    };

    var bindings = function () {

        triggerOpen.off('click').on('click', function () {

            trigger = $(this);
            overlayOpen();

            return false;

        });


        // bind close overlay links

        triggerClose.off('click').on('click', function () {

            overlayClose();
            return false;

        });

    };

    var bindEsc = function () {

        // bind esc key
        $(document).off('click').on("keyup", function (e) {
            if (e.keyCode == 27) {
                overlayClose();
            }
        });

    };


    // Open overlay

    var overlayOpen = function () {

        $("html").addClass("m-locked");

        overlayDisableTabbing(target);
        target.appendTo("body").css("display", "none").removeClass("visuallyhidden").fadeIn();

        window.setTimeout(function () {

            target.addClass("open").attr("aria-hidden", false);
            bindEsc();

        }, 500);



        $(globalItems).attr("aria-hidden", true);

    };


    // Close overlay

    var overlayClose = function () {

        $("html").removeClass("m-locked");
        overlayResetTabbing();

        target.removeClass("open");

        window.setTimeout(function () {

            target.fadeOut(function () {

                target.attr("aria-hidden", true).addClass("visuallyhidden");


            });

        }, 500);

        //focus the link used to open
        if (trigger.length > 0) {
            trigger.focus();
        }

        $(globalItems).attr("aria-hidden", false);

    };


    // disable tabbing underneath overlay

    var overlayDisableTabbing = function (target) {

        // remove for all elements

        for (var i = 0; i < interactiveElements.length; i++) {
            $(interactiveElements[i]).attr('tabindex', -1);
        }

        // add for target overlay

        for (var a = 0; a < interactiveElements.length; a++) {
            target.find(interactiveElements[a]).removeAttr('tabindex');
        }

    };

    // disable tabbing underneath overlay

    var overlayResetTabbing = function (target) {

        ///remove all forced tabindex -1
        for (var i = 0; i < interactiveElements.length; i++) {
            $(interactiveElements[i]).removeAttr('tabindex');
        }
    };

    return {
        init: init
    };


};



//global variable so it can be accessed via appSearch
var subjects = new subjectsSidebar({
    trigger: ".show-subjects-sidebar",
    target: "#subjectsSidebar",
    close: " #subjectsSidebar .icon-close, #subjectsSidebar .mask"
});

subjects.init();


//var lock = new lockScroll(".overlay-scroll");

