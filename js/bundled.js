! function(n, t) {
    "object" == typeof module && "object" == typeof module.exports ? module.exports = n.document ? t(n, !0) : function(n) {
        if (!n.document) throw new Error("jQuery requires a window with a document");
        return t(n)
    } : t(n)
}("undefined" != typeof window ? window : this, function(n, t) {
    function ri(n) {
        var t = n.length,
            r = i.type(n);
        return "function" === r || i.isWindow(n) ? !1 : 1 === n.nodeType && t ? !0 : "array" === r || 0 === t || "number" == typeof t && t > 0 && t - 1 in n
    }

    function ui(n, t, r) {
        if (i.isFunction(t)) return i.grep(n, function(n, i) {
            return !!t.call(n, i, n) !== r
        });
        if (t.nodeType) return i.grep(n, function(n) {
            return n === t !== r
        });
        if ("string" == typeof t) {
            if (re.test(t)) return i.filter(t, n, r);
            t = i.filter(t, n)
        }
        return i.grep(n, function(n) {
            return i.inArray(n, t) >= 0 !== r
        })
    }

    function hr(n, t) {
        do n = n[t]; while (n && 1 !== n.nodeType);
        return n
    }

    function ee(n) {
        var t = fi[n] = {};
        return i.each(n.match(h) || [], function(n, i) {
            t[i] = !0
        }), t
    }

    function cr() {
        u.addEventListener ? (u.removeEventListener("DOMContentLoaded", a, !1), n.removeEventListener("load", a, !1)) : (u.detachEvent("onreadystatechange", a), n.detachEvent("onload", a))
    }

    function a() {
        (u.addEventListener || "load" === event.type || "complete" === u.readyState) && (cr(), i.ready())
    }

    function yr(n, t, r) {
        if (void 0 === r && 1 === n.nodeType) {
            var u = "data-" + t.replace(vr, "-$1").toLowerCase();
            if (r = n.getAttribute(u), "string" == typeof r) {
                try {
                    r = "true" === r ? !0 : "false" === r ? !1 : "null" === r ? null : +r + "" === r ? +r : ar.test(r) ? i.parseJSON(r) : r
                } catch (f) {}
                i.data(n, t, r)
            } else r = void 0
        }
        return r
    }

    function ei(n) {
        var t;
        for (t in n)
            if (("data" !== t || !i.isEmptyObject(n[t])) && "toJSON" !== t) return !1;
        return !0
    }

    function pr(n, t, r, u) {
        if (i.acceptData(n)) {
            var s, e, h = i.expando,
                l = n.nodeType,
                o = l ? i.cache : n,
                f = l ? n[h] : n[h] && h;
            if (f && o[f] && (u || o[f].data) || void 0 !== r || "string" != typeof t) return f || (f = l ? n[h] = c.pop() || i.guid++ : h), o[f] || (o[f] = l ? {} : {
                toJSON: i.noop
            }), ("object" == typeof t || "function" == typeof t) && (u ? o[f] = i.extend(o[f], t) : o[f].data = i.extend(o[f].data, t)), e = o[f], u || (e.data || (e.data = {}), e = e.data), void 0 !== r && (e[i.camelCase(t)] = r), "string" == typeof t ? (s = e[t], null == s && (s = e[i.camelCase(t)])) : s = e, s
        }
    }

    function wr(n, t, u) {
        if (i.acceptData(n)) {
            var o, s, h = n.nodeType,
                f = h ? i.cache : n,
                e = h ? n[i.expando] : i.expando;
            if (f[e]) {
                if (t && (o = u ? f[e] : f[e].data)) {
                    for (i.isArray(t) ? t = t.concat(i.map(t, i.camelCase)) : (t in o) ? t = [t] : (t = i.camelCase(t), t = (t in o) ? [t] : t.split(" ")), s = t.length; s--;) delete o[t[s]];
                    if (u ? !ei(o) : !i.isEmptyObject(o)) return
                }(u || (delete f[e].data, ei(f[e]))) && (h ? i.cleanData([n], !0) : r.deleteExpando || f != f.window ? delete f[e] : f[e] = null)
            }
        }
    }

    function vt() {
        return !0
    }

    function it() {
        return !1
    }

    function dr() {
        try {
            return u.activeElement
        } catch (n) {}
    }

    function gr(n) {
        var i = nu.split("|"),
            t = n.createDocumentFragment();
        if (t.createElement)
            while (i.length) t.createElement(i.pop());
        return t
    }

    function f(n, t) {
        var e, u, s = 0,
            r = typeof n.getElementsByTagName !== o ? n.getElementsByTagName(t || "*") : typeof n.querySelectorAll !== o ? n.querySelectorAll(t || "*") : void 0;
        if (!r)
            for (r = [], e = n.childNodes || n; null != (u = e[s]); s++) !t || i.nodeName(u, t) ? r.push(u) : i.merge(r, f(u, t));
        return void 0 === t || t && i.nodeName(n, t) ? i.merge([n], r) : r
    }

    function we(n) {
        oi.test(n.type) && (n.defaultChecked = n.checked)
    }

    function eu(n, t) {
        return i.nodeName(n, "table") && i.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? n.getElementsByTagName("tbody")[0] || n.appendChild(n.ownerDocument.createElement("tbody")) : n
    }

    function ou(n) {
        return n.type = (null !== i.find.attr(n, "type")) + "/" + n.type, n
    }

    function su(n) {
        var t = ve.exec(n.type);
        return t ? n.type = t[1] : n.removeAttribute("type"), n
    }

    function li(n, t) {
        for (var u, r = 0; null != (u = n[r]); r++) i._data(u, "globalEval", !t || i._data(t[r], "globalEval"))
    }

    function hu(n, t) {
        if (1 === t.nodeType && i.hasData(n)) {
            var u, f, o, s = i._data(n),
                r = i._data(t, s),
                e = s.events;
            if (e) {
                delete r.handle;
                r.events = {};
                for (u in e)
                    for (f = 0, o = e[u].length; o > f; f++) i.event.add(t, u, e[u][f])
            }
            r.data && (r.data = i.extend({}, r.data))
        }
    }

    function be(n, t) {
        var u, e, f;
        if (1 === t.nodeType) {
            if (u = t.nodeName.toLowerCase(), !r.noCloneEvent && t[i.expando]) {
                f = i._data(t);
                for (e in f.events) i.removeEvent(t, e, f.handle);
                t.removeAttribute(i.expando)
            }
            "script" === u && t.text !== n.text ? (ou(t).text = n.text, su(t)) : "object" === u ? (t.parentNode && (t.outerHTML = n.outerHTML), r.html5Clone && n.innerHTML && !i.trim(t.innerHTML) && (t.innerHTML = n.innerHTML)) : "input" === u && oi.test(n.type) ? (t.defaultChecked = t.checked = n.checked, t.value !== n.value && (t.value = n.value)) : "option" === u ? t.defaultSelected = t.selected = n.defaultSelected : ("input" === u || "textarea" === u) && (t.defaultValue = n.defaultValue)
        }
    }

    function cu(t, r) {
        var f, u = i(r.createElement(t)).appendTo(r.body),
            e = n.getDefaultComputedStyle && (f = n.getDefaultComputedStyle(u[0])) ? f.display : i.css(u[0], "display");
        return u.detach(), e
    }

    function yt(n) {
        var r = u,
            t = ai[n];
        return t || (t = cu(n, r), "none" !== t && t || (ot = (ot || i("<iframe frameborder='0' width='0' height='0'/>")).appendTo(r.documentElement), r = (ot[0].contentWindow || ot[0].contentDocument).document, r.write(), r.close(), t = cu(n, r), ot.detach()), ai[n] = t), t
    }

    function au(n, t) {
        return {
            get: function() {
                var i = n();
                if (null != i) return i ? void delete this.get : (this.get = t).apply(this, arguments)
            }
        }
    }

    function pu(n, t) {
        if (t in n) return t;
        for (var r = t.charAt(0).toUpperCase() + t.slice(1), u = t, i = yu.length; i--;)
            if (t = yu[i] + r, t in n) return t;
        return u
    }

    function wu(n, t) {
        for (var f, r, o, e = [], u = 0, s = n.length; s > u; u++) r = n[u], r.style && (e[u] = i._data(r, "olddisplay"), f = r.style.display, t ? (e[u] || "none" !== f || (r.style.display = ""), "" === r.style.display && et(r) && (e[u] = i._data(r, "olddisplay", yt(r.nodeName)))) : (o = et(r), (f && "none" !== f || !o) && i._data(r, "olddisplay", o ? f : i.css(r, "display"))));
        for (u = 0; s > u; u++) r = n[u], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? e[u] || "" : "none"));
        return n
    }

    function bu(n, t, i) {
        var r = no.exec(t);
        return r ? Math.max(0, r[1] - (i || 0)) + (r[2] || "px") : t
    }

    function ku(n, t, r, u, f) {
        for (var e = r === (u ? "border" : "content") ? 4 : "width" === t ? 1 : 0, o = 0; 4 > e; e += 2) "margin" === r && (o += i.css(n, r + w[e], !0, f)), u ? ("content" === r && (o -= i.css(n, "padding" + w[e], !0, f)), "margin" !== r && (o -= i.css(n, "border" + w[e] + "Width", !0, f))) : (o += i.css(n, "padding" + w[e], !0, f), "padding" !== r && (o += i.css(n, "border" + w[e] + "Width", !0, f)));
        return o
    }

    function du(n, t, u) {
        var o = !0,
            f = "width" === t ? n.offsetWidth : n.offsetHeight,
            e = k(n),
            s = r.boxSizing && "border-box" === i.css(n, "boxSizing", !1, e);
        if (0 >= f || null == f) {
            if (f = d(n, t, e), (0 > f || null == f) && (f = n.style[t]), pt.test(f)) return f;
            o = s && (r.boxSizingReliable() || f === n.style[t]);
            f = parseFloat(f) || 0
        }
        return f + ku(n, t, u || (s ? "border" : "content"), o, e) + "px"
    }

    function e(n, t, i, r, u) {
        return new e.prototype.init(n, t, i, r, u)
    }

    function nf() {
        return setTimeout(function() {
            rt = void 0
        }), rt = i.now()
    }

    function kt(n, t) {
        var r, i = {
                height: n
            },
            u = 0;
        for (t = t ? 1 : 0; 4 > u; u += 2 - t) r = w[u], i["margin" + r] = i["padding" + r] = n;
        return t && (i.opacity = i.width = n), i
    }

    function tf(n, t, i) {
        for (var u, f = (st[t] || []).concat(st["*"]), r = 0, e = f.length; e > r; r++)
            if (u = f[r].call(i, t, n)) return u
    }

    function fo(n, t, u) {
        var f, a, p, v, s, w, h, b, l = this,
            y = {},
            o = n.style,
            c = n.nodeType && et(n),
            e = i._data(n, "fxshow");
        u.queue || (s = i._queueHooks(n, "fx"), null == s.unqueued && (s.unqueued = 0, w = s.empty.fire, s.empty.fire = function() {
            s.unqueued || w()
        }), s.unqueued++, l.always(function() {
            l.always(function() {
                s.unqueued--;
                i.queue(n, "fx").length || s.empty.fire()
            })
        }));
        1 === n.nodeType && ("height" in t || "width" in t) && (u.overflow = [o.overflow, o.overflowX, o.overflowY], h = i.css(n, "display"), b = "none" === h ? i._data(n, "olddisplay") || yt(n.nodeName) : h, "inline" === b && "none" === i.css(n, "float") && (r.inlineBlockNeedsLayout && "inline" !== yt(n.nodeName) ? o.zoom = 1 : o.display = "inline-block"));
        u.overflow && (o.overflow = "hidden", r.shrinkWrapBlocks() || l.always(function() {
            o.overflow = u.overflow[0];
            o.overflowX = u.overflow[1];
            o.overflowY = u.overflow[2]
        }));
        for (f in t)
            if (a = t[f], ro.exec(a)) {
                if (delete t[f], p = p || "toggle" === a, a === (c ? "hide" : "show")) {
                    if ("show" !== a || !e || void 0 === e[f]) continue;
                    c = !0
                }
                y[f] = e && e[f] || i.style(n, f)
            } else h = void 0;
        if (i.isEmptyObject(y)) "inline" === ("none" === h ? yt(n.nodeName) : h) && (o.display = h);
        else {
            e ? "hidden" in e && (c = e.hidden) : e = i._data(n, "fxshow", {});
            p && (e.hidden = !c);
            c ? i(n).show() : l.done(function() {
                i(n).hide()
            });
            l.done(function() {
                var t;
                i._removeData(n, "fxshow");
                for (t in y) i.style(n, t, y[t])
            });
            for (f in y) v = tf(c ? e[f] : 0, f, l), f in e || (e[f] = v.start, c && (v.end = v.start, v.start = "width" === f || "height" === f ? 1 : 0))
        }
    }

    function eo(n, t) {
        var r, f, e, u, o;
        for (r in n)
            if (f = i.camelCase(r), e = t[f], u = n[r], i.isArray(u) && (e = u[1], u = n[r] = u[0]), r !== f && (n[f] = u, delete n[r]), o = i.cssHooks[f], o && "expand" in o) {
                u = o.expand(u);
                delete n[f];
                for (r in u) r in n || (n[r] = u[r], t[r] = e)
            } else t[f] = e
    }

    function rf(n, t, r) {
        var h, e, o = 0,
            l = bt.length,
            f = i.Deferred().always(function() {
                delete c.elem
            }),
            c = function() {
                if (e) return !1;
                for (var s = rt || nf(), t = Math.max(0, u.startTime + u.duration - s), h = t / u.duration || 0, i = 1 - h, r = 0, o = u.tweens.length; o > r; r++) u.tweens[r].run(i);
                return f.notifyWith(n, [u, i, t]), 1 > i && o ? t : (f.resolveWith(n, [u]), !1)
            },
            u = f.promise({
                elem: n,
                props: i.extend({}, t),
                opts: i.extend(!0, {
                    specialEasing: {}
                }, r),
                originalProperties: t,
                originalOptions: r,
                startTime: rt || nf(),
                duration: r.duration,
                tweens: [],
                createTween: function(t, r) {
                    var f = i.Tween(n, u.opts, t, r, u.opts.specialEasing[t] || u.opts.easing);
                    return u.tweens.push(f), f
                },
                stop: function(t) {
                    var i = 0,
                        r = t ? u.tweens.length : 0;
                    if (e) return this;
                    for (e = !0; r > i; i++) u.tweens[i].run(1);
                    return t ? f.resolveWith(n, [u, t]) : f.rejectWith(n, [u, t]), this
                }
            }),
            s = u.props;
        for (eo(s, u.opts.specialEasing); l > o; o++)
            if (h = bt[o].call(u, n, s, u.opts)) return h;
        return i.map(s, tf, u), i.isFunction(u.opts.start) && u.opts.start.call(n, u), i.fx.timer(i.extend(c, {
            elem: n,
            anim: u,
            queue: u.opts.queue
        })), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
    }

    function af(n) {
        return function(t, r) {
            "string" != typeof t && (r = t, t = "*");
            var u, f = 0,
                e = t.toLowerCase().match(h) || [];
            if (i.isFunction(r))
                while (u = e[f++]) "+" === u.charAt(0) ? (u = u.slice(1) || "*", (n[u] = n[u] || []).unshift(r)) : (n[u] = n[u] || []).push(r)
        }
    }

    function vf(n, t, r, u) {
        function e(s) {
            var h;
            return f[s] = !0, i.each(n[s] || [], function(n, i) {
                var s = i(t, r, u);
                return "string" != typeof s || o || f[s] ? o ? !(h = s) : void 0 : (t.dataTypes.unshift(s), e(s), !1)
            }), h
        }
        var f = {},
            o = n === bi;
        return e(t.dataTypes[0]) || !f["*"] && e("*")
    }

    function ki(n, t) {
        var u, r, f = i.ajaxSettings.flatOptions || {};
        for (r in t) void 0 !== t[r] && ((f[r] ? n : u || (u = {}))[r] = t[r]);
        return u && i.extend(!0, n, u), n
    }

    function ao(n, t, i) {
        for (var o, e, u, f, s = n.contents, r = n.dataTypes;
            "*" === r[0];) r.shift(), void 0 === e && (e = n.mimeType || t.getResponseHeader("Content-Type"));
        if (e)
            for (f in s)
                if (s[f] && s[f].test(e)) {
                    r.unshift(f);
                    break
                }
        if (r[0] in i) u = r[0];
        else {
            for (f in i) {
                if (!r[0] || n.converters[f + " " + r[0]]) {
                    u = f;
                    break
                }
                o || (o = f)
            }
            u = u || o
        }
        if (u) return (u !== r[0] && r.unshift(u), i[u])
    }

    function vo(n, t, i, r) {
        var h, u, f, s, e, o = {},
            c = n.dataTypes.slice();
        if (c[1])
            for (f in n.converters) o[f.toLowerCase()] = n.converters[f];
        for (u = c.shift(); u;)
            if (n.responseFields[u] && (i[n.responseFields[u]] = t), !e && r && n.dataFilter && (t = n.dataFilter(t, n.dataType)), e = u, u = c.shift())
                if ("*" === u) u = e;
                else if ("*" !== e && e !== u) {
            if (f = o[e + " " + u] || o["* " + u], !f)
                for (h in o)
                    if (s = h.split(" "), s[1] === u && (f = o[e + " " + s[0]] || o["* " + s[0]])) {
                        f === !0 ? f = o[h] : o[h] !== !0 && (u = s[0], c.unshift(s[1]));
                        break
                    }
            if (f !== !0)
                if (f && n.throws) t = f(t);
                else try {
                    t = f(t)
                } catch (l) {
                    return {
                        state: "parsererror",
                        error: f ? l : "No conversion from " + e + " to " + u
                    }
                }
        }
        return {
            state: "success",
            data: t
        }
    }

    function di(n, t, r, u) {
        var f;
        if (i.isArray(t)) i.each(t, function(t, i) {
            r || po.test(n) ? u(n, i) : di(n + "[" + ("object" == typeof i ? t : "") + "]", i, r, u)
        });
        else if (r || "object" !== i.type(t)) u(n, t);
        else
            for (f in t) di(n + "[" + f + "]", t[f], r, u)
    }

    function pf() {
        try {
            return new n.XMLHttpRequest
        } catch (t) {}
    }

    function go() {
        try {
            return new n.ActiveXObject("Microsoft.XMLHTTP")
        } catch (t) {}
    }

    function wf(n) {
        return i.isWindow(n) ? n : 9 === n.nodeType ? n.defaultView || n.parentWindow : !1
    }
    var c = [],
        l = c.slice,
        ir = c.concat,
        ii = c.push,
        rr = c.indexOf,
        ct = {},
        df = ct.toString,
        tt = ct.hasOwnProperty,
        r = {},
        ur = "1.11.1",
        i = function(n, t) {
            return new i.fn.init(n, t)
        },
        gf = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,
        ne = /^-ms-/,
        te = /-([\da-z])/gi,
        ie = function(n, t) {
            return t.toUpperCase()
        },
        p, or, sr, h, fi, lt, o, lr, ar, vr, ot, ai, uf, ef, of , gt, gi, ti, nr, tr, bf, kf;
    i.fn = i.prototype = {
        jquery: ur,
        constructor: i,
        selector: "",
        length: 0,
        toArray: function() {
            return l.call(this)
        },
        get: function(n) {
            return null != n ? 0 > n ? this[n + this.length] : this[n] : l.call(this)
        },
        pushStack: function(n) {
            var t = i.merge(this.constructor(), n);
            return t.prevObject = this, t.context = this.context, t
        },
        each: function(n, t) {
            return i.each(this, n, t)
        },
        map: function(n) {
            return this.pushStack(i.map(this, function(t, i) {
                return n.call(t, i, t)
            }))
        },
        slice: function() {
            return this.pushStack(l.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(n) {
            var i = this.length,
                t = +n + (0 > n ? i : 0);
            return this.pushStack(t >= 0 && i > t ? [this[t]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor(null)
        },
        push: ii,
        sort: c.sort,
        splice: c.splice
    };
    i.extend = i.fn.extend = function() {
        var r, e, t, f, o, s, n = arguments[0] || {},
            u = 1,
            c = arguments.length,
            h = !1;
        for ("boolean" == typeof n && (h = n, n = arguments[u] || {}, u++), "object" == typeof n || i.isFunction(n) || (n = {}), u === c && (n = this, u--); c > u; u++)
            if (null != (o = arguments[u]))
                for (f in o) r = n[f], t = o[f], n !== t && (h && t && (i.isPlainObject(t) || (e = i.isArray(t))) ? (e ? (e = !1, s = r && i.isArray(r) ? r : []) : s = r && i.isPlainObject(r) ? r : {}, n[f] = i.extend(h, s, t)) : void 0 !== t && (n[f] = t));
        return n
    };
    i.extend({
        expando: "jQuery" + (ur + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(n) {
            throw new Error(n);
        },
        noop: function() {},
        isFunction: function(n) {
            return "function" === i.type(n)
        },
        isArray: Array.isArray || function(n) {
            return "array" === i.type(n)
        },
        isWindow: function(n) {
            return null != n && n == n.window
        },
        isNumeric: function(n) {
            return !i.isArray(n) && n - parseFloat(n) >= 0
        },
        isEmptyObject: function(n) {
            var t;
            for (t in n) return !1;
            return !0
        },
        isPlainObject: function(n) {
            var t;
            if (!n || "object" !== i.type(n) || n.nodeType || i.isWindow(n)) return !1;
            try {
                if (n.constructor && !tt.call(n, "constructor") && !tt.call(n.constructor.prototype, "isPrototypeOf")) return !1
            } catch (u) {
                return !1
            }
            if (r.ownLast)
                for (t in n) return tt.call(n, t);
            for (t in n);
            return void 0 === t || tt.call(n, t)
        },
        type: function(n) {
            return null == n ? n + "" : "object" == typeof n || "function" == typeof n ? ct[df.call(n)] || "object" : typeof n
        },
        globalEval: function(t) {
            t && i.trim(t) && (n.execScript || function(t) {
                n.eval.call(n, t)
            })(t)
        },
        camelCase: function(n) {
            return n.replace(ne, "ms-").replace(te, ie)
        },
        nodeName: function(n, t) {
            return n.nodeName && n.nodeName.toLowerCase() === t.toLowerCase()
        },
        each: function(n, t, i) {
            var u, r = 0,
                f = n.length,
                e = ri(n);
            if (i) {
                if (e) {
                    for (; f > r; r++)
                        if (u = t.apply(n[r], i), u === !1) break
                } else
                    for (r in n)
                        if (u = t.apply(n[r], i), u === !1) break
            } else if (e) {
                for (; f > r; r++)
                    if (u = t.call(n[r], r, n[r]), u === !1) break
            } else
                for (r in n)
                    if (u = t.call(n[r], r, n[r]), u === !1) break;
            return n
        },
        trim: function(n) {
            return null == n ? "" : (n + "").replace(gf, "")
        },
        makeArray: function(n, t) {
            var r = t || [];
            return null != n && (ri(Object(n)) ? i.merge(r, "string" == typeof n ? [n] : n) : ii.call(r, n)), r
        },
        inArray: function(n, t, i) {
            var r;
            if (t) {
                if (rr) return rr.call(t, n, i);
                for (r = t.length, i = i ? 0 > i ? Math.max(0, r + i) : i : 0; r > i; i++)
                    if (i in t && t[i] === n) return i
            }
            return -1
        },
        merge: function(n, t) {
            for (var r = +t.length, i = 0, u = n.length; r > i;) n[u++] = t[i++];
            if (r !== r)
                while (void 0 !== t[i]) n[u++] = t[i++];
            return n.length = u, n
        },
        grep: function(n, t, i) {
            for (var u, f = [], r = 0, e = n.length, o = !i; e > r; r++) u = !t(n[r], r), u !== o && f.push(n[r]);
            return f
        },
        map: function(n, t, i) {
            var u, r = 0,
                e = n.length,
                o = ri(n),
                f = [];
            if (o)
                for (; e > r; r++) u = t(n[r], r, i), null != u && f.push(u);
            else
                for (r in n) u = t(n[r], r, i), null != u && f.push(u);
            return ir.apply([], f)
        },
        guid: 1,
        proxy: function(n, t) {
            var u, r, f;
            return "string" == typeof t && (f = n[t], t = n, n = f), i.isFunction(n) ? (u = l.call(arguments, 2), r = function() {
                return n.apply(t || this, u.concat(l.call(arguments)))
            }, r.guid = n.guid = n.guid || i.guid++, r) : void 0
        },
        now: function() {
            return +new Date
        },
        support: r
    });
    i.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(n, t) {
        ct["[object " + t + "]"] = t.toLowerCase()
    });
    p = function(n) {
        function r(n, t, i, r) {
            var w, h, c, v, k, y, d, l, nt, g;
            if ((t ? t.ownerDocument || t : s) !== e && p(t), t = t || e, i = i || [], !n || "string" != typeof n) return i;
            if (1 !== (v = t.nodeType) && 9 !== v) return [];
            if (a && !r) {
                if (w = sr.exec(n))
                    if (c = w[1]) {
                        if (9 === v) {
                            if (h = t.getElementById(c), !h || !h.parentNode) return i;
                            if (h.id === c) return i.push(h), i
                        } else if (t.ownerDocument && (h = t.ownerDocument.getElementById(c)) && ot(t, h) && h.id === c) return i.push(h), i
                    } else {
                        if (w[2]) return b.apply(i, t.getElementsByTagName(n)), i;
                        if ((c = w[3]) && u.getElementsByClassName && t.getElementsByClassName) return b.apply(i, t.getElementsByClassName(c)), i
                    }
                if (u.qsa && (!o || !o.test(n))) {
                    if (l = d = f, nt = t, g = 9 === v && n, 1 === v && "object" !== t.nodeName.toLowerCase()) {
                        for (y = et(n), (d = t.getAttribute("id")) ? l = d.replace(hr, "\\$&") : t.setAttribute("id", l), l = "[id='" + l + "'] ", k = y.length; k--;) y[k] = l + yt(y[k]);
                        nt = gt.test(n) && ii(t.parentNode) || t;
                        g = y.join(",")
                    }
                    if (g) try {
                        return b.apply(i, nt.querySelectorAll(g)), i
                    } catch (tt) {} finally {
                        d || t.removeAttribute("id")
                    }
                }
            }
            return si(n.replace(at, "$1"), t, i, r)
        }

        function ni() {
            function n(r, u) {
                return i.push(r + " ") > t.cacheLength && delete n[i.shift()], n[r + " "] = u
            }
            var i = [];
            return n
        }

        function h(n) {
            return n[f] = !0, n
        }

        function c(n) {
            var t = e.createElement("div");
            try {
                return !!n(t)
            } catch (i) {
                return !1
            } finally {
                t.parentNode && t.parentNode.removeChild(t);
                t = null
            }
        }

        function ti(n, i) {
            for (var u = n.split("|"), r = n.length; r--;) t.attrHandle[u[r]] = i
        }

        function wi(n, t) {
            var i = t && n,
                r = i && 1 === n.nodeType && 1 === t.nodeType && (~t.sourceIndex || ai) - (~n.sourceIndex || ai);
            if (r) return r;
            if (i)
                while (i = i.nextSibling)
                    if (i === t) return -1;
            return n ? 1 : -1
        }

        function cr(n) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return "input" === i && t.type === n
            }
        }

        function lr(n) {
            return function(t) {
                var i = t.nodeName.toLowerCase();
                return ("input" === i || "button" === i) && t.type === n
            }
        }

        function tt(n) {
            return h(function(t) {
                return t = +t, h(function(i, r) {
                    for (var u, f = n([], i.length, t), e = f.length; e--;) i[u = f[e]] && (i[u] = !(r[u] = i[u]))
                })
            })
        }

        function ii(n) {
            return n && typeof n.getElementsByTagName !== ut && n
        }

        function bi() {}

        function yt(n) {
            for (var t = 0, r = n.length, i = ""; r > t; t++) i += n[t].value;
            return i
        }

        function ri(n, t, i) {
            var r = t.dir,
                u = i && "parentNode" === r,
                e = ki++;
            return t.first ? function(t, i, f) {
                while (t = t[r])
                    if (1 === t.nodeType || u) return n(t, i, f)
            } : function(t, i, o) {
                var s, h, c = [v, e];
                if (o) {
                    while (t = t[r])
                        if ((1 === t.nodeType || u) && n(t, i, o)) return !0
                } else
                    while (t = t[r])
                        if (1 === t.nodeType || u) {
                            if (h = t[f] || (t[f] = {}), (s = h[r]) && s[0] === v && s[1] === e) return c[2] = s[2];
                            if (h[r] = c, c[2] = n(t, i, o)) return !0
                        }
            }
        }

        function ui(n) {
            return n.length > 1 ? function(t, i, r) {
                for (var u = n.length; u--;)
                    if (!n[u](t, i, r)) return !1;
                return !0
            } : n[0]
        }

        function ar(n, t, i) {
            for (var u = 0, f = t.length; f > u; u++) r(n, t[u], i);
            return i
        }

        function pt(n, t, i, r, u) {
            for (var e, o = [], f = 0, s = n.length, h = null != t; s > f; f++)(e = n[f]) && (!i || i(e, r, u)) && (o.push(e), h && t.push(f));
            return o
        }

        function fi(n, t, i, r, u, e) {
            return r && !r[f] && (r = fi(r)), u && !u[f] && (u = fi(u, e)), h(function(f, e, o, s) {
                var l, c, a, p = [],
                    y = [],
                    w = e.length,
                    k = f || ar(t || "*", o.nodeType ? [o] : o, []),
                    v = !n || !f && t ? k : pt(k, p, n, o, s),
                    h = i ? u || (f ? n : w || r) ? [] : e : v;
                if (i && i(v, h, o, s), r)
                    for (l = pt(h, y), r(l, [], o, s), c = l.length; c--;)(a = l[c]) && (h[y[c]] = !(v[y[c]] = a));
                if (f) {
                    if (u || n) {
                        if (u) {
                            for (l = [], c = h.length; c--;)(a = h[c]) && l.push(v[c] = a);
                            u(null, h = [], l, s)
                        }
                        for (c = h.length; c--;)(a = h[c]) && (l = u ? nt.call(f, a) : p[c]) > -1 && (f[l] = !(e[l] = a))
                    }
                } else h = pt(h === e ? h.splice(w, h.length) : h), u ? u(null, e, h, s) : b.apply(e, h)
            })
        }

        function ei(n) {
            for (var s, u, r, o = n.length, h = t.relative[n[0].type], c = h || t.relative[" "], i = h ? 1 : 0, l = ri(function(n) {
                    return n === s
                }, c, !0), a = ri(function(n) {
                    return nt.call(s, n) > -1
                }, c, !0), e = [function(n, t, i) {
                    return !h && (i || t !== ct) || ((s = t).nodeType ? l(n, t, i) : a(n, t, i))
                }]; o > i; i++)
                if (u = t.relative[n[i].type]) e = [ri(ui(e), u)];
                else {
                    if (u = t.filter[n[i].type].apply(null, n[i].matches), u[f]) {
                        for (r = ++i; o > r; r++)
                            if (t.relative[n[r].type]) break;
                        return fi(i > 1 && ui(e), i > 1 && yt(n.slice(0, i - 1).concat({
                            value: " " === n[i - 2].type ? "*" : ""
                        })).replace(at, "$1"), u, r > i && ei(n.slice(i, r)), o > r && ei(n = n.slice(r)), o > r && yt(n))
                    }
                    e.push(u)
                }
            return ui(e)
        }

        function vr(n, i) {
            var u = i.length > 0,
                f = n.length > 0,
                o = function(o, s, h, c, l) {
                    var y, d, w, k = 0,
                        a = "0",
                        g = o && [],
                        p = [],
                        nt = ct,
                        tt = o || f && t.find.TAG("*", l),
                        it = v += null == nt ? 1 : Math.random() || .1,
                        rt = tt.length;
                    for (l && (ct = s !== e && s); a !== rt && null != (y = tt[a]); a++) {
                        if (f && y) {
                            for (d = 0; w = n[d++];)
                                if (w(y, s, h)) {
                                    c.push(y);
                                    break
                                }
                            l && (v = it)
                        }
                        u && ((y = !w && y) && k--, o && g.push(y))
                    }
                    if (k += a, u && a !== k) {
                        for (d = 0; w = i[d++];) w(g, p, s, h);
                        if (o) {
                            if (k > 0)
                                while (a--) g[a] || p[a] || (p[a] = gi.call(c));
                            p = pt(p)
                        }
                        b.apply(c, p);
                        l && !o && p.length > 0 && k + i.length > 1 && r.uniqueSort(c)
                    }
                    return l && (v = it, ct = nt), g
                };
            return u ? h(o) : o
        }
        var it, u, t, ht, oi, et, wt, si, ct, y, rt, p, e, l, a, o, g, lt, ot, f = "sizzle" + -new Date,
            s = n.document,
            v = 0,
            ki = 0,
            hi = ni(),
            ci = ni(),
            li = ni(),
            bt = function(n, t) {
                return n === t && (rt = !0), 0
            },
            ut = "undefined",
            ai = -2147483648,
            di = {}.hasOwnProperty,
            w = [],
            gi = w.pop,
            nr = w.push,
            b = w.push,
            vi = w.slice,
            nt = w.indexOf || function(n) {
                for (var t = 0, i = this.length; i > t; t++)
                    if (this[t] === n) return t;
                return -1
            },
            kt = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
            i = "[\\x20\\t\\r\\n\\f]",
            ft = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
            yi = ft.replace("w", "w#"),
            pi = "\\[" + i + "*(" + ft + ")(?:" + i + "*([*^$|!~]?=)" + i + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + yi + "))|)" + i + "*\\]",
            dt = ":(" + ft + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + pi + ")*)|.*)\\)|)",
            at = new RegExp("^" + i + "+|((?:^|[^\\\\])(?:\\\\.)*)" + i + "+$", "g"),
            tr = new RegExp("^" + i + "*," + i + "*"),
            ir = new RegExp("^" + i + "*([>+~]|" + i + ")" + i + "*"),
            rr = new RegExp("=" + i + "*([^\\]'\"]*?)" + i + "*\\]", "g"),
            ur = new RegExp(dt),
            fr = new RegExp("^" + yi + "$"),
            vt = {
                ID: new RegExp("^#(" + ft + ")"),
                CLASS: new RegExp("^\\.(" + ft + ")"),
                TAG: new RegExp("^(" + ft.replace("w", "w*") + ")"),
                ATTR: new RegExp("^" + pi),
                PSEUDO: new RegExp("^" + dt),
                CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + i + "*(even|odd|(([+-]|)(\\d*)n|)" + i + "*(?:([+-]|)" + i + "*(\\d+)|))" + i + "*\\)|)", "i"),
                bool: new RegExp("^(?:" + kt + ")$", "i"),
                needsContext: new RegExp("^" + i + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + i + "*((?:-\\d)?\\d*)" + i + "*\\)|)(?=[^-]|$)", "i")
            },
            er = /^(?:input|select|textarea|button)$/i,
            or = /^h\d$/i,
            st = /^[^{]+\{\s*\[native \w/,
            sr = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
            gt = /[+~]/,
            hr = /'|\\/g,
            k = new RegExp("\\\\([\\da-f]{1,6}" + i + "?|(" + i + ")|.)", "ig"),
            d = function(n, t, i) {
                var r = "0x" + t - 65536;
                return r !== r || i ? t : 0 > r ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
            };
        try {
            b.apply(w = vi.call(s.childNodes), s.childNodes);
            w[s.childNodes.length].nodeType
        } catch (yr) {
            b = {
                apply: w.length ? function(n, t) {
                    nr.apply(n, vi.call(t))
                } : function(n, t) {
                    for (var i = n.length, r = 0; n[i++] = t[r++];);
                    n.length = i - 1
                }
            }
        }
        u = r.support = {};
        oi = r.isXML = function(n) {
            var t = n && (n.ownerDocument || n).documentElement;
            return t ? "HTML" !== t.nodeName : !1
        };
        p = r.setDocument = function(n) {
            var v, r = n ? n.ownerDocument || n : s,
                h = r.defaultView;
            return r !== e && 9 === r.nodeType && r.documentElement ? (e = r, l = r.documentElement, a = !oi(r), h && h !== h.top && (h.addEventListener ? h.addEventListener("unload", function() {
                p()
            }, !1) : h.attachEvent && h.attachEvent("onunload", function() {
                p()
            })), u.attributes = c(function(n) {
                return n.className = "i", !n.getAttribute("className")
            }), u.getElementsByTagName = c(function(n) {
                return n.appendChild(r.createComment("")), !n.getElementsByTagName("*").length
            }), u.getElementsByClassName = st.test(r.getElementsByClassName) && c(function(n) {
                return n.innerHTML = "<div class='a'><\/div><div class='a i'><\/div>", n.firstChild.className = "i", 2 === n.getElementsByClassName("i").length
            }), u.getById = c(function(n) {
                return l.appendChild(n).id = f, !r.getElementsByName || !r.getElementsByName(f).length
            }), u.getById ? (t.find.ID = function(n, t) {
                if (typeof t.getElementById !== ut && a) {
                    var i = t.getElementById(n);
                    return i && i.parentNode ? [i] : []
                }
            }, t.filter.ID = function(n) {
                var t = n.replace(k, d);
                return function(n) {
                    return n.getAttribute("id") === t
                }
            }) : (delete t.find.ID, t.filter.ID = function(n) {
                var t = n.replace(k, d);
                return function(n) {
                    var i = typeof n.getAttributeNode !== ut && n.getAttributeNode("id");
                    return i && i.value === t
                }
            }), t.find.TAG = u.getElementsByTagName ? function(n, t) {
                if (typeof t.getElementsByTagName !== ut) return t.getElementsByTagName(n)
            } : function(n, t) {
                var i, r = [],
                    f = 0,
                    u = t.getElementsByTagName(n);
                if ("*" === n) {
                    while (i = u[f++]) 1 === i.nodeType && r.push(i);
                    return r
                }
                return u
            }, t.find.CLASS = u.getElementsByClassName && function(n, t) {
                if (typeof t.getElementsByClassName !== ut && a) return t.getElementsByClassName(n)
            }, g = [], o = [], (u.qsa = st.test(r.querySelectorAll)) && (c(function(n) {
                n.innerHTML = "<select msallowclip=''><option selected=''><\/option><\/select>";
                n.querySelectorAll("[msallowclip^='']").length && o.push("[*^$]=" + i + "*(?:''|\"\")");
                n.querySelectorAll("[selected]").length || o.push("\\[" + i + "*(?:value|" + kt + ")");
                n.querySelectorAll(":checked").length || o.push(":checked")
            }), c(function(n) {
                var t = r.createElement("input");
                t.setAttribute("type", "hidden");
                n.appendChild(t).setAttribute("name", "D");
                n.querySelectorAll("[name=d]").length && o.push("name" + i + "*[*^$|!~]?=");
                n.querySelectorAll(":enabled").length || o.push(":enabled", ":disabled");
                n.querySelectorAll("*,:x");
                o.push(",.*:")
            })), (u.matchesSelector = st.test(lt = l.matches || l.webkitMatchesSelector || l.mozMatchesSelector || l.oMatchesSelector || l.msMatchesSelector)) && c(function(n) {
                u.disconnectedMatch = lt.call(n, "div");
                lt.call(n, "[s!='']:x");
                g.push("!=", dt)
            }), o = o.length && new RegExp(o.join("|")), g = g.length && new RegExp(g.join("|")), v = st.test(l.compareDocumentPosition), ot = v || st.test(l.contains) ? function(n, t) {
                var r = 9 === n.nodeType ? n.documentElement : n,
                    i = t && t.parentNode;
                return n === i || !(!i || 1 !== i.nodeType || !(r.contains ? r.contains(i) : n.compareDocumentPosition && 16 & n.compareDocumentPosition(i)))
            } : function(n, t) {
                if (t)
                    while (t = t.parentNode)
                        if (t === n) return !0;
                return !1
            }, bt = v ? function(n, t) {
                if (n === t) return rt = !0, 0;
                var i = !n.compareDocumentPosition - !t.compareDocumentPosition;
                return i ? i : (i = (n.ownerDocument || n) === (t.ownerDocument || t) ? n.compareDocumentPosition(t) : 1, 1 & i || !u.sortDetached && t.compareDocumentPosition(n) === i ? n === r || n.ownerDocument === s && ot(s, n) ? -1 : t === r || t.ownerDocument === s && ot(s, t) ? 1 : y ? nt.call(y, n) - nt.call(y, t) : 0 : 4 & i ? -1 : 1)
            } : function(n, t) {
                if (n === t) return rt = !0, 0;
                var i, u = 0,
                    o = n.parentNode,
                    h = t.parentNode,
                    f = [n],
                    e = [t];
                if (!o || !h) return n === r ? -1 : t === r ? 1 : o ? -1 : h ? 1 : y ? nt.call(y, n) - nt.call(y, t) : 0;
                if (o === h) return wi(n, t);
                for (i = n; i = i.parentNode;) f.unshift(i);
                for (i = t; i = i.parentNode;) e.unshift(i);
                while (f[u] === e[u]) u++;
                return u ? wi(f[u], e[u]) : f[u] === s ? -1 : e[u] === s ? 1 : 0
            }, r) : e
        };
        r.matches = function(n, t) {
            return r(n, null, null, t)
        };
        r.matchesSelector = function(n, t) {
            if ((n.ownerDocument || n) !== e && p(n), t = t.replace(rr, "='$1']"), !(!u.matchesSelector || !a || g && g.test(t) || o && o.test(t))) try {
                var i = lt.call(n, t);
                if (i || u.disconnectedMatch || n.document && 11 !== n.document.nodeType) return i
            } catch (f) {}
            return r(t, e, null, [n]).length > 0
        };
        r.contains = function(n, t) {
            return (n.ownerDocument || n) !== e && p(n), ot(n, t)
        };
        r.attr = function(n, i) {
            (n.ownerDocument || n) !== e && p(n);
            var f = t.attrHandle[i.toLowerCase()],
                r = f && di.call(t.attrHandle, i.toLowerCase()) ? f(n, i, !a) : void 0;
            return void 0 !== r ? r : u.attributes || !a ? n.getAttribute(i) : (r = n.getAttributeNode(i)) && r.specified ? r.value : null
        };
        r.error = function(n) {
            throw new Error("Syntax error, unrecognized expression: " + n);
        };
        r.uniqueSort = function(n) {
            var r, f = [],
                t = 0,
                i = 0;
            if (rt = !u.detectDuplicates, y = !u.sortStable && n.slice(0), n.sort(bt), rt) {
                while (r = n[i++]) r === n[i] && (t = f.push(i));
                while (t--) n.splice(f[t], 1)
            }
            return y = null, n
        };
        ht = r.getText = function(n) {
            var r, i = "",
                u = 0,
                t = n.nodeType;
            if (t) {
                if (1 === t || 9 === t || 11 === t) {
                    if ("string" == typeof n.textContent) return n.textContent;
                    for (n = n.firstChild; n; n = n.nextSibling) i += ht(n)
                } else if (3 === t || 4 === t) return n.nodeValue
            } else
                while (r = n[u++]) i += ht(r);
            return i
        };
        t = r.selectors = {
            cacheLength: 50,
            createPseudo: h,
            match: vt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(n) {
                    return n[1] = n[1].replace(k, d), n[3] = (n[3] || n[4] || n[5] || "").replace(k, d), "~=" === n[2] && (n[3] = " " + n[3] + " "), n.slice(0, 4)
                },
                CHILD: function(n) {
                    return n[1] = n[1].toLowerCase(), "nth" === n[1].slice(0, 3) ? (n[3] || r.error(n[0]), n[4] = +(n[4] ? n[5] + (n[6] || 1) : 2 * ("even" === n[3] || "odd" === n[3])), n[5] = +(n[7] + n[8] || "odd" === n[3])) : n[3] && r.error(n[0]), n
                },
                PSEUDO: function(n) {
                    var i, t = !n[6] && n[2];
                    return vt.CHILD.test(n[0]) ? null : (n[3] ? n[2] = n[4] || n[5] || "" : t && ur.test(t) && (i = et(t, !0)) && (i = t.indexOf(")", t.length - i) - t.length) && (n[0] = n[0].slice(0, i), n[2] = t.slice(0, i)), n.slice(0, 3))
                }
            },
            filter: {
                TAG: function(n) {
                    var t = n.replace(k, d).toLowerCase();
                    return "*" === n ? function() {
                        return !0
                    } : function(n) {
                        return n.nodeName && n.nodeName.toLowerCase() === t
                    }
                },
                CLASS: function(n) {
                    var t = hi[n + " "];
                    return t || (t = new RegExp("(^|" + i + ")" + n + "(" + i + "|$)")) && hi(n, function(n) {
                        return t.test("string" == typeof n.className && n.className || typeof n.getAttribute !== ut && n.getAttribute("class") || "")
                    })
                },
                ATTR: function(n, t, i) {
                    return function(u) {
                        var f = r.attr(u, n);
                        return null == f ? "!=" === t : t ? (f += "", "=" === t ? f === i : "!=" === t ? f !== i : "^=" === t ? i && 0 === f.indexOf(i) : "*=" === t ? i && f.indexOf(i) > -1 : "$=" === t ? i && f.slice(-i.length) === i : "~=" === t ? (" " + f + " ").indexOf(i) > -1 : "|=" === t ? f === i || f.slice(0, i.length + 1) === i + "-" : !1) : !0
                    }
                },
                CHILD: function(n, t, i, r, u) {
                    var s = "nth" !== n.slice(0, 3),
                        o = "last" !== n.slice(-4),
                        e = "of-type" === t;
                    return 1 === r && 0 === u ? function(n) {
                        return !!n.parentNode
                    } : function(t, i, h) {
                        var a, k, c, l, y, w, b = s !== o ? "nextSibling" : "previousSibling",
                            p = t.parentNode,
                            g = e && t.nodeName.toLowerCase(),
                            d = !h && !e;
                        if (p) {
                            if (s) {
                                while (b) {
                                    for (c = t; c = c[b];)
                                        if (e ? c.nodeName.toLowerCase() === g : 1 === c.nodeType) return !1;
                                    w = b = "only" === n && !w && "nextSibling"
                                }
                                return !0
                            }
                            if (w = [o ? p.firstChild : p.lastChild], o && d) {
                                for (k = p[f] || (p[f] = {}), a = k[n] || [], y = a[0] === v && a[1], l = a[0] === v && a[2], c = y && p.childNodes[y]; c = ++y && c && c[b] || (l = y = 0) || w.pop();)
                                    if (1 === c.nodeType && ++l && c === t) {
                                        k[n] = [v, y, l];
                                        break
                                    }
                            } else if (d && (a = (t[f] || (t[f] = {}))[n]) && a[0] === v) l = a[1];
                            else
                                while (c = ++y && c && c[b] || (l = y = 0) || w.pop())
                                    if ((e ? c.nodeName.toLowerCase() === g : 1 === c.nodeType) && ++l && (d && ((c[f] || (c[f] = {}))[n] = [v, l]), c === t)) break;
                            return l -= u, l === r || l % r == 0 && l / r >= 0
                        }
                    }
                },
                PSEUDO: function(n, i) {
                    var e, u = t.pseudos[n] || t.setFilters[n.toLowerCase()] || r.error("unsupported pseudo: " + n);
                    return u[f] ? u(i) : u.length > 1 ? (e = [n, n, "", i], t.setFilters.hasOwnProperty(n.toLowerCase()) ? h(function(n, t) {
                        for (var r, f = u(n, i), e = f.length; e--;) r = nt.call(n, f[e]), n[r] = !(t[r] = f[e])
                    }) : function(n) {
                        return u(n, 0, e)
                    }) : u
                }
            },
            pseudos: {
                not: h(function(n) {
                    var i = [],
                        r = [],
                        t = wt(n.replace(at, "$1"));
                    return t[f] ? h(function(n, i, r, u) {
                        for (var e, o = t(n, null, u, []), f = n.length; f--;)(e = o[f]) && (n[f] = !(i[f] = e))
                    }) : function(n, u, f) {
                        return i[0] = n, t(i, null, f, r), !r.pop()
                    }
                }),
                has: h(function(n) {
                    return function(t) {
                        return r(n, t).length > 0
                    }
                }),
                contains: h(function(n) {
                    return function(t) {
                        return (t.textContent || t.innerText || ht(t)).indexOf(n) > -1
                    }
                }),
                lang: h(function(n) {
                    return fr.test(n || "") || r.error("unsupported lang: " + n), n = n.replace(k, d).toLowerCase(),
                        function(t) {
                            var i;
                            do
                                if (i = a ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return i = i.toLowerCase(), i === n || 0 === i.indexOf(n + "-"); while ((t = t.parentNode) && 1 === t.nodeType);
                            return !1
                        }
                }),
                target: function(t) {
                    var i = n.location && n.location.hash;
                    return i && i.slice(1) === t.id
                },
                root: function(n) {
                    return n === l
                },
                focus: function(n) {
                    return n === e.activeElement && (!e.hasFocus || e.hasFocus()) && !!(n.type || n.href || ~n.tabIndex)
                },
                enabled: function(n) {
                    return n.disabled === !1
                },
                disabled: function(n) {
                    return n.disabled === !0
                },
                checked: function(n) {
                    var t = n.nodeName.toLowerCase();
                    return "input" === t && !!n.checked || "option" === t && !!n.selected
                },
                selected: function(n) {
                    return n.parentNode && n.parentNode.selectedIndex, n.selected === !0
                },
                empty: function(n) {
                    for (n = n.firstChild; n; n = n.nextSibling)
                        if (n.nodeType < 6) return !1;
                    return !0
                },
                parent: function(n) {
                    return !t.pseudos.empty(n)
                },
                header: function(n) {
                    return or.test(n.nodeName)
                },
                input: function(n) {
                    return er.test(n.nodeName)
                },
                button: function(n) {
                    var t = n.nodeName.toLowerCase();
                    return "input" === t && "button" === n.type || "button" === t
                },
                text: function(n) {
                    var t;
                    return "input" === n.nodeName.toLowerCase() && "text" === n.type && (null == (t = n.getAttribute("type")) || "text" === t.toLowerCase())
                },
                first: tt(function() {
                    return [0]
                }),
                last: tt(function(n, t) {
                    return [t - 1]
                }),
                eq: tt(function(n, t, i) {
                    return [0 > i ? i + t : i]
                }),
                even: tt(function(n, t) {
                    for (var i = 0; t > i; i += 2) n.push(i);
                    return n
                }),
                odd: tt(function(n, t) {
                    for (var i = 1; t > i; i += 2) n.push(i);
                    return n
                }),
                lt: tt(function(n, t, i) {
                    for (var r = 0 > i ? i + t : i; --r >= 0;) n.push(r);
                    return n
                }),
                gt: tt(function(n, t, i) {
                    for (var r = 0 > i ? i + t : i; ++r < t;) n.push(r);
                    return n
                })
            }
        };
        t.pseudos.nth = t.pseudos.eq;
        for (it in {
                radio: !0,
                checkbox: !0,
                file: !0,
                password: !0,
                image: !0
            }) t.pseudos[it] = cr(it);
        for (it in {
                submit: !0,
                reset: !0
            }) t.pseudos[it] = lr(it);
        return bi.prototype = t.filters = t.pseudos, t.setFilters = new bi, et = r.tokenize = function(n, i) {
            var e, f, s, o, u, h, c, l = ci[n + " "];
            if (l) return i ? 0 : l.slice(0);
            for (u = n, h = [], c = t.preFilter; u;) {
                (!e || (f = tr.exec(u))) && (f && (u = u.slice(f[0].length) || u), h.push(s = []));
                e = !1;
                (f = ir.exec(u)) && (e = f.shift(), s.push({
                    value: e,
                    type: f[0].replace(at, " ")
                }), u = u.slice(e.length));
                for (o in t.filter)(f = vt[o].exec(u)) && (!c[o] || (f = c[o](f))) && (e = f.shift(), s.push({
                    value: e,
                    type: o,
                    matches: f
                }), u = u.slice(e.length));
                if (!e) break
            }
            return i ? u.length : u ? r.error(n) : ci(n, h).slice(0)
        }, wt = r.compile = function(n, t) {
            var r, u = [],
                e = [],
                i = li[n + " "];
            if (!i) {
                for (t || (t = et(n)), r = t.length; r--;) i = ei(t[r]), i[f] ? u.push(i) : e.push(i);
                i = li(n, vr(e, u));
                i.selector = n
            }
            return i
        }, si = r.select = function(n, i, r, f) {
            var s, e, o, l, v, c = "function" == typeof n && n,
                h = !f && et(n = c.selector || n);
            if (r = r || [], 1 === h.length) {
                if (e = h[0] = h[0].slice(0), e.length > 2 && "ID" === (o = e[0]).type && u.getById && 9 === i.nodeType && a && t.relative[e[1].type]) {
                    if (i = (t.find.ID(o.matches[0].replace(k, d), i) || [])[0], !i) return r;
                    c && (i = i.parentNode);
                    n = n.slice(e.shift().value.length)
                }
                for (s = vt.needsContext.test(n) ? 0 : e.length; s--;) {
                    if (o = e[s], t.relative[l = o.type]) break;
                    if ((v = t.find[l]) && (f = v(o.matches[0].replace(k, d), gt.test(e[0].type) && ii(i.parentNode) || i))) {
                        if (e.splice(s, 1), n = f.length && yt(e), !n) return b.apply(r, f), r;
                        break
                    }
                }
            }
            return (c || wt(n, h))(f, i, !a, r, gt.test(n) && ii(i.parentNode) || i), r
        }, u.sortStable = f.split("").sort(bt).join("") === f, u.detectDuplicates = !!rt, p(), u.sortDetached = c(function(n) {
            return 1 & n.compareDocumentPosition(e.createElement("div"))
        }), c(function(n) {
            return n.innerHTML = "<a href='#'><\/a>", "#" === n.firstChild.getAttribute("href")
        }) || ti("type|href|height|width", function(n, t, i) {
            if (!i) return n.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
        }), u.attributes && c(function(n) {
            return n.innerHTML = "<input/>", n.firstChild.setAttribute("value", ""), "" === n.firstChild.getAttribute("value")
        }) || ti("value", function(n, t, i) {
            if (!i && "input" === n.nodeName.toLowerCase()) return n.defaultValue
        }), c(function(n) {
            return null == n.getAttribute("disabled")
        }) || ti(kt, function(n, t, i) {
            var r;
            if (!i) return n[t] === !0 ? t.toLowerCase() : (r = n.getAttributeNode(t)) && r.specified ? r.value : null
        }), r
    }(n);
    i.find = p;
    i.expr = p.selectors;
    i.expr[":"] = i.expr.pseudos;
    i.unique = p.uniqueSort;
    i.text = p.getText;
    i.isXMLDoc = p.isXML;
    i.contains = p.contains;
    var fr = i.expr.match.needsContext,
        er = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
        re = /^.[^:#\[\.,]*$/;
    i.filter = function(n, t, r) {
        var u = t[0];
        return r && (n = ":not(" + n + ")"), 1 === t.length && 1 === u.nodeType ? i.find.matchesSelector(u, n) ? [u] : [] : i.find.matches(n, i.grep(t, function(n) {
            return 1 === n.nodeType
        }))
    };
    i.fn.extend({
        find: function(n) {
            var t, r = [],
                u = this,
                f = u.length;
            if ("string" != typeof n) return this.pushStack(i(n).filter(function() {
                for (t = 0; f > t; t++)
                    if (i.contains(u[t], this)) return !0
            }));
            for (t = 0; f > t; t++) i.find(n, u[t], r);
            return r = this.pushStack(f > 1 ? i.unique(r) : r), r.selector = this.selector ? this.selector + " " + n : n, r
        },
        filter: function(n) {
            return this.pushStack(ui(this, n || [], !1))
        },
        not: function(n) {
            return this.pushStack(ui(this, n || [], !0))
        },
        is: function(n) {
            return !!ui(this, "string" == typeof n && fr.test(n) ? i(n) : n || [], !1).length
        }
    });
    var ft, u = n.document,
        ue = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/,
        fe = i.fn.init = function(n, t) {
            var r, f;
            if (!n) return this;
            if ("string" == typeof n) {
                if (r = "<" === n.charAt(0) && ">" === n.charAt(n.length - 1) && n.length >= 3 ? [null, n, null] : ue.exec(n), !r || !r[1] && t) return !t || t.jquery ? (t || ft).find(n) : this.constructor(t).find(n);
                if (r[1]) {
                    if (t = t instanceof i ? t[0] : t, i.merge(this, i.parseHTML(r[1], t && t.nodeType ? t.ownerDocument || t : u, !0)), er.test(r[1]) && i.isPlainObject(t))
                        for (r in t) i.isFunction(this[r]) ? this[r](t[r]) : this.attr(r, t[r]);
                    return this
                }
                if (f = u.getElementById(r[2]), f && f.parentNode) {
                    if (f.id !== r[2]) return ft.find(n);
                    this.length = 1;
                    this[0] = f
                }
                return this.context = u, this.selector = n, this
            }
            return n.nodeType ? (this.context = this[0] = n, this.length = 1, this) : i.isFunction(n) ? "undefined" != typeof ft.ready ? ft.ready(n) : n(i) : (void 0 !== n.selector && (this.selector = n.selector, this.context = n.context), i.makeArray(n, this))
        };
    fe.prototype = i.fn;
    ft = i(u);
    or = /^(?:parents|prev(?:Until|All))/;
    sr = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    i.extend({
        dir: function(n, t, r) {
            for (var f = [], u = n[t]; u && 9 !== u.nodeType && (void 0 === r || 1 !== u.nodeType || !i(u).is(r));) 1 === u.nodeType && f.push(u), u = u[t];
            return f
        },
        sibling: function(n, t) {
            for (var i = []; n; n = n.nextSibling) 1 === n.nodeType && n !== t && i.push(n);
            return i
        }
    });
    i.fn.extend({
        has: function(n) {
            var t, r = i(n, this),
                u = r.length;
            return this.filter(function() {
                for (t = 0; u > t; t++)
                    if (i.contains(this, r[t])) return !0
            })
        },
        closest: function(n, t) {
            for (var r, f = 0, o = this.length, u = [], e = fr.test(n) || "string" != typeof n ? i(n, t || this.context) : 0; o > f; f++)
                for (r = this[f]; r && r !== t; r = r.parentNode)
                    if (r.nodeType < 11 && (e ? e.index(r) > -1 : 1 === r.nodeType && i.find.matchesSelector(r, n))) {
                        u.push(r);
                        break
                    }
            return this.pushStack(u.length > 1 ? i.unique(u) : u)
        },
        index: function(n) {
            return n ? "string" == typeof n ? i.inArray(this[0], i(n)) : i.inArray(n.jquery ? n[0] : n, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(n, t) {
            return this.pushStack(i.unique(i.merge(this.get(), i(n, t))))
        },
        addBack: function(n) {
            return this.add(null == n ? this.prevObject : this.prevObject.filter(n))
        }
    });
    i.each({
        parent: function(n) {
            var t = n.parentNode;
            return t && 11 !== t.nodeType ? t : null
        },
        parents: function(n) {
            return i.dir(n, "parentNode")
        },
        parentsUntil: function(n, t, r) {
            return i.dir(n, "parentNode", r)
        },
        next: function(n) {
            return hr(n, "nextSibling")
        },
        prev: function(n) {
            return hr(n, "previousSibling")
        },
        nextAll: function(n) {
            return i.dir(n, "nextSibling")
        },
        prevAll: function(n) {
            return i.dir(n, "previousSibling")
        },
        nextUntil: function(n, t, r) {
            return i.dir(n, "nextSibling", r)
        },
        prevUntil: function(n, t, r) {
            return i.dir(n, "previousSibling", r)
        },
        siblings: function(n) {
            return i.sibling((n.parentNode || {}).firstChild, n)
        },
        children: function(n) {
            return i.sibling(n.firstChild)
        },
        contents: function(n) {
            return i.nodeName(n, "iframe") ? n.contentDocument || n.contentWindow.document : i.merge([], n.childNodes)
        }
    }, function(n, t) {
        i.fn[n] = function(r, u) {
            var f = i.map(this, t, r);
            return "Until" !== n.slice(-5) && (u = r), u && "string" == typeof u && (f = i.filter(u, f)), this.length > 1 && (sr[n] || (f = i.unique(f)), or.test(n) && (f = f.reverse())), this.pushStack(f)
        }
    });
    h = /\S+/g;
    fi = {};
    i.Callbacks = function(n) {
        n = "string" == typeof n ? fi[n] || ee(n) : i.extend({}, n);
        var o, u, h, f, e, c, t = [],
            r = !n.once && [],
            l = function(i) {
                for (u = n.memory && i, h = !0, e = c || 0, c = 0, f = t.length, o = !0; t && f > e; e++)
                    if (t[e].apply(i[0], i[1]) === !1 && n.stopOnFalse) {
                        u = !1;
                        break
                    }
                o = !1;
                t && (r ? r.length && l(r.shift()) : u ? t = [] : s.disable())
            },
            s = {
                add: function() {
                    if (t) {
                        var r = t.length;
                        ! function e(r) {
                            i.each(r, function(r, u) {
                                var f = i.type(u);
                                "function" === f ? n.unique && s.has(u) || t.push(u) : u && u.length && "string" !== f && e(u)
                            })
                        }(arguments);
                        o ? f = t.length : u && (c = r, l(u))
                    }
                    return this
                },
                remove: function() {
                    return t && i.each(arguments, function(n, r) {
                        for (var u;
                            (u = i.inArray(r, t, u)) > -1;) t.splice(u, 1), o && (f >= u && f--, e >= u && e--)
                    }), this
                },
                has: function(n) {
                    return n ? i.inArray(n, t) > -1 : !(!t || !t.length)
                },
                empty: function() {
                    return t = [], f = 0, this
                },
                disable: function() {
                    return t = r = u = void 0, this
                },
                disabled: function() {
                    return !t
                },
                lock: function() {
                    return r = void 0, u || s.disable(), this
                },
                locked: function() {
                    return !r
                },
                fireWith: function(n, i) {
                    return !t || h && !r || (i = i || [], i = [n, i.slice ? i.slice() : i], o ? r.push(i) : l(i)), this
                },
                fire: function() {
                    return s.fireWith(this, arguments), this
                },
                fired: function() {
                    return !!h
                }
            };
        return s
    };
    i.extend({
        Deferred: function(n) {
            var u = [
                    ["resolve", "done", i.Callbacks("once memory"), "resolved"],
                    ["reject", "fail", i.Callbacks("once memory"), "rejected"],
                    ["notify", "progress", i.Callbacks("memory")]
                ],
                f = "pending",
                r = {
                    state: function() {
                        return f
                    },
                    always: function() {
                        return t.done(arguments).fail(arguments), this
                    },
                    then: function() {
                        var n = arguments;
                        return i.Deferred(function(f) {
                            i.each(u, function(u, e) {
                                var o = i.isFunction(n[u]) && n[u];
                                t[e[1]](function() {
                                    var n = o && o.apply(this, arguments);
                                    n && i.isFunction(n.promise) ? n.promise().done(f.resolve).fail(f.reject).progress(f.notify) : f[e[0] + "With"](this === r ? f.promise() : this, o ? [n] : arguments)
                                })
                            });
                            n = null
                        }).promise()
                    },
                    promise: function(n) {
                        return null != n ? i.extend(n, r) : r
                    }
                },
                t = {};
            return r.pipe = r.then, i.each(u, function(n, i) {
                var e = i[2],
                    o = i[3];
                r[i[1]] = e.add;
                o && e.add(function() {
                    f = o
                }, u[1 ^ n][2].disable, u[2][2].lock);
                t[i[0]] = function() {
                    return t[i[0] + "With"](this === t ? r : this, arguments), this
                };
                t[i[0] + "With"] = e.fireWith
            }), r.promise(t), n && n.call(t, t), t
        },
        when: function(n) {
            var t = 0,
                u = l.call(arguments),
                r = u.length,
                e = 1 !== r || n && i.isFunction(n.promise) ? r : 0,
                f = 1 === e ? n : i.Deferred(),
                h = function(n, t, i) {
                    return function(r) {
                        t[n] = this;
                        i[n] = arguments.length > 1 ? l.call(arguments) : r;
                        i === o ? f.notifyWith(t, i) : --e || f.resolveWith(t, i)
                    }
                },
                o, c, s;
            if (r > 1)
                for (o = new Array(r), c = new Array(r), s = new Array(r); r > t; t++) u[t] && i.isFunction(u[t].promise) ? u[t].promise().done(h(t, s, u)).fail(f.reject).progress(h(t, c, o)) : --e;
            return e || f.resolveWith(s, u), f.promise()
        }
    });
    i.fn.ready = function(n) {
        return i.ready.promise().done(n), this
    };
    i.extend({
        isReady: !1,
        readyWait: 1,
        holdReady: function(n) {
            n ? i.readyWait++ : i.ready(!0)
        },
        ready: function(n) {
            if (n === !0 ? !--i.readyWait : !i.isReady) {
                if (!u.body) return setTimeout(i.ready);
                i.isReady = !0;
                n !== !0 && --i.readyWait > 0 || (lt.resolveWith(u, [i]), i.fn.triggerHandler && (i(u).triggerHandler("ready"), i(u).off("ready")))
            }
        }
    });
    i.ready.promise = function(t) {
        if (!lt)
            if (lt = i.Deferred(), "complete" === u.readyState) setTimeout(i.ready);
            else if (u.addEventListener) u.addEventListener("DOMContentLoaded", a, !1), n.addEventListener("load", a, !1);
        else {
            u.attachEvent("onreadystatechange", a);
            n.attachEvent("onload", a);
            var r = !1;
            try {
                r = null == n.frameElement && u.documentElement
            } catch (e) {}
            r && r.doScroll && ! function f() {
                if (!i.isReady) {
                    try {
                        r.doScroll("left")
                    } catch (n) {
                        return setTimeout(f, 50)
                    }
                    cr();
                    i.ready()
                }
            }()
        }
        return lt.promise(t)
    };
    o = "undefined";
    for (lr in i(r)) break;
    r.ownLast = "0" !== lr;
    r.inlineBlockNeedsLayout = !1;
    i(function() {
            var f, t, n, i;
            n = u.getElementsByTagName("body")[0];
            n && n.style && (t = u.createElement("div"), i = u.createElement("div"), i.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", n.appendChild(i).appendChild(t), typeof t.style.zoom !== o && (t.style.cssText = "display:inline;margin:0;border:0;padding:1px;width:1px;zoom:1", r.inlineBlockNeedsLayout = f = 3 === t.offsetWidth, f && (n.style.zoom = 1)), n.removeChild(i))
        }),
        function() {
            var n = u.createElement("div");
            if (null == r.deleteExpando) {
                r.deleteExpando = !0;
                try {
                    delete n.test
                } catch (t) {
                    r.deleteExpando = !1
                }
            }
            n = null
        }();
    i.acceptData = function(n) {
        var t = i.noData[(n.nodeName + " ").toLowerCase()],
            r = +n.nodeType || 1;
        return 1 !== r && 9 !== r ? !1 : !t || t !== !0 && n.getAttribute("classid") === t
    };
    ar = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/;
    vr = /([A-Z])/g;
    i.extend({
        cache: {},
        noData: {
            "applet ": !0,
            "embed ": !0,
            "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
        },
        hasData: function(n) {
            return n = n.nodeType ? i.cache[n[i.expando]] : n[i.expando], !!n && !ei(n)
        },
        data: function(n, t, i) {
            return pr(n, t, i)
        },
        removeData: function(n, t) {
            return wr(n, t)
        },
        _data: function(n, t, i) {
            return pr(n, t, i, !0)
        },
        _removeData: function(n, t) {
            return wr(n, t, !0)
        }
    });
    i.fn.extend({
        data: function(n, t) {
            var f, u, e, r = this[0],
                o = r && r.attributes;
            if (void 0 === n) {
                if (this.length && (e = i.data(r), 1 === r.nodeType && !i._data(r, "parsedAttrs"))) {
                    for (f = o.length; f--;) o[f] && (u = o[f].name, 0 === u.indexOf("data-") && (u = i.camelCase(u.slice(5)), yr(r, u, e[u])));
                    i._data(r, "parsedAttrs", !0)
                }
                return e
            }
            return "object" == typeof n ? this.each(function() {
                i.data(this, n)
            }) : arguments.length > 1 ? this.each(function() {
                i.data(this, n, t)
            }) : r ? yr(r, n, i.data(r, n)) : void 0
        },
        removeData: function(n) {
            return this.each(function() {
                i.removeData(this, n)
            })
        }
    });
    i.extend({
        queue: function(n, t, r) {
            var u;
            if (n) return (t = (t || "fx") + "queue", u = i._data(n, t), r && (!u || i.isArray(r) ? u = i._data(n, t, i.makeArray(r)) : u.push(r)), u || [])
        },
        dequeue: function(n, t) {
            t = t || "fx";
            var r = i.queue(n, t),
                e = r.length,
                u = r.shift(),
                f = i._queueHooks(n, t),
                o = function() {
                    i.dequeue(n, t)
                };
            "inprogress" === u && (u = r.shift(), e--);
            u && ("fx" === t && r.unshift("inprogress"), delete f.stop, u.call(n, o, f));
            !e && f && f.empty.fire()
        },
        _queueHooks: function(n, t) {
            var r = t + "queueHooks";
            return i._data(n, r) || i._data(n, r, {
                empty: i.Callbacks("once memory").add(function() {
                    i._removeData(n, t + "queue");
                    i._removeData(n, r)
                })
            })
        }
    });
    i.fn.extend({
        queue: function(n, t) {
            var r = 2;
            return "string" != typeof n && (t = n, n = "fx", r--), arguments.length < r ? i.queue(this[0], n) : void 0 === t ? this : this.each(function() {
                var r = i.queue(this, n, t);
                i._queueHooks(this, n);
                "fx" === n && "inprogress" !== r[0] && i.dequeue(this, n)
            })
        },
        dequeue: function(n) {
            return this.each(function() {
                i.dequeue(this, n)
            })
        },
        clearQueue: function(n) {
            return this.queue(n || "fx", [])
        },
        promise: function(n, t) {
            var r, f = 1,
                e = i.Deferred(),
                u = this,
                o = this.length,
                s = function() {
                    --f || e.resolveWith(u, [u])
                };
            for ("string" != typeof n && (t = n, n = void 0), n = n || "fx"; o--;) r = i._data(u[o], n + "queueHooks"), r && r.empty && (f++, r.empty.add(s));
            return s(), e.promise(t)
        }
    });
    var at = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
        w = ["Top", "Right", "Bottom", "Left"],
        et = function(n, t) {
            return n = t || n, "none" === i.css(n, "display") || !i.contains(n.ownerDocument, n)
        },
        b = i.access = function(n, t, r, u, f, e, o) {
            var s = 0,
                c = n.length,
                h = null == r;
            if ("object" === i.type(r)) {
                f = !0;
                for (s in r) i.access(n, t, s, r[s], !0, e, o)
            } else if (void 0 !== u && (f = !0, i.isFunction(u) || (o = !0), h && (o ? (t.call(n, u), t = null) : (h = t, t = function(n, t, r) {
                    return h.call(i(n), r)
                })), t))
                for (; c > s; s++) t(n[s], r, o ? u : u.call(n[s], s, t(n[s], r)));
            return f ? n : h ? t.call(n) : c ? t(n[0], r) : e
        },
        oi = /^(?:checkbox|radio)$/i;
    ! function() {
        var t = u.createElement("input"),
            n = u.createElement("div"),
            i = u.createDocumentFragment();
        if (n.innerHTML = "  <link/><table><\/table><a href='/a'>a<\/a><input type='checkbox'/>", r.leadingWhitespace = 3 === n.firstChild.nodeType, r.tbody = !n.getElementsByTagName("tbody").length, r.htmlSerialize = !!n.getElementsByTagName("link").length, r.html5Clone = "<:nav><\/:nav>" !== u.createElement("nav").cloneNode(!0).outerHTML, t.type = "checkbox", t.checked = !0, i.appendChild(t), r.appendChecked = t.checked, n.innerHTML = "<textarea>x<\/textarea>", r.noCloneChecked = !!n.cloneNode(!0).lastChild.defaultValue, i.appendChild(n), n.innerHTML = "<input type='radio' checked='checked' name='t'/>", r.checkClone = n.cloneNode(!0).cloneNode(!0).lastChild.checked, r.noCloneEvent = !0, n.attachEvent && (n.attachEvent("onclick", function() {
                r.noCloneEvent = !1
            }), n.cloneNode(!0).click()), null == r.deleteExpando) {
            r.deleteExpando = !0;
            try {
                delete n.test
            } catch (f) {
                r.deleteExpando = !1
            }
        }
    }(),
    function() {
        var t, i, f = u.createElement("div");
        for (t in {
                submit: !0,
                change: !0,
                focusin: !0
            }) i = "on" + t, (r[t + "Bubbles"] = i in n) || (f.setAttribute(i, "t"), r[t + "Bubbles"] = f.attributes[i].expando === !1);
        f = null
    }();
    var si = /^(?:input|select|textarea)$/i,
        oe = /^key/,
        se = /^(?:mouse|pointer|contextmenu)|click/,
        br = /^(?:focusinfocus|focusoutblur)$/,
        kr = /^([^.]*)(?:\.(.+)|)$/;
    i.event = {
        global: {},
        add: function(n, t, r, u, f) {
            var w, y, b, p, s, c, l, a, e, k, d, v = i._data(n);
            if (v) {
                for (r.handler && (p = r, r = p.handler, f = p.selector), r.guid || (r.guid = i.guid++), (y = v.events) || (y = v.events = {}), (c = v.handle) || (c = v.handle = function(n) {
                        if (typeof i !== o && (!n || i.event.triggered !== n.type)) return i.event.dispatch.apply(c.elem, arguments)
                    }, c.elem = n), t = (t || "").match(h) || [""], b = t.length; b--;) w = kr.exec(t[b]) || [], e = d = w[1], k = (w[2] || "").split(".").sort(), e && (s = i.event.special[e] || {}, e = (f ? s.delegateType : s.bindType) || e, s = i.event.special[e] || {}, l = i.extend({
                    type: e,
                    origType: d,
                    data: u,
                    handler: r,
                    guid: r.guid,
                    selector: f,
                    needsContext: f && i.expr.match.needsContext.test(f),
                    namespace: k.join(".")
                }, p), (a = y[e]) || (a = y[e] = [], a.delegateCount = 0, s.setup && s.setup.call(n, u, k, c) !== !1 || (n.addEventListener ? n.addEventListener(e, c, !1) : n.attachEvent && n.attachEvent("on" + e, c))), s.add && (s.add.call(n, l), l.handler.guid || (l.handler.guid = r.guid)), f ? a.splice(a.delegateCount++, 0, l) : a.push(l), i.event.global[e] = !0);
                n = null
            }
        },
        remove: function(n, t, r, u, f) {
            var y, o, s, b, p, a, c, l, e, w, k, v = i.hasData(n) && i._data(n);
            if (v && (a = v.events)) {
                for (t = (t || "").match(h) || [""], p = t.length; p--;)
                    if (s = kr.exec(t[p]) || [], e = k = s[1], w = (s[2] || "").split(".").sort(), e) {
                        for (c = i.event.special[e] || {}, e = (u ? c.delegateType : c.bindType) || e, l = a[e] || [], s = s[2] && new RegExp("(^|\\.)" + w.join("\\.(?:.*\\.|)") + "(\\.|$)"), b = y = l.length; y--;) o = l[y], !f && k !== o.origType || r && r.guid !== o.guid || s && !s.test(o.namespace) || u && u !== o.selector && ("**" !== u || !o.selector) || (l.splice(y, 1), o.selector && l.delegateCount--, c.remove && c.remove.call(n, o));
                        b && !l.length && (c.teardown && c.teardown.call(n, w, v.handle) !== !1 || i.removeEvent(n, e, v.handle), delete a[e])
                    } else
                        for (e in a) i.event.remove(n, e + t[p], r, u, !0);
                i.isEmptyObject(a) && (delete v.handle, i._removeData(n, "events"))
            }
        },
        trigger: function(t, r, f, e) {
            var l, a, o, p, c, h, w, y = [f || u],
                s = tt.call(t, "type") ? t.type : t,
                v = tt.call(t, "namespace") ? t.namespace.split(".") : [];
            if (o = h = f = f || u, 3 !== f.nodeType && 8 !== f.nodeType && !br.test(s + i.event.triggered) && (s.indexOf(".") >= 0 && (v = s.split("."), s = v.shift(), v.sort()), a = s.indexOf(":") < 0 && "on" + s, t = t[i.expando] ? t : new i.Event(s, "object" == typeof t && t), t.isTrigger = e ? 2 : 3, t.namespace = v.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + v.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = f), r = null == r ? [t] : i.makeArray(r, [t]), c = i.event.special[s] || {}, e || !c.trigger || c.trigger.apply(f, r) !== !1)) {
                if (!e && !c.noBubble && !i.isWindow(f)) {
                    for (p = c.delegateType || s, br.test(p + s) || (o = o.parentNode); o; o = o.parentNode) y.push(o), h = o;
                    h === (f.ownerDocument || u) && y.push(h.defaultView || h.parentWindow || n)
                }
                for (w = 0;
                    (o = y[w++]) && !t.isPropagationStopped();) t.type = w > 1 ? p : c.bindType || s, l = (i._data(o, "events") || {})[t.type] && i._data(o, "handle"), l && l.apply(o, r), l = a && o[a], l && l.apply && i.acceptData(o) && (t.result = l.apply(o, r), t.result === !1 && t.preventDefault());
                if (t.type = s, !e && !t.isDefaultPrevented() && (!c._default || c._default.apply(y.pop(), r) === !1) && i.acceptData(f) && a && f[s] && !i.isWindow(f)) {
                    h = f[a];
                    h && (f[a] = null);
                    i.event.triggered = s;
                    try {
                        f[s]()
                    } catch (b) {}
                    i.event.triggered = void 0;
                    h && (f[a] = h)
                }
                return t.result
            }
        },
        dispatch: function(n) {
            n = i.event.fix(n);
            var e, f, t, r, o, s = [],
                h = l.call(arguments),
                c = (i._data(this, "events") || {})[n.type] || [],
                u = i.event.special[n.type] || {};
            if (h[0] = n, n.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, n) !== !1) {
                for (s = i.event.handlers.call(this, n, c), e = 0;
                    (r = s[e++]) && !n.isPropagationStopped();)
                    for (n.currentTarget = r.elem, o = 0;
                        (t = r.handlers[o++]) && !n.isImmediatePropagationStopped();)(!n.namespace_re || n.namespace_re.test(t.namespace)) && (n.handleObj = t, n.data = t.data, f = ((i.event.special[t.origType] || {}).handle || t.handler).apply(r.elem, h), void 0 !== f && (n.result = f) === !1 && (n.preventDefault(), n.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, n), n.result
            }
        },
        handlers: function(n, t) {
            var f, e, u, o, h = [],
                s = t.delegateCount,
                r = n.target;
            if (s && r.nodeType && (!n.button || "click" !== n.type))
                for (; r != this; r = r.parentNode || this)
                    if (1 === r.nodeType && (r.disabled !== !0 || "click" !== n.type)) {
                        for (u = [], o = 0; s > o; o++) e = t[o], f = e.selector + " ", void 0 === u[f] && (u[f] = e.needsContext ? i(f, this).index(r) >= 0 : i.find(f, this, null, [r]).length), u[f] && u.push(e);
                        u.length && h.push({
                            elem: r,
                            handlers: u
                        })
                    }
            return s < t.length && h.push({
                elem: this,
                handlers: t.slice(s)
            }), h
        },
        fix: function(n) {
            if (n[i.expando]) return n;
            var e, o, s, r = n.type,
                f = n,
                t = this.fixHooks[r];
            for (t || (this.fixHooks[r] = t = se.test(r) ? this.mouseHooks : oe.test(r) ? this.keyHooks : {}), s = t.props ? this.props.concat(t.props) : this.props, n = new i.Event(f), e = s.length; e--;) o = s[e], n[o] = f[o];
            return n.target || (n.target = f.srcElement || u), 3 === n.target.nodeType && (n.target = n.target.parentNode), n.metaKey = !!n.metaKey, t.filter ? t.filter(n, f) : n
        },
        props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
        fixHooks: {},
        keyHooks: {
            props: "char charCode key keyCode".split(" "),
            filter: function(n, t) {
                return null == n.which && (n.which = null != t.charCode ? t.charCode : t.keyCode), n
            }
        },
        mouseHooks: {
            props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
            filter: function(n, t) {
                var i, e, r, f = t.button,
                    o = t.fromElement;
                return null == n.pageX && null != t.clientX && (e = n.target.ownerDocument || u, r = e.documentElement, i = e.body, n.pageX = t.clientX + (r && r.scrollLeft || i && i.scrollLeft || 0) - (r && r.clientLeft || i && i.clientLeft || 0), n.pageY = t.clientY + (r && r.scrollTop || i && i.scrollTop || 0) - (r && r.clientTop || i && i.clientTop || 0)), !n.relatedTarget && o && (n.relatedTarget = o === n.target ? t.toElement : o), n.which || void 0 === f || (n.which = 1 & f ? 1 : 2 & f ? 3 : 4 & f ? 2 : 0), n
            }
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== dr() && this.focus) try {
                        return this.focus(), !1
                    } catch (n) {}
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === dr() && this.blur) return (this.blur(), !1)
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if (i.nodeName(this, "input") && "checkbox" === this.type && this.click) return (this.click(), !1)
                },
                _default: function(n) {
                    return i.nodeName(n.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(n) {
                    void 0 !== n.result && n.originalEvent && (n.originalEvent.returnValue = n.result)
                }
            }
        },
        simulate: function(n, t, r, u) {
            var f = i.extend(new i.Event, r, {
                type: n,
                isSimulated: !0,
                originalEvent: {}
            });
            u ? i.event.trigger(f, null, t) : i.event.dispatch.call(t, f);
            f.isDefaultPrevented() && r.preventDefault()
        }
    };
    i.removeEvent = u.removeEventListener ? function(n, t, i) {
        n.removeEventListener && n.removeEventListener(t, i, !1)
    } : function(n, t, i) {
        var r = "on" + t;
        n.detachEvent && (typeof n[r] === o && (n[r] = null), n.detachEvent(r, i))
    };
    i.Event = function(n, t) {
        return this instanceof i.Event ? (n && n.type ? (this.originalEvent = n, this.type = n.type, this.isDefaultPrevented = n.defaultPrevented || void 0 === n.defaultPrevented && n.returnValue === !1 ? vt : it) : this.type = n, t && i.extend(this, t), this.timeStamp = n && n.timeStamp || i.now(), void(this[i.expando] = !0)) : new i.Event(n, t)
    };
    i.Event.prototype = {
        isDefaultPrevented: it,
        isPropagationStopped: it,
        isImmediatePropagationStopped: it,
        preventDefault: function() {
            var n = this.originalEvent;
            this.isDefaultPrevented = vt;
            n && (n.preventDefault ? n.preventDefault() : n.returnValue = !1)
        },
        stopPropagation: function() {
            var n = this.originalEvent;
            this.isPropagationStopped = vt;
            n && (n.stopPropagation && n.stopPropagation(), n.cancelBubble = !0)
        },
        stopImmediatePropagation: function() {
            var n = this.originalEvent;
            this.isImmediatePropagationStopped = vt;
            n && n.stopImmediatePropagation && n.stopImmediatePropagation();
            this.stopPropagation()
        }
    };
    i.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(n, t) {
        i.event.special[n] = {
            delegateType: t,
            bindType: t,
            handle: function(n) {
                var u, f = this,
                    r = n.relatedTarget,
                    e = n.handleObj;
                return (!r || r !== f && !i.contains(f, r)) && (n.type = e.origType, u = e.handler.apply(this, arguments), n.type = t), u
            }
        }
    });
    r.submitBubbles || (i.event.special.submit = {
        setup: function() {
            return i.nodeName(this, "form") ? !1 : void i.event.add(this, "click._submit keypress._submit", function(n) {
                var r = n.target,
                    t = i.nodeName(r, "input") || i.nodeName(r, "button") ? r.form : void 0;
                t && !i._data(t, "submitBubbles") && (i.event.add(t, "submit._submit", function(n) {
                    n._submit_bubble = !0
                }), i._data(t, "submitBubbles", !0))
            })
        },
        postDispatch: function(n) {
            n._submit_bubble && (delete n._submit_bubble, this.parentNode && !n.isTrigger && i.event.simulate("submit", this.parentNode, n, !0))
        },
        teardown: function() {
            return i.nodeName(this, "form") ? !1 : void i.event.remove(this, "._submit")
        }
    });
    r.changeBubbles || (i.event.special.change = {
        setup: function() {
            return si.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (i.event.add(this, "propertychange._change", function(n) {
                "checked" === n.originalEvent.propertyName && (this._just_changed = !0)
            }), i.event.add(this, "click._change", function(n) {
                this._just_changed && !n.isTrigger && (this._just_changed = !1);
                i.event.simulate("change", this, n, !0)
            })), !1) : void i.event.add(this, "beforeactivate._change", function(n) {
                var t = n.target;
                si.test(t.nodeName) && !i._data(t, "changeBubbles") && (i.event.add(t, "change._change", function(n) {
                    !this.parentNode || n.isSimulated || n.isTrigger || i.event.simulate("change", this.parentNode, n, !0)
                }), i._data(t, "changeBubbles", !0))
            })
        },
        handle: function(n) {
            var t = n.target;
            if (this !== t || n.isSimulated || n.isTrigger || "radio" !== t.type && "checkbox" !== t.type) return n.handleObj.handler.apply(this, arguments)
        },
        teardown: function() {
            return i.event.remove(this, "._change"), !si.test(this.nodeName)
        }
    });
    r.focusinBubbles || i.each({
        focus: "focusin",
        blur: "focusout"
    }, function(n, t) {
        var r = function(n) {
            i.event.simulate(t, n.target, i.event.fix(n), !0)
        };
        i.event.special[t] = {
            setup: function() {
                var u = this.ownerDocument || this,
                    f = i._data(u, t);
                f || u.addEventListener(n, r, !0);
                i._data(u, t, (f || 0) + 1)
            },
            teardown: function() {
                var u = this.ownerDocument || this,
                    f = i._data(u, t) - 1;
                f ? i._data(u, t, f) : (u.removeEventListener(n, r, !0), i._removeData(u, t))
            }
        }
    });
    i.fn.extend({
        on: function(n, t, r, u, f) {
            var o, e;
            if ("object" == typeof n) {
                "string" != typeof t && (r = r || t, t = void 0);
                for (o in n) this.on(o, t, r, n[o], f);
                return this
            }
            if (null == r && null == u ? (u = t, r = t = void 0) : null == u && ("string" == typeof t ? (u = r, r = void 0) : (u = r, r = t, t = void 0)), u === !1) u = it;
            else if (!u) return this;
            return 1 === f && (e = u, u = function(n) {
                return i().off(n), e.apply(this, arguments)
            }, u.guid = e.guid || (e.guid = i.guid++)), this.each(function() {
                i.event.add(this, n, u, r, t)
            })
        },
        one: function(n, t, i, r) {
            return this.on(n, t, i, r, 1)
        },
        off: function(n, t, r) {
            var u, f;
            if (n && n.preventDefault && n.handleObj) return u = n.handleObj, i(n.delegateTarget).off(u.namespace ? u.origType + "." + u.namespace : u.origType, u.selector, u.handler), this;
            if ("object" == typeof n) {
                for (f in n) this.off(f, t, n[f]);
                return this
            }
            return (t === !1 || "function" == typeof t) && (r = t, t = void 0), r === !1 && (r = it), this.each(function() {
                i.event.remove(this, n, r, t)
            })
        },
        trigger: function(n, t) {
            return this.each(function() {
                i.event.trigger(n, t, this)
            })
        },
        triggerHandler: function(n, t) {
            var r = this[0];
            if (r) return i.event.trigger(n, t, r, !0)
        }
    });
    var nu = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
        he = / jQuery\d+="(?:null|\d+)"/g,
        tu = new RegExp("<(?:" + nu + ")[\\s/>]", "i"),
        hi = /^\s+/,
        iu = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
        ru = /<([\w:]+)/,
        uu = /<tbody/i,
        ce = /<|&#?\w+;/,
        le = /<(?:script|style|link)/i,
        ae = /checked\s*(?:[^=]|=\s*.checked.)/i,
        fu = /^$|\/(?:java|ecma)script/i,
        ve = /^true\/(.*)/,
        ye = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g,
        s = {
            option: [1, "<select multiple='multiple'>", "<\/select>"],
            legend: [1, "<fieldset>", "<\/fieldset>"],
            area: [1, "<map>", "<\/map>"],
            param: [1, "<object>", "<\/object>"],
            thead: [1, "<table>", "<\/table>"],
            tr: [2, "<table><tbody>", "<\/tbody><\/table>"],
            col: [2, "<table><tbody><\/tbody><colgroup>", "<\/colgroup><\/table>"],
            td: [3, "<table><tbody><tr>", "<\/tr><\/tbody><\/table>"],
            _default: r.htmlSerialize ? [0, "", ""] : [1, "X<div>", "<\/div>"]
        },
        pe = gr(u),
        ci = pe.appendChild(u.createElement("div"));
    s.optgroup = s.option;
    s.tbody = s.tfoot = s.colgroup = s.caption = s.thead;
    s.th = s.td;
    i.extend({
        clone: function(n, t, u) {
            var e, c, s, o, h, l = i.contains(n.ownerDocument, n);
            if (r.html5Clone || i.isXMLDoc(n) || !tu.test("<" + n.nodeName + ">") ? s = n.cloneNode(!0) : (ci.innerHTML = n.outerHTML, ci.removeChild(s = ci.firstChild)), !(r.noCloneEvent && r.noCloneChecked || 1 !== n.nodeType && 11 !== n.nodeType || i.isXMLDoc(n)))
                for (e = f(s), h = f(n), o = 0; null != (c = h[o]); ++o) e[o] && be(c, e[o]);
            if (t)
                if (u)
                    for (h = h || f(n), e = e || f(s), o = 0; null != (c = h[o]); o++) hu(c, e[o]);
                else hu(n, s);
            return e = f(s, "script"), e.length > 0 && li(e, !l && f(n, "script")), e = h = c = null, s
        },
        buildFragment: function(n, t, u, e) {
            for (var c, o, b, h, p, w, a, k = n.length, v = gr(t), l = [], y = 0; k > y; y++)
                if (o = n[y], o || 0 === o)
                    if ("object" === i.type(o)) i.merge(l, o.nodeType ? [o] : o);
                    else if (ce.test(o)) {
                for (h = h || v.appendChild(t.createElement("div")), p = (ru.exec(o) || ["", ""])[1].toLowerCase(), a = s[p] || s._default, h.innerHTML = a[1] + o.replace(iu, "<$1><\/$2>") + a[2], c = a[0]; c--;) h = h.lastChild;
                if (!r.leadingWhitespace && hi.test(o) && l.push(t.createTextNode(hi.exec(o)[0])), !r.tbody)
                    for (o = "table" !== p || uu.test(o) ? "<table>" !== a[1] || uu.test(o) ? 0 : h : h.firstChild, c = o && o.childNodes.length; c--;) i.nodeName(w = o.childNodes[c], "tbody") && !w.childNodes.length && o.removeChild(w);
                for (i.merge(l, h.childNodes), h.textContent = ""; h.firstChild;) h.removeChild(h.firstChild);
                h = v.lastChild
            } else l.push(t.createTextNode(o));
            for (h && v.removeChild(h), r.appendChecked || i.grep(f(l, "input"), we), y = 0; o = l[y++];)
                if ((!e || -1 === i.inArray(o, e)) && (b = i.contains(o.ownerDocument, o), h = f(v.appendChild(o), "script"), b && li(h), u))
                    for (c = 0; o = h[c++];) fu.test(o.type || "") && u.push(o);
            return h = null, v
        },
        cleanData: function(n, t) {
            for (var u, e, f, s, a = 0, h = i.expando, l = i.cache, v = r.deleteExpando, y = i.event.special; null != (u = n[a]); a++)
                if ((t || i.acceptData(u)) && (f = u[h], s = f && l[f])) {
                    if (s.events)
                        for (e in s.events) y[e] ? i.event.remove(u, e) : i.removeEvent(u, e, s.handle);
                    l[f] && (delete l[f], v ? delete u[h] : typeof u.removeAttribute !== o ? u.removeAttribute(h) : u[h] = null, c.push(f))
                }
        }
    });
    i.fn.extend({
        text: function(n) {
            return b(this, function(n) {
                return void 0 === n ? i.text(this) : this.empty().append((this[0] && this[0].ownerDocument || u).createTextNode(n))
            }, null, n, arguments.length)
        },
        append: function() {
            return this.domManip(arguments, function(n) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = eu(this, n);
                    t.appendChild(n)
                }
            })
        },
        prepend: function() {
            return this.domManip(arguments, function(n) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var t = eu(this, n);
                    t.insertBefore(n, t.firstChild)
                }
            })
        },
        before: function() {
            return this.domManip(arguments, function(n) {
                this.parentNode && this.parentNode.insertBefore(n, this)
            })
        },
        after: function() {
            return this.domManip(arguments, function(n) {
                this.parentNode && this.parentNode.insertBefore(n, this.nextSibling)
            })
        },
        remove: function(n, t) {
            for (var r, e = n ? i.filter(n, this) : this, u = 0; null != (r = e[u]); u++) t || 1 !== r.nodeType || i.cleanData(f(r)), r.parentNode && (t && i.contains(r.ownerDocument, r) && li(f(r, "script")), r.parentNode.removeChild(r));
            return this
        },
        empty: function() {
            for (var n, t = 0; null != (n = this[t]); t++) {
                for (1 === n.nodeType && i.cleanData(f(n, !1)); n.firstChild;) n.removeChild(n.firstChild);
                n.options && i.nodeName(n, "select") && (n.options.length = 0)
            }
            return this
        },
        clone: function(n, t) {
            return n = null == n ? !1 : n, t = null == t ? n : t, this.map(function() {
                return i.clone(this, n, t)
            })
        },
        html: function(n) {
            return b(this, function(n) {
                var t = this[0] || {},
                    u = 0,
                    e = this.length;
                if (void 0 === n) return 1 === t.nodeType ? t.innerHTML.replace(he, "") : void 0;
                if (!("string" != typeof n || le.test(n) || !r.htmlSerialize && tu.test(n) || !r.leadingWhitespace && hi.test(n) || s[(ru.exec(n) || ["", ""])[1].toLowerCase()])) {
                    n = n.replace(iu, "<$1><\/$2>");
                    try {
                        for (; e > u; u++) t = this[u] || {}, 1 === t.nodeType && (i.cleanData(f(t, !1)), t.innerHTML = n);
                        t = 0
                    } catch (o) {}
                }
                t && this.empty().append(n)
            }, null, n, arguments.length)
        },
        replaceWith: function() {
            var n = arguments[0];
            return this.domManip(arguments, function(t) {
                n = this.parentNode;
                i.cleanData(f(this));
                n && n.replaceChild(t, this)
            }), n && (n.length || n.nodeType) ? this : this.remove()
        },
        detach: function(n) {
            return this.remove(n, !0)
        },
        domManip: function(n, t) {
            n = ir.apply([], n);
            var h, u, c, o, v, s, e = 0,
                l = this.length,
                p = this,
                w = l - 1,
                a = n[0],
                y = i.isFunction(a);
            if (y || l > 1 && "string" == typeof a && !r.checkClone && ae.test(a)) return this.each(function(i) {
                var r = p.eq(i);
                y && (n[0] = a.call(this, i, r.html()));
                r.domManip(n, t)
            });
            if (l && (s = i.buildFragment(n, this[0].ownerDocument, !1, this), h = s.firstChild, 1 === s.childNodes.length && (s = h), h)) {
                for (o = i.map(f(s, "script"), ou), c = o.length; l > e; e++) u = s, e !== w && (u = i.clone(u, !0, !0), c && i.merge(o, f(u, "script"))), t.call(this[e], u, e);
                if (c)
                    for (v = o[o.length - 1].ownerDocument, i.map(o, su), e = 0; c > e; e++) u = o[e], fu.test(u.type || "") && !i._data(u, "globalEval") && i.contains(v, u) && (u.src ? i._evalUrl && i._evalUrl(u.src) : i.globalEval((u.text || u.textContent || u.innerHTML || "").replace(ye, "")));
                s = h = null
            }
            return this
        }
    });
    i.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(n, t) {
        i.fn[n] = function(n) {
            for (var u, r = 0, f = [], e = i(n), o = e.length - 1; o >= r; r++) u = r === o ? this : this.clone(!0), i(e[r])[t](u), ii.apply(f, u.get());
            return this.pushStack(f)
        }
    });
    ai = {};
    ! function() {
        var n;
        r.shrinkWrapBlocks = function() {
            if (null != n) return n;
            n = !1;
            var t, i, r;
            return i = u.getElementsByTagName("body")[0], i && i.style ? (t = u.createElement("div"), r = u.createElement("div"), r.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", i.appendChild(r).appendChild(t), typeof t.style.zoom !== o && (t.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:1px;width:1px;zoom:1", t.appendChild(u.createElement("div")).style.width = "5px", n = 3 !== t.offsetWidth), i.removeChild(r), n) : void 0
        }
    }();
    var lu = /^margin/,
        pt = new RegExp("^(" + at + ")(?!px)[a-z%]+$", "i"),
        k, d, ke = /^(top|right|bottom|left)$/;
    n.getComputedStyle ? (k = function(n) {
        return n.ownerDocument.defaultView.getComputedStyle(n, null)
    }, d = function(n, t, r) {
        var e, o, s, u, f = n.style;
        return r = r || k(n), u = r ? r.getPropertyValue(t) || r[t] : void 0, r && ("" !== u || i.contains(n.ownerDocument, n) || (u = i.style(n, t)), pt.test(u) && lu.test(t) && (e = f.width, o = f.minWidth, s = f.maxWidth, f.minWidth = f.maxWidth = f.width = u, u = r.width, f.width = e, f.minWidth = o, f.maxWidth = s)), void 0 === u ? u : u + ""
    }) : u.documentElement.currentStyle && (k = function(n) {
        return n.currentStyle
    }, d = function(n, t, i) {
        var o, f, e, r, u = n.style;
        return i = i || k(n), r = i ? i[t] : void 0, null == r && u && u[t] && (r = u[t]), pt.test(r) && !ke.test(t) && (o = u.left, f = n.runtimeStyle, e = f && f.left, e && (f.left = n.currentStyle.left), u.left = "fontSize" === t ? "1em" : r, r = u.pixelLeft + "px", u.left = o, e && (f.left = e)), void 0 === r ? r : r + "" || "auto"
    });
    ! function() {
        var f, t, l, o, s, e, h;
        if (f = u.createElement("div"), f.innerHTML = "  <link/><table><\/table><a href='/a'>a<\/a><input type='checkbox'/>", l = f.getElementsByTagName("a")[0], t = l && l.style) {
            t.cssText = "float:left;opacity:.5";
            r.opacity = "0.5" === t.opacity;
            r.cssFloat = !!t.cssFloat;
            f.style.backgroundClip = "content-box";
            f.cloneNode(!0).style.backgroundClip = "";
            r.clearCloneStyle = "content-box" === f.style.backgroundClip;
            r.boxSizing = "" === t.boxSizing || "" === t.MozBoxSizing || "" === t.WebkitBoxSizing;
            i.extend(r, {
                reliableHiddenOffsets: function() {
                    return null == e && c(), e
                },
                boxSizingReliable: function() {
                    return null == s && c(), s
                },
                pixelPosition: function() {
                    return null == o && c(), o
                },
                reliableMarginRight: function() {
                    return null == h && c(), h
                }
            });

            function c() {
                var i, r, f, t;
                r = u.getElementsByTagName("body")[0];
                r && r.style && (i = u.createElement("div"), f = u.createElement("div"), f.style.cssText = "position:absolute;border:0;width:0;height:0;top:0;left:-9999px", r.appendChild(f).appendChild(i), i.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;display:block;margin-top:1%;top:1%;border:1px;padding:1px;width:4px;position:absolute", o = s = !1, h = !0, n.getComputedStyle && (o = "1%" !== (n.getComputedStyle(i, null) || {}).top, s = "4px" === (n.getComputedStyle(i, null) || {
                    width: "4px"
                }).width, t = i.appendChild(u.createElement("div")), t.style.cssText = i.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;margin:0;border:0;padding:0", t.style.marginRight = t.style.width = "0", i.style.width = "1px", h = !parseFloat((n.getComputedStyle(t, null) || {}).marginRight)), i.innerHTML = "<table><tr><td><\/td><td>t<\/td><\/tr><\/table>", t = i.getElementsByTagName("td"), t[0].style.cssText = "margin:0;border:0;padding:0;display:none", e = 0 === t[0].offsetHeight, e && (t[0].style.display = "", t[1].style.display = "none", e = 0 === t[0].offsetHeight), r.removeChild(f))
            }
        }
    }();
    i.swap = function(n, t, i, r) {
        var f, u, e = {};
        for (u in t) e[u] = n.style[u], n.style[u] = t[u];
        f = i.apply(n, r || []);
        for (u in t) n.style[u] = e[u];
        return f
    };
    var vi = /alpha\([^)]*\)/i,
        de = /opacity\s*=\s*([^)]*)/,
        ge = /^(none|table(?!-c[ea]).+)/,
        no = new RegExp("^(" + at + ")(.*)$", "i"),
        to = new RegExp("^([+-])=(" + at + ")", "i"),
        io = {
            position: "absolute",
            visibility: "hidden",
            display: "block"
        },
        vu = {
            letterSpacing: "0",
            fontWeight: "400"
        },
        yu = ["Webkit", "O", "Moz", "ms"];
    i.extend({
        cssHooks: {
            opacity: {
                get: function(n, t) {
                    if (t) {
                        var i = d(n, "opacity");
                        return "" === i ? "1" : i
                    }
                }
            }
        },
        cssNumber: {
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: r.cssFloat ? "cssFloat" : "styleFloat"
        },
        style: function(n, t, u, f) {
            if (n && 3 !== n.nodeType && 8 !== n.nodeType && n.style) {
                var o, h, e, s = i.camelCase(t),
                    c = n.style;
                if (t = i.cssProps[s] || (i.cssProps[s] = pu(c, s)), e = i.cssHooks[t] || i.cssHooks[s], void 0 === u) return e && "get" in e && void 0 !== (o = e.get(n, !1, f)) ? o : c[t];
                if (h = typeof u, "string" === h && (o = to.exec(u)) && (u = (o[1] + 1) * o[2] + parseFloat(i.css(n, t)), h = "number"), null != u && u === u && ("number" !== h || i.cssNumber[s] || (u += "px"), r.clearCloneStyle || "" !== u || 0 !== t.indexOf("background") || (c[t] = "inherit"), !(e && "set" in e && void 0 === (u = e.set(n, u, f))))) try {
                    c[t] = u
                } catch (l) {}
            }
        },
        css: function(n, t, r, u) {
            var s, f, e, o = i.camelCase(t);
            return t = i.cssProps[o] || (i.cssProps[o] = pu(n.style, o)), e = i.cssHooks[t] || i.cssHooks[o], e && "get" in e && (f = e.get(n, !0, r)), void 0 === f && (f = d(n, t, u)), "normal" === f && t in vu && (f = vu[t]), "" === r || r ? (s = parseFloat(f), r === !0 || i.isNumeric(s) ? s || 0 : f) : f
        }
    });
    i.each(["height", "width"], function(n, t) {
        i.cssHooks[t] = {
            get: function(n, r, u) {
                if (r) return ge.test(i.css(n, "display")) && 0 === n.offsetWidth ? i.swap(n, io, function() {
                    return du(n, t, u)
                }) : du(n, t, u)
            },
            set: function(n, u, f) {
                var e = f && k(n);
                return bu(n, u, f ? ku(n, t, f, r.boxSizing && "border-box" === i.css(n, "boxSizing", !1, e), e) : 0)
            }
        }
    });
    r.opacity || (i.cssHooks.opacity = {
        get: function(n, t) {
            return de.test((t && n.currentStyle ? n.currentStyle.filter : n.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
        },
        set: function(n, t) {
            var r = n.style,
                u = n.currentStyle,
                e = i.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                f = u && u.filter || r.filter || "";
            r.zoom = 1;
            (t >= 1 || "" === t) && "" === i.trim(f.replace(vi, "")) && r.removeAttribute && (r.removeAttribute("filter"), "" === t || u && !u.filter) || (r.filter = vi.test(f) ? f.replace(vi, e) : f + " " + e)
        }
    });
    i.cssHooks.marginRight = au(r.reliableMarginRight, function(n, t) {
        if (t) return i.swap(n, {
            display: "inline-block"
        }, d, [n, "marginRight"])
    });
    i.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(n, t) {
        i.cssHooks[n + t] = {
            expand: function(i) {
                for (var r = 0, f = {}, u = "string" == typeof i ? i.split(" ") : [i]; 4 > r; r++) f[n + w[r] + t] = u[r] || u[r - 2] || u[0];
                return f
            }
        };
        lu.test(n) || (i.cssHooks[n + t].set = bu)
    });
    i.fn.extend({
        css: function(n, t) {
            return b(this, function(n, t, r) {
                var f, e, o = {},
                    u = 0;
                if (i.isArray(t)) {
                    for (f = k(n), e = t.length; e > u; u++) o[t[u]] = i.css(n, t[u], !1, f);
                    return o
                }
                return void 0 !== r ? i.style(n, t, r) : i.css(n, t)
            }, n, t, arguments.length > 1)
        },
        show: function() {
            return wu(this, !0)
        },
        hide: function() {
            return wu(this)
        },
        toggle: function(n) {
            return "boolean" == typeof n ? n ? this.show() : this.hide() : this.each(function() {
                et(this) ? i(this).show() : i(this).hide()
            })
        }
    });
    i.Tween = e;
    e.prototype = {
        constructor: e,
        init: function(n, t, r, u, f, e) {
            this.elem = n;
            this.prop = r;
            this.easing = f || "swing";
            this.options = t;
            this.start = this.now = this.cur();
            this.end = u;
            this.unit = e || (i.cssNumber[r] ? "" : "px")
        },
        cur: function() {
            var n = e.propHooks[this.prop];
            return n && n.get ? n.get(this) : e.propHooks._default.get(this)
        },
        run: function(n) {
            var r, t = e.propHooks[this.prop];
            return this.pos = r = this.options.duration ? i.easing[this.easing](n, this.options.duration * n, 0, 1, this.options.duration) : n, this.now = (this.end - this.start) * r + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), t && t.set ? t.set(this) : e.propHooks._default.set(this), this
        }
    };
    e.prototype.init.prototype = e.prototype;
    e.propHooks = {
        _default: {
            get: function(n) {
                var t;
                return null == n.elem[n.prop] || n.elem.style && null != n.elem.style[n.prop] ? (t = i.css(n.elem, n.prop, ""), t && "auto" !== t ? t : 0) : n.elem[n.prop]
            },
            set: function(n) {
                i.fx.step[n.prop] ? i.fx.step[n.prop](n) : n.elem.style && (null != n.elem.style[i.cssProps[n.prop]] || i.cssHooks[n.prop]) ? i.style(n.elem, n.prop, n.now + n.unit) : n.elem[n.prop] = n.now
            }
        }
    };
    e.propHooks.scrollTop = e.propHooks.scrollLeft = {
        set: function(n) {
            n.elem.nodeType && n.elem.parentNode && (n.elem[n.prop] = n.now)
        }
    };
    i.easing = {
        linear: function(n) {
            return n
        },
        swing: function(n) {
            return .5 - Math.cos(n * Math.PI) / 2
        }
    };
    i.fx = e.prototype.init;
    i.fx.step = {};
    var rt, wt, ro = /^(?:toggle|show|hide)$/,
        gu = new RegExp("^(?:([+-])=|)(" + at + ")([a-z%]*)$", "i"),
        uo = /queueHooks$/,
        bt = [fo],
        st = {
            "*": [function(n, t) {
                var f = this.createTween(n, t),
                    s = f.cur(),
                    r = gu.exec(t),
                    e = r && r[3] || (i.cssNumber[n] ? "" : "px"),
                    u = (i.cssNumber[n] || "px" !== e && +s) && gu.exec(i.css(f.elem, n)),
                    o = 1,
                    h = 20;
                if (u && u[3] !== e) {
                    e = e || u[3];
                    r = r || [];
                    u = +s || 1;
                    do o = o || ".5", u /= o, i.style(f.elem, n, u + e); while (o !== (o = f.cur() / s) && 1 !== o && --h)
                }
                return r && (u = f.start = +u || +s || 0, f.unit = e, f.end = r[1] ? u + (r[1] + 1) * r[2] : +r[2]), f
            }]
        };
    i.Animation = i.extend(rf, {
        tweener: function(n, t) {
            i.isFunction(n) ? (t = n, n = ["*"]) : n = n.split(" ");
            for (var r, u = 0, f = n.length; f > u; u++) r = n[u], st[r] = st[r] || [], st[r].unshift(t)
        },
        prefilter: function(n, t) {
            t ? bt.unshift(n) : bt.push(n)
        }
    });
    i.speed = function(n, t, r) {
        var u = n && "object" == typeof n ? i.extend({}, n) : {
            complete: r || !r && t || i.isFunction(n) && n,
            duration: n,
            easing: r && t || t && !i.isFunction(t) && t
        };
        return u.duration = i.fx.off ? 0 : "number" == typeof u.duration ? u.duration : u.duration in i.fx.speeds ? i.fx.speeds[u.duration] : i.fx.speeds._default, (null == u.queue || u.queue === !0) && (u.queue = "fx"), u.old = u.complete, u.complete = function() {
            i.isFunction(u.old) && u.old.call(this);
            u.queue && i.dequeue(this, u.queue)
        }, u
    };
    i.fn.extend({
        fadeTo: function(n, t, i, r) {
            return this.filter(et).css("opacity", 0).show().end().animate({
                opacity: t
            }, n, i, r)
        },
        animate: function(n, t, r, u) {
            var o = i.isEmptyObject(n),
                e = i.speed(t, r, u),
                f = function() {
                    var t = rf(this, i.extend({}, n), e);
                    (o || i._data(this, "finish")) && t.stop(!0)
                };
            return f.finish = f, o || e.queue === !1 ? this.each(f) : this.queue(e.queue, f)
        },
        stop: function(n, t, r) {
            var u = function(n) {
                var t = n.stop;
                delete n.stop;
                t(r)
            };
            return "string" != typeof n && (r = t, t = n, n = void 0), t && n !== !1 && this.queue(n || "fx", []), this.each(function() {
                var o = !0,
                    t = null != n && n + "queueHooks",
                    e = i.timers,
                    f = i._data(this);
                if (t) f[t] && f[t].stop && u(f[t]);
                else
                    for (t in f) f[t] && f[t].stop && uo.test(t) && u(f[t]);
                for (t = e.length; t--;) e[t].elem !== this || null != n && e[t].queue !== n || (e[t].anim.stop(r), o = !1, e.splice(t, 1));
                (o || !r) && i.dequeue(this, n)
            })
        },
        finish: function(n) {
            return n !== !1 && (n = n || "fx"), this.each(function() {
                var t, f = i._data(this),
                    r = f[n + "queue"],
                    e = f[n + "queueHooks"],
                    u = i.timers,
                    o = r ? r.length : 0;
                for (f.finish = !0, i.queue(this, n, []), e && e.stop && e.stop.call(this, !0), t = u.length; t--;) u[t].elem === this && u[t].queue === n && (u[t].anim.stop(!0), u.splice(t, 1));
                for (t = 0; o > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                delete f.finish
            })
        }
    });
    i.each(["toggle", "show", "hide"], function(n, t) {
        var r = i.fn[t];
        i.fn[t] = function(n, i, u) {
            return null == n || "boolean" == typeof n ? r.apply(this, arguments) : this.animate(kt(t, !0), n, i, u)
        }
    });
    i.each({
        slideDown: kt("show"),
        slideUp: kt("hide"),
        slideToggle: kt("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(n, t) {
        i.fn[n] = function(n, i, r) {
            return this.animate(t, n, i, r)
        }
    });
    i.timers = [];
    i.fx.tick = function() {
        var r, n = i.timers,
            t = 0;
        for (rt = i.now(); t < n.length; t++) r = n[t], r() || n[t] !== r || n.splice(t--, 1);
        n.length || i.fx.stop();
        rt = void 0
    };
    i.fx.timer = function(n) {
        i.timers.push(n);
        n() ? i.fx.start() : i.timers.pop()
    };
    i.fx.interval = 13;
    i.fx.start = function() {
        wt || (wt = setInterval(i.fx.tick, i.fx.interval))
    };
    i.fx.stop = function() {
        clearInterval(wt);
        wt = null
    };
    i.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    };
    i.fn.delay = function(n, t) {
            return n = i.fx ? i.fx.speeds[n] || n : n, t = t || "fx", this.queue(t, function(t, i) {
                var r = setTimeout(t, n);
                i.stop = function() {
                    clearTimeout(r)
                }
            })
        },
        function() {
            var n, t, f, i, e;
            t = u.createElement("div");
            t.setAttribute("className", "t");
            t.innerHTML = "  <link/><table><\/table><a href='/a'>a<\/a><input type='checkbox'/>";
            i = t.getElementsByTagName("a")[0];
            f = u.createElement("select");
            e = f.appendChild(u.createElement("option"));
            n = t.getElementsByTagName("input")[0];
            i.style.cssText = "top:1px";
            r.getSetAttribute = "t" !== t.className;
            r.style = /top/.test(i.getAttribute("style"));
            r.hrefNormalized = "/a" === i.getAttribute("href");
            r.checkOn = !!n.value;
            r.optSelected = e.selected;
            r.enctype = !!u.createElement("form").enctype;
            f.disabled = !0;
            r.optDisabled = !e.disabled;
            n = u.createElement("input");
            n.setAttribute("value", "");
            r.input = "" === n.getAttribute("value");
            n.value = "t";
            n.setAttribute("type", "radio");
            r.radioValue = "t" === n.value
        }();
    uf = /\r/g;
    i.fn.extend({
        val: function(n) {
            var t, r, f, u = this[0];
            return arguments.length ? (f = i.isFunction(n), this.each(function(r) {
                var u;
                1 === this.nodeType && (u = f ? n.call(this, r, i(this).val()) : n, null == u ? u = "" : "number" == typeof u ? u += "" : i.isArray(u) && (u = i.map(u, function(n) {
                    return null == n ? "" : n + ""
                })), t = i.valHooks[this.type] || i.valHooks[this.nodeName.toLowerCase()], t && "set" in t && void 0 !== t.set(this, u, "value") || (this.value = u))
            })) : u ? (t = i.valHooks[u.type] || i.valHooks[u.nodeName.toLowerCase()], t && "get" in t && void 0 !== (r = t.get(u, "value")) ? r : (r = u.value, "string" == typeof r ? r.replace(uf, "") : null == r ? "" : r)) : void 0
        }
    });
    i.extend({
        valHooks: {
            option: {
                get: function(n) {
                    var t = i.find.attr(n, "value");
                    return null != t ? t : i.trim(i.text(n))
                }
            },
            select: {
                get: function(n) {
                    for (var o, t, s = n.options, u = n.selectedIndex, f = "select-one" === n.type || 0 > u, h = f ? null : [], c = f ? u + 1 : s.length, e = 0 > u ? c : f ? u : 0; c > e; e++)
                        if (t = s[e], !(!t.selected && e !== u || (r.optDisabled ? t.disabled : null !== t.getAttribute("disabled")) || t.parentNode.disabled && i.nodeName(t.parentNode, "optgroup"))) {
                            if (o = i(t).val(), f) return o;
                            h.push(o)
                        }
                    return h
                },
                set: function(n, t) {
                    for (var f, r, u = n.options, o = i.makeArray(t), e = u.length; e--;)
                        if (r = u[e], i.inArray(i.valHooks.option.get(r), o) >= 0) try {
                            r.selected = f = !0
                        } catch (s) {
                            r.scrollHeight
                        } else r.selected = !1;
                    return f || (n.selectedIndex = -1), u
                }
            }
        }
    });
    i.each(["radio", "checkbox"], function() {
        i.valHooks[this] = {
            set: function(n, t) {
                if (i.isArray(t)) return n.checked = i.inArray(i(n).val(), t) >= 0
            }
        };
        r.checkOn || (i.valHooks[this].get = function(n) {
            return null === n.getAttribute("value") ? "on" : n.value
        })
    });
    var ut, ff, v = i.expr.attrHandle,
        yi = /^(?:checked|selected)$/i,
        g = r.getSetAttribute,
        dt = r.input;
    i.fn.extend({
        attr: function(n, t) {
            return b(this, i.attr, n, t, arguments.length > 1)
        },
        removeAttr: function(n) {
            return this.each(function() {
                i.removeAttr(this, n)
            })
        }
    });
    i.extend({
        attr: function(n, t, r) {
            var u, f, e = n.nodeType;
            if (n && 3 !== e && 8 !== e && 2 !== e) return typeof n.getAttribute === o ? i.prop(n, t, r) : (1 === e && i.isXMLDoc(n) || (t = t.toLowerCase(), u = i.attrHooks[t] || (i.expr.match.bool.test(t) ? ff : ut)), void 0 === r ? u && "get" in u && null !== (f = u.get(n, t)) ? f : (f = i.find.attr(n, t), null == f ? void 0 : f) : null !== r ? u && "set" in u && void 0 !== (f = u.set(n, r, t)) ? f : (n.setAttribute(t, r + ""), r) : void i.removeAttr(n, t))
        },
        removeAttr: function(n, t) {
            var r, u, e = 0,
                f = t && t.match(h);
            if (f && 1 === n.nodeType)
                while (r = f[e++]) u = i.propFix[r] || r, i.expr.match.bool.test(r) ? dt && g || !yi.test(r) ? n[u] = !1 : n[i.camelCase("default-" + r)] = n[u] = !1 : i.attr(n, r, ""), n.removeAttribute(g ? r : u)
        },
        attrHooks: {
            type: {
                set: function(n, t) {
                    if (!r.radioValue && "radio" === t && i.nodeName(n, "input")) {
                        var u = n.value;
                        return n.setAttribute("type", t), u && (n.value = u), t
                    }
                }
            }
        }
    });
    ff = {
        set: function(n, t, r) {
            return t === !1 ? i.removeAttr(n, r) : dt && g || !yi.test(r) ? n.setAttribute(!g && i.propFix[r] || r, r) : n[i.camelCase("default-" + r)] = n[r] = !0, r
        }
    };
    i.each(i.expr.match.bool.source.match(/\w+/g), function(n, t) {
        var r = v[t] || i.find.attr;
        v[t] = dt && g || !yi.test(t) ? function(n, t, i) {
            var u, f;
            return i || (f = v[t], v[t] = u, u = null != r(n, t, i) ? t.toLowerCase() : null, v[t] = f), u
        } : function(n, t, r) {
            if (!r) return n[i.camelCase("default-" + t)] ? t.toLowerCase() : null
        }
    });
    dt && g || (i.attrHooks.value = {
        set: function(n, t, r) {
            return i.nodeName(n, "input") ? void(n.defaultValue = t) : ut && ut.set(n, t, r)
        }
    });
    g || (ut = {
        set: function(n, t, i) {
            var r = n.getAttributeNode(i);
            return r || n.setAttributeNode(r = n.ownerDocument.createAttribute(i)), r.value = t += "", "value" === i || t === n.getAttribute(i) ? t : void 0
        }
    }, v.id = v.name = v.coords = function(n, t, i) {
        var r;
        if (!i) return (r = n.getAttributeNode(t)) && "" !== r.value ? r.value : null
    }, i.valHooks.button = {
        get: function(n, t) {
            var i = n.getAttributeNode(t);
            if (i && i.specified) return i.value
        },
        set: ut.set
    }, i.attrHooks.contenteditable = {
        set: function(n, t, i) {
            ut.set(n, "" === t ? !1 : t, i)
        }
    }, i.each(["width", "height"], function(n, t) {
        i.attrHooks[t] = {
            set: function(n, i) {
                if ("" === i) return (n.setAttribute(t, "auto"), i)
            }
        }
    }));
    r.style || (i.attrHooks.style = {
        get: function(n) {
            return n.style.cssText || void 0
        },
        set: function(n, t) {
            return n.style.cssText = t + ""
        }
    });
    ef = /^(?:input|select|textarea|button|object)$/i; of = /^(?:a|area)$/i;
    i.fn.extend({
        prop: function(n, t) {
            return b(this, i.prop, n, t, arguments.length > 1)
        },
        removeProp: function(n) {
            return n = i.propFix[n] || n, this.each(function() {
                try {
                    this[n] = void 0;
                    delete this[n]
                } catch (t) {}
            })
        }
    });
    i.extend({
        propFix: {
            "for": "htmlFor",
            "class": "className"
        },
        prop: function(n, t, r) {
            var f, u, o, e = n.nodeType;
            if (n && 3 !== e && 8 !== e && 2 !== e) return o = 1 !== e || !i.isXMLDoc(n), o && (t = i.propFix[t] || t, u = i.propHooks[t]), void 0 !== r ? u && "set" in u && void 0 !== (f = u.set(n, r, t)) ? f : n[t] = r : u && "get" in u && null !== (f = u.get(n, t)) ? f : n[t]
        },
        propHooks: {
            tabIndex: {
                get: function(n) {
                    var t = i.find.attr(n, "tabindex");
                    return t ? parseInt(t, 10) : ef.test(n.nodeName) || of .test(n.nodeName) && n.href ? 0 : -1
                }
            }
        }
    });
    r.hrefNormalized || i.each(["href", "src"], function(n, t) {
        i.propHooks[t] = {
            get: function(n) {
                return n.getAttribute(t, 4)
            }
        }
    });
    r.optSelected || (i.propHooks.selected = {
        get: function(n) {
            var t = n.parentNode;
            return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
        }
    });
    i.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        i.propFix[this.toLowerCase()] = this
    });
    r.enctype || (i.propFix.enctype = "encoding");
    gt = /[\t\r\n\f]/g;
    i.fn.extend({
        addClass: function(n) {
            var o, t, r, u, s, f, e = 0,
                c = this.length,
                l = "string" == typeof n && n;
            if (i.isFunction(n)) return this.each(function(t) {
                i(this).addClass(n.call(this, t, this.className))
            });
            if (l)
                for (o = (n || "").match(h) || []; c > e; e++)
                    if (t = this[e], r = 1 === t.nodeType && (t.className ? (" " + t.className + " ").replace(gt, " ") : " ")) {
                        for (s = 0; u = o[s++];) r.indexOf(" " + u + " ") < 0 && (r += u + " ");
                        f = i.trim(r);
                        t.className !== f && (t.className = f)
                    }
            return this
        },
        removeClass: function(n) {
            var o, t, r, u, s, f, e = 0,
                c = this.length,
                l = 0 === arguments.length || "string" == typeof n && n;
            if (i.isFunction(n)) return this.each(function(t) {
                i(this).removeClass(n.call(this, t, this.className))
            });
            if (l)
                for (o = (n || "").match(h) || []; c > e; e++)
                    if (t = this[e], r = 1 === t.nodeType && (t.className ? (" " + t.className + " ").replace(gt, " ") : "")) {
                        for (s = 0; u = o[s++];)
                            while (r.indexOf(" " + u + " ") >= 0) r = r.replace(" " + u + " ", " ");
                        f = n ? i.trim(r) : "";
                        t.className !== f && (t.className = f)
                    }
            return this
        },
        toggleClass: function(n, t) {
            var r = typeof n;
            return "boolean" == typeof t && "string" === r ? t ? this.addClass(n) : this.removeClass(n) : this.each(i.isFunction(n) ? function(r) {
                i(this).toggleClass(n.call(this, r, this.className, t), t)
            } : function() {
                if ("string" === r)
                    for (var t, f = 0, u = i(this), e = n.match(h) || []; t = e[f++];) u.hasClass(t) ? u.removeClass(t) : u.addClass(t);
                else(r === o || "boolean" === r) && (this.className && i._data(this, "__className__", this.className), this.className = this.className || n === !1 ? "" : i._data(this, "__className__") || "")
            })
        },
        hasClass: function(n) {
            for (var i = " " + n + " ", t = 0, r = this.length; r > t; t++)
                if (1 === this[t].nodeType && (" " + this[t].className + " ").replace(gt, " ").indexOf(i) >= 0) return !0;
            return !1
        }
    });
    i.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(n, t) {
        i.fn[t] = function(n, i) {
            return arguments.length > 0 ? this.on(t, null, n, i) : this.trigger(t)
        }
    });
    i.fn.extend({
        hover: function(n, t) {
            return this.mouseenter(n).mouseleave(t || n)
        },
        bind: function(n, t, i) {
            return this.on(n, null, t, i)
        },
        unbind: function(n, t) {
            return this.off(n, null, t)
        },
        delegate: function(n, t, i, r) {
            return this.on(t, n, i, r)
        },
        undelegate: function(n, t, i) {
            return 1 === arguments.length ? this.off(n, "**") : this.off(t, n || "**", i)
        }
    });
    var pi = i.now(),
        wi = /\?/,
        oo = /(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g;
    i.parseJSON = function(t) {
        if (n.JSON && n.JSON.parse) return n.JSON.parse(t + "");
        var f, r = null,
            u = i.trim(t + "");
        return u && !i.trim(u.replace(oo, function(n, t, i, u) {
            return f && t && (r = 0), 0 === r ? n : (f = i || t, r += !u - !i, "")
        })) ? Function("return " + u)() : i.error("Invalid JSON: " + t)
    };
    i.parseXML = function(t) {
        var r, u;
        if (!t || "string" != typeof t) return null;
        try {
            n.DOMParser ? (u = new DOMParser, r = u.parseFromString(t, "text/xml")) : (r = new ActiveXObject("Microsoft.XMLDOM"), r.async = "false", r.loadXML(t))
        } catch (f) {
            r = void 0
        }
        return r && r.documentElement && !r.getElementsByTagName("parsererror").length || i.error("Invalid XML: " + t), r
    };
    var nt, y, so = /#.*$/,
        sf = /([?&])_=[^&]*/,
        ho = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
        co = /^(?:GET|HEAD)$/,
        lo = /^\/\//,
        hf = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
        cf = {},
        bi = {},
        lf = "*/".concat("*");
    try {
        y = location.href
    } catch (ns) {
        y = u.createElement("a");
        y.href = "";
        y = y.href
    }
    nt = hf.exec(y.toLowerCase()) || [];
    i.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: y,
            type: "GET",
            isLocal: /^(?:about|app|app-storage|.+-extension|file|res|widget):$/.test(nt[1]),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": lf,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /xml/,
                html: /html/,
                json: /json/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": i.parseJSON,
                "text xml": i.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(n, t) {
            return t ? ki(ki(n, i.ajaxSettings), t) : ki(i.ajaxSettings, n)
        },
        ajaxPrefilter: af(cf),
        ajaxTransport: af(bi),
        ajax: function(n, t) {
            function w(n, t, s, h) {
                var v, it, nt, y, w, c = t;
                2 !== e && (e = 2, k && clearTimeout(k), a = void 0, b = h || "", u.readyState = n > 0 ? 4 : 0, v = n >= 200 && 300 > n || 304 === n, s && (y = ao(r, u, s)), y = vo(r, y, u, v), v ? (r.ifModified && (w = u.getResponseHeader("Last-Modified"), w && (i.lastModified[f] = w), w = u.getResponseHeader("etag"), w && (i.etag[f] = w)), 204 === n || "HEAD" === r.type ? c = "nocontent" : 304 === n ? c = "notmodified" : (c = y.state, it = y.data, nt = y.error, v = !nt)) : (nt = c, (n || !c) && (c = "error", 0 > n && (n = 0))), u.status = n, u.statusText = (t || c) + "", v ? g.resolveWith(o, [it, c, u]) : g.rejectWith(o, [u, c, nt]), u.statusCode(p), p = void 0, l && d.trigger(v ? "ajaxSuccess" : "ajaxError", [u, r, v ? it : nt]), tt.fireWith(o, [u, c]), l && (d.trigger("ajaxComplete", [u, r]), --i.active || i.event.trigger("ajaxStop")))
            }
            "object" == typeof n && (t = n, n = void 0);
            t = t || {};
            var s, c, f, b, k, l, a, v, r = i.ajaxSetup({}, t),
                o = r.context || r,
                d = r.context && (o.nodeType || o.jquery) ? i(o) : i.event,
                g = i.Deferred(),
                tt = i.Callbacks("once memory"),
                p = r.statusCode || {},
                it = {},
                rt = {},
                e = 0,
                ut = "canceled",
                u = {
                    readyState: 0,
                    getResponseHeader: function(n) {
                        var t;
                        if (2 === e) {
                            if (!v)
                                for (v = {}; t = ho.exec(b);) v[t[1].toLowerCase()] = t[2];
                            t = v[n.toLowerCase()]
                        }
                        return null == t ? null : t
                    },
                    getAllResponseHeaders: function() {
                        return 2 === e ? b : null
                    },
                    setRequestHeader: function(n, t) {
                        var i = n.toLowerCase();
                        return e || (n = rt[i] = rt[i] || n, it[n] = t), this
                    },
                    overrideMimeType: function(n) {
                        return e || (r.mimeType = n), this
                    },
                    statusCode: function(n) {
                        var t;
                        if (n)
                            if (2 > e)
                                for (t in n) p[t] = [p[t], n[t]];
                            else u.always(n[u.status]);
                        return this
                    },
                    abort: function(n) {
                        var t = n || ut;
                        return a && a.abort(t), w(0, t), this
                    }
                };
            if (g.promise(u).complete = tt.add, u.success = u.done, u.error = u.fail, r.url = ((n || r.url || y) + "").replace(so, "").replace(lo, nt[1] + "//"), r.type = t.method || t.type || r.method || r.type, r.dataTypes = i.trim(r.dataType || "*").toLowerCase().match(h) || [""], null == r.crossDomain && (s = hf.exec(r.url.toLowerCase()), r.crossDomain = !(!s || s[1] === nt[1] && s[2] === nt[2] && (s[3] || ("http:" === s[1] ? "80" : "443")) === (nt[3] || ("http:" === nt[1] ? "80" : "443")))), r.data && r.processData && "string" != typeof r.data && (r.data = i.param(r.data, r.traditional)), vf(cf, r, t, u), 2 === e) return u;
            l = r.global;
            l && 0 == i.active++ && i.event.trigger("ajaxStart");
            r.type = r.type.toUpperCase();
            r.hasContent = !co.test(r.type);
            f = r.url;
            r.hasContent || (r.data && (f = r.url += (wi.test(f) ? "&" : "?") + r.data, delete r.data), r.cache === !1 && (r.url = sf.test(f) ? f.replace(sf, "$1_=" + pi++) : f + (wi.test(f) ? "&" : "?") + "_=" + pi++));
            r.ifModified && (i.lastModified[f] && u.setRequestHeader("If-Modified-Since", i.lastModified[f]), i.etag[f] && u.setRequestHeader("If-None-Match", i.etag[f]));
            (r.data && r.hasContent && r.contentType !== !1 || t.contentType) && u.setRequestHeader("Content-Type", r.contentType);
            u.setRequestHeader("Accept", r.dataTypes[0] && r.accepts[r.dataTypes[0]] ? r.accepts[r.dataTypes[0]] + ("*" !== r.dataTypes[0] ? ", " + lf + "; q=0.01" : "") : r.accepts["*"]);
            for (c in r.headers) u.setRequestHeader(c, r.headers[c]);
            if (r.beforeSend && (r.beforeSend.call(o, u, r) === !1 || 2 === e)) return u.abort();
            ut = "abort";
            for (c in {
                    success: 1,
                    error: 1,
                    complete: 1
                }) u[c](r[c]);
            if (a = vf(bi, r, t, u)) {
                u.readyState = 1;
                l && d.trigger("ajaxSend", [u, r]);
                r.async && r.timeout > 0 && (k = setTimeout(function() {
                    u.abort("timeout")
                }, r.timeout));
                try {
                    e = 1;
                    a.send(it, w)
                } catch (ft) {
                    if (!(2 > e)) throw ft;
                    w(-1, ft)
                }
            } else w(-1, "No Transport");
            return u
        },
        getJSON: function(n, t, r) {
            return i.get(n, t, r, "json")
        },
        getScript: function(n, t) {
            return i.get(n, void 0, t, "script")
        }
    });
    i.each(["get", "post"], function(n, t) {
        i[t] = function(n, r, u, f) {
            return i.isFunction(r) && (f = f || u, u = r, r = void 0), i.ajax({
                url: n,
                type: t,
                dataType: f,
                data: r,
                success: u
            })
        }
    });
    i.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(n, t) {
        i.fn[t] = function(n) {
            return this.on(t, n)
        }
    });
    i._evalUrl = function(n) {
        return i.ajax({
            url: n,
            type: "GET",
            dataType: "script",
            async: !1,
            global: !1,
            throws: !0
        })
    };
    i.fn.extend({
        wrapAll: function(n) {
            if (i.isFunction(n)) return this.each(function(t) {
                i(this).wrapAll(n.call(this, t))
            });
            if (this[0]) {
                var t = i(n, this[0].ownerDocument).eq(0).clone(!0);
                this[0].parentNode && t.insertBefore(this[0]);
                t.map(function() {
                    for (var n = this; n.firstChild && 1 === n.firstChild.nodeType;) n = n.firstChild;
                    return n
                }).append(this)
            }
            return this
        },
        wrapInner: function(n) {
            return this.each(i.isFunction(n) ? function(t) {
                i(this).wrapInner(n.call(this, t))
            } : function() {
                var t = i(this),
                    r = t.contents();
                r.length ? r.wrapAll(n) : t.append(n)
            })
        },
        wrap: function(n) {
            var t = i.isFunction(n);
            return this.each(function(r) {
                i(this).wrapAll(t ? n.call(this, r) : n)
            })
        },
        unwrap: function() {
            return this.parent().each(function() {
                i.nodeName(this, "body") || i(this).replaceWith(this.childNodes)
            }).end()
        }
    });
    i.expr.filters.hidden = function(n) {
        return n.offsetWidth <= 0 && n.offsetHeight <= 0 || !r.reliableHiddenOffsets() && "none" === (n.style && n.style.display || i.css(n, "display"))
    };
    i.expr.filters.visible = function(n) {
        return !i.expr.filters.hidden(n)
    };
    var yo = /%20/g,
        po = /\[\]$/,
        yf = /\r?\n/g,
        wo = /^(?:submit|button|image|reset|file)$/i,
        bo = /^(?:input|select|textarea|keygen)/i;
    i.param = function(n, t) {
        var r, u = [],
            f = function(n, t) {
                t = i.isFunction(t) ? t() : null == t ? "" : t;
                u[u.length] = encodeURIComponent(n) + "=" + encodeURIComponent(t)
            };
        if (void 0 === t && (t = i.ajaxSettings && i.ajaxSettings.traditional), i.isArray(n) || n.jquery && !i.isPlainObject(n)) i.each(n, function() {
            f(this.name, this.value)
        });
        else
            for (r in n) di(r, n[r], t, f);
        return u.join("&").replace(yo, "+")
    };
    i.fn.extend({
        serialize: function() {
            return i.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var n = i.prop(this, "elements");
                return n ? i.makeArray(n) : this
            }).filter(function() {
                var n = this.type;
                return this.name && !i(this).is(":disabled") && bo.test(this.nodeName) && !wo.test(n) && (this.checked || !oi.test(n))
            }).map(function(n, t) {
                var r = i(this).val();
                return null == r ? null : i.isArray(r) ? i.map(r, function(n) {
                    return {
                        name: t.name,
                        value: n.replace(yf, "\r\n")
                    }
                }) : {
                    name: t.name,
                    value: r.replace(yf, "\r\n")
                }
            }).get()
        }
    });
    i.ajaxSettings.xhr = void 0 !== n.ActiveXObject ? function() {
        return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && pf() || go()
    } : pf;
    var ko = 0,
        ni = {},
        ht = i.ajaxSettings.xhr();
    return n.ActiveXObject && i(n).on("unload", function() {
        for (var n in ni) ni[n](void 0, !0)
    }), r.cors = !!ht && "withCredentials" in ht, ht = r.ajax = !!ht, ht && i.ajaxTransport(function(n) {
        if (!n.crossDomain || r.cors) {
            var t;
            return {
                send: function(r, u) {
                    var e, f = n.xhr(),
                        o = ++ko;
                    if (f.open(n.type, n.url, n.async, n.username, n.password), n.xhrFields)
                        for (e in n.xhrFields) f[e] = n.xhrFields[e];
                    n.mimeType && f.overrideMimeType && f.overrideMimeType(n.mimeType);
                    n.crossDomain || r["X-Requested-With"] || (r["X-Requested-With"] = "XMLHttpRequest");
                    for (e in r) void 0 !== r[e] && f.setRequestHeader(e, r[e] + "");
                    f.send(n.hasContent && n.data || null);
                    t = function(r, e) {
                        var s, c, h;
                        if (t && (e || 4 === f.readyState))
                            if (delete ni[o], t = void 0, f.onreadystatechange = i.noop, e) 4 !== f.readyState && f.abort();
                            else {
                                h = {};
                                s = f.status;
                                "string" == typeof f.responseText && (h.text = f.responseText);
                                try {
                                    c = f.statusText
                                } catch (l) {
                                    c = ""
                                }
                                s || !n.isLocal || n.crossDomain ? 1223 === s && (s = 204) : s = h.text ? 200 : 404
                            }
                        h && u(s, c, h, f.getAllResponseHeaders())
                    };
                    n.async ? 4 === f.readyState ? setTimeout(t) : f.onreadystatechange = ni[o] = t : t()
                },
                abort: function() {
                    t && t(void 0, !0)
                }
            }
        }
    }), i.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /(?:java|ecma)script/
        },
        converters: {
            "text script": function(n) {
                return i.globalEval(n), n
            }
        }
    }), i.ajaxPrefilter("script", function(n) {
        void 0 === n.cache && (n.cache = !1);
        n.crossDomain && (n.type = "GET", n.global = !1)
    }), i.ajaxTransport("script", function(n) {
        if (n.crossDomain) {
            var t, r = u.head || i("head")[0] || u.documentElement;
            return {
                send: function(i, f) {
                    t = u.createElement("script");
                    t.async = !0;
                    n.scriptCharset && (t.charset = n.scriptCharset);
                    t.src = n.url;
                    t.onload = t.onreadystatechange = function(n, i) {
                        (i || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, i || f(200, "success"))
                    };
                    r.insertBefore(t, r.firstChild)
                },
                abort: function() {
                    t && t.onload(void 0, !0)
                }
            }
        }
    }), gi = [], ti = /(=)\?(?=&|$)|\?\?/, i.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var n = gi.pop() || i.expando + "_" + pi++;
            return this[n] = !0, n
        }
    }), i.ajaxPrefilter("json jsonp", function(t, r, u) {
        var f, o, e, s = t.jsonp !== !1 && (ti.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && ti.test(t.data) && "data");
        if (s || "jsonp" === t.dataTypes[0]) return (f = t.jsonpCallback = i.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(ti, "$1" + f) : t.jsonp !== !1 && (t.url += (wi.test(t.url) ? "&" : "?") + t.jsonp + "=" + f), t.converters["script json"] = function() {
            return e || i.error(f + " was not called"), e[0]
        }, t.dataTypes[0] = "json", o = n[f], n[f] = function() {
            e = arguments
        }, u.always(function() {
            n[f] = o;
            t[f] && (t.jsonpCallback = r.jsonpCallback, gi.push(f));
            e && i.isFunction(o) && o(e[0]);
            e = o = void 0
        }), "script")
    }), i.parseHTML = function(n, t, r) {
        if (!n || "string" != typeof n) return null;
        "boolean" == typeof t && (r = t, t = !1);
        t = t || u;
        var f = er.exec(n),
            e = !r && [];
        return f ? [t.createElement(f[1])] : (f = i.buildFragment([n], t, e), e && e.length && i(e).remove(), i.merge([], f.childNodes))
    }, nr = i.fn.load, i.fn.load = function(n, t, r) {
        if ("string" != typeof n && nr) return nr.apply(this, arguments);
        var u, o, s, f = this,
            e = n.indexOf(" ");
        return e >= 0 && (u = i.trim(n.slice(e, n.length)), n = n.slice(0, e)), i.isFunction(t) ? (r = t, t = void 0) : t && "object" == typeof t && (s = "POST"), f.length > 0 && i.ajax({
            url: n,
            type: s,
            dataType: "html",
            data: t
        }).done(function(n) {
            o = arguments;
            f.html(u ? i("<div>").append(i.parseHTML(n)).find(u) : n)
        }).complete(r && function(n, t) {
            f.each(r, o || [n.responseText, t, n])
        }), this
    }, i.expr.filters.animated = function(n) {
        return i.grep(i.timers, function(t) {
            return n === t.elem
        }).length
    }, tr = n.document.documentElement, i.offset = {
        setOffset: function(n, t, r) {
            var e, o, s, h, u, c, v, l = i.css(n, "position"),
                a = i(n),
                f = {};
            "static" === l && (n.style.position = "relative");
            u = a.offset();
            s = i.css(n, "top");
            c = i.css(n, "left");
            v = ("absolute" === l || "fixed" === l) && i.inArray("auto", [s, c]) > -1;
            v ? (e = a.position(), h = e.top, o = e.left) : (h = parseFloat(s) || 0, o = parseFloat(c) || 0);
            i.isFunction(t) && (t = t.call(n, r, u));
            null != t.top && (f.top = t.top - u.top + h);
            null != t.left && (f.left = t.left - u.left + o);
            "using" in t ? t.using.call(n, f) : a.css(f)
        }
    }, i.fn.extend({
        offset: function(n) {
            if (arguments.length) return void 0 === n ? this : this.each(function(t) {
                i.offset.setOffset(this, n, t)
            });
            var t, f, u = {
                    top: 0,
                    left: 0
                },
                r = this[0],
                e = r && r.ownerDocument;
            if (e) return t = e.documentElement, i.contains(t, r) ? (typeof r.getBoundingClientRect !== o && (u = r.getBoundingClientRect()), f = wf(e), {
                top: u.top + (f.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                left: u.left + (f.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
            }) : u
        },
        position: function() {
            if (this[0]) {
                var n, r, t = {
                        top: 0,
                        left: 0
                    },
                    u = this[0];
                return "fixed" === i.css(u, "position") ? r = u.getBoundingClientRect() : (n = this.offsetParent(), r = this.offset(), i.nodeName(n[0], "html") || (t = n.offset()), t.top += i.css(n[0], "borderTopWidth", !0), t.left += i.css(n[0], "borderLeftWidth", !0)), {
                    top: r.top - t.top - i.css(u, "marginTop", !0),
                    left: r.left - t.left - i.css(u, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var n = this.offsetParent || tr; n && !i.nodeName(n, "html") && "static" === i.css(n, "position");) n = n.offsetParent;
                return n || tr
            })
        }
    }), i.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(n, t) {
        var r = /Y/.test(t);
        i.fn[n] = function(u) {
            return b(this, function(n, u, f) {
                var e = wf(n);
                return void 0 === f ? e ? t in e ? e[t] : e.document.documentElement[u] : n[u] : void(e ? e.scrollTo(r ? i(e).scrollLeft() : f, r ? f : i(e).scrollTop()) : n[u] = f)
            }, n, u, arguments.length, null)
        }
    }), i.each(["top", "left"], function(n, t) {
        i.cssHooks[t] = au(r.pixelPosition, function(n, r) {
            if (r) return (r = d(n, t), pt.test(r) ? i(n).position()[t] + "px" : r)
        })
    }), i.each({
        Height: "height",
        Width: "width"
    }, function(n, t) {
        i.each({
            padding: "inner" + n,
            content: t,
            "": "outer" + n
        }, function(r, u) {
            i.fn[u] = function(u, f) {
                var e = arguments.length && (r || "boolean" != typeof u),
                    o = r || (u === !0 || f === !0 ? "margin" : "border");
                return b(this, function(t, r, u) {
                    var f;
                    return i.isWindow(t) ? t.document.documentElement["client" + n] : 9 === t.nodeType ? (f = t.documentElement, Math.max(t.body["scroll" + n], f["scroll" + n], t.body["offset" + n], f["offset" + n], f["client" + n])) : void 0 === u ? i.css(t, r, o) : i.style(t, r, u, o)
                }, t, e ? u : void 0, e, null)
            }
        })
    }), i.fn.size = function() {
        return this.length
    }, i.fn.andSelf = i.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
        return i
    }), bf = n.jQuery, kf = n.$, i.noConflict = function(t) {
        return n.$ === i && (n.$ = kf), t && n.jQuery === i && (n.jQuery = bf), i
    }, typeof t === o && (n.jQuery = n.$ = i), i
}),
function(n) {
    "function" == typeof define && define.amd ? define(["jquery"], n) : n(jQuery)
}(function(n) {
    function t(t, r) {
        var u, f, e, o = t.nodeName.toLowerCase();
        return "area" === o ? (u = t.parentNode, f = u.name, t.href && f && "map" === u.nodeName.toLowerCase() ? (e = n("img[usemap='#" + f + "']")[0], !!e && i(e)) : !1) : (/^(input|select|textarea|button|object)$/.test(o) ? !t.disabled : "a" === o ? t.href || r : r) && i(t)
    }

    function i(t) {
        return n.expr.filters.visible(t) && !n(t).parents().addBack().filter(function() {
            return "hidden" === n.css(this, "visibility")
        }).length
    }
    n.ui = n.ui || {};
    n.extend(n.ui, {
        version: "1.11.4",
        keyCode: {
            BACKSPACE: 8,
            COMMA: 188,
            DELETE: 46,
            DOWN: 40,
            END: 35,
            ENTER: 13,
            ESCAPE: 27,
            HOME: 36,
            LEFT: 37,
            PAGE_DOWN: 34,
            PAGE_UP: 33,
            PERIOD: 190,
            RIGHT: 39,
            SPACE: 32,
            TAB: 9,
            UP: 38
        }
    });
    n.fn.extend({
        scrollParent: function(t) {
            var i = this.css("position"),
                u = "absolute" === i,
                f = t ? /(auto|scroll|hidden)/ : /(auto|scroll)/,
                r = this.parents().filter(function() {
                    var t = n(this);
                    return u && "static" === t.css("position") ? !1 : f.test(t.css("overflow") + t.css("overflow-y") + t.css("overflow-x"))
                }).eq(0);
            return "fixed" !== i && r.length ? r : n(this[0].ownerDocument || document)
        },
        uniqueId: function() {
            var n = 0;
            return function() {
                return this.each(function() {
                    this.id || (this.id = "ui-id-" + ++n)
                })
            }
        }(),
        removeUniqueId: function() {
            return this.each(function() {
                /^ui-id-\d+$/.test(this.id) && n(this).removeAttr("id")
            })
        }
    });
    n.extend(n.expr[":"], {
        data: n.expr.createPseudo ? n.expr.createPseudo(function(t) {
            return function(i) {
                return !!n.data(i, t)
            }
        }) : function(t, i, r) {
            return !!n.data(t, r[3])
        },
        focusable: function(i) {
            return t(i, !isNaN(n.attr(i, "tabindex")))
        },
        tabbable: function(i) {
            var r = n.attr(i, "tabindex"),
                u = isNaN(r);
            return (u || r >= 0) && t(i, !u)
        }
    });
    n("<a>").outerWidth(1).jquery || n.each(["Width", "Height"], function(t, i) {
        function r(t, i, r, u) {
            return n.each(e, function() {
                i -= parseFloat(n.css(t, "padding" + this)) || 0;
                r && (i -= parseFloat(n.css(t, "border" + this + "Width")) || 0);
                u && (i -= parseFloat(n.css(t, "margin" + this)) || 0)
            }), i
        }
        var e = "Width" === i ? ["Left", "Right"] : ["Top", "Bottom"],
            u = i.toLowerCase(),
            f = {
                innerWidth: n.fn.innerWidth,
                innerHeight: n.fn.innerHeight,
                outerWidth: n.fn.outerWidth,
                outerHeight: n.fn.outerHeight
            };
        n.fn["inner" + i] = function(t) {
            return void 0 === t ? f["inner" + i].call(this) : this.each(function() {
                n(this).css(u, r(this, t) + "px")
            })
        };
        n.fn["outer" + i] = function(t, e) {
            return "number" != typeof t ? f["outer" + i].call(this, t) : this.each(function() {
                n(this).css(u, r(this, t, !0, e) + "px")
            })
        }
    });
    n.fn.addBack || (n.fn.addBack = function(n) {
        return this.add(null == n ? this.prevObject : this.prevObject.filter(n))
    });
    n("<a>").data("a-b", "a").removeData("a-b").data("a-b") && (n.fn.removeData = function(t) {
        return function(i) {
            return arguments.length ? t.call(this, n.camelCase(i)) : t.call(this)
        }
    }(n.fn.removeData));
    n.ui.ie = !!/msie [\w.]+/.exec(navigator.userAgent.toLowerCase());
    n.fn.extend({
        focus: function(t) {
            return function(i, r) {
                return "number" == typeof i ? this.each(function() {
                    var t = this;
                    setTimeout(function() {
                        n(t).focus();
                        r && r.call(t)
                    }, i)
                }) : t.apply(this, arguments)
            }
        }(n.fn.focus),
        disableSelection: function() {
            var n = "onselectstart" in document.createElement("div") ? "selectstart" : "mousedown";
            return function() {
                return this.bind(n + ".ui-disableSelection", function(n) {
                    n.preventDefault()
                })
            }
        }(),
        enableSelection: function() {
            return this.unbind(".ui-disableSelection")
        },
        zIndex: function(t) {
            if (void 0 !== t) return this.css("zIndex", t);
            if (this.length)
                for (var r, u, i = n(this[0]); i.length && i[0] !== document;) {
                    if (r = i.css("position"), ("absolute" === r || "relative" === r || "fixed" === r) && (u = parseInt(i.css("zIndex"), 10), !isNaN(u) && 0 !== u)) return u;
                    i = i.parent()
                }
            return 0
        }
    });
    n.ui.plugin = {
        add: function(t, i, r) {
            var u, f = n.ui[t].prototype;
            for (u in r) f.plugins[u] = f.plugins[u] || [], f.plugins[u].push([i, r[u]])
        },
        call: function(n, t, i, r) {
            var u, f = n.plugins[t];
            if (f && (r || n.element[0].parentNode && 11 !== n.element[0].parentNode.nodeType))
                for (u = 0; f.length > u; u++) n.options[f[u][0]] && f[u][1].apply(n.element, i)
        }
    };
    var u = 0,
        r = Array.prototype.slice;
    n.cleanData = function(t) {
        return function(i) {
            for (var r, u, f = 0; null != (u = i[f]); f++) try {
                r = n._data(u, "events");
                r && r.remove && n(u).triggerHandler("remove")
            } catch (e) {}
            t(i)
        }
    }(n.cleanData);
    n.widget = function(t, i, r) {
        var s, f, u, o, h = {},
            e = t.split(".")[0];
        return t = t.split(".")[1], s = e + "-" + t, r || (r = i, i = n.Widget), n.expr[":"][s.toLowerCase()] = function(t) {
            return !!n.data(t, s)
        }, n[e] = n[e] || {}, f = n[e][t], u = n[e][t] = function(n, t) {
            return this._createWidget ? (arguments.length && this._createWidget(n, t), void 0) : new u(n, t)
        }, n.extend(u, f, {
            version: r.version,
            _proto: n.extend({}, r),
            _childConstructors: []
        }), o = new i, o.options = n.widget.extend({}, o.options), n.each(r, function(t, r) {
            return n.isFunction(r) ? (h[t] = function() {
                var n = function() {
                        return i.prototype[t].apply(this, arguments)
                    },
                    u = function(n) {
                        return i.prototype[t].apply(this, n)
                    };
                return function() {
                    var t, i = this._super,
                        f = this._superApply;
                    return this._super = n, this._superApply = u, t = r.apply(this, arguments), this._super = i, this._superApply = f, t
                }
            }(), void 0) : (h[t] = r, void 0)
        }), u.prototype = n.widget.extend(o, {
            widgetEventPrefix: f ? o.widgetEventPrefix || t : t
        }, h, {
            constructor: u,
            namespace: e,
            widgetName: t,
            widgetFullName: s
        }), f ? (n.each(f._childConstructors, function(t, i) {
            var r = i.prototype;
            n.widget(r.namespace + "." + r.widgetName, u, i._proto)
        }), delete f._childConstructors) : i._childConstructors.push(u), n.widget.bridge(t, u), u
    };
    n.widget.extend = function(t) {
        for (var i, u, e = r.call(arguments, 1), f = 0, o = e.length; o > f; f++)
            for (i in e[f]) u = e[f][i], e[f].hasOwnProperty(i) && void 0 !== u && (t[i] = n.isPlainObject(u) ? n.isPlainObject(t[i]) ? n.widget.extend({}, t[i], u) : n.widget.extend({}, u) : u);
        return t
    };
    n.widget.bridge = function(t, i) {
        var u = i.prototype.widgetFullName || t;
        n.fn[t] = function(f) {
            var s = "string" == typeof f,
                o = r.call(arguments, 1),
                e = this;
            return s ? this.each(function() {
                var i, r = n.data(this, u);
                return "instance" === f ? (e = r, !1) : r ? n.isFunction(r[f]) && "_" !== f.charAt(0) ? (i = r[f].apply(r, o), i !== r && void 0 !== i ? (e = i && i.jquery ? e.pushStack(i.get()) : i, !1) : void 0) : n.error("no such method '" + f + "' for " + t + " widget instance") : n.error("cannot call methods on " + t + " prior to initialization; attempted to call method '" + f + "'")
            }) : (o.length && (f = n.widget.extend.apply(null, [f].concat(o))), this.each(function() {
                var t = n.data(this, u);
                t ? (t.option(f || {}), t._init && t._init()) : n.data(this, u, new i(f, this))
            })), e
        }
    };
    n.Widget = function() {};
    n.Widget._childConstructors = [];
    n.Widget.prototype = {
        widgetName: "widget",
        widgetEventPrefix: "",
        defaultElement: "<div>",
        options: {
            disabled: !1,
            create: null
        },
        _createWidget: function(t, i) {
            i = n(i || this.defaultElement || this)[0];
            this.element = n(i);
            this.uuid = u++;
            this.eventNamespace = "." + this.widgetName + this.uuid;
            this.bindings = n();
            this.hoverable = n();
            this.focusable = n();
            i !== this && (n.data(i, this.widgetFullName, this), this._on(!0, this.element, {
                remove: function(n) {
                    n.target === i && this.destroy()
                }
            }), this.document = n(i.style ? i.ownerDocument : i.document || i), this.window = n(this.document[0].defaultView || this.document[0].parentWindow));
            this.options = n.widget.extend({}, this.options, this._getCreateOptions(), t);
            this._create();
            this._trigger("create", null, this._getCreateEventData());
            this._init()
        },
        _getCreateOptions: n.noop,
        _getCreateEventData: n.noop,
        _create: n.noop,
        _init: n.noop,
        destroy: function() {
            this._destroy();
            this.element.unbind(this.eventNamespace).removeData(this.widgetFullName).removeData(n.camelCase(this.widgetFullName));
            this.widget().unbind(this.eventNamespace).removeAttr("aria-disabled").removeClass(this.widgetFullName + "-disabled ui-state-disabled");
            this.bindings.unbind(this.eventNamespace);
            this.hoverable.removeClass("ui-state-hover");
            this.focusable.removeClass("ui-state-focus")
        },
        _destroy: n.noop,
        widget: function() {
            return this.element
        },
        option: function(t, i) {
            var r, u, f, e = t;
            if (0 === arguments.length) return n.widget.extend({}, this.options);
            if ("string" == typeof t)
                if (e = {}, r = t.split("."), t = r.shift(), r.length) {
                    for (u = e[t] = n.widget.extend({}, this.options[t]), f = 0; r.length - 1 > f; f++) u[r[f]] = u[r[f]] || {}, u = u[r[f]];
                    if (t = r.pop(), 1 === arguments.length) return void 0 === u[t] ? null : u[t];
                    u[t] = i
                } else {
                    if (1 === arguments.length) return void 0 === this.options[t] ? null : this.options[t];
                    e[t] = i
                }
            return this._setOptions(e), this
        },
        _setOptions: function(n) {
            var t;
            for (t in n) this._setOption(t, n[t]);
            return this
        },
        _setOption: function(n, t) {
            return this.options[n] = t, "disabled" === n && (this.widget().toggleClass(this.widgetFullName + "-disabled", !!t), t && (this.hoverable.removeClass("ui-state-hover"), this.focusable.removeClass("ui-state-focus"))), this
        },
        enable: function() {
            return this._setOptions({
                disabled: !1
            })
        },
        disable: function() {
            return this._setOptions({
                disabled: !0
            })
        },
        _on: function(t, i, r) {
            var f, u = this;
            "boolean" != typeof t && (r = i, i = t, t = !1);
            r ? (i = f = n(i), this.bindings = this.bindings.add(i)) : (r = i, i = this.element, f = this.widget());
            n.each(r, function(r, e) {
                function o() {
                    if (t || u.options.disabled !== !0 && !n(this).hasClass("ui-state-disabled")) return ("string" == typeof e ? u[e] : e).apply(u, arguments)
                }
                "string" != typeof e && (o.guid = e.guid = e.guid || o.guid || n.guid++);
                var s = r.match(/^([\w:-]*)\s*(.*)$/),
                    h = s[1] + u.eventNamespace,
                    c = s[2];
                c ? f.delegate(c, h, o) : i.bind(h, o)
            })
        },
        _off: function(t, i) {
            i = (i || "").split(" ").join(this.eventNamespace + " ") + this.eventNamespace;
            t.unbind(i).undelegate(i);
            this.bindings = n(this.bindings.not(t).get());
            this.focusable = n(this.focusable.not(t).get());
            this.hoverable = n(this.hoverable.not(t).get())
        },
        _delay: function(n, t) {
            function r() {
                return ("string" == typeof n ? i[n] : n).apply(i, arguments)
            }
            var i = this;
            return setTimeout(r, t || 0)
        },
        _hoverable: function(t) {
            this.hoverable = this.hoverable.add(t);
            this._on(t, {
                mouseenter: function(t) {
                    n(t.currentTarget).addClass("ui-state-hover")
                },
                mouseleave: function(t) {
                    n(t.currentTarget).removeClass("ui-state-hover")
                }
            })
        },
        _focusable: function(t) {
            this.focusable = this.focusable.add(t);
            this._on(t, {
                focusin: function(t) {
                    n(t.currentTarget).addClass("ui-state-focus")
                },
                focusout: function(t) {
                    n(t.currentTarget).removeClass("ui-state-focus")
                }
            })
        },
        _trigger: function(t, i, r) {
            var u, f, e = this.options[t];
            if (r = r || {}, i = n.Event(i), i.type = (t === this.widgetEventPrefix ? t : this.widgetEventPrefix + t).toLowerCase(), i.target = this.element[0], f = i.originalEvent)
                for (u in f) u in i || (i[u] = f[u]);
            return this.element.trigger(i, r), !(n.isFunction(e) && e.apply(this.element[0], [i].concat(r)) === !1 || i.isDefaultPrevented())
        }
    };
    n.each({
        show: "fadeIn",
        hide: "fadeOut"
    }, function(t, i) {
        n.Widget.prototype["_" + t] = function(r, u, f) {
            "string" == typeof u && (u = {
                effect: u
            });
            var o, e = u ? u === !0 || "number" == typeof u ? i : u.effect || i : t;
            u = u || {};
            "number" == typeof u && (u = {
                duration: u
            });
            o = !n.isEmptyObject(u);
            u.complete = f;
            u.delay && r.delay(u.delay);
            o && n.effects && n.effects.effect[e] ? r[t](u) : e !== t && r[e] ? r[e](u.duration, u.easing, f) : r.queue(function(i) {
                n(this)[t]();
                f && f.call(r[0]);
                i()
            })
        }
    });
    n.widget,
        function() {
            function f(n, t, i) {
                return [parseFloat(n[0]) * (a.test(n[0]) ? t / 100 : 1), parseFloat(n[1]) * (a.test(n[1]) ? i / 100 : 1)]
            }

            function i(t, i) {
                return parseInt(n.css(t, i), 10) || 0
            }

            function v(t) {
                var i = t[0];
                return 9 === i.nodeType ? {
                    width: t.width(),
                    height: t.height(),
                    offset: {
                        top: 0,
                        left: 0
                    }
                } : n.isWindow(i) ? {
                    width: t.width(),
                    height: t.height(),
                    offset: {
                        top: t.scrollTop(),
                        left: t.scrollLeft()
                    }
                } : i.preventDefault ? {
                    width: 0,
                    height: 0,
                    offset: {
                        top: i.pageY,
                        left: i.pageX
                    }
                } : {
                    width: t.outerWidth(),
                    height: t.outerHeight(),
                    offset: t.offset()
                }
            }
            n.ui = n.ui || {};
            var u, e, r = Math.max,
                t = Math.abs,
                o = Math.round,
                s = /left|center|right/,
                h = /top|center|bottom/,
                c = /[\+\-]\d+(\.[\d]+)?%?/,
                l = /^\w+/,
                a = /%$/,
                y = n.fn.position;
            n.position = {
                scrollbarWidth: function() {
                    if (void 0 !== u) return u;
                    var r, i, t = n("<div style='display:block;position:absolute;width:50px;height:50px;overflow:hidden;'><div style='height:100px;width:auto;'><\/div><\/div>"),
                        f = t.children()[0];
                    return n("body").append(t), r = f.offsetWidth, t.css("overflow", "scroll"), i = f.offsetWidth, r === i && (i = t[0].clientWidth), t.remove(), u = r - i
                },
                getScrollInfo: function(t) {
                    var i = t.isWindow || t.isDocument ? "" : t.element.css("overflow-x"),
                        r = t.isWindow || t.isDocument ? "" : t.element.css("overflow-y"),
                        u = "scroll" === i || "auto" === i && t.width < t.element[0].scrollWidth,
                        f = "scroll" === r || "auto" === r && t.height < t.element[0].scrollHeight;
                    return {
                        width: f ? n.position.scrollbarWidth() : 0,
                        height: u ? n.position.scrollbarWidth() : 0
                    }
                },
                getWithinInfo: function(t) {
                    var i = n(t || window),
                        r = n.isWindow(i[0]),
                        u = !!i[0] && 9 === i[0].nodeType;
                    return {
                        element: i,
                        isWindow: r,
                        isDocument: u,
                        offset: i.offset() || {
                            left: 0,
                            top: 0
                        },
                        scrollLeft: i.scrollLeft(),
                        scrollTop: i.scrollTop(),
                        width: r || u ? i.width() : i.outerWidth(),
                        height: r || u ? i.height() : i.outerHeight()
                    }
                }
            };
            n.fn.position = function(u) {
                if (!u || !u.of) return y.apply(this, arguments);
                u = n.extend({}, u);
                var k, a, p, b, w, g, nt = n(u.of),
                    it = n.position.getWithinInfo(u.within),
                    rt = n.position.getScrollInfo(it),
                    d = (u.collision || "flip").split(" "),
                    tt = {};
                return g = v(nt), nt[0].preventDefault && (u.at = "left top"), a = g.width, p = g.height, b = g.offset, w = n.extend({}, b), n.each(["my", "at"], function() {
                    var t, i, n = (u[this] || "").split(" ");
                    1 === n.length && (n = s.test(n[0]) ? n.concat(["center"]) : h.test(n[0]) ? ["center"].concat(n) : ["center", "center"]);
                    n[0] = s.test(n[0]) ? n[0] : "center";
                    n[1] = h.test(n[1]) ? n[1] : "center";
                    t = c.exec(n[0]);
                    i = c.exec(n[1]);
                    tt[this] = [t ? t[0] : 0, i ? i[0] : 0];
                    u[this] = [l.exec(n[0])[0], l.exec(n[1])[0]]
                }), 1 === d.length && (d[1] = d[0]), "right" === u.at[0] ? w.left += a : "center" === u.at[0] && (w.left += a / 2), "bottom" === u.at[1] ? w.top += p : "center" === u.at[1] && (w.top += p / 2), k = f(tt.at, a, p), w.left += k[0], w.top += k[1], this.each(function() {
                    var y, g, h = n(this),
                        c = h.outerWidth(),
                        l = h.outerHeight(),
                        ut = i(this, "marginLeft"),
                        ft = i(this, "marginTop"),
                        et = c + ut + i(this, "marginRight") + rt.width,
                        ot = l + ft + i(this, "marginBottom") + rt.height,
                        s = n.extend({}, w),
                        v = f(tt.my, h.outerWidth(), h.outerHeight());
                    "right" === u.my[0] ? s.left -= c : "center" === u.my[0] && (s.left -= c / 2);
                    "bottom" === u.my[1] ? s.top -= l : "center" === u.my[1] && (s.top -= l / 2);
                    s.left += v[0];
                    s.top += v[1];
                    e || (s.left = o(s.left), s.top = o(s.top));
                    y = {
                        marginLeft: ut,
                        marginTop: ft
                    };
                    n.each(["left", "top"], function(t, i) {
                        n.ui.position[d[t]] && n.ui.position[d[t]][i](s, {
                            targetWidth: a,
                            targetHeight: p,
                            elemWidth: c,
                            elemHeight: l,
                            collisionPosition: y,
                            collisionWidth: et,
                            collisionHeight: ot,
                            offset: [k[0] + v[0], k[1] + v[1]],
                            my: u.my,
                            at: u.at,
                            within: it,
                            elem: h
                        })
                    });
                    u.using && (g = function(n) {
                        var i = b.left - s.left,
                            o = i + a - c,
                            f = b.top - s.top,
                            v = f + p - l,
                            e = {
                                target: {
                                    element: nt,
                                    left: b.left,
                                    top: b.top,
                                    width: a,
                                    height: p
                                },
                                element: {
                                    element: h,
                                    left: s.left,
                                    top: s.top,
                                    width: c,
                                    height: l
                                },
                                horizontal: 0 > o ? "left" : i > 0 ? "right" : "center",
                                vertical: 0 > v ? "top" : f > 0 ? "bottom" : "middle"
                            };
                        c > a && a > t(i + o) && (e.horizontal = "center");
                        l > p && p > t(f + v) && (e.vertical = "middle");
                        e.important = r(t(i), t(o)) > r(t(f), t(v)) ? "horizontal" : "vertical";
                        u.using.call(this, n, e)
                    });
                    h.offset(n.extend(s, {
                        using: g
                    }))
                })
            };
            n.ui.position = {
                    fit: {
                        left: function(n, t) {
                            var h, e = t.within,
                                u = e.isWindow ? e.scrollLeft : e.offset.left,
                                o = e.width,
                                s = n.left - t.collisionPosition.marginLeft,
                                i = u - s,
                                f = s + t.collisionWidth - o - u;
                            t.collisionWidth > o ? i > 0 && 0 >= f ? (h = n.left + i + t.collisionWidth - o - u, n.left += i - h) : n.left = f > 0 && 0 >= i ? u : i > f ? u + o - t.collisionWidth : u : i > 0 ? n.left += i : f > 0 ? n.left -= f : n.left = r(n.left - s, n.left)
                        },
                        top: function(n, t) {
                            var h, o = t.within,
                                u = o.isWindow ? o.scrollTop : o.offset.top,
                                e = t.within.height,
                                s = n.top - t.collisionPosition.marginTop,
                                i = u - s,
                                f = s + t.collisionHeight - e - u;
                            t.collisionHeight > e ? i > 0 && 0 >= f ? (h = n.top + i + t.collisionHeight - e - u, n.top += i - h) : n.top = f > 0 && 0 >= i ? u : i > f ? u + e - t.collisionHeight : u : i > 0 ? n.top += i : f > 0 ? n.top -= f : n.top = r(n.top - s, n.top)
                        }
                    },
                    flip: {
                        left: function(n, i) {
                            var o, s, r = i.within,
                                y = r.offset.left + r.scrollLeft,
                                c = r.width,
                                h = r.isWindow ? r.scrollLeft : r.offset.left,
                                l = n.left - i.collisionPosition.marginLeft,
                                a = l - h,
                                v = l + i.collisionWidth - c - h,
                                u = "left" === i.my[0] ? -i.elemWidth : "right" === i.my[0] ? i.elemWidth : 0,
                                f = "left" === i.at[0] ? i.targetWidth : "right" === i.at[0] ? -i.targetWidth : 0,
                                e = -2 * i.offset[0];
                            0 > a ? (o = n.left + u + f + e + i.collisionWidth - c - y, (0 > o || t(a) > o) && (n.left += u + f + e)) : v > 0 && (s = n.left - i.collisionPosition.marginLeft + u + f + e - h, (s > 0 || v > t(s)) && (n.left += u + f + e))
                        },
                        top: function(n, i) {
                            var o, s, r = i.within,
                                y = r.offset.top + r.scrollTop,
                                c = r.height,
                                h = r.isWindow ? r.scrollTop : r.offset.top,
                                l = n.top - i.collisionPosition.marginTop,
                                a = l - h,
                                v = l + i.collisionHeight - c - h,
                                p = "top" === i.my[1],
                                u = p ? -i.elemHeight : "bottom" === i.my[1] ? i.elemHeight : 0,
                                f = "top" === i.at[1] ? i.targetHeight : "bottom" === i.at[1] ? -i.targetHeight : 0,
                                e = -2 * i.offset[1];
                            0 > a ? (s = n.top + u + f + e + i.collisionHeight - c - y, (0 > s || t(a) > s) && (n.top += u + f + e)) : v > 0 && (o = n.top - i.collisionPosition.marginTop + u + f + e - h, (o > 0 || v > t(o)) && (n.top += u + f + e))
                        }
                    },
                    flipfit: {
                        left: function() {
                            n.ui.position.flip.left.apply(this, arguments);
                            n.ui.position.fit.left.apply(this, arguments)
                        },
                        top: function() {
                            n.ui.position.flip.top.apply(this, arguments);
                            n.ui.position.fit.top.apply(this, arguments)
                        }
                    }
                },
                function() {
                    var t, i, r, u, f, o = document.getElementsByTagName("body")[0],
                        s = document.createElement("div");
                    t = document.createElement(o ? "div" : "body");
                    r = {
                        visibility: "hidden",
                        width: 0,
                        height: 0,
                        border: 0,
                        margin: 0,
                        background: "none"
                    };
                    o && n.extend(r, {
                        position: "absolute",
                        left: "-1000px",
                        top: "-1000px"
                    });
                    for (f in r) t.style[f] = r[f];
                    t.appendChild(s);
                    i = o || document.documentElement;
                    i.insertBefore(t, i.firstChild);
                    s.style.cssText = "position: absolute; left: 10.7432222px;";
                    u = n(s).offset().left;
                    e = u > 10 && 11 > u;
                    t.innerHTML = "";
                    i.removeChild(t)
                }()
        }();
    n.ui.position;
    n.widget("ui.menu", {
        version: "1.11.4",
        defaultElement: "<ul>",
        delay: 300,
        options: {
            icons: {
                submenu: "ui-icon-carat-1-e"
            },
            items: "> *",
            menus: "ul",
            position: {
                my: "left-1 top",
                at: "right top"
            },
            role: "menu",
            blur: null,
            focus: null,
            select: null
        },
        _create: function() {
            this.activeMenu = this.element;
            this.mouseHandled = !1;
            this.element.uniqueId().addClass("ui-menu ui-widget ui-widget-content").toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length).attr({
                role: this.options.role,
                tabIndex: 0
            });
            this.options.disabled && this.element.addClass("ui-state-disabled").attr("aria-disabled", "true");
            this._on({
                "mousedown .ui-menu-item": function(n) {
                    n.preventDefault()
                },
                "click .ui-menu-item": function(t) {
                    var i = n(t.target);
                    !this.mouseHandled && i.not(".ui-state-disabled").length && (this.select(t), t.isPropagationStopped() || (this.mouseHandled = !0), i.has(".ui-menu").length ? this.expand(t) : !this.element.is(":focus") && n(this.document[0].activeElement).closest(".ui-menu").length && (this.element.trigger("focus", [!0]), this.active && 1 === this.active.parents(".ui-menu").length && clearTimeout(this.timer)))
                },
                "mouseenter .ui-menu-item": function(t) {
                    if (!this.previousFilter) {
                        var i = n(t.currentTarget);
                        i.siblings(".ui-state-active").removeClass("ui-state-active");
                        this.focus(t, i)
                    }
                },
                mouseleave: "collapseAll",
                "mouseleave .ui-menu": "collapseAll",
                focus: function(n, t) {
                    var i = this.active || this.element.find(this.options.items).eq(0);
                    t || this.focus(n, i)
                },
                blur: function(t) {
                    this._delay(function() {
                        n.contains(this.element[0], this.document[0].activeElement) || this.collapseAll(t)
                    })
                },
                keydown: "_keydown"
            });
            this.refresh();
            this._on(this.document, {
                click: function(n) {
                    this._closeOnDocumentClick(n) && this.collapseAll(n);
                    this.mouseHandled = !1
                }
            })
        },
        _destroy: function() {
            this.element.removeAttr("aria-activedescendant").find(".ui-menu").addBack().removeClass("ui-menu ui-widget ui-widget-content ui-menu-icons ui-front").removeAttr("role").removeAttr("tabIndex").removeAttr("aria-labelledby").removeAttr("aria-expanded").removeAttr("aria-hidden").removeAttr("aria-disabled").removeUniqueId().show();
            this.element.find(".ui-menu-item").removeClass("ui-menu-item").removeAttr("role").removeAttr("aria-disabled").removeUniqueId().removeClass("ui-state-hover").removeAttr("tabIndex").removeAttr("role").removeAttr("aria-haspopup").children().each(function() {
                var t = n(this);
                t.data("ui-menu-submenu-carat") && t.remove()
            });
            this.element.find(".ui-menu-divider").removeClass("ui-menu-divider ui-widget-content")
        },
        _keydown: function(t) {
            var i, u, r, f, e = !0;
            switch (t.keyCode) {
                case n.ui.keyCode.PAGE_UP:
                    this.previousPage(t);
                    break;
                case n.ui.keyCode.PAGE_DOWN:
                    this.nextPage(t);
                    break;
                case n.ui.keyCode.HOME:
                    this._move("first", "first", t);
                    break;
                case n.ui.keyCode.END:
                    this._move("last", "last", t);
                    break;
                case n.ui.keyCode.UP:
                    this.previous(t);
                    break;
                case n.ui.keyCode.DOWN:
                    this.next(t);
                    break;
                case n.ui.keyCode.LEFT:
                    this.collapse(t);
                    break;
                case n.ui.keyCode.RIGHT:
                    this.active && !this.active.is(".ui-state-disabled") && this.expand(t);
                    break;
                case n.ui.keyCode.ENTER:
                case n.ui.keyCode.SPACE:
                    this._activate(t);
                    break;
                case n.ui.keyCode.ESCAPE:
                    this.collapse(t);
                    break;
                default:
                    e = !1;
                    u = this.previousFilter || "";
                    r = String.fromCharCode(t.keyCode);
                    f = !1;
                    clearTimeout(this.filterTimer);
                    r === u ? f = !0 : r = u + r;
                    i = this._filterMenuItems(r);
                    i = f && -1 !== i.index(this.active.next()) ? this.active.nextAll(".ui-menu-item") : i;
                    i.length || (r = String.fromCharCode(t.keyCode), i = this._filterMenuItems(r));
                    i.length ? (this.focus(t, i), this.previousFilter = r, this.filterTimer = this._delay(function() {
                        delete this.previousFilter
                    }, 1e3)) : delete this.previousFilter
            }
            e && t.preventDefault()
        },
        _activate: function(n) {
            this.active.is(".ui-state-disabled") || (this.active.is("[aria-haspopup='true']") ? this.expand(n) : this.select(n))
        },
        refresh: function() {
            var i, t, u = this,
                f = this.options.icons.submenu,
                r = this.element.find(this.options.menus);
            this.element.toggleClass("ui-menu-icons", !!this.element.find(".ui-icon").length);
            r.filter(":not(.ui-menu)").addClass("ui-menu ui-widget ui-widget-content ui-front").hide().attr({
                role: this.options.role,
                "aria-hidden": "true",
                "aria-expanded": "false"
            }).each(function() {
                var t = n(this),
                    i = t.parent(),
                    r = n("<span>").addClass("ui-menu-icon ui-icon " + f).data("ui-menu-submenu-carat", !0);
                i.attr("aria-haspopup", "true").prepend(r);
                t.attr("aria-labelledby", i.attr("id"))
            });
            i = r.add(this.element);
            t = i.find(this.options.items);
            t.not(".ui-menu-item").each(function() {
                var t = n(this);
                u._isDivider(t) && t.addClass("ui-widget-content ui-menu-divider")
            });
            t.not(".ui-menu-item, .ui-menu-divider").addClass("ui-menu-item").uniqueId().attr({
                tabIndex: -1,
                role: this._itemRole()
            });
            t.filter(".ui-state-disabled").attr("aria-disabled", "true");
            this.active && !n.contains(this.element[0], this.active[0]) && this.blur()
        },
        _itemRole: function() {
            return {
                menu: "menuitem",
                listbox: "option"
            }[this.options.role]
        },
        _setOption: function(n, t) {
            "icons" === n && this.element.find(".ui-menu-icon").removeClass(this.options.icons.submenu).addClass(t.submenu);
            "disabled" === n && this.element.toggleClass("ui-state-disabled", !!t).attr("aria-disabled", t);
            this._super(n, t)
        },
        focus: function(n, t) {
            var i, r;
            this.blur(n, n && "focus" === n.type);
            this._scrollIntoView(t);
            this.active = t.first();
            r = this.active.addClass("ui-state-focus").removeClass("ui-state-active");
            this.options.role && this.element.attr("aria-activedescendant", r.attr("id"));
            this.active.parent().closest(".ui-menu-item").addClass("ui-state-active");
            n && "keydown" === n.type ? this._close() : this.timer = this._delay(function() {
                this._close()
            }, this.delay);
            i = t.children(".ui-menu");
            i.length && n && /^mouse/.test(n.type) && this._startOpening(i);
            this.activeMenu = t.parent();
            this._trigger("focus", n, {
                item: t
            })
        },
        _scrollIntoView: function(t) {
            var e, o, i, r, u, f;
            this._hasScroll() && (e = parseFloat(n.css(this.activeMenu[0], "borderTopWidth")) || 0, o = parseFloat(n.css(this.activeMenu[0], "paddingTop")) || 0, i = t.offset().top - this.activeMenu.offset().top - e - o, r = this.activeMenu.scrollTop(), u = this.activeMenu.height(), f = t.outerHeight(), 0 > i ? this.activeMenu.scrollTop(r + i) : i + f > u && this.activeMenu.scrollTop(r + i - u + f))
        },
        blur: function(n, t) {
            t || clearTimeout(this.timer);
            this.active && (this.active.removeClass("ui-state-focus"), this.active = null, this._trigger("blur", n, {
                item: this.active
            }))
        },
        _startOpening: function(n) {
            clearTimeout(this.timer);
            "true" === n.attr("aria-hidden") && (this.timer = this._delay(function() {
                this._close();
                this._open(n)
            }, this.delay))
        },
        _open: function(t) {
            var i = n.extend({ of: this.active
            }, this.options.position);
            clearTimeout(this.timer);
            this.element.find(".ui-menu").not(t.parents(".ui-menu")).hide().attr("aria-hidden", "true");
            t.show().removeAttr("aria-hidden").attr("aria-expanded", "true").position(i)
        },
        collapseAll: function(t, i) {
            clearTimeout(this.timer);
            this.timer = this._delay(function() {
                var r = i ? this.element : n(t && t.target).closest(this.element.find(".ui-menu"));
                r.length || (r = this.element);
                this._close(r);
                this.blur(t);
                this.activeMenu = r
            }, this.delay)
        },
        _close: function(n) {
            n || (n = this.active ? this.active.parent() : this.element);
            n.find(".ui-menu").hide().attr("aria-hidden", "true").attr("aria-expanded", "false").end().find(".ui-state-active").not(".ui-state-focus").removeClass("ui-state-active")
        },
        _closeOnDocumentClick: function(t) {
            return !n(t.target).closest(".ui-menu").length
        },
        _isDivider: function(n) {
            return !/[^\-\u2014\u2013\s]/.test(n.text())
        },
        collapse: function(n) {
            var t = this.active && this.active.parent().closest(".ui-menu-item", this.element);
            t && t.length && (this._close(), this.focus(n, t))
        },
        expand: function(n) {
            var t = this.active && this.active.children(".ui-menu ").find(this.options.items).first();
            t && t.length && (this._open(t.parent()), this._delay(function() {
                this.focus(n, t)
            }))
        },
        next: function(n) {
            this._move("next", "first", n)
        },
        previous: function(n) {
            this._move("prev", "last", n)
        },
        isFirstItem: function() {
            return this.active && !this.active.prevAll(".ui-menu-item").length
        },
        isLastItem: function() {
            return this.active && !this.active.nextAll(".ui-menu-item").length
        },
        _move: function(n, t, i) {
            var r;
            this.active && (r = "first" === n || "last" === n ? this.active["first" === n ? "prevAll" : "nextAll"](".ui-menu-item").eq(-1) : this.active[n + "All"](".ui-menu-item").eq(0));
            r && r.length && this.active || (r = this.activeMenu.find(this.options.items)[t]());
            this.focus(i, r)
        },
        nextPage: function(t) {
            var i, r, u;
            return this.active ? (this.isLastItem() || (this._hasScroll() ? (r = this.active.offset().top, u = this.element.height(), this.active.nextAll(".ui-menu-item").each(function() {
                return i = n(this), 0 > i.offset().top - r - u
            }), this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items)[this.active ? "last" : "first"]())), void 0) : (this.next(t), void 0)
        },
        previousPage: function(t) {
            var i, r, u;
            return this.active ? (this.isFirstItem() || (this._hasScroll() ? (r = this.active.offset().top, u = this.element.height(), this.active.prevAll(".ui-menu-item").each(function() {
                return i = n(this), i.offset().top - r + u > 0
            }), this.focus(t, i)) : this.focus(t, this.activeMenu.find(this.options.items).first())), void 0) : (this.next(t), void 0)
        },
        _hasScroll: function() {
            return this.element.outerHeight() < this.element.prop("scrollHeight")
        },
        select: function(t) {
            this.active = this.active || n(t.target).closest(".ui-menu-item");
            var i = {
                item: this.active
            };
            this.active.has(".ui-menu").length || this.collapseAll(t, !0);
            this._trigger("select", t, i)
        },
        _filterMenuItems: function(t) {
            var i = t.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&"),
                r = RegExp("^" + i, "i");
            return this.activeMenu.find(this.options.items).filter(".ui-menu-item").filter(function() {
                return r.test(n.trim(n(this).text()))
            })
        }
    });
    n.widget("ui.autocomplete", {
        version: "1.11.4",
        defaultElement: "<input>",
        options: {
            appendTo: null,
            autoFocus: !1,
            delay: 300,
            minLength: 1,
            position: {
                my: "left top",
                at: "left bottom",
                collision: "none"
            },
            source: null,
            change: null,
            close: null,
            focus: null,
            open: null,
            response: null,
            search: null,
            select: null
        },
        requestIndex: 0,
        pending: 0,
        _create: function() {
            var t, i, r, u = this.element[0].nodeName.toLowerCase(),
                f = "textarea" === u,
                e = "input" === u;
            this.isMultiLine = f ? !0 : e ? !1 : this.element.prop("isContentEditable");
            this.valueMethod = this.element[f || e ? "val" : "text"];
            this.isNewMenu = !0;
            this.element.addClass("ui-autocomplete-input").attr("autocomplete", "off");
            this._on(this.element, {
                keydown: function(u) {
                    if (this.element.prop("readOnly")) return t = !0, r = !0, i = !0, void 0;
                    t = !1;
                    r = !1;
                    i = !1;
                    var f = n.ui.keyCode;
                    switch (u.keyCode) {
                        case f.PAGE_UP:
                            t = !0;
                            this._move("previousPage", u);
                            break;
                        case f.PAGE_DOWN:
                            t = !0;
                            this._move("nextPage", u);
                            break;
                        case f.UP:
                            t = !0;
                            this._keyEvent("previous", u);
                            break;
                        case f.DOWN:
                            t = !0;
                            this._keyEvent("next", u);
                            break;
                        case f.ENTER:
                            this.menu.active && (t = !0, u.preventDefault(), this.menu.select(u));
                            break;
                        case f.TAB:
                            this.menu.active && this.menu.select(u);
                            break;
                        case f.ESCAPE:
                            this.menu.element.is(":visible") && (this.isMultiLine || this._value(this.term), this.close(u), u.preventDefault());
                            break;
                        default:
                            i = !0;
                            this._searchTimeout(u)
                    }
                },
                keypress: function(r) {
                    if (t) return t = !1, (!this.isMultiLine || this.menu.element.is(":visible")) && r.preventDefault(), void 0;
                    if (!i) {
                        var u = n.ui.keyCode;
                        switch (r.keyCode) {
                            case u.PAGE_UP:
                                this._move("previousPage", r);
                                break;
                            case u.PAGE_DOWN:
                                this._move("nextPage", r);
                                break;
                            case u.UP:
                                this._keyEvent("previous", r);
                                break;
                            case u.DOWN:
                                this._keyEvent("next", r)
                        }
                    }
                },
                input: function(n) {
                    return r ? (r = !1, n.preventDefault(), void 0) : (this._searchTimeout(n), void 0)
                },
                focus: function() {
                    this.selectedItem = null;
                    this.previous = this._value()
                },
                blur: function(n) {
                    return this.cancelBlur ? (delete this.cancelBlur, void 0) : (clearTimeout(this.searching), this.close(n), this._change(n), void 0)
                }
            });
            this._initSource();
            this.menu = n("<ul>").addClass("ui-autocomplete ui-front").appendTo(this._appendTo()).menu({
                role: null
            }).hide().menu("instance");
            this._on(this.menu.element, {
                mousedown: function(t) {
                    t.preventDefault();
                    this.cancelBlur = !0;
                    this._delay(function() {
                        delete this.cancelBlur
                    });
                    var i = this.menu.element[0];
                    n(t.target).closest(".ui-menu-item").length || this._delay(function() {
                        var t = this;
                        this.document.one("mousedown", function(r) {
                            r.target === t.element[0] || r.target === i || n.contains(i, r.target) || t.close()
                        })
                    })
                },
                menufocus: function(t, i) {
                    var r, u;
                    return this.isNewMenu && (this.isNewMenu = !1, t.originalEvent && /^mouse/.test(t.originalEvent.type)) ? (this.menu.blur(), this.document.one("mousemove", function() {
                        n(t.target).trigger(t.originalEvent)
                    }), void 0) : (u = i.item.data("ui-autocomplete-item"), !1 !== this._trigger("focus", t, {
                        item: u
                    }) && t.originalEvent && /^key/.test(t.originalEvent.type) && this._value(u.value), r = i.item.attr("aria-label") || u.value, r && n.trim(r).length && (this.liveRegion.children().hide(), n("<div>").text(r).appendTo(this.liveRegion)), void 0)
                },
                menuselect: function(n, t) {
                    var i = t.item.data("ui-autocomplete-item"),
                        r = this.previous;
                    this.element[0] !== this.document[0].activeElement && (this.element.focus(), this.previous = r, this._delay(function() {
                        this.previous = r;
                        this.selectedItem = i
                    }));
                    !1 !== this._trigger("select", n, {
                        item: i
                    }) && this._value(i.value);
                    this.term = this._value();
                    this.close(n);
                    this.selectedItem = i
                }
            });
            this.liveRegion = n("<span>", {
                role: "status",
                "aria-live": "assertive",
                "aria-relevant": "additions"
            }).addClass("ui-helper-hidden-accessible").appendTo(this.document[0].body);
            this._on(this.window, {
                beforeunload: function() {
                    this.element.removeAttr("autocomplete")
                }
            })
        },
        _destroy: function() {
            clearTimeout(this.searching);
            this.element.removeClass("ui-autocomplete-input").removeAttr("autocomplete");
            this.menu.element.remove();
            this.liveRegion.remove()
        },
        _setOption: function(n, t) {
            this._super(n, t);
            "source" === n && this._initSource();
            "appendTo" === n && this.menu.element.appendTo(this._appendTo());
            "disabled" === n && t && this.xhr && this.xhr.abort()
        },
        _appendTo: function() {
            var t = this.options.appendTo;
            return t && (t = t.jquery || t.nodeType ? n(t) : this.document.find(t).eq(0)), t && t[0] || (t = this.element.closest(".ui-front")), t.length || (t = this.document[0].body), t
        },
        _initSource: function() {
            var i, r, t = this;
            n.isArray(this.options.source) ? (i = this.options.source, this.source = function(t, r) {
                r(n.ui.autocomplete.filter(i, t.term))
            }) : "string" == typeof this.options.source ? (r = this.options.source, this.source = function(i, u) {
                t.xhr && t.xhr.abort();
                t.xhr = n.ajax({
                    url: r,
                    data: i,
                    dataType: "json",
                    success: function(n) {
                        u(n)
                    },
                    error: function() {
                        u([])
                    }
                })
            }) : this.source = this.options.source
        },
        _searchTimeout: function(n) {
            clearTimeout(this.searching);
            this.searching = this._delay(function() {
                var t = this.term === this._value(),
                    i = this.menu.element.is(":visible"),
                    r = n.altKey || n.ctrlKey || n.metaKey || n.shiftKey;
                t && (!t || i || r) || (this.selectedItem = null, this.search(null, n))
            }, this.options.delay)
        },
        search: function(n, t) {
            return n = null != n ? n : this._value(), this.term = this._value(), n.length < this.options.minLength ? this.close(t) : this._trigger("search", t) !== !1 ? this._search(n) : void 0
        },
        _search: function(n) {
            this.pending++;
            this.element.addClass("ui-autocomplete-loading");
            this.cancelSearch = !1;
            this.source({
                term: n
            }, this._response())
        },
        _response: function() {
            var t = ++this.requestIndex;
            return n.proxy(function(n) {
                t === this.requestIndex && this.__response(n);
                this.pending--;
                this.pending || this.element.removeClass("ui-autocomplete-loading")
            }, this)
        },
        __response: function(n) {
            n && (n = this._normalize(n));
            this._trigger("response", null, {
                content: n
            });
            !this.options.disabled && n && n.length && !this.cancelSearch ? (this._suggest(n), this._trigger("open")) : this._close()
        },
        close: function(n) {
            this.cancelSearch = !0;
            this._close(n)
        },
        _close: function(n) {
            this.menu.element.is(":visible") && (this.menu.element.hide(), this.menu.blur(), this.isNewMenu = !0, this._trigger("close", n))
        },
        _change: function(n) {
            this.previous !== this._value() && this._trigger("change", n, {
                item: this.selectedItem
            })
        },
        _normalize: function(t) {
            return t.length && t[0].label && t[0].value ? t : n.map(t, function(t) {
                return "string" == typeof t ? {
                    label: t,
                    value: t
                } : n.extend({}, t, {
                    label: t.label || t.value,
                    value: t.value || t.label
                })
            })
        },
        _suggest: function(t) {
            var i = this.menu.element.empty();
            this._renderMenu(i, t);
            this.isNewMenu = !0;
            this.menu.refresh();
            i.show();
            this._resizeMenu();
            i.position(n.extend({ of: this.element
            }, this.options.position));
            this.options.autoFocus && this.menu.next()
        },
        _resizeMenu: function() {
            var n = this.menu.element;
            n.outerWidth(Math.max(n.width("").outerWidth() + 1, this.element.outerWidth()))
        },
        _renderMenu: function(t, i) {
            var r = this;
            n.each(i, function(n, i) {
                r._renderItemData(t, i)
            })
        },
        _renderItemData: function(n, t) {
            return this._renderItem(n, t).data("ui-autocomplete-item", t)
        },
        _renderItem: function(t, i) {
            return n("<li>").text(i.label).appendTo(t)
        },
        _move: function(n, t) {
            return this.menu.element.is(":visible") ? this.menu.isFirstItem() && /^previous/.test(n) || this.menu.isLastItem() && /^next/.test(n) ? (this.isMultiLine || this._value(this.term), this.menu.blur(), void 0) : (this.menu[n](t), void 0) : (this.search(null, t), void 0)
        },
        widget: function() {
            return this.menu.element
        },
        _value: function() {
            return this.valueMethod.apply(this.element, arguments)
        },
        _keyEvent: function(n, t) {
            (!this.isMultiLine || this.menu.element.is(":visible")) && (this._move(n, t), t.preventDefault())
        }
    });
    n.extend(n.ui.autocomplete, {
        escapeRegex: function(n) {
            return n.replace(/[\-\[\]{}()*+?.,\\\^$|#\s]/g, "\\$&")
        },
        filter: function(t, i) {
            var r = RegExp(n.ui.autocomplete.escapeRegex(i), "i");
            return n.grep(t, function(n) {
                return r.test(n.label || n.value || n)
            })
        }
    });
    n.widget("ui.autocomplete", n.ui.autocomplete, {
        options: {
            messages: {
                noResults: "No search results.",
                results: function(n) {
                    return n + (n > 1 ? " results are" : " result is") + " available, use up and down arrow keys to navigate."
                }
            }
        },
        __response: function(t) {
            var i;
            this._superApply(arguments);
            this.options.disabled || this.cancelSearch || (i = t && t.length ? this.options.messages.results(t.length) : this.options.messages.noResults, this.liveRegion.children().hide(), n("<div>").text(i).appendTo(this.liveRegion))
        }
    });
    n.ui.autocomplete
});
! function(n, t) {
    function v(n, t, r) {
        var e = n.children(),
            o = !1,
            u, s, f;
        for (n.empty(), u = 0, s = e.length; s > u; u++) {
            if (f = e.eq(u), n.append(f), r && n.append(r), i(n, t)) {
                f.remove();
                o = !0;
                break
            }
            r && r.detach()
        }
        return o
    }

    function f(t, r, u, e, o) {
        var s = !1,
            h = "a, table, thead, tbody, tfoot, tr, col, colgroup, object, embed, param, ol, ul, dl, blockquote, select, optgroup, option, textarea, script, style",
            c = "script, .dotdotdot-keep";
        return t.contents().detach().each(function() {
            var a = this,
                l = n(a);
            if ("undefined" == typeof a) return !0;
            if (l.is(c)) t.append(l);
            else {
                if (s) return !0;
                t.append(l);
                !o || l.is(e.after) || l.find(e.after).length || t[t.is(h) ? "after" : "append"](o);
                i(u, e) && (s = 3 == a.nodeType ? y(l, r, u, e, o) : f(l, r, u, e, o), s || (l.detach(), s = !0));
                s || o && o.detach()
            }
        }), s
    }

    function y(t, f, o, h, c) {
        var l = t[0],
            nt, k, d;
        if (!l) return !1;
        var y = s(l),
            tt = -1 !== y.indexOf(" ") ? " " : "　",
            p = "letter" == h.wrap ? "" : tt,
            a = y.split(p),
            g = -1,
            w = -1,
            b = 0,
            v = a.length - 1;
        for (h.fallbackToLetter && 0 == b && 0 == v && (p = "", a = y.split(p), v = a.length - 1); v >= b && (0 != b || 0 != v);) {
            if (nt = Math.floor((b + v) / 2), nt == w) break;
            w = nt;
            u(l, a.slice(0, w + 1).join(p) + h.ellipsis);
            i(o, h) ? (v = w, h.fallbackToLetter && 0 == b && 0 == v && (p = "", a = a[0].split(p), g = -1, w = -1, b = 0, v = a.length - 1)) : (g = w, b = w)
        }
        return -1 == g || 1 == a.length && 0 == a[0].length ? (k = t.parent(), t.detach(), d = c && c.closest(k).length ? c.length : 0, k.contents().length > d ? l = r(k.contents().eq(-1 - d), f) : (l = r(k, f, !0), d || k.detach()), l && (y = e(s(l), h), u(l, y), d && c && n(l).parent().append(c))) : (y = e(a.slice(0, g + 1).join(p), h), u(l, y)), !0
    }

    function i(n, t) {
        return n.innerHeight() > t.maxHeight
    }

    function e(t, i) {
        for (; n.inArray(t.slice(-1), i.lastCharacter.remove) > -1;) t = t.slice(0, -1);
        return n.inArray(t.slice(-1), i.lastCharacter.noEllipsis) < 0 && (t += i.ellipsis), t
    }

    function o(n) {
        return {
            width: n.innerWidth(),
            height: n.innerHeight()
        }
    }

    function u(n, t) {
        n.innerText ? n.innerText = t : n.nodeValue ? n.nodeValue = t : n.textContent && (n.textContent = t)
    }

    function s(n) {
        return n.innerText ? n.innerText : n.nodeValue ? n.nodeValue : n.textContent ? n.textContent : ""
    }

    function h(n) {
        do n = n.previousSibling; while (n && 1 !== n.nodeType && 3 !== n.nodeType);
        return n
    }

    function r(t, i, u) {
        var f, e = t && t[0];
        if (e) {
            if (!u) {
                if (3 === e.nodeType) return e;
                if (n.trim(t.text())) return r(t.contents().last(), i)
            }
            for (f = h(e); !f;) {
                if (t = t.parent(), t.is(i) || !t.length) return !1;
                f = h(t[0])
            }
            if (f) return r(n(f), i)
        }
        return !1
    }

    function p(t, i) {
        return t ? "string" == typeof t ? (t = n(t, i), t.length ? t : !1) : t.jquery ? t : !1 : !1
    }

    function w(n) {
        for (var t, r = n.innerHeight(), u = ["paddingTop", "paddingBottom"], i = 0, f = u.length; f > i; i++) t = parseInt(n.css(u[i]), 10), isNaN(t) && (t = 0), r -= t;
        return r
    }
    var c, l, a;
    n.fn.dotdotdot || (n.fn.dotdotdot = function(t) {
        var r;
        if (0 == this.length) return n.fn.dotdotdot.debug('No element found for "' + this.selector + '".'), this;
        if (this.length > 1) return this.each(function() {
            n(this).dotdotdot(t)
        });
        r = this;
        r.data("dotdotdot") && r.trigger("destroy.dot");
        r.data("dotdotdot-style", r.attr("style") || "");
        r.css("word-wrap", "break-word");
        "nowrap" === r.css("white-space") && r.css("white-space", "normal");
        r.bind_events = function() {
            return r.bind("update.dot", function(t, o) {
                t.preventDefault();
                t.stopPropagation();
                u.maxHeight = "number" == typeof u.height ? u.height : w(r);
                u.maxHeight += u.tolerance;
                "undefined" != typeof o && (("string" == typeof o || "nodeType" in o && 1 === o.nodeType) && (o = n("<div />").append(o).contents()), o instanceof n && (h = o));
                s = r.wrapInner('<div class="dotdotdot" />').children();
                s.contents().detach().end().append(h.clone(!0)).find("br").replaceWith("  <br />  ").end().css({
                    height: "auto",
                    width: "auto",
                    border: "none",
                    padding: 0,
                    margin: 0
                });
                var c = !1,
                    l = !1;
                return e.afterElement && (c = e.afterElement.clone(!0), c.show(), e.afterElement.detach()), i(s, u) && (l = "children" == u.wrap ? v(s, u, c) : f(s, r, s, u, c)), s.replaceWith(s.contents()), s = null, n.isFunction(u.callback) && u.callback.call(r[0], l, h), e.isTruncated = l, l
            }).bind("isTruncated.dot", function(n, t) {
                return n.preventDefault(), n.stopPropagation(), "function" == typeof t && t.call(r[0], e.isTruncated), e.isTruncated
            }).bind("originalContent.dot", function(n, t) {
                return n.preventDefault(), n.stopPropagation(), "function" == typeof t && t.call(r[0], h), h
            }).bind("destroy.dot", function(n) {
                n.preventDefault();
                n.stopPropagation();
                r.unwatch().unbind_events().contents().detach().end().append(h).attr("style", r.data("dotdotdot-style") || "").data("dotdotdot", !1)
            }), r
        };
        r.unbind_events = function() {
            return r.unbind(".dot"), r
        };
        r.watch = function() {
            if (r.unwatch(), "window" == u.watch) {
                var t = n(window),
                    i = t.width(),
                    f = t.height();
                t.bind("resize.dot" + e.dotId, function() {
                    i == t.width() && f == t.height() && u.windowResizeFix || (i = t.width(), f = t.height(), l && clearInterval(l), l = setTimeout(function() {
                        r.trigger("update.dot")
                    }, 100))
                })
            } else a = o(r), l = setInterval(function() {
                if (r.is(":visible")) {
                    var n = o(r);
                    (a.width != n.width || a.height != n.height) && (r.trigger("update.dot"), a = n)
                }
            }, 500);
            return r
        };
        r.unwatch = function() {
            return n(window).unbind("resize.dot" + e.dotId), l && clearInterval(l), r
        };
        var h = r.contents(),
            u = n.extend(!0, {}, n.fn.dotdotdot.defaults, t),
            e = {},
            a = {},
            l = null,
            s = null;
        return u.lastCharacter.remove instanceof Array || (u.lastCharacter.remove = n.fn.dotdotdot.defaultArrays.lastCharacter.remove), u.lastCharacter.noEllipsis instanceof Array || (u.lastCharacter.noEllipsis = n.fn.dotdotdot.defaultArrays.lastCharacter.noEllipsis), e.afterElement = p(u.after, r), e.isTruncated = !1, e.dotId = c++, r.data("dotdotdot", !0).bind_events().trigger("update.dot"), u.watch && r.watch(), r
    }, n.fn.dotdotdot.defaults = {
        ellipsis: "... ",
        wrap: "word",
        fallbackToLetter: !0,
        lastCharacter: {},
        tolerance: 0,
        callback: null,
        after: null,
        height: null,
        watch: !1,
        windowResizeFix: !0
    }, n.fn.dotdotdot.defaultArrays = {
        lastCharacter: {
            remove: [" ", "　", ",", ";", ".", "!", "?"],
            noEllipsis: []
        }
    }, n.fn.dotdotdot.debug = function() {}, c = 1, l = n.fn.html, n.fn.html = function(i) {
        return i != t && !n.isFunction(i) && this.data("dotdotdot") ? this.trigger("update", [i]) : l.apply(this, arguments)
    }, a = n.fn.text, n.fn.text = function(i) {
        return i != t && !n.isFunction(i) && this.data("dotdotdot") ? (i = n("<div />").text(i).html(), this.trigger("update", [i])) : a.apply(this, arguments)
    })
}(jQuery);
! function(n) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], n) : "undefined" != typeof exports ? module.exports = n(require("jquery")) : n(jQuery)
}(function(n) {
    "use strict";
    var t = window.Slick || {};
    t = function() {
        function t(t, r) {
            var f, u = this;
            u.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: n(t),
                appendDots: n(t),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous<\/button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next<\/button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(n, t) {
                    return '<button type="button" data-role="none" role="button" aria-required="false" tabindex="0">' + (t + 1) + "<\/button>"
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            };
            u.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            };
            n.extend(u, u.initials);
            u.activeBreakpoint = null;
            u.animType = null;
            u.animProp = null;
            u.breakpoints = [];
            u.breakpointSettings = [];
            u.cssTransitions = !1;
            u.hidden = "hidden";
            u.paused = !1;
            u.positionProp = null;
            u.respondTo = null;
            u.rowCount = 1;
            u.shouldClick = !0;
            u.$slider = n(t);
            u.$slidesCache = null;
            u.transformType = null;
            u.transitionType = null;
            u.visibilityChange = "visibilitychange";
            u.windowWidth = 0;
            u.windowTimer = null;
            f = n(t).data("slick") || {};
            u.options = n.extend({}, u.defaults, f, r);
            u.currentSlide = u.options.initialSlide;
            u.originalSettings = u.options;
            "undefined" != typeof document.mozHidden ? (u.hidden = "mozHidden", u.visibilityChange = "mozvisibilitychange") : "undefined" != typeof document.webkitHidden && (u.hidden = "webkitHidden", u.visibilityChange = "webkitvisibilitychange");
            u.autoPlay = n.proxy(u.autoPlay, u);
            u.autoPlayClear = n.proxy(u.autoPlayClear, u);
            u.changeSlide = n.proxy(u.changeSlide, u);
            u.clickHandler = n.proxy(u.clickHandler, u);
            u.selectHandler = n.proxy(u.selectHandler, u);
            u.setPosition = n.proxy(u.setPosition, u);
            u.swipeHandler = n.proxy(u.swipeHandler, u);
            u.dragHandler = n.proxy(u.dragHandler, u);
            u.keyHandler = n.proxy(u.keyHandler, u);
            u.autoPlayIterator = n.proxy(u.autoPlayIterator, u);
            u.instanceUid = i++;
            u.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/;
            u.registerBreakpoints();
            u.init(!0);
            u.checkResponsive(!0)
        }
        var i = 0;
        return t
    }();
    t.prototype.addSlide = t.prototype.slickAdd = function(t, i, r) {
        var u = this;
        if ("boolean" == typeof i) r = i, i = null;
        else if (0 > i || i >= u.slideCount) return !1;
        u.unload();
        "number" == typeof i ? 0 === i && 0 === u.$slides.length ? n(t).appendTo(u.$slideTrack) : r ? n(t).insertBefore(u.$slides.eq(i)) : n(t).insertAfter(u.$slides.eq(i)) : r === !0 ? n(t).prependTo(u.$slideTrack) : n(t).appendTo(u.$slideTrack);
        u.$slides = u.$slideTrack.children(this.options.slide);
        u.$slideTrack.children(this.options.slide).detach();
        u.$slideTrack.append(u.$slides);
        u.$slides.each(function(t, i) {
            n(i).attr("data-slick-index", t)
        });
        u.$slidesCache = u.$slides;
        u.reinit()
    };
    t.prototype.animateHeight = function() {
        var n = this,
            t;
        1 === n.options.slidesToShow && n.options.adaptiveHeight === !0 && n.options.vertical === !1 && (t = n.$slides.eq(n.currentSlide).outerHeight(!0), n.$list.animate({
            height: t
        }, n.options.speed))
    };
    t.prototype.animateSlide = function(t, i) {
        var u = {},
            r = this;
        r.animateHeight();
        r.options.rtl === !0 && r.options.vertical === !1 && (t = -t);
        r.transformsEnabled === !1 ? r.options.vertical === !1 ? r.$slideTrack.animate({
            left: t
        }, r.options.speed, r.options.easing, i) : r.$slideTrack.animate({
            top: t
        }, r.options.speed, r.options.easing, i) : r.cssTransitions === !1 ? (r.options.rtl === !0 && (r.currentLeft = -r.currentLeft), n({
            animStart: r.currentLeft
        }).animate({
            animStart: t
        }, {
            duration: r.options.speed,
            easing: r.options.easing,
            step: function(n) {
                n = Math.ceil(n);
                r.options.vertical === !1 ? (u[r.animType] = "translate(" + n + "px, 0px)", r.$slideTrack.css(u)) : (u[r.animType] = "translate(0px," + n + "px)", r.$slideTrack.css(u))
            },
            complete: function() {
                i && i.call()
            }
        })) : (r.applyTransition(), t = Math.ceil(t), u[r.animType] = r.options.vertical === !1 ? "translate3d(" + t + "px, 0px, 0px)" : "translate3d(0px," + t + "px, 0px)", r.$slideTrack.css(u), i && setTimeout(function() {
            r.disableTransition();
            i.call()
        }, r.options.speed))
    };
    t.prototype.asNavFor = function(t) {
        var r = this,
            i = r.options.asNavFor;
        i && null !== i && (i = n(i).not(r.$slider));
        null !== i && "object" == typeof i && i.each(function() {
            var i = n(this).slick("getSlick");
            i.unslicked || i.slideHandler(t, !0)
        })
    };
    t.prototype.applyTransition = function(n) {
        var t = this,
            i = {};
        i[t.transitionType] = t.options.fade === !1 ? t.transformType + " " + t.options.speed + "ms " + t.options.cssEase : "opacity " + t.options.speed + "ms " + t.options.cssEase;
        t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(n).css(i)
    };
    t.prototype.autoPlay = function() {
        var n = this;
        n.autoPlayTimer && clearInterval(n.autoPlayTimer);
        n.slideCount > n.options.slidesToShow && n.paused !== !0 && (n.autoPlayTimer = setInterval(n.autoPlayIterator, n.options.autoplaySpeed))
    };
    t.prototype.autoPlayClear = function() {
        var n = this;
        n.autoPlayTimer && clearInterval(n.autoPlayTimer)
    };
    t.prototype.autoPlayIterator = function() {
        var n = this;
        n.options.infinite === !1 ? 1 === n.direction ? (n.currentSlide + 1 === n.slideCount - 1 && (n.direction = 0), n.slideHandler(n.currentSlide + n.options.slidesToScroll)) : (0 == n.currentSlide - 1 && (n.direction = 1), n.slideHandler(n.currentSlide - n.options.slidesToScroll)) : n.slideHandler(n.currentSlide + n.options.slidesToScroll)
    };
    t.prototype.buildArrows = function() {
        var t = this;
        t.options.arrows === !0 && (t.$prevArrow = n(t.options.prevArrow).addClass("slick-arrow"), t.$nextArrow = n(t.options.nextArrow).addClass("slick-arrow"), t.slideCount > t.options.slidesToShow ? (t.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"), t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.prependTo(t.options.appendArrows), t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.appendTo(t.options.appendArrows), t.options.infinite !== !0 && t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : t.$prevArrow.add(t.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    };
    t.prototype.buildDots = function() {
        var i, r, t = this;
        if (t.options.dots === !0 && t.slideCount > t.options.slidesToShow) {
            for (r = '<ul class="' + t.options.dotsClass + '">', i = 0; i <= t.getDotCount(); i += 1) r += "<li>" + t.options.customPaging.call(this, t, i) + "<\/li>";
            r += "<\/ul>";
            t.$dots = n(r).appendTo(t.options.appendDots);
            t.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    };
    t.prototype.buildOut = function() {
        var t = this;
        t.$slides = t.$slider.children(t.options.slide + ":not(.slick-cloned)").addClass("slick-slide");
        t.slideCount = t.$slides.length;
        t.$slides.each(function(t, i) {
            n(i).attr("data-slick-index", t).data("originalStyling", n(i).attr("style") || "")
        });
        t.$slidesCache = t.$slides;
        t.$slider.addClass("slick-slider");
        t.$slideTrack = 0 === t.slideCount ? n('<div class="slick-track"/>').appendTo(t.$slider) : t.$slides.wrapAll('<div class="slick-track"/>').parent();
        t.$list = t.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent();
        t.$slideTrack.css("opacity", 0);
        (t.options.centerMode === !0 || t.options.swipeToSlide === !0) && (t.options.slidesToScroll = 1);
        n("img[data-lazy]", t.$slider).not("[src]").addClass("slick-loading");
        t.setupInfinite();
        t.buildArrows();
        t.buildDots();
        t.updateDots();
        t.setSlideClasses("number" == typeof t.currentSlide ? t.currentSlide : 0);
        t.options.draggable === !0 && t.$list.addClass("draggable")
    };
    t.prototype.buildRows = function() {
        var t, i, r, f, c, u, e, n = this,
            o, s, h;
        if (f = document.createDocumentFragment(), u = n.$slider.children(), n.options.rows > 1) {
            for (e = n.options.slidesPerRow * n.options.rows, c = Math.ceil(u.length / e), t = 0; c > t; t++) {
                for (o = document.createElement("div"), i = 0; i < n.options.rows; i++) {
                    for (s = document.createElement("div"), r = 0; r < n.options.slidesPerRow; r++) h = t * e + (i * n.options.slidesPerRow + r), u.get(h) && s.appendChild(u.get(h));
                    o.appendChild(s)
                }
                f.appendChild(o)
            }
            n.$slider.html(f);
            n.$slider.children().children().children().css({
                width: 100 / n.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    };
    t.prototype.checkResponsive = function(t, i) {
        var f, u, e, r = this,
            o = !1,
            s = r.$slider.width(),
            h = window.innerWidth || n(window).width();
        if ("window" === r.respondTo ? e = h : "slider" === r.respondTo ? e = s : "min" === r.respondTo && (e = Math.min(h, s)), r.options.responsive && r.options.responsive.length && null !== r.options.responsive) {
            u = null;
            for (f in r.breakpoints) r.breakpoints.hasOwnProperty(f) && (r.originalSettings.mobileFirst === !1 ? e < r.breakpoints[f] && (u = r.breakpoints[f]) : e > r.breakpoints[f] && (u = r.breakpoints[f]));
            null !== u ? null !== r.activeBreakpoint ? (u !== r.activeBreakpoint || i) && (r.activeBreakpoint = u, "unslick" === r.breakpointSettings[u] ? r.unslick(u) : (r.options = n.extend({}, r.originalSettings, r.breakpointSettings[u]), t === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(t)), o = u) : (r.activeBreakpoint = u, "unslick" === r.breakpointSettings[u] ? r.unslick(u) : (r.options = n.extend({}, r.originalSettings, r.breakpointSettings[u]), t === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(t)), o = u) : null !== r.activeBreakpoint && (r.activeBreakpoint = null, r.options = r.originalSettings, t === !0 && (r.currentSlide = r.options.initialSlide), r.refresh(t), o = u);
            t || o === !1 || r.$slider.trigger("breakpoint", [r, o])
        }
    };
    t.prototype.changeSlide = function(t, i) {
        var f, e, o, r = this,
            u = n(t.target),
            s;
        switch (u.is("a") && t.preventDefault(), u.is("li") || (u = u.closest("li")), o = 0 != r.slideCount % r.options.slidesToScroll, f = o ? 0 : (r.slideCount - r.currentSlide) % r.options.slidesToScroll, t.data.message) {
            case "previous":
                e = 0 === f ? r.options.slidesToScroll : r.options.slidesToShow - f;
                r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide - e, !1, i);
                break;
            case "next":
                e = 0 === f ? r.options.slidesToScroll : f;
                r.slideCount > r.options.slidesToShow && r.slideHandler(r.currentSlide + e, !1, i);
                break;
            case "index":
                s = 0 === t.data.index ? 0 : t.data.index || u.index() * r.options.slidesToScroll;
                r.slideHandler(r.checkNavigable(s), !1, i);
                u.children().trigger("focus");
                break;
            default:
                return
        }
    };
    t.prototype.checkNavigable = function(n) {
        var t, i, u = this,
            r;
        if (t = u.getNavigableIndexes(), i = 0, n > t[t.length - 1]) n = t[t.length - 1];
        else
            for (r in t) {
                if (n < t[r]) {
                    n = i;
                    break
                }
                i = t[r]
            }
        return n
    };
    t.prototype.cleanUpEvents = function() {
        var t = this;
        t.options.dots && null !== t.$dots && (n("li", t.$dots).off("click.slick", t.changeSlide), t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && n("li", t.$dots).off("mouseenter.slick", n.proxy(t.setPaused, t, !0)).off("mouseleave.slick", n.proxy(t.setPaused, t, !1)));
        t.options.arrows === !0 && t.slideCount > t.options.slidesToShow && (t.$prevArrow && t.$prevArrow.off("click.slick", t.changeSlide), t.$nextArrow && t.$nextArrow.off("click.slick", t.changeSlide));
        t.$list.off("touchstart.slick mousedown.slick", t.swipeHandler);
        t.$list.off("touchmove.slick mousemove.slick", t.swipeHandler);
        t.$list.off("touchend.slick mouseup.slick", t.swipeHandler);
        t.$list.off("touchcancel.slick mouseleave.slick", t.swipeHandler);
        t.$list.off("click.slick", t.clickHandler);
        n(document).off(t.visibilityChange, t.visibility);
        t.$list.off("mouseenter.slick", n.proxy(t.setPaused, t, !0));
        t.$list.off("mouseleave.slick", n.proxy(t.setPaused, t, !1));
        t.options.accessibility === !0 && t.$list.off("keydown.slick", t.keyHandler);
        t.options.focusOnSelect === !0 && n(t.$slideTrack).children().off("click.slick", t.selectHandler);
        n(window).off("orientationchange.slick.slick-" + t.instanceUid, t.orientationChange);
        n(window).off("resize.slick.slick-" + t.instanceUid, t.resize);
        n("[draggable!=true]", t.$slideTrack).off("dragstart", t.preventDefault);
        n(window).off("load.slick.slick-" + t.instanceUid, t.setPosition);
        n(document).off("ready.slick.slick-" + t.instanceUid, t.setPosition)
    };
    t.prototype.cleanUpRows = function() {
        var n, t = this;
        t.options.rows > 1 && (n = t.$slides.children().children(), n.removeAttr("style"), t.$slider.html(n))
    };
    t.prototype.clickHandler = function(n) {
        var t = this;
        t.shouldClick === !1 && (n.stopImmediatePropagation(), n.stopPropagation(), n.preventDefault())
    };
    t.prototype.destroy = function(t) {
        var i = this;
        i.autoPlayClear();
        i.touchObject = {};
        i.cleanUpEvents();
        n(".slick-cloned", i.$slider).detach();
        i.$dots && i.$dots.remove();
        i.$prevArrow && i.$prevArrow.length && (i.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.prevArrow) && i.$prevArrow.remove());
        i.$nextArrow && i.$nextArrow.length && (i.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""), i.htmlExpr.test(i.options.nextArrow) && i.$nextArrow.remove());
        i.$slides && (i.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            n(this).attr("style", n(this).data("originalStyling"))
        }), i.$slideTrack.children(this.options.slide).detach(), i.$slideTrack.detach(), i.$list.detach(), i.$slider.append(i.$slides));
        i.cleanUpRows();
        i.$slider.removeClass("slick-slider");
        i.$slider.removeClass("slick-initialized");
        i.unslicked = !0;
        t || i.$slider.trigger("destroy", [i])
    };
    t.prototype.disableTransition = function(n) {
        var t = this,
            i = {};
        i[t.transitionType] = "";
        t.options.fade === !1 ? t.$slideTrack.css(i) : t.$slides.eq(n).css(i)
    };
    t.prototype.fadeSlide = function(n, t) {
        var i = this;
        i.cssTransitions === !1 ? (i.$slides.eq(n).css({
            zIndex: i.options.zIndex
        }), i.$slides.eq(n).animate({
            opacity: 1
        }, i.options.speed, i.options.easing, t)) : (i.applyTransition(n), i.$slides.eq(n).css({
            opacity: 1,
            zIndex: i.options.zIndex
        }), t && setTimeout(function() {
            i.disableTransition(n);
            t.call()
        }, i.options.speed))
    };
    t.prototype.fadeSlideOut = function(n) {
        var t = this;
        t.cssTransitions === !1 ? t.$slides.eq(n).animate({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }, t.options.speed, t.options.easing) : (t.applyTransition(n), t.$slides.eq(n).css({
            opacity: 0,
            zIndex: t.options.zIndex - 2
        }))
    };
    t.prototype.filterSlides = t.prototype.slickFilter = function(n) {
        var t = this;
        null !== n && (t.unload(), t.$slideTrack.children(this.options.slide).detach(), t.$slidesCache.filter(n).appendTo(t.$slideTrack), t.reinit())
    };
    t.prototype.getCurrent = t.prototype.slickCurrentSlide = function() {
        var n = this;
        return n.currentSlide
    };
    t.prototype.getDotCount = function() {
        var n = this,
            t = 0,
            i = 0,
            r = 0;
        if (n.options.infinite === !0)
            for (; t < n.slideCount;) ++r, t = i + n.options.slidesToShow, i += n.options.slidesToScroll <= n.options.slidesToShow ? n.options.slidesToScroll : n.options.slidesToShow;
        else if (n.options.centerMode === !0) r = n.slideCount;
        else
            for (; t < n.slideCount;) ++r, t = i + n.options.slidesToShow, i += n.options.slidesToScroll <= n.options.slidesToShow ? n.options.slidesToScroll : n.options.slidesToShow;
        return r - 1
    };
    t.prototype.getLeft = function(n) {
        var f, r, i, t = this,
            u = 0;
        return t.slideOffset = 0, r = t.$slides.first().outerHeight(!0), t.options.infinite === !0 ? (t.slideCount > t.options.slidesToShow && (t.slideOffset = -1 * t.slideWidth * t.options.slidesToShow, u = -1 * r * t.options.slidesToShow), 0 != t.slideCount % t.options.slidesToScroll && n + t.options.slidesToScroll > t.slideCount && t.slideCount > t.options.slidesToShow && (n > t.slideCount ? (t.slideOffset = -1 * (t.options.slidesToShow - (n - t.slideCount)) * t.slideWidth, u = -1 * (t.options.slidesToShow - (n - t.slideCount)) * r) : (t.slideOffset = -1 * t.slideCount % t.options.slidesToScroll * t.slideWidth, u = -1 * t.slideCount % t.options.slidesToScroll * r))) : n + t.options.slidesToShow > t.slideCount && (t.slideOffset = (n + t.options.slidesToShow - t.slideCount) * t.slideWidth, u = (n + t.options.slidesToShow - t.slideCount) * r), t.slideCount <= t.options.slidesToShow && (t.slideOffset = 0, u = 0), t.options.centerMode === !0 && t.options.infinite === !0 ? t.slideOffset += t.slideWidth * Math.floor(t.options.slidesToShow / 2) - t.slideWidth : t.options.centerMode === !0 && (t.slideOffset = 0, t.slideOffset += t.slideWidth * Math.floor(t.options.slidesToShow / 2)), f = t.options.vertical === !1 ? -1 * n * t.slideWidth + t.slideOffset : -1 * n * r + u, t.options.variableWidth === !0 && (i = t.slideCount <= t.options.slidesToShow || t.options.infinite === !1 ? t.$slideTrack.children(".slick-slide").eq(n) : t.$slideTrack.children(".slick-slide").eq(n + t.options.slidesToShow), f = i[0] ? -1 * i[0].offsetLeft : 0, t.options.centerMode === !0 && (i = t.options.infinite === !1 ? t.$slideTrack.children(".slick-slide").eq(n) : t.$slideTrack.children(".slick-slide").eq(n + t.options.slidesToShow + 1), f = i[0] ? -1 * i[0].offsetLeft : 0, f += (t.$list.width() - i.outerWidth()) / 2)), f
    };
    t.prototype.getOption = t.prototype.slickGetOption = function(n) {
        var t = this;
        return t.options[n]
    };
    t.prototype.getNavigableIndexes = function() {
        var i, n = this,
            t = 0,
            r = 0,
            u = [];
        for (n.options.infinite === !1 ? i = n.slideCount : (t = -1 * n.options.slidesToScroll, r = -1 * n.options.slidesToScroll, i = 2 * n.slideCount); i > t;) u.push(t), t = r + n.options.slidesToScroll, r += n.options.slidesToScroll <= n.options.slidesToShow ? n.options.slidesToScroll : n.options.slidesToShow;
        return u
    };
    t.prototype.getSlick = function() {
        return this
    };
    t.prototype.getSlideCount = function() {
        var u, i, r, t = this;
        return r = t.options.centerMode === !0 ? t.slideWidth * Math.floor(t.options.slidesToShow / 2) : 0, t.options.swipeToSlide === !0 ? (t.$slideTrack.find(".slick-slide").each(function(u, f) {
            if (f.offsetLeft - r + n(f).outerWidth() / 2 > -1 * t.swipeLeft) return (i = f, !1)
        }), u = Math.abs(n(i).attr("data-slick-index") - t.currentSlide) || 1) : t.options.slidesToScroll
    };
    t.prototype.goTo = t.prototype.slickGoTo = function(n, t) {
        var i = this;
        i.changeSlide({
            data: {
                message: "index",
                index: parseInt(n)
            }
        }, t)
    };
    t.prototype.init = function(t) {
        var i = this;
        n(i.$slider).hasClass("slick-initialized") || (n(i.$slider).addClass("slick-initialized"), i.buildRows(), i.buildOut(), i.setProps(), i.startLoad(), i.loadSlider(), i.initializeEvents(), i.updateArrows(), i.updateDots());
        t && i.$slider.trigger("init", [i]);
        i.options.accessibility === !0 && i.initADA()
    };
    t.prototype.initArrowEvents = function() {
        var n = this;
        n.options.arrows === !0 && n.slideCount > n.options.slidesToShow && (n.$prevArrow.on("click.slick", {
            message: "previous"
        }, n.changeSlide), n.$nextArrow.on("click.slick", {
            message: "next"
        }, n.changeSlide))
    };
    t.prototype.initDotEvents = function() {
        var t = this;
        t.options.dots === !0 && t.slideCount > t.options.slidesToShow && n("li", t.$dots).on("click.slick", {
            message: "index"
        }, t.changeSlide);
        t.options.dots === !0 && t.options.pauseOnDotsHover === !0 && t.options.autoplay === !0 && n("li", t.$dots).on("mouseenter.slick", n.proxy(t.setPaused, t, !0)).on("mouseleave.slick", n.proxy(t.setPaused, t, !1))
    };
    t.prototype.initializeEvents = function() {
        var t = this;
        t.initArrowEvents();
        t.initDotEvents();
        t.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, t.swipeHandler);
        t.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, t.swipeHandler);
        t.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, t.swipeHandler);
        t.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, t.swipeHandler);
        t.$list.on("click.slick", t.clickHandler);
        n(document).on(t.visibilityChange, n.proxy(t.visibility, t));
        t.$list.on("mouseenter.slick", n.proxy(t.setPaused, t, !0));
        t.$list.on("mouseleave.slick", n.proxy(t.setPaused, t, !1));
        t.options.accessibility === !0 && t.$list.on("keydown.slick", t.keyHandler);
        t.options.focusOnSelect === !0 && n(t.$slideTrack).children().on("click.slick", t.selectHandler);
        n(window).on("orientationchange.slick.slick-" + t.instanceUid, n.proxy(t.orientationChange, t));
        n(window).on("resize.slick.slick-" + t.instanceUid, n.proxy(t.resize, t));
        n("[draggable!=true]", t.$slideTrack).on("dragstart", t.preventDefault);
        n(window).on("load.slick.slick-" + t.instanceUid, t.setPosition);
        n(document).on("ready.slick.slick-" + t.instanceUid, t.setPosition)
    };
    t.prototype.initUI = function() {
        var n = this;
        n.options.arrows === !0 && n.slideCount > n.options.slidesToShow && (n.$prevArrow.show(), n.$nextArrow.show());
        n.options.dots === !0 && n.slideCount > n.options.slidesToShow && n.$dots.show();
        n.options.autoplay === !0 && n.autoPlay()
    };
    t.prototype.keyHandler = function(n) {
        var t = this;
        n.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === n.keyCode && t.options.accessibility === !0 ? t.changeSlide({
            data: {
                message: "previous"
            }
        }) : 39 === n.keyCode && t.options.accessibility === !0 && t.changeSlide({
            data: {
                message: "next"
            }
        }))
    };
    t.prototype.lazyLoad = function() {
        function f(t) {
            n("img[data-lazy]", t).each(function() {
                var t = n(this),
                    i = n(this).attr("data-lazy"),
                    r = document.createElement("img");
                r.onload = function() {
                    t.animate({
                        opacity: 0
                    }, 100, function() {
                        t.attr("src", i).animate({
                            opacity: 1
                        }, 200, function() {
                            t.removeAttr("data-lazy").removeClass("slick-loading")
                        })
                    })
                };
                r.src = i
            })
        }
        var e, r, i, u, t = this;
        t.options.centerMode === !0 ? t.options.infinite === !0 ? (i = t.currentSlide + (t.options.slidesToShow / 2 + 1), u = i + t.options.slidesToShow + 2) : (i = Math.max(0, t.currentSlide - (t.options.slidesToShow / 2 + 1)), u = 2 + (t.options.slidesToShow / 2 + 1) + t.currentSlide) : (i = t.options.infinite ? t.options.slidesToShow + t.currentSlide : t.currentSlide, u = i + t.options.slidesToShow, t.options.fade === !0 && (i > 0 && i--, u <= t.slideCount && u++));
        e = t.$slider.find(".slick-slide").slice(i, u);
        f(e);
        t.slideCount <= t.options.slidesToShow ? (r = t.$slider.find(".slick-slide"), f(r)) : t.currentSlide >= t.slideCount - t.options.slidesToShow ? (r = t.$slider.find(".slick-cloned").slice(0, t.options.slidesToShow), f(r)) : 0 === t.currentSlide && (r = t.$slider.find(".slick-cloned").slice(-1 * t.options.slidesToShow), f(r))
    };
    t.prototype.loadSlider = function() {
        var n = this;
        n.setPosition();
        n.$slideTrack.css({
            opacity: 1
        });
        n.$slider.removeClass("slick-loading");
        n.initUI();
        "progressive" === n.options.lazyLoad && n.progressiveLazyLoad()
    };
    t.prototype.next = t.prototype.slickNext = function() {
        var n = this;
        n.changeSlide({
            data: {
                message: "next"
            }
        })
    };
    t.prototype.orientationChange = function() {
        var n = this;
        n.checkResponsive();
        n.setPosition()
    };
    t.prototype.pause = t.prototype.slickPause = function() {
        var n = this;
        n.autoPlayClear();
        n.paused = !0
    };
    t.prototype.play = t.prototype.slickPlay = function() {
        var n = this;
        n.paused = !1;
        n.autoPlay()
    };
    t.prototype.postSlide = function(n) {
        var t = this;
        t.$slider.trigger("afterChange", [t, n]);
        t.animating = !1;
        t.setPosition();
        t.swipeLeft = null;
        t.options.autoplay === !0 && t.paused === !1 && t.autoPlay();
        t.options.accessibility === !0 && t.initADA()
    };
    t.prototype.prev = t.prototype.slickPrev = function() {
        var n = this;
        n.changeSlide({
            data: {
                message: "previous"
            }
        })
    };
    t.prototype.preventDefault = function(n) {
        n.preventDefault()
    };
    t.prototype.progressiveLazyLoad = function() {
        var r, i, t = this;
        r = n("img[data-lazy]", t.$slider).length;
        r > 0 && (i = n("img[data-lazy]", t.$slider).first(), i.attr("src", i.attr("data-lazy")).removeClass("slick-loading").load(function() {
            i.removeAttr("data-lazy");
            t.progressiveLazyLoad();
            t.options.adaptiveHeight === !0 && t.setPosition()
        }).error(function() {
            i.removeAttr("data-lazy");
            t.progressiveLazyLoad()
        }))
    };
    t.prototype.refresh = function(t) {
        var i = this,
            r = i.currentSlide;
        i.destroy(!0);
        n.extend(i, i.initials, {
            currentSlide: r
        });
        i.init();
        t || i.changeSlide({
            data: {
                message: "index",
                index: r
            }
        }, !1)
    };
    t.prototype.registerBreakpoints = function() {
        var u, f, i, t = this,
            r = t.options.responsive || null;
        if ("array" === n.type(r) && r.length) {
            t.respondTo = t.options.respondTo || "window";
            for (u in r)
                if (i = t.breakpoints.length - 1, f = r[u].breakpoint, r.hasOwnProperty(u)) {
                    for (; i >= 0;) t.breakpoints[i] && t.breakpoints[i] === f && t.breakpoints.splice(i, 1), i--;
                    t.breakpoints.push(f);
                    t.breakpointSettings[f] = r[u].settings
                }
            t.breakpoints.sort(function(n, i) {
                return t.options.mobileFirst ? n - i : i - n
            })
        }
    };
    t.prototype.reinit = function() {
        var t = this;
        t.$slides = t.$slideTrack.children(t.options.slide).addClass("slick-slide");
        t.slideCount = t.$slides.length;
        t.currentSlide >= t.slideCount && 0 !== t.currentSlide && (t.currentSlide = t.currentSlide - t.options.slidesToScroll);
        t.slideCount <= t.options.slidesToShow && (t.currentSlide = 0);
        t.registerBreakpoints();
        t.setProps();
        t.setupInfinite();
        t.buildArrows();
        t.updateArrows();
        t.initArrowEvents();
        t.buildDots();
        t.updateDots();
        t.initDotEvents();
        t.checkResponsive(!1, !0);
        t.options.focusOnSelect === !0 && n(t.$slideTrack).children().on("click.slick", t.selectHandler);
        t.setSlideClasses(0);
        t.setPosition();
        t.$slider.trigger("reInit", [t]);
        t.options.autoplay === !0 && t.focusHandler()
    };
    t.prototype.resize = function() {
        var t = this;
        n(window).width() !== t.windowWidth && (clearTimeout(t.windowDelay), t.windowDelay = window.setTimeout(function() {
            t.windowWidth = n(window).width();
            t.checkResponsive();
            t.unslicked || t.setPosition()
        }, 50))
    };
    t.prototype.removeSlide = t.prototype.slickRemove = function(n, t, i) {
        var r = this;
        return "boolean" == typeof n ? (t = n, n = t === !0 ? 0 : r.slideCount - 1) : n = t === !0 ? --n : n, r.slideCount < 1 || 0 > n || n > r.slideCount - 1 ? !1 : (r.unload(), i === !0 ? r.$slideTrack.children().remove() : r.$slideTrack.children(this.options.slide).eq(n).remove(), r.$slides = r.$slideTrack.children(this.options.slide), r.$slideTrack.children(this.options.slide).detach(), r.$slideTrack.append(r.$slides), r.$slidesCache = r.$slides, r.reinit(), void 0)
    };
    t.prototype.setCSS = function(n) {
        var r, u, t = this,
            i = {};
        t.options.rtl === !0 && (n = -n);
        r = "left" == t.positionProp ? Math.ceil(n) + "px" : "0px";
        u = "top" == t.positionProp ? Math.ceil(n) + "px" : "0px";
        i[t.positionProp] = n;
        t.transformsEnabled === !1 ? t.$slideTrack.css(i) : (i = {}, t.cssTransitions === !1 ? (i[t.animType] = "translate(" + r + ", " + u + ")", t.$slideTrack.css(i)) : (i[t.animType] = "translate3d(" + r + ", " + u + ", 0px)", t.$slideTrack.css(i)))
    };
    t.prototype.setDimensions = function() {
        var n = this,
            t;
        n.options.vertical === !1 ? n.options.centerMode === !0 && n.$list.css({
            padding: "0px " + n.options.centerPadding
        }) : (n.$list.height(n.$slides.first().outerHeight(!0) * n.options.slidesToShow), n.options.centerMode === !0 && n.$list.css({
            padding: n.options.centerPadding + " 0px"
        }));
        n.listWidth = n.$list.width();
        n.listHeight = n.$list.height();
        n.options.vertical === !1 && n.options.variableWidth === !1 ? (n.slideWidth = Math.ceil(n.listWidth / n.options.slidesToShow), n.$slideTrack.width(Math.ceil(n.slideWidth * n.$slideTrack.children(".slick-slide").length))) : n.options.variableWidth === !0 ? n.$slideTrack.width(5e3 * n.slideCount) : (n.slideWidth = Math.ceil(n.listWidth), n.$slideTrack.height(Math.ceil(n.$slides.first().outerHeight(!0) * n.$slideTrack.children(".slick-slide").length)));
        t = n.$slides.first().outerWidth(!0) - n.$slides.first().width();
        n.options.variableWidth === !1 && n.$slideTrack.children(".slick-slide").width(n.slideWidth - t)
    };
    t.prototype.setFade = function() {
        var i, t = this;
        t.$slides.each(function(r, u) {
            i = -1 * t.slideWidth * r;
            t.options.rtl === !0 ? n(u).css({
                position: "relative",
                right: i,
                top: 0,
                zIndex: t.options.zIndex - 2,
                opacity: 0
            }) : n(u).css({
                position: "relative",
                left: i,
                top: 0,
                zIndex: t.options.zIndex - 2,
                opacity: 0
            })
        });
        t.$slides.eq(t.currentSlide).css({
            zIndex: t.options.zIndex - 1,
            opacity: 1
        })
    };
    t.prototype.setHeight = function() {
        var n = this,
            t;
        1 === n.options.slidesToShow && n.options.adaptiveHeight === !0 && n.options.vertical === !1 && (t = n.$slides.eq(n.currentSlide).outerHeight(!0), n.$list.css("height", t))
    };
    t.prototype.setOption = t.prototype.slickSetOption = function(t, i, r) {
        var f, e, u = this;
        if ("responsive" === t && "array" === n.type(i))
            for (e in i)
                if ("array" !== n.type(u.options.responsive)) u.options.responsive = [i[e]];
                else {
                    for (f = u.options.responsive.length - 1; f >= 0;) u.options.responsive[f].breakpoint === i[e].breakpoint && u.options.responsive.splice(f, 1), f--;
                    u.options.responsive.push(i[e])
                }
        else u.options[t] = i;
        r === !0 && (u.unload(), u.reinit())
    };
    t.prototype.setPosition = function() {
        var n = this;
        n.setDimensions();
        n.setHeight();
        n.options.fade === !1 ? n.setCSS(n.getLeft(n.currentSlide)) : n.setFade();
        n.$slider.trigger("setPosition", [n])
    };
    t.prototype.setProps = function() {
        var n = this,
            t = document.body.style;
        n.positionProp = n.options.vertical === !0 ? "top" : "left";
        "top" === n.positionProp ? n.$slider.addClass("slick-vertical") : n.$slider.removeClass("slick-vertical");
        (void 0 !== t.WebkitTransition || void 0 !== t.MozTransition || void 0 !== t.msTransition) && n.options.useCSS === !0 && (n.cssTransitions = !0);
        n.options.fade && ("number" == typeof n.options.zIndex ? n.options.zIndex < 3 && (n.options.zIndex = 3) : n.options.zIndex = n.defaults.zIndex);
        void 0 !== t.OTransform && (n.animType = "OTransform", n.transformType = "-o-transform", n.transitionType = "OTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (n.animType = !1));
        void 0 !== t.MozTransform && (n.animType = "MozTransform", n.transformType = "-moz-transform", n.transitionType = "MozTransition", void 0 === t.perspectiveProperty && void 0 === t.MozPerspective && (n.animType = !1));
        void 0 !== t.webkitTransform && (n.animType = "webkitTransform", n.transformType = "-webkit-transform", n.transitionType = "webkitTransition", void 0 === t.perspectiveProperty && void 0 === t.webkitPerspective && (n.animType = !1));
        void 0 !== t.msTransform && (n.animType = "msTransform", n.transformType = "-ms-transform", n.transitionType = "msTransition", void 0 === t.msTransform && (n.animType = !1));
        void 0 !== t.transform && n.animType !== !1 && (n.animType = "transform", n.transformType = "transform", n.transitionType = "transition");
        n.transformsEnabled = null !== n.animType && n.animType !== !1
    };
    t.prototype.setSlideClasses = function(n) {
        var u, i, r, f, t = this;
        i = t.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true");
        t.$slides.eq(n).addClass("slick-current");
        t.options.centerMode === !0 ? (u = Math.floor(t.options.slidesToShow / 2), t.options.infinite === !0 && (n >= u && n <= t.slideCount - 1 - u ? t.$slides.slice(n - u, n + u + 1).addClass("slick-active").attr("aria-hidden", "false") : (r = t.options.slidesToShow + n, i.slice(r - u + 1, r + u + 2).addClass("slick-active").attr("aria-hidden", "false")), 0 === n ? i.eq(i.length - 1 - t.options.slidesToShow).addClass("slick-center") : n === t.slideCount - 1 && i.eq(t.options.slidesToShow).addClass("slick-center")), t.$slides.eq(n).addClass("slick-center")) : n >= 0 && n <= t.slideCount - t.options.slidesToShow ? t.$slides.slice(n, n + t.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : i.length <= t.options.slidesToShow ? i.addClass("slick-active").attr("aria-hidden", "false") : (f = t.slideCount % t.options.slidesToShow, r = t.options.infinite === !0 ? t.options.slidesToShow + n : n, t.options.slidesToShow == t.options.slidesToScroll && t.slideCount - n < t.options.slidesToShow ? i.slice(r - (t.options.slidesToShow - f), r + f).addClass("slick-active").attr("aria-hidden", "false") : i.slice(r, r + t.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false"));
        "ondemand" === t.options.lazyLoad && t.lazyLoad()
    };
    t.prototype.setupInfinite = function() {
        var i, r, u, t = this;
        if (t.options.fade === !0 && (t.options.centerMode = !1), t.options.infinite === !0 && t.options.fade === !1 && (r = null, t.slideCount > t.options.slidesToShow)) {
            for (u = t.options.centerMode === !0 ? t.options.slidesToShow + 1 : t.options.slidesToShow, i = t.slideCount; i > t.slideCount - u; i -= 1) r = i - 1, n(t.$slides[r]).clone(!0).attr("id", "").attr("data-slick-index", r - t.slideCount).prependTo(t.$slideTrack).addClass("slick-cloned");
            for (i = 0; u > i; i += 1) r = i, n(t.$slides[r]).clone(!0).attr("id", "").attr("data-slick-index", r + t.slideCount).appendTo(t.$slideTrack).addClass("slick-cloned");
            t.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                n(this).attr("id", "")
            })
        }
    };
    t.prototype.setPaused = function(n) {
        var t = this;
        t.options.autoplay === !0 && t.options.pauseOnHover === !0 && (t.paused = n, n ? t.autoPlayClear() : t.autoPlay())
    };
    t.prototype.selectHandler = function(t) {
        var i = this,
            u = n(t.target).is(".slick-slide") ? n(t.target) : n(t.target).parents(".slick-slide"),
            r = parseInt(u.attr("data-slick-index"));
        return r || (r = 0), i.slideCount <= i.options.slidesToShow ? (i.setSlideClasses(r), i.asNavFor(r), void 0) : (i.slideHandler(r), void 0)
    };
    t.prototype.slideHandler = function(n, t, i) {
        var u, f, o, e, s = null,
            r = this;
        return t = t || !1, r.animating === !0 && r.options.waitForAnimate === !0 || r.options.fade === !0 && r.currentSlide === n || r.slideCount <= r.options.slidesToShow ? void 0 : (t === !1 && r.asNavFor(n), u = n, s = r.getLeft(u), e = r.getLeft(r.currentSlide), r.currentLeft = null === r.swipeLeft ? e : r.swipeLeft, r.options.infinite === !1 && r.options.centerMode === !1 && (0 > n || n > r.getDotCount() * r.options.slidesToScroll) ? (r.options.fade === !1 && (u = r.currentSlide, i !== !0 ? r.animateSlide(e, function() {
            r.postSlide(u)
        }) : r.postSlide(u)), void 0) : r.options.infinite === !1 && r.options.centerMode === !0 && (0 > n || n > r.slideCount - r.options.slidesToScroll) ? (r.options.fade === !1 && (u = r.currentSlide, i !== !0 ? r.animateSlide(e, function() {
            r.postSlide(u)
        }) : r.postSlide(u)), void 0) : (r.options.autoplay === !0 && clearInterval(r.autoPlayTimer), f = 0 > u ? 0 != r.slideCount % r.options.slidesToScroll ? r.slideCount - r.slideCount % r.options.slidesToScroll : r.slideCount + u : u >= r.slideCount ? 0 != r.slideCount % r.options.slidesToScroll ? 0 : u - r.slideCount : u, r.animating = !0, r.$slider.trigger("beforeChange", [r, r.currentSlide, f]), o = r.currentSlide, r.currentSlide = f, r.setSlideClasses(r.currentSlide), r.updateDots(), r.updateArrows(), r.options.fade === !0 ? (i !== !0 ? (r.fadeSlideOut(o), r.fadeSlide(f, function() {
            r.postSlide(f)
        })) : r.postSlide(f), r.animateHeight(), void 0) : (i !== !0 ? r.animateSlide(s, function() {
            r.postSlide(f)
        }) : r.postSlide(f), void 0)))
    };
    t.prototype.startLoad = function() {
        var n = this;
        n.options.arrows === !0 && n.slideCount > n.options.slidesToShow && (n.$prevArrow.hide(), n.$nextArrow.hide());
        n.options.dots === !0 && n.slideCount > n.options.slidesToShow && n.$dots.hide();
        n.$slider.addClass("slick-loading")
    };
    t.prototype.swipeDirection = function() {
        var i, r, u, n, t = this;
        return i = t.touchObject.startX - t.touchObject.curX, r = t.touchObject.startY - t.touchObject.curY, u = Math.atan2(r, i), n = Math.round(180 * u / Math.PI), 0 > n && (n = 360 - Math.abs(n)), 45 >= n && n >= 0 ? t.options.rtl === !1 ? "left" : "right" : 360 >= n && n >= 315 ? t.options.rtl === !1 ? "left" : "right" : n >= 135 && 225 >= n ? t.options.rtl === !1 ? "right" : "left" : t.options.verticalSwiping === !0 ? n >= 35 && 135 >= n ? "left" : "right" : "vertical"
    };
    t.prototype.swipeEnd = function() {
        var t, n = this;
        if (n.dragging = !1, n.shouldClick = n.touchObject.swipeLength > 10 ? !1 : !0, void 0 === n.touchObject.curX) return !1;
        if (n.touchObject.edgeHit === !0 && n.$slider.trigger("edge", [n, n.swipeDirection()]), n.touchObject.swipeLength >= n.touchObject.minSwipe) switch (n.swipeDirection()) {
            case "left":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide + n.getSlideCount()) : n.currentSlide + n.getSlideCount();
                n.slideHandler(t);
                n.currentDirection = 0;
                n.touchObject = {};
                n.$slider.trigger("swipe", [n, "left"]);
                break;
            case "right":
                t = n.options.swipeToSlide ? n.checkNavigable(n.currentSlide - n.getSlideCount()) : n.currentSlide - n.getSlideCount();
                n.slideHandler(t);
                n.currentDirection = 1;
                n.touchObject = {};
                n.$slider.trigger("swipe", [n, "right"])
        } else n.touchObject.startX !== n.touchObject.curX && (n.slideHandler(n.currentSlide), n.touchObject = {})
    };
    t.prototype.swipeHandler = function(n) {
        var t = this;
        if (!(t.options.swipe === !1 || "ontouchend" in document && t.options.swipe === !1 || t.options.draggable === !1 && -1 !== n.type.indexOf("mouse"))) switch (t.touchObject.fingerCount = n.originalEvent && void 0 !== n.originalEvent.touches ? n.originalEvent.touches.length : 1, t.touchObject.minSwipe = t.listWidth / t.options.touchThreshold, t.options.verticalSwiping === !0 && (t.touchObject.minSwipe = t.listHeight / t.options.touchThreshold), n.data.action) {
            case "start":
                t.swipeStart(n);
                break;
            case "move":
                t.swipeMove(n);
                break;
            case "end":
                t.swipeEnd(n)
        }
    };
    t.prototype.swipeMove = function(n) {
        var f, e, r, u, i, t = this;
        return i = void 0 !== n.originalEvent ? n.originalEvent.touches : null, !t.dragging || i && 1 !== i.length ? !1 : (f = t.getLeft(t.currentSlide), t.touchObject.curX = void 0 !== i ? i[0].pageX : n.clientX, t.touchObject.curY = void 0 !== i ? i[0].pageY : n.clientY, t.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(t.touchObject.curX - t.touchObject.startX, 2))), t.options.verticalSwiping === !0 && (t.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(t.touchObject.curY - t.touchObject.startY, 2)))), e = t.swipeDirection(), "vertical" !== e ? (void 0 !== n.originalEvent && t.touchObject.swipeLength > 4 && n.preventDefault(), u = (t.options.rtl === !1 ? 1 : -1) * (t.touchObject.curX > t.touchObject.startX ? 1 : -1), t.options.verticalSwiping === !0 && (u = t.touchObject.curY > t.touchObject.startY ? 1 : -1), r = t.touchObject.swipeLength, t.touchObject.edgeHit = !1, t.options.infinite === !1 && (0 === t.currentSlide && "right" === e || t.currentSlide >= t.getDotCount() && "left" === e) && (r = t.touchObject.swipeLength * t.options.edgeFriction, t.touchObject.edgeHit = !0), t.swipeLeft = t.options.vertical === !1 ? f + r * u : f + r * (t.$list.height() / t.listWidth) * u, t.options.verticalSwiping === !0 && (t.swipeLeft = f + r * u), t.options.fade === !0 || t.options.touchMove === !1 ? !1 : t.animating === !0 ? (t.swipeLeft = null, !1) : (t.setCSS(t.swipeLeft), void 0)) : void 0)
    };
    t.prototype.swipeStart = function(n) {
        var i, t = this;
        return 1 !== t.touchObject.fingerCount || t.slideCount <= t.options.slidesToShow ? (t.touchObject = {}, !1) : (void 0 !== n.originalEvent && void 0 !== n.originalEvent.touches && (i = n.originalEvent.touches[0]), t.touchObject.startX = t.touchObject.curX = void 0 !== i ? i.pageX : n.clientX, t.touchObject.startY = t.touchObject.curY = void 0 !== i ? i.pageY : n.clientY, t.dragging = !0, void 0)
    };
    t.prototype.unfilterSlides = t.prototype.slickUnfilter = function() {
        var n = this;
        null !== n.$slidesCache && (n.unload(), n.$slideTrack.children(this.options.slide).detach(), n.$slidesCache.appendTo(n.$slideTrack), n.reinit())
    };
    t.prototype.unload = function() {
        var t = this;
        n(".slick-cloned", t.$slider).remove();
        t.$dots && t.$dots.remove();
        t.$prevArrow && t.htmlExpr.test(t.options.prevArrow) && t.$prevArrow.remove();
        t.$nextArrow && t.htmlExpr.test(t.options.nextArrow) && t.$nextArrow.remove();
        t.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    };
    t.prototype.unslick = function(n) {
        var t = this;
        t.$slider.trigger("unslick", [t, n]);
        t.destroy()
    };
    t.prototype.updateArrows = function() {
        var t, n = this;
        t = Math.floor(n.options.slidesToShow / 2);
        n.options.arrows === !0 && n.slideCount > n.options.slidesToShow && !n.options.infinite && (n.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), n.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"), 0 === n.currentSlide ? (n.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"), n.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : n.currentSlide >= n.slideCount - n.options.slidesToShow && n.options.centerMode === !1 ? (n.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), n.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : n.currentSlide >= n.slideCount - 1 && n.options.centerMode === !0 && (n.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"), n.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    };
    t.prototype.updateDots = function() {
        var n = this;
        null !== n.$dots && (n.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"), n.$dots.find("li").eq(Math.floor(n.currentSlide / n.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    };
    t.prototype.visibility = function() {
        var n = this;
        document[n.hidden] ? (n.paused = !0, n.autoPlayClear()) : n.options.autoplay === !0 && (n.paused = !1, n.autoPlay())
    };
    t.prototype.initADA = function() {
        var t = this;
        t.$slides.add(t.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        });
        t.$slideTrack.attr("role", "listbox");
        t.$slides.not(t.$slideTrack.find(".slick-cloned")).each(function(i) {
            n(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + t.instanceUid + i
            })
        });
        null !== t.$dots && t.$dots.attr("role", "tablist").find("li").each(function(i) {
            n(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + t.instanceUid + i,
                id: "slick-slide" + t.instanceUid + i
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar");
        t.activateADA()
    };
    t.prototype.activateADA = function() {
        var n = this,
            t = n.$slider.find("*").is(":focus");
        n.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false",
            tabindex: "0"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        });
        t && n.$slideTrack.find(".slick-active").focus()
    };
    t.prototype.focusHandler = function() {
        var t = this;
        t.$slider.on("focus.slick blur.slick", "*", function(i) {
            i.stopImmediatePropagation();
            var r = n(this);
            setTimeout(function() {
                t.isPlay && (r.is(":focus") ? (t.autoPlayClear(), t.paused = !0) : (t.paused = !1, t.autoPlay()))
            }, 0)
        })
    };
    n.fn.slick = function() {
        var u, i = this,
            r = arguments[0],
            f = Array.prototype.slice.call(arguments, 1),
            e = i.length,
            n = 0;
        for (n; e > n; n++)
            if ("object" == typeof r || "undefined" == typeof r ? i[n].slick = new t(i[n], r) : u = i[n].slick[r].apply(i[n].slick, f), "undefined" != typeof u) return u;
        return i
    }
}),
function(n) {
    function it(n, t, i) {
        switch (arguments.length) {
            case 2:
                return null != n ? n : t;
            case 3:
                return null != n ? n : null != t ? t : i;
            default:
                throw new Error("Implement me");
        }
    }

    function p(n, t) {
        return uf.call(n, t)
    }

    function ot() {
        return {
            empty: !1,
            unusedTokens: [],
            unusedInput: [],
            overflow: -2,
            charsLeftOver: 0,
            nullInput: !1,
            invalidMonth: null,
            invalidFormat: !1,
            userInvalidated: !1,
            iso: !1
        }
    }

    function ri(n) {
        t.suppressDeprecationWarnings === !1 && "undefined" != typeof console && console.warn && console.warn("Deprecation warning: " + n)
    }

    function o(n, t) {
        var i = !0;
        return w(function() {
            return i && (ri(n), i = !1), t.apply(this, arguments)
        }, t)
    }

    function dr(n, t) {
        br[n] || (ri(t), br[n] = !0)
    }

    function ui(n, t) {
        return function(i) {
            return r(n.call(this, i), t)
        }
    }

    function gr(n, t) {
        return function(i) {
            return this.localeData().ordinal(n.call(this, i), t)
        }
    }

    function nu(n, t) {
        var r, f, u = 12 * (t.year() - n.year()) + (t.month() - n.month()),
            i = n.clone().add(u, "months");
        return 0 > t - i ? (r = n.clone().add(u - 1, "months"), f = (t - i) / (i - r)) : (r = n.clone().add(u + 1, "months"), f = (t - i) / (r - i)), -(u + f)
    }

    function tu(n, t, i) {
        var r;
        return null == i ? t : null != n.meridiemHour ? n.meridiemHour(t, i) : null != n.isPM ? (r = n.isPM(i), r && 12 > t && (t += 12), r || 12 !== t || (t = 0), t) : t
    }

    function fi() {}

    function rt(n, i) {
        i !== !1 && pi(n);
        ei(this, n);
        this._d = new Date(+n._d);
        ii === !1 && (ii = !0, t.updateOffset(this), ii = !1)
    }

    function st(n) {
        var i = li(n),
            r = i.year || 0,
            u = i.quarter || 0,
            f = i.month || 0,
            e = i.week || 0,
            o = i.day || 0,
            s = i.hour || 0,
            h = i.minute || 0,
            c = i.second || 0,
            l = i.millisecond || 0;
        this._milliseconds = +l + 1e3 * c + 6e4 * h + 36e5 * s;
        this._days = +o + 7 * e;
        this._months = +f + 3 * u + 12 * r;
        this._data = {};
        this._locale = t.localeData();
        this._bubble()
    }

    function w(n, t) {
        for (var i in t) p(t, i) && (n[i] = t[i]);
        return p(t, "toString") && (n.toString = t.toString), p(t, "valueOf") && (n.valueOf = t.valueOf), n
    }

    function ei(n, t) {
        var u, i, r;
        if ("undefined" != typeof t._isAMomentObject && (n._isAMomentObject = t._isAMomentObject), "undefined" != typeof t._i && (n._i = t._i), "undefined" != typeof t._f && (n._f = t._f), "undefined" != typeof t._l && (n._l = t._l), "undefined" != typeof t._strict && (n._strict = t._strict), "undefined" != typeof t._tzm && (n._tzm = t._tzm), "undefined" != typeof t._isUTC && (n._isUTC = t._isUTC), "undefined" != typeof t._offset && (n._offset = t._offset), "undefined" != typeof t._pf && (n._pf = t._pf), "undefined" != typeof t._locale && (n._locale = t._locale), ft.length > 0)
            for (u in ft) i = ft[u], r = t[i], "undefined" != typeof r && (n[i] = r);
        return n
    }

    function s(n) {
        return 0 > n ? Math.ceil(n) : Math.floor(n)
    }

    function r(n, t, i) {
        for (var r = "" + Math.abs(n), u = n >= 0; r.length < t;) r = "0" + r;
        return (u ? i ? "+" : "" : "-") + r
    }

    function oi(n, t) {
        var i = {
            milliseconds: 0,
            months: 0
        };
        return i.months = t.month() - n.month() + 12 * (t.year() - n.year()), n.clone().add(i.months, "M").isAfter(t) && --i.months, i.milliseconds = +t - +n.clone().add(i.months, "M"), i
    }

    function iu(n, t) {
        var i;
        return t = lt(t, n), n.isBefore(t) ? i = oi(n, t) : (i = oi(t, n), i.milliseconds = -i.milliseconds, i.months = -i.months), i
    }

    function si(n, i) {
        return function(r, u) {
            var f, e;
            return null === u || isNaN(+u) || (dr(i, "moment()." + i + "(period, number) is deprecated. Please use moment()." + i + "(number, period)."), e = r, r = u, u = e), r = "string" == typeof r ? +r : r, f = t.duration(r, u), hi(this, f, n), this
        }
    }

    function hi(n, i, r, u) {
        var o = i._milliseconds,
            f = i._days,
            e = i._months;
        u = null == u ? !0 : u;
        o && n._d.setTime(+n._d + o * r);
        f && rr(n, "Date", bt(n, "Date") + f * r);
        e && ir(n, bt(n, "Month") + e * r);
        u && t.updateOffset(n, f || e)
    }

    function ut(n) {
        return "[object Array]" === Object.prototype.toString.call(n)
    }

    function ht(n) {
        return "[object Date]" === Object.prototype.toString.call(n) || n instanceof Date
    }

    function ci(n, t, r) {
        for (var e = Math.min(n.length, t.length), o = Math.abs(n.length - t.length), f = 0, u = 0; e > u; u++)(r && n[u] !== t[u] || !r && i(n[u]) !== i(t[u])) && f++;
        return f + o
    }

    function f(n) {
        if (n) {
            var t = n.toLowerCase().replace(/(.)s$/, "$1");
            n = ne[n] || te[t] || t
        }
        return n
    }

    function li(n) {
        var i, t, r = {};
        for (t in n) p(n, t) && (i = f(t), i && (r[i] = n[t]));
        return r
    }

    function ru(i) {
        var r, u;
        if (0 === i.indexOf("week")) r = 7, u = "day";
        else {
            if (0 !== i.indexOf("month")) return;
            r = 12;
            u = "month"
        }
        t[i] = function(f, e) {
            var o, s, c = t._locale[i],
                h = [];
            if ("number" == typeof f && (e = f, f = n), s = function(n) {
                    var i = t().utc().set(u, n);
                    return c.call(t._locale, i, f || "")
                }, null != e) return s(e);
            for (o = 0; r > o; o++) h.push(s(o));
            return h
        }
    }

    function i(n) {
        var t = +n,
            i = 0;
        return 0 !== t && isFinite(t) && (i = t >= 0 ? Math.floor(t) : Math.ceil(t)), i
    }

    function ct(n, t) {
        return new Date(Date.UTC(n, t + 1, 0)).getUTCDate()
    }

    function ai(n, i, r) {
        return b(t([n, 11, 31 + i - r]), i, r).week
    }

    function vi(n) {
        return yi(n) ? 366 : 365
    }

    function yi(n) {
        return n % 4 == 0 && n % 100 != 0 || n % 400 == 0
    }

    function pi(n) {
        var t;
        n._a && -2 === n._pf.overflow && (t = n._a[a] < 0 || n._a[a] > 11 ? a : n._a[h] < 1 || n._a[h] > ct(n._a[l], n._a[a]) ? h : n._a[e] < 0 || n._a[e] > 24 || 24 === n._a[e] && (0 !== n._a[d] || 0 !== n._a[g] || 0 !== n._a[nt]) ? e : n._a[d] < 0 || n._a[d] > 59 ? d : n._a[g] < 0 || n._a[g] > 59 ? g : n._a[nt] < 0 || n._a[nt] > 999 ? nt : -1, n._pf._overflowDayOfYear && (l > t || t > h) && (t = h), n._pf.overflow = t)
    }

    function wi(t) {
        return null == t._isValid && (t._isValid = !isNaN(t._d.getTime()) && t._pf.overflow < 0 && !t._pf.empty && !t._pf.invalidMonth && !t._pf.nullInput && !t._pf.invalidFormat && !t._pf.userInvalidated, t._strict && (t._isValid = t._isValid && 0 === t._pf.charsLeftOver && 0 === t._pf.unusedTokens.length && t._pf.bigHour === n)), t._isValid
    }

    function bi(n) {
        return n ? n.toLowerCase().replace("_", "-") : n
    }

    function uu(n) {
        for (var i, t, f, r, u = 0; u < n.length;) {
            for (r = bi(n[u]).split("-"), i = r.length, t = bi(n[u + 1]), t = t ? t.split("-") : null; i > 0;) {
                if (f = ki(r.slice(0, i).join("-"))) return f;
                if (t && t.length >= i && ci(r, t, !0) >= i - 1) break;
                i--
            }
            u++
        }
        return null
    }

    function ki(n) {
        var i = null;
        if (!tt[n] && sr) try {
            i = t.locale();
            require("./locale/" + n);
            t.locale(i)
        } catch (r) {}
        return tt[n]
    }

    function lt(n, i) {
        var r, u;
        return i._isUTC ? (r = i.clone(), u = (t.isMoment(n) || ht(n) ? +n : +t(n)) - +r, r._d.setTime(+r._d + u), t.updateOffset(r, !1), r) : t(n).local()
    }

    function fu(n) {
        return n.match(/\[[\s\S]/) ? n.replace(/^\[|\]$/g, "") : n.replace(/\\/g, "")
    }

    function eu(n) {
        for (var i = n.match(hr), t = 0, r = i.length; r > t; t++) i[t] = v[i[t]] ? v[i[t]] : fu(i[t]);
        return function(u) {
            var f = "";
            for (t = 0; r > t; t++) f += i[t] instanceof Function ? i[t].call(u, n) : i[t];
            return f
        }
    }

    function at(n, t) {
        return n.isValid() ? (t = di(t, n.localeData()), ti[t] || (ti[t] = eu(t)), ti[t](n)) : n.localeData().invalidDate()
    }

    function di(n, t) {
        function r(n) {
            return t.longDateFormat(n) || n
        }
        var i = 5;
        for (et.lastIndex = 0; i >= 0 && et.test(n);) n = n.replace(et, r), et.lastIndex = 0, i -= 1;
        return n
    }

    function ou(n, t) {
        var i = t._strict;
        switch (n) {
            case "Q":
                return lr;
            case "DDDD":
                return vr;
            case "YYYY":
            case "GGGG":
            case "gggg":
                return i ? wf : hf;
            case "Y":
            case "G":
            case "g":
                return kf;
            case "YYYYYY":
            case "YYYYY":
            case "GGGGG":
            case "ggggg":
                return i ? bf : cf;
            case "S":
                if (i) return lr;
            case "SS":
                if (i) return ar;
            case "SSS":
                if (i) return vr;
            case "DDD":
                return sf;
            case "MMM":
            case "MMMM":
            case "dd":
            case "ddd":
            case "dddd":
                return af;
            case "a":
            case "A":
                return t._locale._meridiemParse;
            case "x":
                return yf;
            case "X":
                return pf;
            case "Z":
            case "ZZ":
                return dt;
            case "T":
                return vf;
            case "SSSS":
                return lf;
            case "MM":
            case "DD":
            case "YY":
            case "GG":
            case "gg":
            case "HH":
            case "hh":
            case "mm":
            case "ss":
            case "ww":
            case "WW":
                return i ? ar : cr;
            case "M":
            case "D":
            case "d":
            case "H":
            case "h":
            case "m":
            case "s":
            case "w":
            case "W":
            case "e":
            case "E":
                return cr;
            case "Do":
                return i ? t._locale._ordinalParse : t._locale._ordinalParseLenient;
            default:
                return new RegExp(vu(au(n.replace("\\", "")), "i"))
        }
    }

    function vt(n) {
        n = n || "";
        var r = n.match(dt) || [],
            f = r[r.length - 1] || [],
            t = (f + "").match(gf) || ["-", 0, 0],
            u = +(60 * t[1]) + i(t[2]);
        return "+" === t[0] ? u : -u
    }

    function su(n, r, u) {
        var o, f = u._a;
        switch (n) {
            case "Q":
                null != r && (f[a] = 3 * (i(r) - 1));
                break;
            case "M":
            case "MM":
                null != r && (f[a] = i(r) - 1);
                break;
            case "MMM":
            case "MMMM":
                o = u._locale.monthsParse(r, n, u._strict);
                null != o ? f[a] = o : u._pf.invalidMonth = r;
                break;
            case "D":
            case "DD":
                null != r && (f[h] = i(r));
                break;
            case "Do":
                null != r && (f[h] = i(parseInt(r.match(/\d{1,2}/)[0], 10)));
                break;
            case "DDD":
            case "DDDD":
                null != r && (u._dayOfYear = i(r));
                break;
            case "YY":
                f[l] = t.parseTwoDigitYear(r);
                break;
            case "YYYY":
            case "YYYYY":
            case "YYYYYY":
                f[l] = i(r);
                break;
            case "a":
            case "A":
                u._meridiem = r;
                break;
            case "h":
            case "hh":
                u._pf.bigHour = !0;
            case "H":
            case "HH":
                f[e] = i(r);
                break;
            case "m":
            case "mm":
                f[d] = i(r);
                break;
            case "s":
            case "ss":
                f[g] = i(r);
                break;
            case "S":
            case "SS":
            case "SSS":
            case "SSSS":
                f[nt] = i(1e3 * ("0." + r));
                break;
            case "x":
                u._d = new Date(i(r));
                break;
            case "X":
                u._d = new Date(1e3 * parseFloat(r));
                break;
            case "Z":
            case "ZZ":
                u._useUTC = !0;
                u._tzm = vt(r);
                break;
            case "dd":
            case "ddd":
            case "dddd":
                o = u._locale.weekdaysParse(r);
                null != o ? (u._w = u._w || {}, u._w.d = o) : u._pf.invalidWeekday = r;
                break;
            case "w":
            case "ww":
            case "W":
            case "WW":
            case "d":
            case "e":
            case "E":
                n = n.substr(0, 1);
            case "gggg":
            case "GGGG":
            case "GGGGG":
                n = n.substr(0, 2);
                r && (u._w = u._w || {}, u._w[n] = i(r));
                break;
            case "gg":
            case "GG":
                u._w = u._w || {};
                u._w[n] = t.parseTwoDigitYear(r)
        }
    }

    function hu(n) {
        var i, o, f, u, r, e, s;
        i = n._w;
        null != i.GG || null != i.W || null != i.E ? (r = 1, e = 4, o = it(i.GG, n._a[l], b(t(), 1, 4).year), f = it(i.W, 1), u = it(i.E, 1)) : (r = n._locale._week.dow, e = n._locale._week.doy, o = it(i.gg, n._a[l], b(t(), r, e).year), f = it(i.w, 1), null != i.d ? (u = i.d, r > u && ++f) : u = null != i.e ? i.e + r : r);
        s = tf(o, f, u, e, r);
        n._a[l] = s.year;
        n._dayOfYear = s.dayOfYear
    }

    function yt(n) {
        var t, i, r, u, f = [];
        if (!n._d) {
            for (r = lu(n), n._w && null == n._a[h] && null == n._a[a] && hu(n), n._dayOfYear && (u = it(n._a[l], r[l]), n._dayOfYear > vi(u) && (n._pf._overflowDayOfYear = !0), i = wt(u, 0, n._dayOfYear), n._a[a] = i.getUTCMonth(), n._a[h] = i.getUTCDate()), t = 0; 3 > t && null == n._a[t]; ++t) n._a[t] = f[t] = r[t];
            for (; 7 > t; t++) n._a[t] = f[t] = null == n._a[t] ? 2 === t ? 1 : 0 : n._a[t];
            24 === n._a[e] && 0 === n._a[d] && 0 === n._a[g] && 0 === n._a[nt] && (n._nextDay = !0, n._a[e] = 0);
            n._d = (n._useUTC ? wt : ku).apply(null, f);
            null != n._tzm && n._d.setUTCMinutes(n._d.getUTCMinutes() - n._tzm);
            n._nextDay && (n._a[e] = 24)
        }
    }

    function cu(n) {
        var t;
        n._d || (t = li(n._i), n._a = [t.year, t.month, t.day || t.date, t.hour, t.minute, t.second, t.millisecond], yt(n))
    }

    function lu(n) {
        var t = new Date;
        return n._useUTC ? [t.getUTCFullYear(), t.getUTCMonth(), t.getUTCDate()] : [t.getFullYear(), t.getMonth(), t.getDate()]
    }

    function pt(i) {
        if (i._f === t.ISO_8601) return void gi(i);
        i._a = [];
        i._pf.empty = !0;
        for (var r, f, h, u = "" + i._i, l = u.length, c = 0, s = di(i._f, i._locale).match(hr) || [], o = 0; o < s.length; o++) f = s[o], r = (u.match(ou(f, i)) || [])[0], r && (h = u.substr(0, u.indexOf(r)), h.length > 0 && i._pf.unusedInput.push(h), u = u.slice(u.indexOf(r) + r.length), c += r.length), v[f] ? (r ? i._pf.empty = !1 : i._pf.unusedTokens.push(f), su(f, r, i)) : i._strict && !r && i._pf.unusedTokens.push(f);
        i._pf.charsLeftOver = l - c;
        u.length > 0 && i._pf.unusedInput.push(u);
        i._pf.bigHour === !0 && i._a[e] <= 12 && (i._pf.bigHour = n);
        i._a[e] = tu(i._locale, i._a[e], i._meridiem);
        yt(i);
        pi(i)
    }

    function au(n) {
        return n.replace(/\\(\[)|\\(\])|\[([^\]\[]*)\]|\\(.)/g, function(n, t, i, r, u) {
            return t || i || r || u
        })
    }

    function vu(n) {
        return n.replace(/[-\/\\^$*+?.()|[\]{}]/g, "\\$&")
    }

    function yu(n) {
        var t, f, u, r, i;
        if (0 === n._f.length) return n._pf.invalidFormat = !0, void(n._d = new Date(NaN));
        for (r = 0; r < n._f.length; r++) i = 0, t = ei({}, n), null != n._useUTC && (t._useUTC = n._useUTC), t._pf = ot(), t._f = n._f[r], pt(t), wi(t) && (i += t._pf.charsLeftOver, i += 10 * t._pf.unusedTokens.length, t._pf.score = i, (null == u || u > i) && (u = i, f = t));
        w(n, f || t)
    }

    function gi(n) {
        var t, i, r = n._i,
            u = df.exec(r);
        if (u) {
            for (n._pf.iso = !0, t = 0, i = gt.length; i > t; t++)
                if (gt[t][1].exec(r)) {
                    n._f = gt[t][0] + (u[6] || " ");
                    break
                }
            for (t = 0, i = ni.length; i > t; t++)
                if (ni[t][1].exec(r)) {
                    n._f += ni[t][0];
                    break
                }
            r.match(dt) && (n._f += "Z");
            pt(n)
        } else n._isValid = !1
    }

    function pu(n) {
        gi(n);
        n._isValid === !1 && (delete n._isValid, t.createFromInputFallback(n))
    }

    function wu(n, t) {
        for (var r = [], i = 0; i < n.length; ++i) r.push(t(n[i], i));
        return r
    }

    function bu(i) {
        var u, r = i._i;
        r === n ? i._d = new Date : ht(r) ? i._d = new Date(+r) : null !== (u = ff.exec(r)) ? i._d = new Date(+u[1]) : "string" == typeof r ? pu(i) : ut(r) ? (i._a = wu(r.slice(0), function(n) {
            return parseInt(n, 10)
        }), yt(i)) : "object" == typeof r ? cu(i) : "number" == typeof r ? i._d = new Date(r) : t.createFromInputFallback(i)
    }

    function ku(n, t, i, r, u, f, e) {
        var o = new Date(n, t, i, r, u, f, e);
        return 1970 > n && o.setFullYear(n), o
    }

    function wt(n) {
        var t = new Date(Date.UTC.apply(null, arguments));
        return 1970 > n && t.setUTCFullYear(n), t
    }

    function du(n, t) {
        if ("string" == typeof n)
            if (isNaN(n)) {
                if (n = t.weekdaysParse(n), "number" != typeof n) return null
            } else n = parseInt(n, 10);
        return n
    }

    function gu(n, t, i, r, u) {
        return u.relativeTime(t || 1, !!i, n, r)
    }

    function nf(n, i, r) {
        var u = t.duration(n).abs(),
            c = k(u.as("s")),
            e = k(u.as("m")),
            o = k(u.as("h")),
            s = k(u.as("d")),
            h = k(u.as("M")),
            l = k(u.as("y")),
            f = c < y.s && ["s", c] || 1 === e && ["m"] || e < y.m && ["mm", e] || 1 === o && ["h"] || o < y.h && ["hh", o] || 1 === s && ["d"] || s < y.d && ["dd", s] || 1 === h && ["M"] || h < y.M && ["MM", h] || 1 === l && ["y"] || ["yy", l];
        return f[2] = i, f[3] = +n > 0, f[4] = r, gu.apply({}, f)
    }

    function b(n, i, r) {
        var f, e = r - i,
            u = r - n.day();
        return u > e && (u -= 7), e - 7 > u && (u += 7), f = t(n).add(u, "d"), {
            week: Math.ceil(f.dayOfYear() / 7),
            year: f.year()
        }
    }

    function tf(n, t, i, r, u) {
        var o, e, f = wt(n, 0, 1).getUTCDay();
        return f = 0 === f ? 7 : f, i = null != i ? i : u, o = u - f + (f > r ? 7 : 0) - (u > f ? 7 : 0), e = 7 * (t - 1) + (i - u) + o + 1, {
            year: e > 0 ? n : n - 1,
            dayOfYear: e > 0 ? e : vi(n - 1) + e
        }
    }

    function nr(i) {
        var u, r = i._i,
            f = i._f;
        return i._locale = i._locale || t.localeData(i._l), null === r || f === n && "" === r ? t.invalid({
            nullInput: !0
        }) : ("string" == typeof r && (i._i = r = i._locale.preparse(r)), t.isMoment(r) ? new rt(r, !0) : (f ? ut(f) ? yu(i) : pt(i) : bu(i), u = new rt(i), u._nextDay && (u.add(1, "d"), u._nextDay = n), u))
    }

    function tr(n, i) {
        var u, r;
        if (1 === i.length && ut(i[0]) && (i = i[0]), !i.length) return t();
        for (u = i[0], r = 1; r < i.length; ++r) i[r][n](u) && (u = i[r]);
        return u
    }

    function ir(n, t) {
        var i;
        return "string" == typeof t && (t = n.localeData().monthsParse(t), "number" != typeof t) ? n : (i = Math.min(n.date(), ct(n.year(), t)), n._d["set" + (n._isUTC ? "UTC" : "") + "Month"](t, i), n)
    }

    function bt(n, t) {
        return n._d["get" + (n._isUTC ? "UTC" : "") + t]()
    }

    function rr(n, t, i) {
        return "Month" === t ? ir(n, i) : n._d["set" + (n._isUTC ? "UTC" : "") + t](i)
    }

    function c(n, i) {
        return function(r) {
            return null != r ? (rr(this, n, r), t.updateOffset(this, i), this) : bt(this, n)
        }
    }

    function ur(n) {
        return 400 * n / 146097
    }

    function fr(n) {
        return 146097 * n / 400
    }

    function rf(n) {
        t.duration.fn[n] = function() {
            return this._data[n]
        }
    }

    function er(n) {
        "undefined" == typeof ender && (or = kt.moment, kt.moment = n ? o("Accessing Moment through the global scope is deprecated, and will be removed in an upcoming release.", t) : t)
    }
    for (var t, or, u, kt = "undefined" == typeof global || "undefined" != typeof window && window !== global.window ? this : global, k = Math.round, uf = Object.prototype.hasOwnProperty, l = 0, a = 1, h = 2, e = 3, d = 4, g = 5, nt = 6, tt = {}, ft = [], sr = "undefined" != typeof module && module && module.exports, ff = /^\/?Date\((\-?\d+)/i, ef = /(\-)?(?:(\d*)\.)?(\d+)\:(\d+)(?:\:(\d+)\.?(\d{3})?)?/, of = /^(-)?P(?:(?:([0-9,.]*)Y)?(?:([0-9,.]*)M)?(?:([0-9,.]*)D)?(?:T(?:([0-9,.]*)H)?(?:([0-9,.]*)M)?(?:([0-9,.]*)S)?)?|([0-9,.]*)W)$/, hr = /(\[[^\[]*\])|(\\)?(Mo|MM?M?M?|Do|DDDo|DD?D?D?|ddd?d?|do?|w[o|w]?|W[o|W]?|Q|YYYYYY|YYYYY|YYYY|YY|gg(ggg?)?|GG(GGG?)?|e|E|a|A|hh?|HH?|mm?|ss?|S{1,4}|x|X|zz?|ZZ?|.)/g, et = /(\[[^\[]*\])|(\\)?(LTS|LT|LL?L?L?|l{1,4})/g, cr = /\d\d?/, sf = /\d{1,3}/, hf = /\d{1,4}/, cf = /[+\-]?\d{1,6}/, lf = /\d+/, af = /[0-9]*['a-z\u00A0-\u05FF\u0700-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+|[\u0600-\u06FF\/]+(\s*?[\u0600-\u06FF]+){1,2}/i, dt = /Z|[\+\-]\d\d:?\d\d/gi, vf = /T/i, yf = /[\+\-]?\d+/, pf = /[\+\-]?\d+(\.\d{1,3})?/, lr = /\d/, ar = /\d\d/, vr = /\d{3}/, wf = /\d{4}/, bf = /[+-]?\d{6}/, kf = /[+-]?\d+/, df = /^\s*(?:[+-]\d{6}|\d{4})-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?([\+\-]\d\d(?::?\d\d)?|\s*Z)?)?$/, gt = [
            ["YYYYYY-MM-DD", /[+-]\d{6}-\d{2}-\d{2}/],
            ["YYYY-MM-DD", /\d{4}-\d{2}-\d{2}/],
            ["GGGG-[W]WW-E", /\d{4}-W\d{2}-\d/],
            ["GGGG-[W]WW", /\d{4}-W\d{2}/],
            ["YYYY-DDD", /\d{4}-\d{3}/]
        ], ni = [
            ["HH:mm:ss.SSSS", /(T| )\d\d:\d\d:\d\d\.\d+/],
            ["HH:mm:ss", /(T| )\d\d:\d\d:\d\d/],
            ["HH:mm", /(T| )\d\d:\d\d/],
            ["HH", /(T| )\d\d/]
        ], gf = /([\+\-]|\d\d)/gi, yr = ("Date|Hours|Minutes|Seconds|Milliseconds".split("|"), {
            Milliseconds: 1,
            Seconds: 1e3,
            Minutes: 6e4,
            Hours: 36e5,
            Days: 864e5,
            Months: 2592e6,
            Years: 31536e6
        }), ne = {
            ms: "millisecond",
            s: "second",
            m: "minute",
            h: "hour",
            d: "day",
            D: "date",
            w: "week",
            W: "isoWeek",
            M: "month",
            Q: "quarter",
            y: "year",
            DDD: "dayOfYear",
            e: "weekday",
            E: "isoWeekday",
            gg: "weekYear",
            GG: "isoWeekYear"
        }, te = {
            dayofyear: "dayOfYear",
            isoweekday: "isoWeekday",
            isoweek: "isoWeek",
            weekyear: "weekYear",
            isoweekyear: "isoWeekYear"
        }, ti = {}, y = {
            s: 45,
            m: 45,
            h: 22,
            d: 26,
            M: 11
        }, pr = "DDD w W M D d".split(" "), wr = "M D H h m s w W".split(" "), v = {
            M: function() {
                return this.month() + 1
            },
            MMM: function(n) {
                return this.localeData().monthsShort(this, n)
            },
            MMMM: function(n) {
                return this.localeData().months(this, n)
            },
            D: function() {
                return this.date()
            },
            DDD: function() {
                return this.dayOfYear()
            },
            d: function() {
                return this.day()
            },
            dd: function(n) {
                return this.localeData().weekdaysMin(this, n)
            },
            ddd: function(n) {
                return this.localeData().weekdaysShort(this, n)
            },
            dddd: function(n) {
                return this.localeData().weekdays(this, n)
            },
            w: function() {
                return this.week()
            },
            W: function() {
                return this.isoWeek()
            },
            YY: function() {
                return r(this.year() % 100, 2)
            },
            YYYY: function() {
                return r(this.year(), 4)
            },
            YYYYY: function() {
                return r(this.year(), 5)
            },
            YYYYYY: function() {
                var n = this.year(),
                    t = n >= 0 ? "+" : "-";
                return t + r(Math.abs(n), 6)
            },
            gg: function() {
                return r(this.weekYear() % 100, 2)
            },
            gggg: function() {
                return r(this.weekYear(), 4)
            },
            ggggg: function() {
                return r(this.weekYear(), 5)
            },
            GG: function() {
                return r(this.isoWeekYear() % 100, 2)
            },
            GGGG: function() {
                return r(this.isoWeekYear(), 4)
            },
            GGGGG: function() {
                return r(this.isoWeekYear(), 5)
            },
            e: function() {
                return this.weekday()
            },
            E: function() {
                return this.isoWeekday()
            },
            a: function() {
                return this.localeData().meridiem(this.hours(), this.minutes(), !0)
            },
            A: function() {
                return this.localeData().meridiem(this.hours(), this.minutes(), !1)
            },
            H: function() {
                return this.hours()
            },
            h: function() {
                return this.hours() % 12 || 12
            },
            m: function() {
                return this.minutes()
            },
            s: function() {
                return this.seconds()
            },
            S: function() {
                return i(this.milliseconds() / 100)
            },
            SS: function() {
                return r(i(this.milliseconds() / 10), 2)
            },
            SSS: function() {
                return r(this.milliseconds(), 3)
            },
            SSSS: function() {
                return r(this.milliseconds(), 3)
            },
            Z: function() {
                var n = this.utcOffset(),
                    t = "+";
                return 0 > n && (n = -n, t = "-"), t + r(i(n / 60), 2) + ":" + r(i(n) % 60, 2)
            },
            ZZ: function() {
                var n = this.utcOffset(),
                    t = "+";
                return 0 > n && (n = -n, t = "-"), t + r(i(n / 60), 2) + r(i(n) % 60, 2)
            },
            z: function() {
                return this.zoneAbbr()
            },
            zz: function() {
                return this.zoneName()
            },
            x: function() {
                return this.valueOf()
            },
            X: function() {
                return this.unix()
            },
            Q: function() {
                return this.quarter()
            }
        }, br = {}, kr = ["months", "monthsShort", "weekdays", "weekdaysShort", "weekdaysMin"], ii = !1; pr.length;) u = pr.pop(), v[u + "o"] = gr(v[u], u);
    for (; wr.length;) u = wr.pop(), v[u + u] = ui(v[u], 2);
    for (v.DDDD = ui(v.DDD, 3), w(fi.prototype, {
            set: function(n) {
                var t, i;
                for (i in n) t = n[i], "function" == typeof t ? this[i] = t : this["_" + i] = t;
                this._ordinalParseLenient = new RegExp(this._ordinalParse.source + "|" + /\d{1,2}/.source)
            },
            _months: "January_February_March_April_May_June_July_August_September_October_November_December".split("_"),
            months: function(n) {
                return this._months[n.month()]
            },
            _monthsShort: "Jan_Feb_Mar_Apr_May_Jun_Jul_Aug_Sep_Oct_Nov_Dec".split("_"),
            monthsShort: function(n) {
                return this._monthsShort[n.month()]
            },
            monthsParse: function(n, i, r) {
                var u, f, e;
                for (this._monthsParse || (this._monthsParse = [], this._longMonthsParse = [], this._shortMonthsParse = []), u = 0; 12 > u; u++)
                    if ((f = t.utc([2e3, u]), r && !this._longMonthsParse[u] && (this._longMonthsParse[u] = new RegExp("^" + this.months(f, "").replace(".", "") + "$", "i"), this._shortMonthsParse[u] = new RegExp("^" + this.monthsShort(f, "").replace(".", "") + "$", "i")), r || this._monthsParse[u] || (e = "^" + this.months(f, "") + "|^" + this.monthsShort(f, ""), this._monthsParse[u] = new RegExp(e.replace(".", ""), "i")), r && "MMMM" === i && this._longMonthsParse[u].test(n)) || r && "MMM" === i && this._shortMonthsParse[u].test(n) || !r && this._monthsParse[u].test(n)) return u
            },
            _weekdays: "Sunday_Monday_Tuesday_Wednesday_Thursday_Friday_Saturday".split("_"),
            weekdays: function(n) {
                return this._weekdays[n.day()]
            },
            _weekdaysShort: "Sun_Mon_Tue_Wed_Thu_Fri_Sat".split("_"),
            weekdaysShort: function(n) {
                return this._weekdaysShort[n.day()]
            },
            _weekdaysMin: "Su_Mo_Tu_We_Th_Fr_Sa".split("_"),
            weekdaysMin: function(n) {
                return this._weekdaysMin[n.day()]
            },
            weekdaysParse: function(n) {
                var i, r, u;
                for (this._weekdaysParse || (this._weekdaysParse = []), i = 0; 7 > i; i++)
                    if (this._weekdaysParse[i] || (r = t([2e3, 1]).day(i), u = "^" + this.weekdays(r, "") + "|^" + this.weekdaysShort(r, "") + "|^" + this.weekdaysMin(r, ""), this._weekdaysParse[i] = new RegExp(u.replace(".", ""), "i")), this._weekdaysParse[i].test(n)) return i
            },
            _longDateFormat: {
                LTS: "h:mm:ss A",
                LT: "h:mm A",
                L: "MM/DD/YYYY",
                LL: "MMMM D, YYYY",
                LLL: "MMMM D, YYYY LT",
                LLLL: "dddd, MMMM D, YYYY LT"
            },
            longDateFormat: function(n) {
                var t = this._longDateFormat[n];
                return !t && this._longDateFormat[n.toUpperCase()] && (t = this._longDateFormat[n.toUpperCase()].replace(/MMMM|MM|DD|dddd/g, function(n) {
                    return n.slice(1)
                }), this._longDateFormat[n] = t), t
            },
            isPM: function(n) {
                return "p" === (n + "").toLowerCase().charAt(0)
            },
            _meridiemParse: /[ap]\.?m?\.?/i,
            meridiem: function(n, t, i) {
                return n > 11 ? i ? "pm" : "PM" : i ? "am" : "AM"
            },
            _calendar: {
                sameDay: "[Today at] LT",
                nextDay: "[Tomorrow at] LT",
                nextWeek: "dddd [at] LT",
                lastDay: "[Yesterday at] LT",
                lastWeek: "[Last] dddd [at] LT",
                sameElse: "L"
            },
            calendar: function(n, t, i) {
                var r = this._calendar[n];
                return "function" == typeof r ? r.apply(t, [i]) : r
            },
            _relativeTime: {
                future: "in %s",
                past: "%s ago",
                s: "a few seconds",
                m: "a minute",
                mm: "%d minutes",
                h: "an hour",
                hh: "%d hours",
                d: "a day",
                dd: "%d days",
                M: "a month",
                MM: "%d months",
                y: "a year",
                yy: "%d years"
            },
            relativeTime: function(n, t, i, r) {
                var u = this._relativeTime[i];
                return "function" == typeof u ? u(n, t, i, r) : u.replace(/%d/i, n)
            },
            pastFuture: function(n, t) {
                var i = this._relativeTime[n > 0 ? "future" : "past"];
                return "function" == typeof i ? i(t) : i.replace(/%s/i, t)
            },
            ordinal: function(n) {
                return this._ordinal.replace("%d", n)
            },
            _ordinal: "%d",
            _ordinalParse: /\d{1,2}/,
            preparse: function(n) {
                return n
            },
            postformat: function(n) {
                return n
            },
            week: function(n) {
                return b(n, this._week.dow, this._week.doy).week
            },
            _week: {
                dow: 0,
                doy: 6
            },
            firstDayOfWeek: function() {
                return this._week.dow
            },
            firstDayOfYear: function() {
                return this._week.doy
            },
            _invalidDate: "Invalid date",
            invalidDate: function() {
                return this._invalidDate
            }
        }), t = function(t, i, r, u) {
            var f;
            return "boolean" == typeof r && (u = r, r = n), f = {}, f._isAMomentObject = !0, f._i = t, f._f = i, f._l = r, f._strict = u, f._isUTC = !1, f._pf = ot(), nr(f)
        }, t.suppressDeprecationWarnings = !1, t.createFromInputFallback = o("moment construction falls back to js Date. This is discouraged and will be removed in upcoming major release. Please refer to https://github.com/moment/moment/issues/1407 for more info.", function(n) {
            n._d = new Date(n._i + (n._useUTC ? " UTC" : ""))
        }), t.min = function() {
            var n = [].slice.call(arguments, 0);
            return tr("isBefore", n)
        }, t.max = function() {
            var n = [].slice.call(arguments, 0);
            return tr("isAfter", n)
        }, t.utc = function(t, i, r, u) {
            var f;
            return "boolean" == typeof r && (u = r, r = n), f = {}, f._isAMomentObject = !0, f._useUTC = !0, f._isUTC = !0, f._l = r, f._i = t, f._f = i, f._strict = u, f._pf = ot(), nr(f).utc()
        }, t.unix = function(n) {
            return t(1e3 * n)
        }, t.duration = function(n, r) {
            var o, c, s, l, u = n,
                f = null;
            return t.isDuration(n) ? u = {
                ms: n._milliseconds,
                d: n._days,
                M: n._months
            } : "number" == typeof n ? (u = {}, r ? u[r] = n : u.milliseconds = n) : (f = ef.exec(n)) ? (o = "-" === f[1] ? -1 : 1, u = {
                y: 0,
                d: i(f[h]) * o,
                h: i(f[e]) * o,
                m: i(f[d]) * o,
                s: i(f[g]) * o,
                ms: i(f[nt]) * o
            }) : (f = of .exec(n)) ? (o = "-" === f[1] ? -1 : 1, s = function(n) {
                var t = n && parseFloat(n.replace(",", "."));
                return (isNaN(t) ? 0 : t) * o
            }, u = {
                y: s(f[2]),
                M: s(f[3]),
                d: s(f[4]),
                h: s(f[5]),
                m: s(f[6]),
                s: s(f[7]),
                w: s(f[8])
            }) : null == u ? u = {} : "object" == typeof u && ("from" in u || "to" in u) && (l = iu(t(u.from), t(u.to)), u = {}, u.ms = l.milliseconds, u.M = l.months), c = new st(u), t.isDuration(n) && p(n, "_locale") && (c._locale = n._locale), c
        }, t.version = "2.9.0", t.defaultFormat = "YYYY-MM-DDTHH:mm:ssZ", t.ISO_8601 = function() {}, t.momentProperties = ft, t.updateOffset = function() {}, t.relativeTimeThreshold = function(t, i) {
            return y[t] === n ? !1 : i === n ? y[t] : (y[t] = i, !0)
        }, t.lang = o("moment.lang is deprecated. Use moment.locale instead.", function(n, i) {
            return t.locale(n, i)
        }), t.locale = function(n, i) {
            var r;
            return n && (r = "undefined" != typeof i ? t.defineLocale(n, i) : t.localeData(n), r && (t.duration._locale = t._locale = r)), t._locale._abbr
        }, t.defineLocale = function(n, i) {
            return null !== i ? (i.abbr = n, tt[n] || (tt[n] = new fi), tt[n].set(i), t.locale(n), tt[n]) : (delete tt[n], null)
        }, t.langData = o("moment.langData is deprecated. Use moment.localeData instead.", function(n) {
            return t.localeData(n)
        }), t.localeData = function(n) {
            var i;
            if (n && n._locale && n._locale._abbr && (n = n._locale._abbr), !n) return t._locale;
            if (!ut(n)) {
                if (i = ki(n)) return i;
                n = [n]
            }
            return uu(n)
        }, t.isMoment = function(n) {
            return n instanceof rt || null != n && p(n, "_isAMomentObject")
        }, t.isDuration = function(n) {
            return n instanceof st
        }, u = kr.length - 1; u >= 0; --u) ru(kr[u]);
    t.normalizeUnits = function(n) {
        return f(n)
    };
    t.invalid = function(n) {
        var i = t.utc(NaN);
        return null != n ? w(i._pf, n) : i._pf.userInvalidated = !0, i
    };
    t.parseZone = function() {
        return t.apply(null, arguments).parseZone()
    };
    t.parseTwoDigitYear = function(n) {
        return i(n) + (i(n) > 68 ? 1900 : 2e3)
    };
    t.isDate = ht;
    w(t.fn = rt.prototype, {
        clone: function() {
            return t(this)
        },
        valueOf: function() {
            return +this._d - 6e4 * (this._offset || 0)
        },
        unix: function() {
            return Math.floor(+this / 1e3)
        },
        toString: function() {
            return this.clone().locale("en").format("ddd MMM DD YYYY HH:mm:ss [GMT]ZZ")
        },
        toDate: function() {
            return this._offset ? new Date(+this) : this._d
        },
        toISOString: function() {
            var n = t(this).utc();
            return 0 < n.year() && n.year() <= 9999 ? "function" == typeof Date.prototype.toISOString ? this.toDate().toISOString() : at(n, "YYYY-MM-DD[T]HH:mm:ss.SSS[Z]") : at(n, "YYYYYY-MM-DD[T]HH:mm:ss.SSS[Z]")
        },
        toArray: function() {
            var n = this;
            return [n.year(), n.month(), n.date(), n.hours(), n.minutes(), n.seconds(), n.milliseconds()]
        },
        isValid: function() {
            return wi(this)
        },
        isDSTShifted: function() {
            return this._a ? this.isValid() && ci(this._a, (this._isUTC ? t.utc(this._a) : t(this._a)).toArray()) > 0 : !1
        },
        parsingFlags: function() {
            return w({}, this._pf)
        },
        invalidAt: function() {
            return this._pf.overflow
        },
        utc: function(n) {
            return this.utcOffset(0, n)
        },
        local: function(n) {
            return this._isUTC && (this.utcOffset(0, n), this._isUTC = !1, n && this.subtract(this._dateUtcOffset(), "m")), this
        },
        format: function(n) {
            var i = at(this, n || t.defaultFormat);
            return this.localeData().postformat(i)
        },
        add: si(1, "add"),
        subtract: si(-1, "subtract"),
        diff: function(n, t, i) {
            var r, u, e = lt(n, this),
                o = 6e4 * (e.utcOffset() - this.utcOffset());
            return t = f(t), "year" === t || "month" === t || "quarter" === t ? (u = nu(this, e), "quarter" === t ? u /= 3 : "year" === t && (u /= 12)) : (r = this - e, u = "second" === t ? r / 1e3 : "minute" === t ? r / 6e4 : "hour" === t ? r / 36e5 : "day" === t ? (r - o) / 864e5 : "week" === t ? (r - o) / 6048e5 : r), i ? u : s(u)
        },
        from: function(n, i) {
            return t.duration({
                to: this,
                from: n
            }).locale(this.locale()).humanize(!i)
        },
        fromNow: function(n) {
            return this.from(t(), n)
        },
        calendar: function(n) {
            var r = n || t(),
                u = lt(r, this).startOf("day"),
                i = this.diff(u, "days", !0),
                f = -6 > i ? "sameElse" : -1 > i ? "lastWeek" : 0 > i ? "lastDay" : 1 > i ? "sameDay" : 2 > i ? "nextDay" : 7 > i ? "nextWeek" : "sameElse";
            return this.format(this.localeData().calendar(f, this, t(r)))
        },
        isLeapYear: function() {
            return yi(this.year())
        },
        isDST: function() {
            return this.utcOffset() > this.clone().month(0).utcOffset() || this.utcOffset() > this.clone().month(5).utcOffset()
        },
        day: function(n) {
            var t = this._isUTC ? this._d.getUTCDay() : this._d.getDay();
            return null != n ? (n = du(n, this.localeData()), this.add(n - t, "d")) : t
        },
        month: c("Month", !0),
        startOf: function(n) {
            switch (n = f(n)) {
                case "year":
                    this.month(0);
                case "quarter":
                case "month":
                    this.date(1);
                case "week":
                case "isoWeek":
                case "day":
                    this.hours(0);
                case "hour":
                    this.minutes(0);
                case "minute":
                    this.seconds(0);
                case "second":
                    this.milliseconds(0)
            }
            return "week" === n ? this.weekday(0) : "isoWeek" === n && this.isoWeekday(1), "quarter" === n && this.month(3 * Math.floor(this.month() / 3)), this
        },
        endOf: function(t) {
            return t = f(t), t === n || "millisecond" === t ? this : this.startOf(t).add(1, "isoWeek" === t ? "week" : t).subtract(1, "ms")
        },
        isAfter: function(n, i) {
            var r;
            return i = f("undefined" != typeof i ? i : "millisecond"), "millisecond" === i ? (n = t.isMoment(n) ? n : t(n), +this > +n) : (r = t.isMoment(n) ? +n : +t(n), r < +this.clone().startOf(i))
        },
        isBefore: function(n, i) {
            var r;
            return i = f("undefined" != typeof i ? i : "millisecond"), "millisecond" === i ? (n = t.isMoment(n) ? n : t(n), +n > +this) : (r = t.isMoment(n) ? +n : +t(n), +this.clone().endOf(i) < r)
        },
        isBetween: function(n, t, i) {
            return this.isAfter(n, i) && this.isBefore(t, i)
        },
        isSame: function(n, i) {
            var r;
            return i = f(i || "millisecond"), "millisecond" === i ? (n = t.isMoment(n) ? n : t(n), +this == +n) : (r = +t(n), +this.clone().startOf(i) <= r && r <= +this.clone().endOf(i))
        },
        min: o("moment().min is deprecated, use moment.min instead. https://github.com/moment/moment/issues/1548", function(n) {
            return n = t.apply(null, arguments), this > n ? this : n
        }),
        max: o("moment().max is deprecated, use moment.max instead. https://github.com/moment/moment/issues/1548", function(n) {
            return n = t.apply(null, arguments), n > this ? this : n
        }),
        zone: o("moment().zone is deprecated, use moment().utcOffset instead. https://github.com/moment/moment/issues/1779", function(n, t) {
            return null != n ? ("string" != typeof n && (n = -n), this.utcOffset(n, t), this) : -this.utcOffset()
        }),
        utcOffset: function(n, i) {
            var r, u = this._offset || 0;
            return null != n ? ("string" == typeof n && (n = vt(n)), Math.abs(n) < 16 && (n = 60 * n), !this._isUTC && i && (r = this._dateUtcOffset()), this._offset = n, this._isUTC = !0, null != r && this.add(r, "m"), u !== n && (!i || this._changeInProgress ? hi(this, t.duration(n - u, "m"), 1, !1) : this._changeInProgress || (this._changeInProgress = !0, t.updateOffset(this, !0), this._changeInProgress = null)), this) : this._isUTC ? u : this._dateUtcOffset()
        },
        isLocal: function() {
            return !this._isUTC
        },
        isUtcOffset: function() {
            return this._isUTC
        },
        isUtc: function() {
            return this._isUTC && 0 === this._offset
        },
        zoneAbbr: function() {
            return this._isUTC ? "UTC" : ""
        },
        zoneName: function() {
            return this._isUTC ? "Coordinated Universal Time" : ""
        },
        parseZone: function() {
            return this._tzm ? this.utcOffset(this._tzm) : "string" == typeof this._i && this.utcOffset(vt(this._i)), this
        },
        hasAlignedHourOffset: function(n) {
            return n = n ? t(n).utcOffset() : 0, (this.utcOffset() - n) % 60 == 0
        },
        daysInMonth: function() {
            return ct(this.year(), this.month())
        },
        dayOfYear: function(n) {
            var i = k((t(this).startOf("day") - t(this).startOf("year")) / 864e5) + 1;
            return null == n ? i : this.add(n - i, "d")
        },
        quarter: function(n) {
            return null == n ? Math.ceil((this.month() + 1) / 3) : this.month(3 * (n - 1) + this.month() % 3)
        },
        weekYear: function(n) {
            var t = b(this, this.localeData()._week.dow, this.localeData()._week.doy).year;
            return null == n ? t : this.add(n - t, "y")
        },
        isoWeekYear: function(n) {
            var t = b(this, 1, 4).year;
            return null == n ? t : this.add(n - t, "y")
        },
        week: function(n) {
            var t = this.localeData().week(this);
            return null == n ? t : this.add(7 * (n - t), "d")
        },
        isoWeek: function(n) {
            var t = b(this, 1, 4).week;
            return null == n ? t : this.add(7 * (n - t), "d")
        },
        weekday: function(n) {
            var t = (this.day() + 7 - this.localeData()._week.dow) % 7;
            return null == n ? t : this.add(n - t, "d")
        },
        isoWeekday: function(n) {
            return null == n ? this.day() || 7 : this.day(this.day() % 7 ? n : n - 7)
        },
        isoWeeksInYear: function() {
            return ai(this.year(), 1, 4)
        },
        weeksInYear: function() {
            var n = this.localeData()._week;
            return ai(this.year(), n.dow, n.doy)
        },
        get: function(n) {
            return n = f(n), this[n]()
        },
        set: function(n, t) {
            var i;
            if ("object" == typeof n)
                for (i in n) this.set(i, n[i]);
            else n = f(n), "function" == typeof this[n] && this[n](t);
            return this
        },
        locale: function(i) {
            var r;
            return i === n ? this._locale._abbr : (r = t.localeData(i), null != r && (this._locale = r), this)
        },
        lang: o("moment().lang() is deprecated. Instead, use moment().localeData() to get the language configuration. Use moment().locale() to change languages.", function(t) {
            return t === n ? this.localeData() : this.locale(t)
        }),
        localeData: function() {
            return this._locale
        },
        _dateUtcOffset: function() {
            return 15 * -Math.round(this._d.getTimezoneOffset() / 15)
        }
    });
    t.fn.millisecond = t.fn.milliseconds = c("Milliseconds", !1);
    t.fn.second = t.fn.seconds = c("Seconds", !1);
    t.fn.minute = t.fn.minutes = c("Minutes", !1);
    t.fn.hour = t.fn.hours = c("Hours", !0);
    t.fn.date = c("Date", !0);
    t.fn.dates = o("dates accessor is deprecated. Use date instead.", c("Date", !0));
    t.fn.year = c("FullYear", !0);
    t.fn.years = o("years accessor is deprecated. Use year instead.", c("FullYear", !0));
    t.fn.days = t.fn.day;
    t.fn.months = t.fn.month;
    t.fn.weeks = t.fn.week;
    t.fn.isoWeeks = t.fn.isoWeek;
    t.fn.quarters = t.fn.quarter;
    t.fn.toJSON = t.fn.toISOString;
    t.fn.isUTC = t.fn.isUtc;
    w(t.duration.fn = st.prototype, {
        _bubble: function() {
            var u, f, e, o = this._milliseconds,
                t = this._days,
                i = this._months,
                n = this._data,
                r = 0;
            n.milliseconds = o % 1e3;
            u = s(o / 1e3);
            n.seconds = u % 60;
            f = s(u / 60);
            n.minutes = f % 60;
            e = s(f / 60);
            n.hours = e % 24;
            t += s(e / 24);
            r = s(ur(t));
            t -= s(fr(r));
            i += s(t / 30);
            t %= 30;
            r += s(i / 12);
            i %= 12;
            n.days = t;
            n.months = i;
            n.years = r
        },
        abs: function() {
            return this._milliseconds = Math.abs(this._milliseconds), this._days = Math.abs(this._days), this._months = Math.abs(this._months), this._data.milliseconds = Math.abs(this._data.milliseconds), this._data.seconds = Math.abs(this._data.seconds), this._data.minutes = Math.abs(this._data.minutes), this._data.hours = Math.abs(this._data.hours), this._data.months = Math.abs(this._data.months), this._data.years = Math.abs(this._data.years), this
        },
        weeks: function() {
            return s(this.days() / 7)
        },
        valueOf: function() {
            return this._milliseconds + 864e5 * this._days + this._months % 12 * 2592e6 + 31536e6 * i(this._months / 12)
        },
        humanize: function(n) {
            var t = nf(this, !n, this.localeData());
            return n && (t = this.localeData().pastFuture(+this, t)), this.localeData().postformat(t)
        },
        add: function(n, i) {
            var r = t.duration(n, i);
            return this._milliseconds += r._milliseconds, this._days += r._days, this._months += r._months, this._bubble(), this
        },
        subtract: function(n, i) {
            var r = t.duration(n, i);
            return this._milliseconds -= r._milliseconds, this._days -= r._days, this._months -= r._months, this._bubble(), this
        },
        get: function(n) {
            return n = f(n), this[n.toLowerCase() + "s"]()
        },
        as: function(n) {
            var t, i;
            if (n = f(n), "month" === n || "year" === n) return t = this._days + this._milliseconds / 864e5, i = this._months + 12 * ur(t), "month" === n ? i : i / 12;
            switch (t = this._days + Math.round(fr(this._months / 12)), n) {
                case "week":
                    return t / 7 + this._milliseconds / 6048e5;
                case "day":
                    return t + this._milliseconds / 864e5;
                case "hour":
                    return 24 * t + this._milliseconds / 36e5;
                case "minute":
                    return 1440 * t + this._milliseconds / 6e4;
                case "second":
                    return 86400 * t + this._milliseconds / 1e3;
                case "millisecond":
                    return Math.floor(864e5 * t) + this._milliseconds;
                default:
                    throw new Error("Unknown unit " + n);
            }
        },
        lang: t.fn.lang,
        locale: t.fn.locale,
        toIsoString: o("toIsoString() is deprecated. Please use toISOString() instead (notice the capitals)", function() {
            return this.toISOString()
        }),
        toISOString: function() {
            var r = Math.abs(this.years()),
                u = Math.abs(this.months()),
                f = Math.abs(this.days()),
                n = Math.abs(this.hours()),
                t = Math.abs(this.minutes()),
                i = Math.abs(this.seconds() + this.milliseconds() / 1e3);
            return this.asSeconds() ? (this.asSeconds() < 0 ? "-" : "") + "P" + (r ? r + "Y" : "") + (u ? u + "M" : "") + (f ? f + "D" : "") + (n || t || i ? "T" : "") + (n ? n + "H" : "") + (t ? t + "M" : "") + (i ? i + "S" : "") : "P0D"
        },
        localeData: function() {
            return this._locale
        },
        toJSON: function() {
            return this.toISOString()
        }
    });
    t.duration.fn.toString = t.duration.fn.toISOString;
    for (u in yr) p(yr, u) && rf(u.toLowerCase());
    t.duration.fn.asMilliseconds = function() {
        return this.as("ms")
    };
    t.duration.fn.asSeconds = function() {
        return this.as("s")
    };
    t.duration.fn.asMinutes = function() {
        return this.as("m")
    };
    t.duration.fn.asHours = function() {
        return this.as("h")
    };
    t.duration.fn.asDays = function() {
        return this.as("d")
    };
    t.duration.fn.asWeeks = function() {
        return this.as("weeks")
    };
    t.duration.fn.asMonths = function() {
        return this.as("M")
    };
    t.duration.fn.asYears = function() {
        return this.as("y")
    };
    t.locale("en", {
        ordinalParse: /\d{1,2}(th|st|nd|rd)/,
        ordinal: function(n) {
            var t = n % 10,
                r = 1 === i(n % 100 / 10) ? "th" : 1 === t ? "st" : 2 === t ? "nd" : 3 === t ? "rd" : "th";
            return n + r
        }
    });
    sr ? module.exports = t : "function" == typeof define && define.amd ? (define(function(n, i, r) {
        return r.config && r.config() && r.config().noGlobal === !0 && (kt.moment = or), t
    }), er(!0)) : er()
}.call(this),
    function() {
        function t() {}

        function r(n, t) {
            for (var i = n.length; i--;)
                if (n[i].listener === t) return i;
            return -1
        }

        function i(n) {
            return function() {
                return this[n].apply(this, arguments)
            }
        }
        var n = t.prototype,
            u = this,
            f = u.EventEmitter;
        n.getListeners = function(n) {
            var r, t, i = this._getEvents();
            if ("object" == typeof n) {
                r = {};
                for (t in i) i.hasOwnProperty(t) && n.test(t) && (r[t] = i[t])
            } else r = i[n] || (i[n] = []);
            return r
        };
        n.flattenListeners = function(n) {
            for (var i = [], t = 0; n.length > t; t += 1) i.push(n[t].listener);
            return i
        };
        n.getListenersAsObject = function(n) {
            var t, i = this.getListeners(n);
            return i instanceof Array && (t = {}, t[n] = i), t || i
        };
        n.addListener = function(n, t) {
            var i, u = this.getListenersAsObject(n),
                f = "object" == typeof t;
            for (i in u) u.hasOwnProperty(i) && -1 === r(u[i], t) && u[i].push(f ? t : {
                listener: t,
                once: !1
            });
            return this
        };
        n.on = i("addListener");
        n.addOnceListener = function(n, t) {
            return this.addListener(n, {
                listener: t,
                once: !0
            })
        };
        n.once = i("addOnceListener");
        n.defineEvent = function(n) {
            return this.getListeners(n), this
        };
        n.defineEvents = function(n) {
            for (var t = 0; n.length > t; t += 1) this.defineEvent(n[t]);
            return this
        };
        n.removeListener = function(n, t) {
            var f, i, u = this.getListenersAsObject(n);
            for (i in u) u.hasOwnProperty(i) && (f = r(u[i], t), -1 !== f && u[i].splice(f, 1));
            return this
        };
        n.off = i("removeListener");
        n.addListeners = function(n, t) {
            return this.manipulateListeners(!1, n, t)
        };
        n.removeListeners = function(n, t) {
            return this.manipulateListeners(!0, n, t)
        };
        n.manipulateListeners = function(n, t, i) {
            var r, u, f = n ? this.removeListener : this.addListener,
                e = n ? this.removeListeners : this.addListeners;
            if ("object" != typeof t || t instanceof RegExp)
                for (r = i.length; r--;) f.call(this, t, i[r]);
            else
                for (r in t) t.hasOwnProperty(r) && (u = t[r]) && ("function" == typeof u ? f.call(this, r, u) : e.call(this, r, u));
            return this
        };
        n.removeEvent = function(n) {
            var t, r = typeof n,
                i = this._getEvents();
            if ("string" === r) delete i[n];
            else if ("object" === r)
                for (t in i) i.hasOwnProperty(t) && n.test(t) && delete i[t];
            else delete this._events;
            return this
        };
        n.removeAllListeners = i("removeEvent");
        n.emitEvent = function(n, t) {
            var i, f, r, e, u = this.getListenersAsObject(n);
            for (r in u)
                if (u.hasOwnProperty(r))
                    for (f = u[r].length; f--;) i = u[r][f], i.once === !0 && this.removeListener(n, i.listener), e = i.listener.apply(this, t || []), e === this._getOnceReturnValue() && this.removeListener(n, i.listener);
            return this
        };
        n.trigger = i("emitEvent");
        n.emit = function(n) {
            var t = Array.prototype.slice.call(arguments, 1);
            return this.emitEvent(n, t)
        };
        n.setOnceReturnValue = function(n) {
            return this._onceReturnValue = n, this
        };
        n._getOnceReturnValue = function() {
            return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
        };
        n._getEvents = function() {
            return this._events || (this._events = {})
        };
        t.noConflict = function() {
            return u.EventEmitter = f, t
        };
        "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
            return t
        }) : "object" == typeof module && module.exports ? module.exports = t : this.EventEmitter = t
    }.call(this),
    function(n) {
        function f(t) {
            var i = n.event;
            return i.target = i.target || i.srcElement || t, i
        }
        var t = document.documentElement,
            r = function() {},
            i, u;
        t.addEventListener ? r = function(n, t, i) {
            n.addEventListener(t, i, !1)
        } : t.attachEvent && (r = function(n, t, i) {
            n[t + i] = i.handleEvent ? function() {
                var t = f(n);
                i.handleEvent.call(i, t)
            } : function() {
                var t = f(n);
                i.call(n, t)
            };
            n.attachEvent("on" + t, n[t + i])
        });
        i = function() {};
        t.removeEventListener ? i = function(n, t, i) {
            n.removeEventListener(t, i, !1)
        } : t.detachEvent && (i = function(n, t, i) {
            n.detachEvent("on" + t, n[t + i]);
            try {
                delete n[t + i]
            } catch (r) {
                n[t + i] = void 0
            }
        });
        u = {
            bind: r,
            unbind: i
        };
        "function" == typeof define && define.amd ? define("eventie/eventie", u) : n.eventie = u
    }(this),
    function(n, t) {
        "function" == typeof define && define.amd ? define(["eventEmitter/EventEmitter", "eventie/eventie"], function(i, r) {
            return t(n, i, r)
        }) : "object" == typeof exports ? module.exports = t(n, require("wolfy87-eventemitter"), require("eventie")) : n.imagesLoaded = t(n, n.EventEmitter, n.eventie)
    }(window, function(n, t, i) {
        function s(n, t) {
            for (var i in t) n[i] = t[i];
            return n
        }

        function c(n) {
            return "[object Array]" === v.call(n)
        }

        function l(n) {
            var t = [],
                i, r;
            if (c(n)) t = n;
            else if ("number" == typeof n.length)
                for (i = 0, r = n.length; r > i; i++) t.push(n[i]);
            else t.push(n);
            return t
        }

        function r(n, t, i) {
            if (!(this instanceof r)) return new r(n, t);
            "string" == typeof n && (n = document.querySelectorAll(n));
            this.elements = l(n);
            this.options = s({}, this.options);
            "function" == typeof t ? i = t : s(this.options, t);
            i && this.on("always", i);
            this.getImages();
            f && (this.jqDeferred = new f.Deferred);
            var u = this;
            setTimeout(function() {
                u.check()
            })
        }

        function e(n) {
            this.img = n
        }

        function u(n) {
            this.src = n;
            o[n] = this
        }
        var f = n.jQuery,
            h = n.console,
            a = h !== void 0,
            v = Object.prototype.toString,
            o;
        return r.prototype = new t, r.prototype.options = {}, r.prototype.getImages = function() {
            var i, u, n, t, e;
            for (this.images = [], i = 0, u = this.elements.length; u > i; i++)
                if (n = this.elements[i], "IMG" === n.nodeName && this.addImage(n), t = n.nodeType, t && (1 === t || 9 === t || 11 === t))
                    for (var f = n.querySelectorAll("img"), r = 0, o = f.length; o > r; r++) e = f[r], this.addImage(e)
        }, r.prototype.addImage = function(n) {
            var t = new e(n);
            this.images.push(t)
        }, r.prototype.check = function() {
            function f(n, r) {
                return t.options.debug && a && h.log("confirm", n, r), t.progress(n), u++, u === i && t.complete(), !0
            }
            var t = this,
                u = 0,
                i = this.images.length,
                n, r;
            if (this.hasAnyBroken = !1, !i) return this.complete(), void 0;
            for (n = 0; i > n; n++) r = this.images[n], r.on("confirm", f), r.check()
        }, r.prototype.progress = function(n) {
            this.hasAnyBroken = this.hasAnyBroken || !n.isLoaded;
            var t = this;
            setTimeout(function() {
                t.emit("progress", t, n);
                t.jqDeferred && t.jqDeferred.notify && t.jqDeferred.notify(t, n)
            })
        }, r.prototype.complete = function() {
            var t = this.hasAnyBroken ? "fail" : "done",
                n;
            this.isComplete = !0;
            n = this;
            setTimeout(function() {
                if (n.emit(t, n), n.emit("always", n), n.jqDeferred) {
                    var i = n.hasAnyBroken ? "reject" : "resolve";
                    n.jqDeferred[i](n)
                }
            })
        }, f && (f.fn.imagesLoaded = function(n, t) {
            var i = new r(this, n, t);
            return i.jqDeferred.promise(f(this))
        }), e.prototype = new t, e.prototype.check = function() {
            var n = o[this.img.src] || new u(this.img.src),
                t;
            if (n.isConfirmed) return this.confirm(n.isLoaded, "cached was confirmed"), void 0;
            if (this.img.complete && void 0 !== this.img.naturalWidth) return this.confirm(0 !== this.img.naturalWidth, "naturalWidth"), void 0;
            t = this;
            n.on("confirm", function(n, i) {
                return t.confirm(n.isLoaded, i), !0
            });
            n.check()
        }, e.prototype.confirm = function(n, t) {
            this.isLoaded = n;
            this.emit("confirm", this, t)
        }, o = {}, u.prototype = new t, u.prototype.check = function() {
            if (!this.isChecked) {
                var n = new Image;
                i.bind(n, "load", this);
                i.bind(n, "error", this);
                n.src = this.src;
                this.isChecked = !0
            }
        }, u.prototype.handleEvent = function(n) {
            var t = "on" + n.type;
            this[t] && this[t](n)
        }, u.prototype.onload = function(n) {
            this.confirm(!0, "onload");
            this.unbindProxyEvents(n)
        }, u.prototype.onerror = function(n) {
            this.confirm(!1, "onerror");
            this.unbindProxyEvents(n)
        }, u.prototype.confirm = function(n, t) {
            this.isConfirmed = !0;
            this.isLoaded = n;
            this.emit("confirm", this, t)
        }, u.prototype.unbindProxyEvents = function(n) {
            i.unbind(n.target, "load", this);
            i.unbind(n.target, "error", this)
        }, r
    });
! function(n) {
    function i() {}

    function t(n) {
        function u(t) {
            t.prototype.option || (t.prototype.option = function(t) {
                n.isPlainObject(t) && (this.options = n.extend(!0, this.options, t))
            })
        }

        function f(i, u) {
            n.fn[i] = function(f) {
                var h, e, s;
                if ("string" == typeof f) {
                    for (var c = r.call(arguments, 1), o = 0, l = this.length; l > o; o++)
                        if (h = this[o], e = n.data(h, i), e)
                            if (n.isFunction(e[f]) && "_" !== f.charAt(0)) {
                                if (s = e[f].apply(e, c), void 0 !== s) return s
                            } else t("no such method '" + f + "' for " + i + " instance");
                    else t("cannot call methods on " + i + " prior to initialization; attempted to call '" + f + "'");
                    return this
                }
                return this.each(function() {
                    var t = n.data(this, i);
                    t ? (t.option(f), t._init()) : (t = new u(this, f), n.data(this, i, t))
                })
            }
        }
        if (n) {
            var t = "undefined" == typeof console ? i : function(n) {
                console.error(n)
            };
            return n.bridget = function(n, t) {
                u(t);
                f(n, t)
            }, n.bridget
        }
    }
    var r = Array.prototype.slice;
    "function" == typeof define && define.amd ? define("jquery-bridget/jquery.bridget", ["jquery"], t) : t("object" == typeof exports ? require("jquery") : n.jQuery)
}(window),
function(n) {
    function f(t) {
        var i = n.event;
        return i.target = i.target || i.srcElement || t, i
    }
    var t = document.documentElement,
        u = function() {},
        i, r;
    t.addEventListener ? u = function(n, t, i) {
        n.addEventListener(t, i, !1)
    } : t.attachEvent && (u = function(n, t, i) {
        n[t + i] = i.handleEvent ? function() {
            var t = f(n);
            i.handleEvent.call(i, t)
        } : function() {
            var t = f(n);
            i.call(n, t)
        };
        n.attachEvent("on" + t, n[t + i])
    });
    i = function() {};
    t.removeEventListener ? i = function(n, t, i) {
        n.removeEventListener(t, i, !1)
    } : t.detachEvent && (i = function(n, t, i) {
        n.detachEvent("on" + t, n[t + i]);
        try {
            delete n[t + i]
        } catch (r) {
            n[t + i] = void 0
        }
    });
    r = {
        bind: u,
        unbind: i
    };
    "function" == typeof define && define.amd ? define("eventie/eventie", r) : "object" == typeof exports ? module.exports = r : n.eventie = r
}(this),
function(n) {
    function t(n) {
        "function" == typeof n && (t.isReady ? n() : f.push(n))
    }

    function r(n) {
        var r = "readystatechange" === n.type && "complete" !== i.readyState;
        t.isReady || r || e()
    }

    function e() {
        var n, i, r;
        for (t.isReady = !0, n = 0, i = f.length; i > n; n++) r = f[n], r()
    }

    function u(u) {
        return "complete" === i.readyState ? e() : (u.bind(i, "DOMContentLoaded", r), u.bind(i, "readystatechange", r), u.bind(n, "load", r)), t
    }
    var i = n.document,
        f = [];
    t.isReady = !1;
    "function" == typeof define && define.amd ? define("doc-ready/doc-ready", ["eventie/eventie"], u) : "object" == typeof exports ? module.exports = u(require("eventie")) : n.docReady = u(n.eventie)
}(window),
function() {
    function t() {}

    function u(n, t) {
        for (var i = n.length; i--;)
            if (n[i].listener === t) return i;
        return -1
    }

    function i(n) {
        return function() {
            return this[n].apply(this, arguments)
        }
    }
    var n = t.prototype,
        r = this,
        f = r.EventEmitter;
    n.getListeners = function(n) {
        var r, t, i = this._getEvents();
        if (n instanceof RegExp) {
            r = {};
            for (t in i) i.hasOwnProperty(t) && n.test(t) && (r[t] = i[t])
        } else r = i[n] || (i[n] = []);
        return r
    };
    n.flattenListeners = function(n) {
        for (var i = [], t = 0; t < n.length; t += 1) i.push(n[t].listener);
        return i
    };
    n.getListenersAsObject = function(n) {
        var t, i = this.getListeners(n);
        return i instanceof Array && (t = {}, t[n] = i), t || i
    };
    n.addListener = function(n, t) {
        var i, r = this.getListenersAsObject(n),
            f = "object" == typeof t;
        for (i in r) r.hasOwnProperty(i) && -1 === u(r[i], t) && r[i].push(f ? t : {
            listener: t,
            once: !1
        });
        return this
    };
    n.on = i("addListener");
    n.addOnceListener = function(n, t) {
        return this.addListener(n, {
            listener: t,
            once: !0
        })
    };
    n.once = i("addOnceListener");
    n.defineEvent = function(n) {
        return this.getListeners(n), this
    };
    n.defineEvents = function(n) {
        for (var t = 0; t < n.length; t += 1) this.defineEvent(n[t]);
        return this
    };
    n.removeListener = function(n, t) {
        var f, i, r = this.getListenersAsObject(n);
        for (i in r) r.hasOwnProperty(i) && (f = u(r[i], t), -1 !== f && r[i].splice(f, 1));
        return this
    };
    n.off = i("removeListener");
    n.addListeners = function(n, t) {
        return this.manipulateListeners(!1, n, t)
    };
    n.removeListeners = function(n, t) {
        return this.manipulateListeners(!0, n, t)
    };
    n.manipulateListeners = function(n, t, i) {
        var r, u, f = n ? this.removeListener : this.addListener,
            e = n ? this.removeListeners : this.addListeners;
        if ("object" != typeof t || t instanceof RegExp)
            for (r = i.length; r--;) f.call(this, t, i[r]);
        else
            for (r in t) t.hasOwnProperty(r) && (u = t[r]) && ("function" == typeof u ? f.call(this, r, u) : e.call(this, r, u));
        return this
    };
    n.removeEvent = function(n) {
        var t, r = typeof n,
            i = this._getEvents();
        if ("string" === r) delete i[n];
        else if (n instanceof RegExp)
            for (t in i) i.hasOwnProperty(t) && n.test(t) && delete i[t];
        else delete this._events;
        return this
    };
    n.removeAllListeners = i("removeEvent");
    n.emitEvent = function(n, t) {
        var i, f, r, e, u = this.getListenersAsObject(n);
        for (r in u)
            if (u.hasOwnProperty(r))
                for (f = u[r].length; f--;) i = u[r][f], i.once === !0 && this.removeListener(n, i.listener), e = i.listener.apply(this, t || []), e === this._getOnceReturnValue() && this.removeListener(n, i.listener);
        return this
    };
    n.trigger = i("emitEvent");
    n.emit = function(n) {
        var t = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(n, t)
    };
    n.setOnceReturnValue = function(n) {
        return this._onceReturnValue = n, this
    };
    n._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    };
    n._getEvents = function() {
        return this._events || (this._events = {})
    };
    t.noConflict = function() {
        return r.EventEmitter = f, t
    };
    "function" == typeof define && define.amd ? define("eventEmitter/EventEmitter", [], function() {
        return t
    }) : "object" == typeof module && module.exports ? module.exports = t : r.EventEmitter = t
}.call(this),
    function(n) {
        function t(n) {
            if (n) {
                if ("string" == typeof r[n]) return n;
                n = n.charAt(0).toUpperCase() + n.slice(1);
                for (var t, u = 0, f = i.length; f > u; u++)
                    if (t = i[u] + n, "string" == typeof r[t]) return t
            }
        }
        var i = "Webkit Moz ms Ms O".split(" "),
            r = document.documentElement.style;
        "function" == typeof define && define.amd ? define("get-style-property/get-style-property", [], function() {
            return t
        }) : "object" == typeof exports ? module.exports = t : n.getStyleProperty = t
    }(window),
    function(n) {
        function i(n) {
            var t = parseFloat(n),
                i = -1 === n.indexOf("%") && !isNaN(t);
            return i && t
        }

        function u() {}

        function f() {
            for (var r, i = {
                    width: 0,
                    height: 0,
                    innerWidth: 0,
                    innerHeight: 0,
                    outerWidth: 0,
                    outerHeight: 0
                }, n = 0, u = t.length; u > n; n++) r = t[n], i[r] = 0;
            return i
        }

        function r(r) {
            function c() {
                var f, t, c, l;
                h || (h = !0, f = n.getComputedStyle, (o = function() {
                    var n = f ? function(n) {
                        return f(n, null)
                    } : function(n) {
                        return n.currentStyle
                    };
                    return function(t) {
                        var i = n(t);
                        return i || e("Style returned " + i + ". Are you running this code in a hidden iframe on Firefox? See http://bit.ly/getsizebug1"), i
                    }
                }(), u = r("boxSizing")) && (t = document.createElement("div"), t.style.width = "200px", t.style.padding = "1px 2px 3px 4px", t.style.borderStyle = "solid", t.style.borderWidth = "1px 2px 3px 4px", t.style[u] = "border-box", c = document.body || document.documentElement, c.appendChild(t), l = o(t), s = 200 === i(l.width), c.removeChild(t)))
            }

            function l(n) {
                var e, r, v, h, y, p;
                if (c(), "string" == typeof n && (n = document.querySelector(n)), n && "object" == typeof n && n.nodeType) {
                    if (e = o(n), "none" === e.display) return f();
                    r = {};
                    r.width = n.offsetWidth;
                    r.height = n.offsetHeight;
                    for (var tt = r.isBorderBox = !(!u || !e[u] || "border-box" !== e[u]), l = 0, it = t.length; it > l; l++) v = t[l], h = e[v], h = a(n, h), y = parseFloat(h), r[v] = isNaN(y) ? 0 : y;
                    var w = r.paddingLeft + r.paddingRight,
                        b = r.paddingTop + r.paddingBottom,
                        rt = r.marginLeft + r.marginRight,
                        ut = r.marginTop + r.marginBottom,
                        k = r.borderLeftWidth + r.borderRightWidth,
                        d = r.borderTopWidth + r.borderBottomWidth,
                        g = tt && s,
                        nt = i(e.width);
                    return nt !== !1 && (r.width = nt + (g ? 0 : w + k)), p = i(e.height), p !== !1 && (r.height = p + (g ? 0 : b + d)), r.innerWidth = r.width - (w + k), r.innerHeight = r.height - (b + d), r.outerWidth = r.width + rt, r.outerHeight = r.height + ut, r
                }
            }

            function a(t, i) {
                if (n.getComputedStyle || -1 === i.indexOf("%")) return i;
                var r = t.style,
                    e = r.left,
                    u = t.runtimeStyle,
                    f = u && u.left;
                return f && (u.left = t.currentStyle.left), r.left = i, i = r.pixelLeft, r.left = e, f && (u.left = f), i
            }
            var o, u, s, h = !1;
            return l
        }
        var e = "undefined" == typeof console ? u : function(n) {
                console.error(n)
            },
            t = ["paddingLeft", "paddingRight", "paddingTop", "paddingBottom", "marginLeft", "marginRight", "marginTop", "marginBottom", "borderLeftWidth", "borderRightWidth", "borderTopWidth", "borderBottomWidth"];
        "function" == typeof define && define.amd ? define("get-size/get-size", ["get-style-property/get-style-property"], r) : "object" == typeof exports ? module.exports = r(require("desandro-get-style-property")) : n.getSize = r(n.getStyleProperty)
    }(window),
    function(n) {
        function i(n, t) {
            return n[u](t)
        }

        function r(n) {
            if (!n.parentNode) {
                var t = document.createDocumentFragment();
                t.appendChild(n)
            }
        }

        function o(n, t) {
            r(n);
            for (var u = n.parentNode.querySelectorAll(t), i = 0, f = u.length; f > i; i++)
                if (u[i] === n) return !0;
            return !1
        }

        function s(n, t) {
            return r(n), i(n, t)
        }
        var t, u = function() {
                var u, i;
                if (n.matchesSelector) return "matchesSelector";
                for (var r = ["webkit", "moz", "ms", "o"], t = 0, f = r.length; f > t; t++)
                    if (u = r[t], i = u + "MatchesSelector", n[i]) return i
            }(),
            f, e;
        u ? (f = document.createElement("div"), e = i(f, "div"), t = e ? i : s) : t = o;
        "function" == typeof define && define.amd ? define("matches-selector/matches-selector", [], function() {
            return t
        }) : "object" == typeof exports ? module.exports = t : window.matchesSelector = t
    }(Element.prototype),
    function(n) {
        function r(n, t) {
            for (var i in t) n[i] = t[i];
            return n
        }

        function u(n) {
            for (var t in n) return !1;
            return t = null, !0
        }

        function f(n) {
            return n.replace(/([A-Z])/g, function(n) {
                return "-" + n.toLowerCase()
            })
        }

        function t(n, t, i) {
            function o(n, t) {
                n && (this.element = n, this.layout = t, this.position = {
                    x: 0,
                    y: 0
                }, this._create())
            }
            var s = i("transition"),
                h = i("transform"),
                w = s && h,
                b = !!i("perspective"),
                c = {
                    WebkitTransition: "webkitTransitionEnd",
                    MozTransition: "transitionend",
                    OTransition: "otransitionend",
                    transition: "transitionend"
                }[s],
                l = ["transform", "transition", "transitionDuration", "transitionProperty"],
                k = function() {
                    for (var n, t, u = {}, r = 0, f = l.length; f > r; r++) n = l[r], t = i(n), t && t !== n && (u[n] = t);
                    return u
                }(),
                a, v, y, p;
            return r(o.prototype, n.prototype), o.prototype._create = function() {
                this._transn = {
                    ingProperties: {},
                    clean: {},
                    onEnd: {}
                };
                this.css({
                    position: "absolute"
                })
            }, o.prototype.handleEvent = function(n) {
                var t = "on" + n.type;
                this[t] && this[t](n)
            }, o.prototype.getSize = function() {
                this.size = t(this.element)
            }, o.prototype.css = function(n) {
                var r = this.element.style,
                    t, i;
                for (t in n) i = k[t] || t, r[i] = n[t]
            }, o.prototype.getPosition = function() {
                var r = e(this.element),
                    u = this.layout.options,
                    f = u.isOriginLeft,
                    o = u.isOriginTop,
                    n = parseInt(r[f ? "left" : "right"], 10),
                    t = parseInt(r[o ? "top" : "bottom"], 10),
                    i;
                n = isNaN(n) ? 0 : n;
                t = isNaN(t) ? 0 : t;
                i = this.layout.size;
                n -= f ? i.paddingLeft : i.paddingRight;
                t -= o ? i.paddingTop : i.paddingBottom;
                this.position.x = n;
                this.position.y = t
            }, o.prototype.layoutPosition = function() {
                var t = this.layout.size,
                    i = this.layout.options,
                    n = {};
                i.isOriginLeft ? (n.left = this.position.x + t.paddingLeft + "px", n.right = "") : (n.right = this.position.x + t.paddingRight + "px", n.left = "");
                i.isOriginTop ? (n.top = this.position.y + t.paddingTop + "px", n.bottom = "") : (n.bottom = this.position.y + t.paddingBottom + "px", n.top = "");
                this.css(n);
                this.emitEvent("layout", [this])
            }, a = b ? function(n, t) {
                return "translate3d(" + n + "px, " + t + "px, 0)"
            } : function(n, t) {
                return "translate(" + n + "px, " + t + "px)"
            }, o.prototype._transitionTo = function(n, t) {
                this.getPosition();
                var e = this.position.x,
                    o = this.position.y,
                    s = parseInt(n, 10),
                    h = parseInt(t, 10),
                    c = s === this.position.x && h === this.position.y;
                if (this.setPosition(n, t), c && !this.isTransitioning) return void this.layoutPosition();
                var i = n - e,
                    r = t - o,
                    u = {},
                    f = this.layout.options;
                i = f.isOriginLeft ? i : -i;
                r = f.isOriginTop ? r : -r;
                u.transform = a(i, r);
                this.transition({
                    to: u,
                    onTransitionEnd: {
                        transform: this.layoutPosition
                    },
                    isCleaning: !0
                })
            }, o.prototype.goTo = function(n, t) {
                this.setPosition(n, t);
                this.layoutPosition()
            }, o.prototype.moveTo = w ? o.prototype._transitionTo : o.prototype.goTo, o.prototype.setPosition = function(n, t) {
                this.position.x = parseInt(n, 10);
                this.position.y = parseInt(t, 10)
            }, o.prototype._nonTransition = function(n) {
                this.css(n.to);
                n.isCleaning && this._removeStyles(n.to);
                for (var t in n.onTransitionEnd) n.onTransitionEnd[t].call(this)
            }, o.prototype._transition = function(n) {
                var i, t, r;
                if (!parseFloat(this.layout.options.transitionDuration)) return void this._nonTransition(n);
                i = this._transn;
                for (t in n.onTransitionEnd) i.onEnd[t] = n.onTransitionEnd[t];
                for (t in n.to) i.ingProperties[t] = !0, n.isCleaning && (i.clean[t] = !0);
                n.from && (this.css(n.from), r = this.element.offsetHeight, r = null);
                this.enableTransition(n.to);
                this.css(n.to);
                this.isTransitioning = !0
            }, v = h && f(h) + ",opacity", o.prototype.enableTransition = function() {
                this.isTransitioning || (this.css({
                    transitionProperty: v,
                    transitionDuration: this.layout.options.transitionDuration
                }), this.element.addEventListener(c, this, !1))
            }, o.prototype.transition = o.prototype[s ? "_transition" : "_nonTransition"], o.prototype.onwebkitTransitionEnd = function(n) {
                this.ontransitionend(n)
            }, o.prototype.onotransitionend = function(n) {
                this.ontransitionend(n)
            }, y = {
                "-webkit-transform": "transform",
                "-moz-transform": "transform",
                "-o-transform": "transform"
            }, o.prototype.ontransitionend = function(n) {
                var t, i, r;
                n.target === this.element && (t = this._transn, i = y[n.propertyName] || n.propertyName, (delete t.ingProperties[i], u(t.ingProperties) && this.disableTransition(), i in t.clean && (this.element.style[n.propertyName] = "", delete t.clean[i]), i in t.onEnd) && (r = t.onEnd[i], r.call(this), delete t.onEnd[i]), this.emitEvent("transitionEnd", [this]))
            }, o.prototype.disableTransition = function() {
                this.removeTransitionStyles();
                this.element.removeEventListener(c, this, !1);
                this.isTransitioning = !1
            }, o.prototype._removeStyles = function(n) {
                var t = {},
                    i;
                for (i in n) t[i] = "";
                this.css(t)
            }, p = {
                transitionProperty: "",
                transitionDuration: ""
            }, o.prototype.removeTransitionStyles = function() {
                this.css(p)
            }, o.prototype.removeElem = function() {
                this.element.parentNode.removeChild(this.element);
                this.emitEvent("remove", [this])
            }, o.prototype.remove = function() {
                if (!s || !parseFloat(this.layout.options.transitionDuration)) return void this.removeElem();
                var n = this;
                this.on("transitionEnd", function() {
                    return n.removeElem(), !0
                });
                this.hide()
            }, o.prototype.reveal = function() {
                delete this.isHidden;
                this.css({
                    display: ""
                });
                var n = this.layout.options;
                this.transition({
                    from: n.hiddenStyle,
                    to: n.visibleStyle,
                    isCleaning: !0
                })
            }, o.prototype.hide = function() {
                this.isHidden = !0;
                this.css({
                    display: ""
                });
                var n = this.layout.options;
                this.transition({
                    from: n.visibleStyle,
                    to: n.hiddenStyle,
                    isCleaning: !0,
                    onTransitionEnd: {
                        opacity: function() {
                            this.isHidden && this.css({
                                display: "none"
                            })
                        }
                    }
                })
            }, o.prototype.destroy = function() {
                this.css({
                    position: "",
                    left: "",
                    right: "",
                    top: "",
                    bottom: "",
                    transition: "",
                    transform: ""
                })
            }, o
        }
        var i = n.getComputedStyle,
            e = i ? function(n) {
                return i(n, null)
            } : function(n) {
                return n.currentStyle
            };
        "function" == typeof define && define.amd ? define("outlayer/item", ["eventEmitter/EventEmitter", "get-size/get-size", "get-style-property/get-style-property"], t) : "object" == typeof exports ? module.exports = t(require("wolfy87-eventemitter"), require("get-size"), require("desandro-get-style-property")) : (n.Outlayer = {}, n.Outlayer.Item = t(n.EventEmitter, n.getSize, n.getStyleProperty))
    }(window),
    function(n) {
        function t(n, t) {
            for (var i in t) n[i] = t[i];
            return n
        }

        function c(n) {
            return "[object Array]" === a.call(n)
        }

        function u(n) {
            var t = [],
                i, r;
            if (c(n)) t = n;
            else if (n && "number" == typeof n.length)
                for (i = 0, r = n.length; r > i; i++) t.push(n[i]);
            else t.push(n);
            return t
        }

        function o(n, t) {
            var i = v(t, n); - 1 !== i && t.splice(i, 1)
        }

        function l(n) {
            return n.replace(/(.)([A-Z])/g, function(n, t, i) {
                return t + "-" + i
            }).toLowerCase()
        }

        function f(f, c, a, v, y, p) {
            function w(n, i) {
                if ("string" == typeof n && (n = s.querySelector(n)), !n || !e(n)) return void(r && r.error("Bad " + this.constructor.namespace + " element: " + n));
                this.element = n;
                this.options = t({}, this.constructor.defaults);
                this.option(i);
                var u = ++k;
                this.element.outlayerGUID = u;
                b[u] = this;
                this._create();
                this.options.isInitLayout && this.layout()
            }
            var k = 0,
                b = {};
            return w.namespace = "outlayer", w.Item = p, w.defaults = {
                containerStyle: {
                    position: "relative"
                },
                isInitLayout: !0,
                isOriginLeft: !0,
                isOriginTop: !0,
                isResizeBound: !0,
                isResizingContainer: !0,
                transitionDuration: "0.4s",
                hiddenStyle: {
                    opacity: 0,
                    transform: "scale(0.001)"
                },
                visibleStyle: {
                    opacity: 1,
                    transform: "scale(1)"
                }
            }, t(w.prototype, a.prototype), w.prototype.option = function(n) {
                t(this.options, n)
            }, w.prototype._create = function() {
                this.reloadItems();
                this.stamps = [];
                this.stamp(this.options.stamp);
                t(this.element.style, this.options.containerStyle);
                this.options.isResizeBound && this.bindResize()
            }, w.prototype.reloadItems = function() {
                this.items = this._itemize(this.element.children)
            }, w.prototype._itemize = function(n) {
                for (var u, f, i = this._filterFindItemElements(n), e = this.constructor.Item, r = [], t = 0, o = i.length; o > t; t++) u = i[t], f = new e(u, this), r.push(f);
                return r
            }, w.prototype._filterFindItemElements = function(n) {
                var t;
                n = u(n);
                for (var r = this.options.itemSelector, i = [], f = 0, h = n.length; h > f; f++)
                    if (t = n[f], e(t))
                        if (r) {
                            y(t, r) && i.push(t);
                            for (var s = t.querySelectorAll(r), o = 0, c = s.length; c > o; o++) i.push(s[o])
                        } else i.push(t);
                return i
            }, w.prototype.getItemElements = function() {
                for (var t = [], n = 0, i = this.items.length; i > n; n++) t.push(this.items[n].element);
                return t
            }, w.prototype.layout = function() {
                this._resetLayout();
                this._manageStamps();
                var n = void 0 !== this.options.isLayoutInstant ? this.options.isLayoutInstant : !this._isLayoutInited;
                this.layoutItems(this.items, n);
                this._isLayoutInited = !0
            }, w.prototype._init = w.prototype.layout, w.prototype._resetLayout = function() {
                this.getSize()
            }, w.prototype.getSize = function() {
                this.size = v(this.element)
            }, w.prototype._getMeasurement = function(n, t) {
                var r, i = this.options[n];
                i ? ("string" == typeof i ? r = this.element.querySelector(i) : e(i) && (r = i), this[n] = r ? v(r)[t] : i) : this[n] = 0
            }, w.prototype.layoutItems = function(n, t) {
                n = this._getItemsForLayout(n);
                this._layoutItems(n, t);
                this._postLayout()
            }, w.prototype._getItemsForLayout = function(n) {
                for (var i, r = [], t = 0, u = n.length; u > t; t++) i = n[t], i.isIgnored || r.push(i);
                return r
            }, w.prototype._layoutItems = function(n, t) {
                function f() {
                    e.emitEvent("layoutComplete", [e, n])
                }
                var e = this,
                    i, r;
                if (!n || !n.length) return void f();
                this._itemsOn(n, "layout", f);
                for (var o = [], u = 0, s = n.length; s > u; u++) i = n[u], r = this._getItemLayoutPosition(i), r.item = i, r.isInstant = t || i.isLayoutInstant, o.push(r);
                this._processLayoutQueue(o)
            }, w.prototype._getItemLayoutPosition = function() {
                return {
                    x: 0,
                    y: 0
                }
            }, w.prototype._processLayoutQueue = function(n) {
                for (var t, i = 0, r = n.length; r > i; i++) t = n[i], this._positionItem(t.item, t.x, t.y, t.isInstant)
            }, w.prototype._positionItem = function(n, t, i, r) {
                r ? n.goTo(t, i) : n.moveTo(t, i)
            }, w.prototype._postLayout = function() {
                this.resizeContainer()
            }, w.prototype.resizeContainer = function() {
                if (this.options.isResizingContainer) {
                    var n = this._getContainerSize();
                    n && (this._setContainerMeasure(n.width, !0), this._setContainerMeasure(n.height, !1))
                }
            }, w.prototype._getContainerSize = h, w.prototype._setContainerMeasure = function(n, t) {
                if (void 0 !== n) {
                    var i = this.size;
                    i.isBorderBox && (n += t ? i.paddingLeft + i.paddingRight + i.borderLeftWidth + i.borderRightWidth : i.paddingBottom + i.paddingTop + i.borderTopWidth + i.borderBottomWidth);
                    n = Math.max(n, 0);
                    this.element.style[t ? "width" : "height"] = n + "px"
                }
            }, w.prototype._itemsOn = function(n, t, i) {
                function e() {
                    return u++, u === o && i.call(s), !0
                }
                for (var f, u = 0, o = n.length, s = this, r = 0, h = n.length; h > r; r++) {
                    f = n[r];
                    f.on(t, e)
                }
            }, w.prototype.ignore = function(n) {
                var t = this.getItem(n);
                t && (t.isIgnored = !0)
            }, w.prototype.unignore = function(n) {
                var t = this.getItem(n);
                t && delete t.isIgnored
            }, w.prototype.stamp = function(n) {
                var t, i, r;
                if (n = this._find(n))
                    for (this.stamps = this.stamps.concat(n), t = 0, i = n.length; i > t; t++) r = n[t], this.ignore(r)
            }, w.prototype.unstamp = function(n) {
                var t, r, i;
                if (n = this._find(n))
                    for (t = 0, r = n.length; r > t; t++) i = n[t], o(i, this.stamps), this.unignore(i)
            }, w.prototype._find = function(n) {
                if (n) return ("string" == typeof n && (n = this.element.querySelectorAll(n)), n = u(n))
            }, w.prototype._manageStamps = function() {
                var n, t, i;
                if (this.stamps && this.stamps.length)
                    for (this._getBoundingRect(), n = 0, t = this.stamps.length; t > n; n++) i = this.stamps[n], this._manageStamp(i)
            }, w.prototype._getBoundingRect = function() {
                var t = this.element.getBoundingClientRect(),
                    n = this.size;
                this._boundingRect = {
                    left: t.left + n.paddingLeft + n.borderLeftWidth,
                    top: t.top + n.paddingTop + n.borderTopWidth,
                    right: t.right - (n.paddingRight + n.borderRightWidth),
                    bottom: t.bottom - (n.paddingBottom + n.borderBottomWidth)
                }
            }, w.prototype._manageStamp = h, w.prototype._getElementOffset = function(n) {
                var t = n.getBoundingClientRect(),
                    i = this._boundingRect,
                    r = v(n);
                return {
                    left: t.left - i.left - r.marginLeft,
                    top: t.top - i.top - r.marginTop,
                    right: i.right - t.right - r.marginRight,
                    bottom: i.bottom - t.bottom - r.marginBottom
                }
            }, w.prototype.handleEvent = function(n) {
                var t = "on" + n.type;
                this[t] && this[t](n)
            }, w.prototype.bindResize = function() {
                this.isResizeBound || (f.bind(n, "resize", this), this.isResizeBound = !0)
            }, w.prototype.unbindResize = function() {
                this.isResizeBound && f.unbind(n, "resize", this);
                this.isResizeBound = !1
            }, w.prototype.onresize = function() {
                function t() {
                    n.resize();
                    delete n.resizeTimeout
                }
                this.resizeTimeout && clearTimeout(this.resizeTimeout);
                var n = this;
                this.resizeTimeout = setTimeout(t, 100)
            }, w.prototype.resize = function() {
                this.isResizeBound && this.needsResizeLayout() && this.layout()
            }, w.prototype.needsResizeLayout = function() {
                var n = v(this.element),
                    t = this.size && n;
                return t && n.innerWidth !== this.size.innerWidth
            }, w.prototype.addItems = function(n) {
                var t = this._itemize(n);
                return t.length && (this.items = this.items.concat(t)), t
            }, w.prototype.appended = function(n) {
                var t = this.addItems(n);
                t.length && (this.layoutItems(t, !0), this.reveal(t))
            }, w.prototype.prepended = function(n) {
                var t = this._itemize(n),
                    i;
                t.length && (i = this.items.slice(0), this.items = t.concat(i), this._resetLayout(), this._manageStamps(), this.layoutItems(t, !0), this.reveal(t), this.layoutItems(i))
            }, w.prototype.reveal = function(n) {
                var i = n && n.length,
                    t, r;
                if (i)
                    for (t = 0; i > t; t++) r = n[t], r.reveal()
            }, w.prototype.hide = function(n) {
                var i = n && n.length,
                    t, r;
                if (i)
                    for (t = 0; i > t; t++) r = n[t], r.hide()
            }, w.prototype.getItem = function(n) {
                for (var i, t = 0, r = this.items.length; r > t; t++)
                    if (i = this.items[t], i.element === n) return i
            }, w.prototype.getItems = function(n) {
                var u, i;
                if (n && n.length) {
                    for (var r = [], t = 0, f = n.length; f > t; t++) u = n[t], i = this.getItem(u), i && r.push(i);
                    return r
                }
            }, w.prototype.remove = function(n) {
                var t, i, f, r;
                if (n = u(n), t = this.getItems(n), t && t.length)
                    for (this._itemsOn(t, "remove", function() {
                            this.emitEvent("removeComplete", [this, t])
                        }), i = 0, f = t.length; f > i; i++) r = t[i], r.remove(), o(r, this.items)
            }, w.prototype.destroy = function() {
                var t = this.element.style,
                    n, r, u, f;
                for (t.height = "", t.position = "", t.width = "", n = 0, r = this.items.length; r > n; n++) u = this.items[n], u.destroy();
                this.unbindResize();
                f = this.element.outlayerGUID;
                delete b[f];
                delete this.element.outlayerGUID;
                i && i.removeData(this.element, this.constructor.namespace)
            }, w.data = function(n) {
                var t = n && n.outlayerGUID;
                return t && b[t]
            }, w.create = function(n, u) {
                function f() {
                    w.apply(this, arguments)
                }
                return Object.create ? f.prototype = Object.create(w.prototype) : t(f.prototype, w.prototype), f.prototype.constructor = f, f.defaults = t({}, w.defaults), t(f.defaults, u), f.prototype.settings = {}, f.namespace = n, f.data = w.data, f.Item = function() {
                    p.apply(this, arguments)
                }, f.Item.prototype = new p, c(function() {
                    for (var a, t, e, v, o = l(n), h = s.querySelectorAll(".js-" + o), c = "data-" + o + "-options", u = 0, y = h.length; y > u; u++) {
                        t = h[u];
                        e = t.getAttribute(c);
                        try {
                            a = e && JSON.parse(e)
                        } catch (p) {
                            r && r.error("Error parsing " + c + " on " + t.nodeName.toLowerCase() + (t.id ? "#" + t.id : "") + ": " + p);
                            continue
                        }
                        v = new f(t, a);
                        i && i.data(t, n, v)
                    }
                }), i && i.bridget && i.bridget(n, f), f
            }, w.Item = p, w
        }
        var s = n.document,
            r = n.console,
            i = n.jQuery,
            h = function() {},
            a = Object.prototype.toString,
            e = "function" == typeof HTMLElement || "object" == typeof HTMLElement ? function(n) {
                return n instanceof HTMLElement
            } : function(n) {
                return n && "object" == typeof n && 1 === n.nodeType && "string" == typeof n.nodeName
            },
            v = Array.prototype.indexOf ? function(n, t) {
                return n.indexOf(t)
            } : function(n, t) {
                for (var i = 0, r = n.length; r > i; i++)
                    if (n[i] === t) return i;
                return -1
            };
        "function" == typeof define && define.amd ? define("outlayer/outlayer", ["eventie/eventie", "doc-ready/doc-ready", "eventEmitter/EventEmitter", "get-size/get-size", "matches-selector/matches-selector", "./item"], f) : "object" == typeof exports ? module.exports = f(require("eventie"), require("doc-ready"), require("wolfy87-eventemitter"), require("get-size"), require("desandro-matches-selector"), require("./item")) : n.Outlayer = f(n.eventie, n.docReady, n.EventEmitter, n.getSize, n.matchesSelector, n.Outlayer.Item)
    }(window),
    function(n) {
        function t(n, t) {
            var r = n.create("masonry");
            return r.prototype._resetLayout = function() {
                this.getSize();
                this._getMeasurement("columnWidth", "outerWidth");
                this._getMeasurement("gutter", "outerWidth");
                this.measureColumns();
                var n = this.cols;
                for (this.colYs = []; n--;) this.colYs.push(0);
                this.maxY = 0
            }, r.prototype.measureColumns = function() {
                if (this.getContainerWidth(), !this.columnWidth) {
                    var n = this.items[0],
                        i = n && n.element;
                    this.columnWidth = i && t(i).outerWidth || this.containerWidth
                }
                this.columnWidth += this.gutter;
                this.cols = Math.floor((this.containerWidth + this.gutter) / this.columnWidth);
                this.cols = Math.max(this.cols, 1)
            }, r.prototype.getContainerWidth = function() {
                var i = this.options.isFitWidth ? this.element.parentNode : this.element,
                    n = t(i);
                this.containerWidth = n && n.innerWidth
            }, r.prototype._getItemLayoutPosition = function(n) {
                n.getSize();
                var e = n.size.outerWidth % this.columnWidth,
                    s = e && 1 > e ? "round" : "ceil",
                    t = Math[s](n.size.outerWidth / this.columnWidth);
                t = Math.min(t, this.cols);
                for (var r = this._getColGroup(t), u = Math.min.apply(Math, r), o = i(r, u), h = {
                        x: this.columnWidth * o,
                        y: u
                    }, c = u + n.size.outerHeight, l = this.cols + 1 - r.length, f = 0; l > f; f++) this.colYs[o + f] = c;
                return h
            }, r.prototype._getColGroup = function(n) {
                var r;
                if (2 > n) return this.colYs;
                for (var i = [], u = this.cols + 1 - n, t = 0; u > t; t++) r = this.colYs.slice(t, t + n), i[t] = Math.max.apply(Math, r);
                return i
            }, r.prototype._manageStamp = function(n) {
                var e = t(n),
                    u = this._getElementOffset(n),
                    o = this.options.isOriginLeft ? u.left : u.right,
                    s = o + e.outerWidth,
                    f = Math.floor(o / this.columnWidth),
                    i, h, r;
                for (f = Math.max(0, f), i = Math.floor(s / this.columnWidth), i -= s % this.columnWidth ? 0 : 1, i = Math.min(this.cols - 1, i), h = (this.options.isOriginTop ? u.top : u.bottom) + e.outerHeight, r = f; i >= r; r++) this.colYs[r] = Math.max(h, this.colYs[r])
            }, r.prototype._getContainerSize = function() {
                this.maxY = Math.max.apply(Math, this.colYs);
                var n = {
                    height: this.maxY
                };
                return this.options.isFitWidth && (n.width = this._getContainerFitWidth()), n
            }, r.prototype._getContainerFitWidth = function() {
                for (var n = 0, t = this.cols; --t && 0 === this.colYs[t];) n++;
                return (this.cols - n) * this.columnWidth - this.gutter
            }, r.prototype.needsResizeLayout = function() {
                var n = this.containerWidth;
                return this.getContainerWidth(), n !== this.containerWidth
            }, r
        }
        var i = Array.prototype.indexOf ? function(n, t) {
            return n.indexOf(t)
        } : function(n, t) {
            for (var u, i = 0, r = n.length; r > i; i++)
                if (u = n[i], u === t) return i;
            return -1
        };
        "function" == typeof define && define.amd ? define(["outlayer/outlayer", "get-size/get-size"], t) : "object" == typeof exports ? module.exports = t(require("outlayer"), require("get-size")) : n.Masonry = t(n.Outlayer, n.getSize)
    }(window),
    function(n) {
        "function" == typeof define && define.amd ? define(["jquery", "moment"], n) : "object" == typeof exports ? module.exports = n(require("jquery"), require("moment")) : n(jQuery, moment)
    }(function(n, t) {
        function k() {
            for (var e, t, u, f, i = Array.prototype.slice.call(arguments), o = {}, r = 0; ft.length > r; r++) {
                for (e = ft[r], t = null, u = 0; i.length > u; u++) f = i[u][e], n.isPlainObject(f) ? t = n.extend(t || {}, f) : null != f && (t = null);
                null !== t && (o[e] = t)
            }
            return i.unshift({}), i.push(o), n.extend.apply(n, i)
        }

        function gr(t) {
            var i, r = {
                views: t.views || {}
            };
            return n.each(t, function(t, u) {
                "views" != t && (n.isPlainObject(u) && !/(time|duration|interval)$/i.test(t) && -1 == n.inArray(t, ft) ? (i = null, n.each(u, function(n, u) {
                    /^(month|week|day|default|basic(Week|Day)?|agenda(Week|Day)?)$/.test(n) ? (r.views[n] || (r.views[n] = {}), r.views[n][t] = u) : (i || (i = {}), i[n] = u)
                }), i && (r[t] = i)) : r[t] = u)
            }), r
        }

        function oi(n, t) {
            t.left && n.css({
                "border-left-width": 1,
                "margin-left": t.left - 1
            });
            t.right && n.css({
                "border-right-width": 1,
                "margin-right": t.right - 1
            })
        }

        function si(n) {
            n.css({
                "margin-left": "",
                "margin-right": "",
                "border-left-width": "",
                "border-right-width": ""
            })
        }

        function nt() {
            n("body").addClass("fc-not-allowed")
        }

        function w() {
            n("body").removeClass("fc-not-allowed")
        }

        function hi(t, i, r) {
            var u = Math.floor(i / t.length),
                e = Math.floor(i - u * (t.length - 1)),
                f = [],
                o = [],
                s = [],
                h = 0;
            ci(t);
            t.each(function(i, r) {
                var l = i === t.length - 1 ? e : u,
                    c = n(r).outerHeight(!0);
                l > c ? (f.push(r), o.push(c), s.push(n(r).height())) : h += c
            });
            r && (i -= h, u = Math.floor(i / f.length), e = Math.floor(i - u * (f.length - 1)));
            n(f).each(function(t, i) {
                var r = t === f.length - 1 ? e : u,
                    h = o[t],
                    c = s[t],
                    l = r - (h - c);
                r > h && n(i).height(l)
            })
        }

        function ci(n) {
            n.height("")
        }

        function li(t) {
            var i = 0;
            return t.find("> *").each(function(t, r) {
                var u = n(r).outerWidth();
                u > i && (i = u)
            }), i++, t.width(i), i
        }

        function ai(n, t) {
            return n.height(t).addClass("fc-scroller"), n[0].scrollHeight - 1 > n[0].clientHeight ? !0 : (ht(n), !1)
        }

        function ht(n) {
            n.height("").removeClass("fc-scroller")
        }

        function vi(t) {
            var r = t.css("position"),
                i = t.parents().filter(function() {
                    var t = n(this);
                    return /(auto|scroll)/.test(t.css("overflow") + t.css("overflow-y") + t.css("overflow-x"))
                }).eq(0);
            return "fixed" !== r && i.length ? i : n(t[0].ownerDocument || document)
        }

        function yi(n) {
            var t = n.offset();
            return {
                left: t.left,
                right: t.left + n.outerWidth(),
                top: t.top,
                bottom: t.top + n.outerHeight()
            }
        }

        function pi(n) {
            var t = n.offset(),
                i = tt(n),
                r = t.left + b(n, "border-left-width") + i.left,
                u = t.top + b(n, "border-top-width") + i.top;
            return {
                left: r,
                right: r + n[0].clientWidth,
                top: u,
                bottom: u + n[0].clientHeight
            }
        }

        function nu(n) {
            var t = n.offset(),
                i = t.left + b(n, "border-left-width") + b(n, "padding-left"),
                r = t.top + b(n, "border-top-width") + b(n, "padding-top");
            return {
                left: i,
                right: i + n.width(),
                top: r,
                bottom: r + n.height()
            }
        }

        function tt(n) {
            var i = n.innerWidth() - n[0].clientWidth,
                t = {
                    left: 0,
                    right: 0,
                    top: 0,
                    bottom: n.innerHeight() - n[0].clientHeight
                };
            return tu() && "rtl" == n.css("direction") ? t.left = i : t.right = i, t
        }

        function tu() {
            return null === et && (et = iu()), et
        }

        function iu() {
            var t = n("<div><div/><\/div>").css({
                    position: "absolute",
                    top: -1e3,
                    left: 0,
                    border: 0,
                    padding: 0,
                    overflow: "scroll",
                    direction: "rtl"
                }).appendTo("body"),
                i = t.children(),
                r = i.offset().left > t.offset().left;
            return t.remove(), r
        }

        function b(n, t) {
            return parseFloat(n.css(t)) || 0
        }

        function wi(n) {
            return 1 == n.which && !n.ctrlKey
        }

        function ru(n, t) {
            var i = {
                left: Math.max(n.left, t.left),
                right: Math.min(n.right, t.right),
                top: Math.max(n.top, t.top),
                bottom: Math.min(n.bottom, t.bottom)
            };
            return i.left < i.right && i.top < i.bottom ? i : !1
        }

        function uu(n, t) {
            return {
                left: Math.min(Math.max(n.left, t.left), t.right),
                top: Math.min(Math.max(n.top, t.top), t.bottom)
            }
        }

        function fu(n) {
            return {
                left: (n.left + n.right) / 2,
                top: (n.top + n.bottom) / 2
            }
        }

        function eu(n, t) {
            return {
                left: n.left - t.left,
                top: n.top - t.top
            }
        }

        function ct(n, t) {
            var i, r, u, f, e = n.start,
                o = n.end,
                s = t.start,
                h = t.end;
            if (o > s && h > e) return (e >= s ? (i = e.clone(), u = !0) : (i = s.clone(), u = !1), h >= o ? (r = o.clone(), f = !0) : (r = h.clone(), f = !1), {
                start: i,
                end: r,
                isStart: u,
                isEnd: f
            })
        }

        function bi(n, i) {
            return t.duration({
                days: n.clone().stripTime().diff(i.clone().stripTime(), "days"),
                ms: n.time() - i.time()
            })
        }

        function ou(n, i) {
            return t.duration({
                days: n.clone().stripTime().diff(i.clone().stripTime(), "days")
            })
        }

        function ki(n, i, r) {
            return t.duration(Math.round(n.diff(i, r, !0)), r)
        }

        function lt(n, t) {
            for (var r, u, i = 0; ii.length > i && (r = ii[i], u = su(r, n, t), !(u >= 1 && nr(u))); i++);
            return r
        }

        function su(n, i, r) {
            return null != r ? r.diff(i, n, !0) : t.isDuration(i) ? i.as(n) : i.end.diff(i.start, n, !0)
        }

        function at(n) {
            return Boolean(n.hours() || n.minutes() || n.seconds() || n.milliseconds())
        }

        function hu(n) {
            return "[object Date]" === Object.prototype.toString.call(n) || n instanceof Date
        }

        function di(n) {
            return /^\d+\:\d+(?:\:\d+\.?(?:\d{3})?)?$/.test(n)
        }

        function it(n) {
            var t = function() {};
            return t.prototype = n, new t
        }

        function vt(n, t) {
            for (var i in n) gi(n, i) && (t[i] = n[i])
        }

        function cu(n, t) {
            for (var i, u = ["constructor", "toString", "valueOf"], r = 0; u.length > r; r++) i = u[r], n[i] !== Object.prototype[i] && (t[i] = n[i])
        }

        function gi(n, t) {
            return lf.call(n, t)
        }

        function lu(t) {
            return /undefined|null|boolean|number|string/.test(n.type(t))
        }

        function rt(t, i, r) {
            if (n.isFunction(t) && (t = [t]), t) {
                for (var f, u = 0; t.length > u; u++) f = t[u].apply(i, r) || f;
                return f
            }
        }

        function o() {
            for (var n = 0; arguments.length > n; n++)
                if (void 0 !== arguments[n]) return arguments[n]
        }

        function r(n) {
            return (n + "").replace(/&/g, "&amp;").replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/'/g, "&#039;").replace(/"/g, "&quot;").replace(/\n/g, "<br />")
        }

        function yt(n) {
            return n.replace(/&.*?;/g, "")
        }

        function ut(t) {
            var i = [];
            return n.each(t, function(n, t) {
                null != t && i.push(n + ":" + t)
            }), i.join(";")
        }

        function au(n) {
            return n.charAt(0).toUpperCase() + n.slice(1)
        }

        function vu(n, t) {
            return n - t
        }

        function nr(n) {
            return 0 == n % 1
        }

        function h(n, t) {
            var i = n[t];
            return function() {
                return i.apply(n, arguments)
            }
        }

        function pt(n, t) {
            var i, r, u, f, e = function() {
                var o = +new Date - f;
                t > o && o > 0 ? i = setTimeout(e, t - o) : (i = null, n.apply(u, r), i || (u = r = null))
            };
            return function() {
                u = this;
                r = arguments;
                f = +new Date;
                i || (i = setTimeout(e, t))
            }
        }

        function wt(i, r, u) {
            var o, s, h, f, e = i[0],
                c = 1 == i.length && "string" == typeof e;
            return t.isMoment(e) ? (f = t.apply(null, i), tr(e, f)) : hu(e) || void 0 === e ? f = t.apply(null, i) : (o = !1, s = !1, c ? af.test(e) ? (e += "-01", i = [e], o = !0, s = !0) : (h = vf.exec(e)) && (o = !h[5], s = !0) : n.isArray(e) && (s = !0), f = r || o ? t.utc.apply(t, i) : t.apply(null, i), o ? (f._ambigTime = !0, f._ambigZone = !0) : u && (s ? f._ambigZone = !0 : c && (f.utcOffset ? f.utcOffset(e) : f.zone(e)))), f._fullCalendar = !0, f
        }

        function bt(n, r) {
            for (var u, o = !1, s = !1, h = n.length, e = [], f = 0; h > f; f++) u = n[f], t.isMoment(u) || (u = i.moment.parseZone(u)), o = o || u._ambigTime, s = s || u._ambigZone, e.push(u);
            for (f = 0; h > f; f++) u = e[f], r || !o || u._ambigTime ? s && !u._ambigZone && (e[f] = u.clone().stripZone()) : e[f] = u.clone().stripTime();
            return e
        }

        function tr(n, t) {
            n._ambigTime ? t._ambigTime = !0 : t._ambigTime && (t._ambigTime = !1);
            n._ambigZone ? t._ambigZone = !0 : t._ambigZone && (t._ambigZone = !1)
        }

        function ir(n, t) {
            n.year(t[0] || 0).month(t[1] || 0).date(t[2] || 0).hours(t[3] || 0).minutes(t[4] || 0).seconds(t[5] || 0).milliseconds(t[6] || 0)
        }

        function l(n, t) {
            return f.format.call(n, t)
        }

        function yu(n, t) {
            return rr(n, er(t))
        }

        function rr(n, t) {
            for (var r = "", i = 0; t.length > i; i++) r += kt(n, t[i]);
            return r
        }

        function kt(n, t) {
            var i, r;
            return "string" == typeof t ? t : (i = t.token) ? ri[i] ? ri[i](n) : l(n, i) : t.maybe && (r = rr(n, t.maybe), r.match(/[1-9]/)) ? r : ""
        }

        function ur(n, t, r, u, f) {
            var e;
            return n = i.moment.parseZone(n), t = i.moment.parseZone(t), e = (n.localeData || n.lang).call(n), r = e.longDateFormat(r) || r, u = u || " - ", pu(n, t, er(r), u, f)
        }

        function pu(n, t, i, r, u) {
            for (var f, o, s, a = "", l = "", h = "", c = "", v = "", e = 0; i.length > e && (f = fr(n, t, i[e]), f !== !1); e++) a += f;
            for (o = i.length - 1; o > e && (f = fr(n, t, i[o]), f !== !1); o--) l = f + l;
            for (s = e; o >= s; s++) h += kt(n, i[s]), c += kt(t, i[s]);
            return (h || c) && (v = u ? c + r + h : h + r + c), a + v + l
        }

        function fr(n, t, i) {
            var r, u;
            return "string" == typeof i ? i : (r = i.token) && (u = wr[r.charAt(0)], u && n.isSame(t, u)) ? l(n, r) : !1
        }

        function er(n) {
            return n in ot ? ot[n] : ot[n] = or(n)
        }

        function or(n) {
            for (var t, i = [], r = /\[([^\]]*)\]|\(([^\)]*)\)|(LTS|LT|(\w)\4*o?)|([^\w\[\(]+)/g; t = r.exec(n);) t[1] ? i.push(t[1]) : t[2] ? i.push({
                maybe: or(t[2])
            }) : t[3] ? i.push({
                token: t[3]
            }) : t[5] && i.push(t[5]);
            return i
        }

        function s() {}

        function sr(n, t) {
            return n || t ? n && t ? n.grid === t.grid && n.row === t.row && n.col === t.col : !1 : !0
        }

        function wu(n) {
            var t = hr(n);
            return "background" === t || "inverse-background" === t
        }

        function bu(n) {
            return "inverse-background" === hr(n)
        }

        function hr(n) {
            return o((n.source || {}).rendering, n.rendering)
        }

        function ku(n) {
            for (var i, r = {}, t = 0; n.length > t; t++) i = n[t], (r[i._id] || (r[i._id] = [])).push(i);
            return r
        }

        function du(n, t) {
            return n.eventStartMS - t.eventStartMS
        }

        function d(n, t) {
            return n.eventStartMS - t.eventStartMS || t.eventDurationMS - n.eventDurationMS || t.event.allDay - n.event.allDay || (n.event.title || "").localeCompare(t.event.title)
        }

        function gu(r) {
            var u, f, e, s, o = i.dataAttrPrefix;
            return o && (o += "-"), u = r.data(o + "event") || null, u && (u = "object" == typeof u ? n.extend({}, u) : {}, f = u.start, null == f && (f = u.time), e = u.duration, s = u.stick, delete u.start, delete u.time, delete u.duration, delete u.stick), null == f && (f = r.data(o + "start")), null == f && (f = r.data(o + "time")), null == e && (e = r.data(o + "duration")), null == s && (s = r.data(o + "stick")), f = null != f ? t.duration(f) : null, e = null != e ? t.duration(e) : null, s = Boolean(s), {
                eventProps: u,
                startTime: f,
                duration: e,
                stick: s
            }
        }

        function nf(n, t) {
            for (var r, i = 0; t.length > i; i++)
                if (r = t[i], r.leftCol <= n.rightCol && r.rightCol >= n.leftCol) return !0;
            return !1
        }

        function tf(n, t) {
            return n.leftCol - t.leftCol
        }

        function rf(n) {
            var r, i, t;
            if (n.sort(d), r = uf(n), ff(r), i = r[0]) {
                for (t = 0; i.length > t; t++) cr(i[t]);
                for (t = 0; i.length > t; t++) dt(i[t], 0, 0)
            }
        }

        function uf(n) {
            for (var u, t, i = [], r = 0; n.length > r; r++) {
                for (u = n[r], t = 0; i.length > t && lr(u, i[t]).length; t++);
                u.level = t;
                (i[t] || (i[t] = [])).push(u)
            }
            return i
        }

        function ff(n) {
            for (var f, i, r, u, t = 0; n.length > t; t++)
                for (f = n[t], i = 0; f.length > i; i++)
                    for (r = f[i], r.forwardSegs = [], u = t + 1; n.length > u; u++) lr(r, n[u], r.forwardSegs)
        }

        function cr(n) {
            var t, i, u = n.forwardSegs,
                r = 0;
            if (void 0 === n.forwardPressure) {
                for (t = 0; u.length > t; t++) i = u[t], cr(i), r = Math.max(r, 1 + i.forwardPressure);
                n.forwardPressure = r
            }
        }

        function dt(n, t, i) {
            var u, r = n.forwardSegs;
            if (void 0 === n.forwardCoord)
                for (r.length ? (r.sort( of ), dt(r[0], t + 1, i), n.forwardCoord = r[0].backwardCoord) : n.forwardCoord = 1, n.backwardCoord = n.forwardCoord - (n.forwardCoord - i) / (t + 1), u = 0; r.length > u; u++) dt(r[u], 0, n.forwardCoord)
        }

        function lr(n, t, i) {
            i = i || [];
            for (var r = 0; t.length > r; r++) ef(n, t[r]) && i.push(t[r]);
            return i
        }

        function ef(n, t) {
            return n.bottom > t.top && n.top < t.bottom
        }

        function of (n, t) {
            return t.forwardPressure - n.forwardPressure || (n.backwardCoord || 0) - (t.backwardCoord || 0) || d(n, t)
        }

        function sf(r, u) {
            function ht() {
                a ? v() && (rt(), h()) : ct()
            }

            function ct() {
                pi = e.theme ? "ui" : "fc";
                r.addClass("fc");
                e.isRTL ? r.addClass("fc-rtl") : r.addClass("fc-ltr");
                e.theme ? r.addClass("ui-widget") : r.addClass("fc-unthemed");
                a = n("<div class='fc-view-container'/>").prependTo(r);
                l = f.header = new hf(f, e);
                y = l.render();
                y && r.prepend(y);
                h(e.defaultView);
                e.handleWindowResize && (d = pt(at, e.windowResizeDelay), n(window).resize(d))
            }

            function lt() {
                o && o.removeElement();
                l.destroy();
                a.remove();
                r.removeClass("fc fc-ltr fc-rtl fc-unthemed ui-widget");
                d && n(window).unbind("resize", d)
            }

            function v() {
                return r.is(":visible")
            }

            function h(t) {
                p++;
                o && t && o.type !== t && (l.deactivateButton(o.type), b(), o.removeElement(), o = f.view = null);
                !o && t && (o = f.view = ot[t] || (ot[t] = f.instantiateView(t)), o.setElement(n("<div class='fc-view fc-" + t + "-view' />").appendTo(a)), l.activateButton(t));
                o && (s = o.massageCurrentDate(s), o.isDisplayed && s.isWithin(o.intervalStart, o.intervalEnd) || v() && (b(), o.display(s), k(), dt(), gt(), wt()));
                k();
                p--
            }

            function tt(n) {
                if (v()) return (n && ut(), p++, o.updateSize(!0), p--, !0)
            }

            function rt() {
                v() && ut()
            }

            function ut() {
                nt = "number" == typeof e.contentHeight ? e.contentHeight : "number" == typeof e.height ? e.height - (y ? y.outerHeight(!0) : 0) : Math.round(a.width() / Math.max(e.aspectRatio, .5))
            }

            function at(n) {
                !p && n.target === window && o.start && tt(!0) && o.trigger("windowResize", et)
            }

            function vt() {
                yt();
                ft()
            }

            function w() {
                v() && (b(), o.displayEvents(st), k())
            }

            function yt() {
                b();
                o.clearEvents();
                k()
            }

            function wt() {
                !e.lazyFetching || wi(o.start, o.end) ? ft() : w()
            }

            function ft() {
                bi(o.start, o.end)
            }

            function bt(n) {
                st = n;
                w()
            }

            function kt() {
                w()
            }

            function dt() {
                l.updateTitle(o.title)
            }

            function gt() {
                var n = f.getNow();
                n.isWithin(o.intervalStart, o.intervalEnd) ? l.disableButton("today") : l.enableButton("today")
            }

            function ni(n, t) {
                n = f.moment(n);
                t = t ? f.moment(t) : n.hasTime() ? n.clone().add(f.defaultTimedEventDuration) : n.clone().add(f.defaultAllDayEventDuration);
                o.select({
                    start: n,
                    end: t
                })
            }

            function ti() {
                o && o.unselect()
            }

            function ii() {
                s = o.computePrevDate(s);
                h()
            }

            function ri() {
                s = o.computeNextDate(s);
                h()
            }

            function ui() {
                s.add(-1, "years");
                h()
            }

            function fi() {
                s.add(1, "years");
                h()
            }

            function ei() {
                s = f.getNow();
                h()
            }

            function oi(n) {
                s = f.moment(n);
                h()
            }

            function si(n) {
                s.add(t.duration(n));
                h()
            }

            function hi(n, t) {
                var i;
                t = t || "day";
                i = f.getViewSpec(t) || f.getUnitViewSpec(t);
                s = n;
                h(i ? i.type : null)
            }

            function ci() {
                return s.clone()
            }

            function b() {
                a.css({
                    width: "100%",
                    height: a.height(),
                    overflow: "hidden"
                })
            }

            function k() {
                a.css({
                    width: "",
                    height: "",
                    overflow: ""
                })
            }

            function li() {
                return f
            }

            function ai() {
                return o
            }

            function vi(n, t) {
                return void 0 === t ? e[n] : (("height" == n || "contentHeight" == n || "aspectRatio" == n) && (e[n] = t, tt(!0)), void 0)
            }

            function yi(n, t) {
                if (e[n]) return e[n].apply(t || et, Array.prototype.slice.call(arguments, 2))
            }
            var f = this,
                e, c, g;
            f.initOptions(u || {});
            e = this.options;
            f.render = ht;
            f.destroy = lt;
            f.refetchEvents = vt;
            f.reportEvents = bt;
            f.reportEventChange = kt;
            f.rerenderEvents = w;
            f.changeView = h;
            f.select = ni;
            f.unselect = ti;
            f.prev = ii;
            f.next = ri;
            f.prevYear = ui;
            f.nextYear = fi;
            f.today = ei;
            f.gotoDate = oi;
            f.incrementDate = si;
            f.zoomTo = hi;
            f.getDate = ci;
            f.getCalendar = li;
            f.getView = ai;
            f.option = vi;
            f.trigger = yi;
            c = it(vr(e.lang));
            (e.monthNames && (c._months = e.monthNames), e.monthNamesShort && (c._monthsShort = e.monthNamesShort), e.dayNames && (c._weekdays = e.dayNames), e.dayNamesShort && (c._weekdaysShort = e.dayNamesShort), null != e.firstDay) && (g = it(c._week), g.dow = e.firstDay, c._week = g);
            c._fullCalendar_weekCalc = function(n) {
                return "function" == typeof n ? n : "local" === n ? n : "iso" === n || "ISO" === n ? "ISO" : void 0
            }(e.weekNumberCalculation);
            f.defaultAllDayEventDuration = t.duration(e.defaultAllDayEventDuration);
            f.defaultTimedEventDuration = t.duration(e.defaultTimedEventDuration);
            f.moment = function() {
                var n;
                return "local" === e.timezone ? (n = i.moment.apply(null, arguments), n.hasTime() && n.local()) : n = "UTC" === e.timezone ? i.moment.utc.apply(null, arguments) : i.moment.parseZone.apply(null, arguments), "_locale" in n ? n._locale = c : n._lang = c, n
            };
            f.getIsAmbigTimezone = function() {
                return "local" !== e.timezone && "UTC" !== e.timezone
            };
            f.rezoneDate = function(n) {
                return f.moment(n.toArray())
            };
            f.getNow = function() {
                var n = e.now;
                return "function" == typeof n && (n = n()), f.moment(n)
            };
            f.getEventEnd = function(n) {
                return n.end ? n.end.clone() : f.getDefaultEventEnd(n.allDay, n.start)
            };
            f.getDefaultEventEnd = function(n, t) {
                var i = t.clone();
                return n ? i.stripTime().add(f.defaultAllDayEventDuration) : i.add(f.defaultTimedEventDuration), f.getIsAmbigTimezone() && i.stripZone(), i
            };
            f.humanizeDuration = function(n) {
                return (n.locale || n.lang).call(n, e.lang).humanize()
            };
            cf.call(f, e);
            var l, y, a, pi, o, nt, d, s, wi = f.isFetchNeeded,
                bi = f.fetchEvents,
                et = r[0],
                ot = {},
                p = 0,
                st = [];
            s = null != e.defaultDate ? f.moment(e.defaultDate) : f.getNow();
            f.getSuggestedViewHeight = function() {
                return void 0 === nt && rt(), nt
            };
            f.isHeightAuto = function() {
                return "auto" === e.contentHeight || "auto" === e.height
            }
        }

        function ar(t) {
            n.each(gf, function(n, i) {
                null == t[n] && (t[n] = i(t))
            })
        }

        function vr(n) {
            var i = t.localeData || t.langData;
            return i.call(t, n) || i.call(t, "en")
        }

        function hf(t, i) {
            function h() {
                var t = i.header;
                return u = i.theme ? "ui" : "fc", t ? e = n("<div class='fc-toolbar'/>").append(o("left")).append(o("right")).append(o("center")).append('<div class="fc-clear"/>') : void 0
            }

            function c() {
                e.remove()
            }

            function o(f) {
                var e = n('<div class="fc-' + f + '"/>'),
                    o = i.header[f];
                return o && n.each(o.split(" "), function() {
                    var o, f = n(),
                        h = !0;
                    n.each(this.split(","), function(e, o) {
                        var l, a, v, y, p, w, b, k, c;
                        "title" == o ? (f = f.add(n("<h2>&nbsp;<\/h2>")), h = !1) : (l = t.getViewSpec(o), l ? (a = function() {
                            t.changeView(o)
                        }, s.push(o), v = l.buttonTextOverride, y = l.buttonTextDefault) : t[o] && (a = function() {
                            t[o]()
                        }, v = (t.overrides.buttonText || {})[o], y = i.buttonText[o]), a && (p = i.themeButtonIcons[o], w = i.buttonIcons[o], b = v ? r(v) : p && i.theme ? "<span class='ui-icon ui-icon-" + p + "'><\/span>" : w && !i.theme ? "<span class='fc-icon fc-icon-" + w + "'><\/span>" : r(y), k = ["fc-" + o + "-button", u + "-button", u + "-state-default"], c = n('<button type="button" class="' + k.join(" ") + '">' + b + "<\/button>").click(function() {
                            c.hasClass(u + "-state-disabled") || (a(), (c.hasClass(u + "-state-active") || c.hasClass(u + "-state-disabled")) && c.removeClass(u + "-state-hover"))
                        }).mousedown(function() {
                            c.not("." + u + "-state-active").not("." + u + "-state-disabled").addClass(u + "-state-down")
                        }).mouseup(function() {
                            c.removeClass(u + "-state-down")
                        }).hover(function() {
                            c.not("." + u + "-state-active").not("." + u + "-state-disabled").addClass(u + "-state-hover")
                        }, function() {
                            c.removeClass(u + "-state-hover").removeClass(u + "-state-down")
                        }), f = f.add(c)))
                    });
                    h && f.first().addClass(u + "-corner-left").end().last().addClass(u + "-corner-right").end();
                    f.length > 1 ? (o = n("<div/>"), h && o.addClass("fc-button-group"), o.append(f), e.append(o)) : e.append(f)
                }), e
            }

            function l(n) {
                e.find("h2").text(n)
            }

            function a(n) {
                e.find(".fc-" + n + "-button").addClass(u + "-state-active")
            }

            function v(n) {
                e.find(".fc-" + n + "-button").removeClass(u + "-state-active")
            }

            function y(n) {
                e.find(".fc-" + n + "-button").attr("disabled", "disabled").addClass(u + "-state-disabled")
            }

            function p(n) {
                e.find(".fc-" + n + "-button").removeAttr("disabled").removeClass(u + "-state-disabled")
            }

            function w() {
                return s
            }
            var f = this,
                u, e, s;
            f.render = h;
            f.destroy = c;
            f.updateTitle = l;
            f.activateButton = a;
            f.deactivateButton = v;
            f.disableButton = y;
            f.enableButton = p;
            f.getViewsWithButtons = w;
            e = n();
            s = []
        }

        function cf(r) {
            function bt(n, t) {
                return !s || n.clone().stripZone() < s.clone().stripZone() || t.clone().stripZone() > c.clone().stripZone()
            }

            function kt(n, t) {
                var u, r, i;
                for (s = n, c = t, f = [], u = ++k, r = e.length, y = r, i = 0; r > i; i++) d(e[i], u)
            }

            function d(t, i) {
                g(t, function(r) {
                    var u, e, o, s = n.isArray(t.events);
                    if (i == k) {
                        if (r)
                            for (u = 0; r.length > u; u++) e = r[u], o = s ? e : h(e, t), o && f.push.apply(f, a(o));
                        y--;
                        y || v(f)
                    }
                })
            }

            function g(t, f) {
                for (var l, v = i.sourceFetchers, e, y, h = 0; v.length > h; h++) {
                    if (l = v[h].call(u, t, s.clone(), c.clone(), r.timezone, f), l === !0) return;
                    if ("object" == typeof l) return g(l, f), void 0
                }
                if (e = t.events, e) n.isFunction(e) ? (ft(), e.call(u, s.clone(), c.clone(), r.timezone, function(n) {
                    f(n);
                    et()
                })) : n.isArray(e) ? f(e) : f();
                else if (y = t.url, y) {
                    var p, k = t.success,
                        d = t.error,
                        nt = t.complete;
                    p = n.isFunction(t.data) ? t.data() : t.data;
                    var a = n.extend({}, p || {}),
                        w = o(t.startParam, r.startParam),
                        b = o(t.endParam, r.endParam),
                        tt = o(t.timezoneParam, r.timezoneParam);
                    w && (a[w] = s.format());
                    b && (a[b] = c.format());
                    r.timezone && "local" != r.timezone && (a[tt] = r.timezone);
                    ft();
                    n.ajax(n.extend({}, kr, t, {
                        data: a,
                        success: function(t) {
                            t = t || [];
                            var i = rt(k, this, arguments);
                            n.isArray(i) && (t = i);
                            f(t)
                        },
                        error: function() {
                            rt(d, this, arguments);
                            f()
                        },
                        complete: function() {
                            rt(nt, this, arguments);
                            et()
                        }
                    }))
                } else f()
            }

            function dt(n) {
                var t = nt(n);
                t && (e.push(t), y++, d(t, k))
            }

            function nt(t) {
                var r, f, e = i.sourceNormalizers;
                if (n.isFunction(t) || n.isArray(t) ? r = {
                        events: t
                    } : "string" == typeof t ? r = {
                        url: t
                    } : "object" == typeof t && (r = n.extend({}, t)), r) {
                    for (r.className ? "string" == typeof r.className && (r.className = r.className.split(/\s+/)) : r.className = [], n.isArray(r.events) && (r.origArray = r.events, r.events = n.map(r.events, function(n) {
                            return h(n, r)
                        })), f = 0; e.length > f; f++) e[f].call(u, r);
                    return r
                }
            }

            function ni(t) {
                e = n.grep(e, function(n) {
                    return !tt(n, t)
                });
                f = n.grep(f, function(n) {
                    return !tt(n.source, t)
                });
                v(f)
            }

            function tt(n, t) {
                return n && t && it(n) == it(t)
            }

            function it(n) {
                return ("object" == typeof n ? n.origArray || n.googleCalendarId || n.url || n.events : null) || n
            }

            function ti(n) {
                n.start = u.moment(n.start);
                n.end = n.end ? u.moment(n.end) : null;
                ht(n, ii(n));
                v(f)
            }

            function ii(t) {
                var i = {};
                return n.each(t, function(n, t) {
                    ut(n) && void 0 !== t && lu(t) && (i[n] = t)
                }), i
            }

            function ut(n) {
                return !/^_|^(id|allDay|start|end)$/.test(n)
            }

            function ri(n, t) {
                var r, u, i, e = h(n);
                if (e) {
                    for (r = a(e), u = 0; r.length > u; u++) i = r[u], i.source || (t && (b.events.push(i), i.source = b), f.push(i));
                    return v(f), r
                }
                return []
            }

            function ui(t) {
                var r, i;
                for (null == t ? t = function() {
                        return !0
                    } : n.isFunction(t) || (r = t + "", t = function(n) {
                        return n._id == r
                    }), f = n.grep(f, t, !0), i = 0; e.length > i; i++) n.isArray(e[i].events) && (e[i].events = n.grep(e[i].events, t, !0));
                v(f)
            }

            function p(t) {
                return n.isFunction(t) ? n.grep(f, t) : null != t ? (t += "", n.grep(f, function(n) {
                    return n._id == t
                })) : f
            }

            function ft() {
                wt++ || yt("loading", null, !0, pt())
            }

            function et() {
                --wt || yt("loading", null, !1, pt())
            }

            function h(i, f) {
                var s, e, c, h = {};
                if (r.eventDataTransform && (i = r.eventDataTransform(i)), f && f.eventDataTransform && (i = f.eventDataTransform(i)), n.extend(h, i), f && (h.source = f), h._id = i._id || (void 0 === i.id ? "_fc" + dr++ : i.id + ""), h.className = i.className ? "string" == typeof i.className ? i.className.split(/\s+/) : i.className : [], s = i.start || i.date, e = i.end, di(s) && (s = t.duration(s)), di(e) && (e = t.duration(e)), i.dow || t.isDuration(s) || t.isDuration(e)) h.start = s ? t.duration(s) : null, h.end = e ? t.duration(e) : null, h._recurring = !0;
                else {
                    if (s && (s = u.moment(s), !s.isValid())) return !1;
                    e && (e = u.moment(e), e.isValid() || (e = null));
                    c = i.allDay;
                    void 0 === c && (c = o(f ? f.allDayDefault : void 0, r.allDayDefault));
                    ot(s, e, c, h)
                }
                return h
            }

            function ot(n, t, i, r) {
                r.start = n;
                r.end = t;
                r.allDay = i;
                l(r);
                gt(r)
            }

            function l(n) {
                st(n);
                n.end && !n.end.isAfter(n.start) && (n.end = null);
                n.end || (n.end = r.forceEventDuration ? u.getDefaultEventEnd(n.allDay, n.start) : null)
            }

            function st(n) {
                null == n.allDay && (n.allDay = !(n.start.hasTime() || n.end && n.end.hasTime()));
                n.allDay ? (n.start.stripTime(), n.end && n.end.stripTime()) : (n.start.hasTime() || (n.start = u.rezoneDate(n.start)), n.end && !n.end.hasTime() && (n.end = u.rezoneDate(n.end)))
            }

            function w(t) {
                var i;
                return t.end || (i = t.allDay, null == i && (i = !t.start.hasTime()), t = n.extend({}, t), t.end = u.getDefaultEventEnd(i, t.start)), t
            }

            function a(t, i, r) {
                var f, a, e, u, o, h, l, v, y, p = [];
                if (i = i || s, r = r || c, t)
                    if (t._recurring) {
                        if (a = t.dow)
                            for (f = {}, e = 0; a.length > e; e++) f[a[e]] = !0;
                        for (u = i.clone().stripTime(); u.isBefore(r);)(!f || f[u.day()]) && (o = t.start, h = t.end, l = u.clone(), v = null, o && (l = l.time(o)), h && (v = u.clone().time(h)), y = n.extend({}, t), ot(l, v, !o && !h, y), p.push(y)), u.add(1, "days")
                    } else p.push(t);
                return p
            }

            function ht(t, i, r) {
                function s(n, t) {
                    return r ? ki(n, t, r) : i.allDay ? ou(n, t) : bi(n, t)
                }
                var f, h, e, c, o, a, v = {};
                return i = i || {}, i.start || (i.start = t.start.clone()), void 0 === i.end && (i.end = t.end ? t.end.clone() : null), null == i.allDay && (i.allDay = t.allDay), l(i), f = {
                    start: t._start.clone(),
                    end: t._end ? t._end.clone() : u.getDefaultEventEnd(t._allDay, t._start),
                    allDay: i.allDay
                }, l(f), h = null !== t._end && null === i.end, e = s(i.start, f.start), i.end ? (c = s(i.end, f.end), o = c.subtract(e)) : o = null, n.each(i, function(n, t) {
                    ut(n) && void 0 !== t && (v[n] = t)
                }), a = fi(p(t._id), h, i.allDay, e, o, v), {
                    dateDelta: e,
                    durationDelta: o,
                    undo: a
                }
            }

            function fi(t, i, r, f, e, o) {
                var h = u.getIsAmbigTimezone(),
                    s = [];
                return f && !f.valueOf() && (f = null), e && !e.valueOf() && (e = null), n.each(t, function(t, c) {
                        var v, a;
                        v = {
                            start: c.start.clone(),
                            end: c.end ? c.end.clone() : null,
                            allDay: c.allDay
                        };
                        n.each(o, function(n) {
                            v[n] = c[n]
                        });
                        a = {
                            start: c._start,
                            end: c._end,
                            allDay: r
                        };
                        l(a);
                        i ? a.end = null : e && !a.end && (a.end = u.getDefaultEventEnd(a.allDay, a.start));
                        f && (a.start.add(f), a.end && a.end.add(f));
                        e && a.end.add(e);
                        h && !a.allDay && (f || e) && (a.start.stripZone(), a.end && a.end.stripZone());
                        n.extend(c, o, a);
                        gt(c);
                        s.push(function() {
                            n.extend(c, v);
                            gt(c)
                        })
                    }),
                    function() {
                        for (var n = 0; s.length > n; n++) s[n]()
                    }
            }

            function ct(t) {
                var i, f = r.businessHours,
                    e = u.getView();
                return f && (i = n.extend({}, {
                    className: "fc-nonbusiness",
                    start: "09:00",
                    end: "17:00",
                    dow: [1, 2, 3, 4, 5],
                    rendering: "inverse-background"
                }, "object" == typeof f ? f : {})), i ? (t && (i.start = null, i.end = null), a(h(i), e.start, e.end)) : []
            }

            function lt(n, t) {
                var i = t.source || {},
                    u = o(t.constraint, i.constraint, r.eventConstraint),
                    f = o(t.overlap, i.overlap, r.eventOverlap);
                return n = w(n), vt(n, u, f, t)
            }

            function at(n) {
                return vt(n, r.selectConstraint, r.selectOverlap)
            }

            function ei(t, i) {
                var u, r;
                return i && (u = n.extend({}, i, t), r = a(h(u))[0]), r ? lt(t, r) : (t = w(t), at(t))
            }

            function vt(t, i, r, f) {
                var c, l, a, e, s, h;
                if (t = n.extend({}, t), t.start = t.start.clone().stripZone(), t.end = t.end.clone().stripZone(), null != i) {
                    for (c = oi(i), l = !1, e = 0; c.length > e; e++)
                        if (si(c[e], t)) {
                            l = !0;
                            break
                        }
                    if (!l) return !1
                }
                for (a = u.getPeerEvents(f, t), e = 0; a.length > e; e++)
                    if ((s = a[e], hi(s, t)) && (r === !1 || "function" == typeof r && !r(s, f) || f && ((h = o(s.overlap, (s.source || {}).overlap), h === !1) || "function" == typeof h && !h(f, s)))) return !1;
                return !0
            }

            function oi(n) {
                return "businessHours" === n ? ct() : "object" == typeof n ? a(h(n)) : p(n)
            }

            function si(n, t) {
                var i = n.start.clone().stripZone(),
                    r = u.getEventEnd(n).stripZone();
                return t.start >= i && r >= t.end
            }

            function hi(n, t) {
                var i = n.start.clone().stripZone(),
                    r = u.getEventEnd(n).stripZone();
                return r > t.start && t.end > i
            }
            var u = this;
            u.isFetchNeeded = bt;
            u.fetchEvents = kt;
            u.addEventSource = dt;
            u.removeEventSource = ni;
            u.updateEvent = ti;
            u.renderEvent = ri;
            u.removeEvents = ui;
            u.clientEvents = p;
            u.mutateEvent = ht;
            u.normalizeEventRange = l;
            u.normalizeEventRangeTimes = st;
            u.ensureVisibleEventRange = w;
            var s, c, yt = u.trigger,
                pt = u.getView,
                v = u.reportEvents,
                b = {
                    events: []
                },
                e = [b],
                k = 0,
                y = 0,
                wt = 0,
                f = [];
            n.each((r.events ? [r.events] : []).concat(r.eventSources || []), function(n, t) {
                var i = nt(t);
                i && e.push(i)
            });
            u.getBusinessHoursEvents = ct;
            u.isEventRangeAllowed = lt;
            u.isSelectionRangeAllowed = at;
            u.isExternalDropRangeAllowed = ei;
            u.getEventCache = function() {
                return f
            }
        }

        function gt(n) {
            n._allDay = n.allDay;
            n._start = n.start.clone();
            n._end = n.end ? n.end.clone() : null
        }
        var i = n.fullCalendar = {
                version: "2.3.1"
            },
            a = i.views = {},
            ft, et, ri, wr, ot, g, ui, p, e, y, kr, dr, fi, ei;
        n.fn.fullCalendar = function(t) {
            var u = Array.prototype.slice.call(arguments, 1),
                r = this;
            return this.each(function(f, e) {
                var h, s = n(e),
                    o = s.data("fullCalendar");
                "string" == typeof t ? o && n.isFunction(o[t]) && (h = o[t].apply(o, u), f || (r = h), "destroy" === t && s.removeData("fullCalendar")) : o || (o = new i.CalendarBase(s, t), s.data("fullCalendar", o), o.render())
            }), r
        };
        ft = ["header", "buttonText", "buttonIcons", "themeButtonIcons"];
        i.intersectionToSeg = ct;
        i.applyAll = rt;
        i.debounce = pt;
        i.isInt = nr;
        i.htmlEscape = r;
        i.cssToStr = ut;
        i.proxy = h;
        i.getClientRect = pi;
        i.getContentRect = nu;
        i.getScrollbarWidths = tt;
        et = null;
        i.computeIntervalUnit = lt;
        i.durationHasTime = at;
        var ni, ti, yr, pr = ["sun", "mon", "tue", "wed", "thu", "fri", "sat"],
            ii = ["year", "month", "week", "day", "hour", "minute", "second", "millisecond"],
            lf = {}.hasOwnProperty,
            af = /^\s*\d{4}-\d\d$/,
            vf = /^\s*\d{4}-(?:(\d\d-\d\d)|(W\d\d$)|(W\d\d-\d)|(\d\d\d))((T| )(\d\d(:\d\d(:\d\d(\.\d+)?)?)?)?)?$/,
            u = t.fn,
            f = n.extend({}, u);
        i.moment = function() {
            return wt(arguments)
        };
        i.moment.utc = function() {
            var n = wt(arguments, !0);
            return n.hasTime() && n.utc(), n
        };
        i.moment.parseZone = function() {
            return wt(arguments, !0, !0)
        };
        u.clone = function() {
            var n = f.clone.apply(this, arguments);
            return tr(this, n), this._fullCalendar && (n._fullCalendar = !0), n
        };
        u.week = u.weeks = function(n) {
            var t = (this._locale || this._lang)._fullCalendar_weekCalc;
            return null == n && "function" == typeof t ? t(this) : "ISO" === t ? f.isoWeek.apply(this, arguments) : f.week.apply(this, arguments)
        };
        u.time = function(n) {
            if (!this._fullCalendar) return f.time.apply(this, arguments);
            if (null == n) return t.duration({
                hours: this.hours(),
                minutes: this.minutes(),
                seconds: this.seconds(),
                milliseconds: this.milliseconds()
            });
            this._ambigTime = !1;
            t.isDuration(n) || t.isMoment(n) || (n = t.duration(n));
            var i = 0;
            return t.isDuration(n) && (i = 24 * Math.floor(n.asDays())), this.hours(i + n.hours()).minutes(n.minutes()).seconds(n.seconds()).milliseconds(n.milliseconds())
        };
        u.stripTime = function() {
            var n;
            return this._ambigTime || (n = this.toArray(), this.utc(), ti(this, n.slice(0, 3)), this._ambigTime = !0, this._ambigZone = !0), this
        };
        u.hasTime = function() {
            return !this._ambigTime
        };
        u.stripZone = function() {
            var n, t;
            return this._ambigZone || (n = this.toArray(), t = this._ambigTime, this.utc(), ti(this, n), this._ambigTime = t || !1, this._ambigZone = !0), this
        };
        u.hasZone = function() {
            return !this._ambigZone
        };
        u.local = function() {
            var n = this.toArray(),
                t = this._ambigZone;
            return f.local.apply(this, arguments), this._ambigTime = !1, this._ambigZone = !1, t && yr(this, n), this
        };
        u.utc = function() {
            return f.utc.apply(this, arguments), this._ambigTime = !1, this._ambigZone = !1, this
        };
        n.each(["zone", "utcOffset"], function(n, t) {
            f[t] && (u[t] = function(n) {
                return null != n && (this._ambigTime = !1, this._ambigZone = !1), f[t].apply(this, arguments)
            })
        });
        u.format = function() {
            return this._fullCalendar && arguments[0] ? yu(this, arguments[0]) : this._ambigTime ? l(this, "YYYY-MM-DD") : this._ambigZone ? l(this, "YYYY-MM-DD[T]HH:mm:ss") : f.format.apply(this, arguments)
        };
        u.toISOString = function() {
            return this._ambigTime ? l(this, "YYYY-MM-DD") : this._ambigZone ? l(this, "YYYY-MM-DD[T]HH:mm:ss") : f.toISOString.apply(this, arguments)
        };
        u.isWithin = function(n, t) {
            var i = bt([this, n, t]);
            return i[0] >= i[1] && i[0] < i[2]
        };
        u.isSame = function(n, t) {
            var r;
            return this._fullCalendar ? t ? (r = bt([this, n], !0), f.isSame.call(r[0], r[1], t)) : (n = i.moment.parseZone(n), f.isSame.call(this, n) && Boolean(this._ambigTime) === Boolean(n._ambigTime) && Boolean(this._ambigZone) === Boolean(n._ambigZone)) : f.isSame.apply(this, arguments)
        };
        n.each(["isBefore", "isAfter"], function(n, t) {
            u[t] = function(n, i) {
                var r;
                return this._fullCalendar ? (r = bt([this, n]), f[t].call(r[0], r[1], i)) : f[t].apply(this, arguments)
            }
        });
        ni = "_d" in t() && "updateOffset" in t;
        ti = ni ? function(n, i) {
            n._d.setTime(Date.UTC.apply(Date, i));
            t.updateOffset(n, !1)
        } : ir;
        yr = ni ? function(n, i) {
            n._d.setTime(+new Date(i[0] || 0, i[1] || 0, i[2] || 0, i[3] || 0, i[4] || 0, i[5] || 0, i[6] || 0));
            t.updateOffset(n, !1)
        } : ir;
        ri = {
            t: function(n) {
                return l(n, "a").charAt(0)
            },
            T: function(n) {
                return l(n, "A").charAt(0)
            }
        };
        i.formatRange = ur;
        wr = {
            Y: "year",
            M: "month",
            D: "day",
            d: "day",
            A: "second",
            a: "second",
            T: "second",
            t: "second",
            H: "second",
            h: "second",
            m: "second",
            s: "second"
        };
        ot = {};
        i.Class = s;
        s.extend = function(n) {
            var t, i = this;
            return n = n || {}, gi(n, "constructor") && (t = n.constructor), "function" != typeof t && (t = n.constructor = function() {
                i.apply(this, arguments)
            }), t.prototype = it(i.prototype), vt(n, t.prototype), cu(n, t.prototype), vt(i, t), t
        };
        s.mixin = function(n) {
            vt(n.prototype || n, this.prototype)
        };
        var yf = s.extend({
                isHidden: !0,
                options: null,
                el: null,
                documentMousedownProxy: null,
                margin: 10,
                constructor: function(n) {
                    this.options = n || {}
                },
                show: function() {
                    this.isHidden && (this.el || this.render(), this.el.show(), this.position(), this.isHidden = !1, this.trigger("show"))
                },
                hide: function() {
                    this.isHidden || (this.el.hide(), this.isHidden = !0, this.trigger("hide"))
                },
                render: function() {
                    var i = this,
                        t = this.options;
                    this.el = n('<div class="fc-popover"/>').addClass(t.className || "").css({
                        top: 0,
                        left: 0
                    }).append(t.content).appendTo(t.parentEl);
                    this.el.on("click", ".fc-close", function() {
                        i.hide()
                    });
                    t.autoHide && n(document).on("mousedown", this.documentMousedownProxy = h(this, "documentMousedown"))
                },
                documentMousedown: function(t) {
                    this.el && !n(t.target).closest(this.el).length && this.hide()
                },
                destroy: function() {
                    this.hide();
                    this.el && (this.el.remove(), this.el = null);
                    n(document).off("mousedown", this.documentMousedownProxy)
                },
                position: function() {
                    var f, e, o, t, i, r = this.options,
                        h = this.el.offsetParent().offset(),
                        c = this.el.outerWidth(),
                        l = this.el.outerHeight(),
                        s = n(window),
                        u = vi(this.el);
                    t = r.top || 0;
                    i = void 0 !== r.left ? r.left : void 0 !== r.right ? r.right - c : 0;
                    u.is(window) || u.is(document) ? (u = s, f = 0, e = 0) : (o = u.offset(), f = o.top, e = o.left);
                    f += s.scrollTop();
                    e += s.scrollLeft();
                    r.viewportConstrain !== !1 && (t = Math.min(t, f + u.outerHeight() - l - this.margin), t = Math.max(t, f + this.margin), i = Math.min(i, e + u.outerWidth() - c - this.margin), i = Math.max(i, e + this.margin));
                    this.el.css({
                        top: t - h.top,
                        left: i - h.left
                    })
                },
                trigger: function(n) {
                    this.options[n] && this.options[n].apply(this, Array.prototype.slice.call(arguments, 1))
                }
            }),
            pf = s.extend({
                grid: null,
                rowCoords: null,
                colCoords: null,
                containerEl: null,
                bounds: null,
                constructor: function(n) {
                    this.grid = n
                },
                build: function() {
                    this.rowCoords = this.grid.computeRowCoords();
                    this.colCoords = this.grid.computeColCoords();
                    this.computeBounds()
                },
                clear: function() {
                    this.rowCoords = null;
                    this.colCoords = null
                },
                getCell: function(t, i) {
                    var r, u, f, s = this.rowCoords,
                        c = s.length,
                        h = this.colCoords,
                        l = h.length,
                        e = null,
                        o = null;
                    if (this.inBounds(t, i)) {
                        for (r = 0; c > r; r++)
                            if (u = s[r], i >= u.top && u.bottom > i) {
                                e = r;
                                break
                            }
                        for (r = 0; l > r; r++)
                            if (u = h[r], t >= u.left && u.right > t) {
                                o = r;
                                break
                            }
                        if (null !== e && null !== o) return f = this.grid.getCell(e, o), f.grid = this.grid, n.extend(f, s[e], h[o]), f
                    }
                    return null
                },
                computeBounds: function() {
                    this.bounds = this.containerEl ? pi(this.containerEl) : null
                },
                inBounds: function(n, t) {
                    var i = this.bounds;
                    return i ? n >= i.left && i.right > n && t >= i.top && i.bottom > t : !0
                }
            }),
            wf = s.extend({
                coordMaps: null,
                constructor: function(n) {
                    this.coordMaps = n
                },
                build: function() {
                    for (var t = this.coordMaps, n = 0; t.length > n; n++) t[n].build()
                },
                getCell: function(n, t) {
                    for (var u = this.coordMaps, r = null, i = 0; u.length > i && !r; i++) r = u[i].getCell(n, t);
                    return r
                },
                clear: function() {
                    for (var t = this.coordMaps, n = 0; t.length > n; n++) t[n].clear()
                }
            }),
            v = i.DragListener = s.extend({
                options: null,
                isListening: !1,
                isDragging: !1,
                originX: null,
                originY: null,
                mousemoveProxy: null,
                mouseupProxy: null,
                subjectEl: null,
                subjectHref: null,
                scrollEl: null,
                scrollBounds: null,
                scrollTopVel: null,
                scrollLeftVel: null,
                scrollIntervalId: null,
                scrollHandlerProxy: null,
                scrollSensitivity: 30,
                scrollSpeed: 200,
                scrollIntervalMs: 50,
                constructor: function(n) {
                    n = n || {};
                    this.options = n;
                    this.subjectEl = n.subjectEl
                },
                mousedown: function(n) {
                    wi(n) && (n.preventDefault(), this.startListening(n), this.options.distance || this.startDrag(n))
                },
                startListening: function(t) {
                    var i;
                    this.isListening || (t && this.options.scroll && (i = vi(n(t.target)), i.is(window) || i.is(document) || (this.scrollEl = i, this.scrollHandlerProxy = pt(h(this, "scrollHandler"), 100), this.scrollEl.on("scroll", this.scrollHandlerProxy))), n(document).on("mousemove", this.mousemoveProxy = h(this, "mousemove")).on("mouseup", this.mouseupProxy = h(this, "mouseup")).on("selectstart", this.preventDefault), t ? (this.originX = t.pageX, this.originY = t.pageY) : (this.originX = 0, this.originY = 0), this.isListening = !0, this.listenStart(t))
                },
                listenStart: function(n) {
                    this.trigger("listenStart", n)
                },
                mousemove: function(n) {
                    var t, u, i = n.pageX - this.originX,
                        r = n.pageY - this.originY;
                    this.isDragging || (t = this.options.distance || 1, u = i * i + r * r, u >= t * t && this.startDrag(n));
                    this.isDragging && this.drag(i, r, n)
                },
                startDrag: function(n) {
                    this.isListening || this.startListening();
                    this.isDragging || (this.isDragging = !0, this.dragStart(n))
                },
                dragStart: function(n) {
                    var t = this.subjectEl;
                    this.trigger("dragStart", n);
                    (this.subjectHref = t ? t.attr("href") : null) && t.removeAttr("href")
                },
                drag: function(n, t, i) {
                    this.trigger("drag", n, t, i);
                    this.updateScroll(i)
                },
                mouseup: function(n) {
                    this.stopListening(n)
                },
                stopDrag: function(n) {
                    this.isDragging && (this.stopScrolling(), this.dragStop(n), this.isDragging = !1)
                },
                dragStop: function(n) {
                    var t = this;
                    this.trigger("dragStop", n);
                    setTimeout(function() {
                        t.subjectHref && t.subjectEl.attr("href", t.subjectHref)
                    }, 0)
                },
                stopListening: function(t) {
                    this.stopDrag(t);
                    this.isListening && (this.scrollEl && (this.scrollEl.off("scroll", this.scrollHandlerProxy), this.scrollHandlerProxy = null), n(document).off("mousemove", this.mousemoveProxy).off("mouseup", this.mouseupProxy).off("selectstart", this.preventDefault), this.mousemoveProxy = null, this.mouseupProxy = null, this.isListening = !1, this.listenStop(t))
                },
                listenStop: function(n) {
                    this.trigger("listenStop", n)
                },
                trigger: function(n) {
                    this.options[n] && this.options[n].apply(this, Array.prototype.slice.call(arguments, 1))
                },
                preventDefault: function(n) {
                    n.preventDefault()
                },
                computeScrollBounds: function() {
                    var n = this.scrollEl;
                    this.scrollBounds = n ? yi(n) : null
                },
                updateScroll: function(n) {
                    var r, u, f, e, t = this.scrollSensitivity,
                        i = this.scrollBounds,
                        o = 0,
                        s = 0;
                    i && (r = (t - (n.pageY - i.top)) / t, u = (t - (i.bottom - n.pageY)) / t, f = (t - (n.pageX - i.left)) / t, e = (t - (i.right - n.pageX)) / t, r >= 0 && 1 >= r ? o = -1 * r * this.scrollSpeed : u >= 0 && 1 >= u && (o = u * this.scrollSpeed), f >= 0 && 1 >= f ? s = -1 * f * this.scrollSpeed : e >= 0 && 1 >= e && (s = e * this.scrollSpeed));
                    this.setScrollVel(o, s)
                },
                setScrollVel: function(n, t) {
                    this.scrollTopVel = n;
                    this.scrollLeftVel = t;
                    this.constrainScrollVel();
                    !this.scrollTopVel && !this.scrollLeftVel || this.scrollIntervalId || (this.scrollIntervalId = setInterval(h(this, "scrollIntervalFunc"), this.scrollIntervalMs))
                },
                constrainScrollVel: function() {
                    var n = this.scrollEl;
                    0 > this.scrollTopVel ? 0 >= n.scrollTop() && (this.scrollTopVel = 0) : this.scrollTopVel > 0 && n.scrollTop() + n[0].clientHeight >= n[0].scrollHeight && (this.scrollTopVel = 0);
                    0 > this.scrollLeftVel ? 0 >= n.scrollLeft() && (this.scrollLeftVel = 0) : this.scrollLeftVel > 0 && n.scrollLeft() + n[0].clientWidth >= n[0].scrollWidth && (this.scrollLeftVel = 0)
                },
                scrollIntervalFunc: function() {
                    var n = this.scrollEl,
                        t = this.scrollIntervalMs / 1e3;
                    this.scrollTopVel && n.scrollTop(n.scrollTop() + this.scrollTopVel * t);
                    this.scrollLeftVel && n.scrollLeft(n.scrollLeft() + this.scrollLeftVel * t);
                    this.constrainScrollVel();
                    this.scrollTopVel || this.scrollLeftVel || this.stopScrolling()
                },
                stopScrolling: function() {
                    this.scrollIntervalId && (clearInterval(this.scrollIntervalId), this.scrollIntervalId = null, this.scrollStop())
                },
                scrollHandler: function() {
                    this.scrollIntervalId || this.scrollStop()
                },
                scrollStop: function() {}
            }),
            st = v.extend({
                coordMap: null,
                origCell: null,
                cell: null,
                coordAdjust: null,
                constructor: function(n, t) {
                    v.prototype.constructor.call(this, t);
                    this.coordMap = n
                },
                listenStart: function(n) {
                    var i, r, t, u = this.subjectEl;
                    v.prototype.listenStart.apply(this, arguments);
                    this.computeCoords();
                    n ? (r = {
                        left: n.pageX,
                        top: n.pageY
                    }, t = r, u && (i = yi(u), t = uu(t, i)), this.origCell = this.getCell(t.left, t.top), u && this.options.subjectCenter && (this.origCell && (i = ru(this.origCell, i) || i), t = fu(i)), this.coordAdjust = eu(t, r)) : (this.origCell = null, this.coordAdjust = null)
                },
                computeCoords: function() {
                    this.coordMap.build();
                    this.computeScrollBounds()
                },
                dragStart: function(n) {
                    var t;
                    v.prototype.dragStart.apply(this, arguments);
                    t = this.getCell(n.pageX, n.pageY);
                    t && this.cellOver(t)
                },
                drag: function(n, t, i) {
                    var r;
                    v.prototype.drag.apply(this, arguments);
                    r = this.getCell(i.pageX, i.pageY);
                    sr(r, this.cell) || (this.cell && this.cellOut(), r && this.cellOver(r))
                },
                dragStop: function() {
                    this.cellDone();
                    v.prototype.dragStop.apply(this, arguments)
                },
                cellOver: function(n) {
                    this.cell = n;
                    this.trigger("cellOver", n, sr(n, this.origCell), this.origCell)
                },
                cellOut: function() {
                    this.cell && (this.trigger("cellOut", this.cell), this.cellDone(), this.cell = null)
                },
                cellDone: function() {
                    this.cell && this.trigger("cellDone", this.cell)
                },
                listenStop: function() {
                    v.prototype.listenStop.apply(this, arguments);
                    this.origCell = this.cell = null;
                    this.coordMap.clear()
                },
                scrollStop: function() {
                    v.prototype.scrollStop.apply(this, arguments);
                    this.computeCoords()
                },
                getCell: function(n, t) {
                    return this.coordAdjust && (n += this.coordAdjust.left, t += this.coordAdjust.top), this.coordMap.getCell(n, t)
                }
            }),
            bf = s.extend({
                options: null,
                sourceEl: null,
                el: null,
                parentEl: null,
                top0: null,
                left0: null,
                mouseY0: null,
                mouseX0: null,
                topDelta: null,
                leftDelta: null,
                mousemoveProxy: null,
                isFollowing: !1,
                isHidden: !1,
                isAnimating: !1,
                constructor: function(t, i) {
                    this.options = i = i || {};
                    this.sourceEl = t;
                    this.parentEl = i.parentEl ? n(i.parentEl) : t.parent()
                },
                start: function(t) {
                    this.isFollowing || (this.isFollowing = !0, this.mouseY0 = t.pageY, this.mouseX0 = t.pageX, this.topDelta = 0, this.leftDelta = 0, this.isHidden || this.updatePosition(), n(document).on("mousemove", this.mousemoveProxy = h(this, "mousemove")))
                },
                stop: function(t, i) {
                    function r() {
                        this.isAnimating = !1;
                        f.destroyEl();
                        this.top0 = this.left0 = null;
                        i && i()
                    }
                    var f = this,
                        u = this.options.revertDuration;
                    this.isFollowing && !this.isAnimating && (this.isFollowing = !1, n(document).off("mousemove", this.mousemoveProxy), t && u && !this.isHidden ? (this.isAnimating = !0, this.el.animate({
                        top: this.top0,
                        left: this.left0
                    }, {
                        duration: u,
                        complete: r
                    })) : r())
                },
                getEl: function() {
                    var n = this.el;
                    return n || (this.sourceEl.width(), n = this.el = this.sourceEl.clone().css({
                        position: "absolute",
                        visibility: "",
                        display: this.isHidden ? "none" : "",
                        margin: 0,
                        right: "auto",
                        bottom: "auto",
                        width: this.sourceEl.width(),
                        height: this.sourceEl.height(),
                        opacity: this.options.opacity || "",
                        zIndex: this.options.zIndex
                    }).appendTo(this.parentEl)), n
                },
                destroyEl: function() {
                    this.el && (this.el.remove(), this.el = null)
                },
                updatePosition: function() {
                    var n, t;
                    this.getEl();
                    null === this.top0 && (this.sourceEl.width(), n = this.sourceEl.offset(), t = this.el.offsetParent().offset(), this.top0 = n.top - t.top, this.left0 = n.left - t.left);
                    this.el.css({
                        top: this.top0 + this.topDelta,
                        left: this.left0 + this.leftDelta
                    })
                },
                mousemove: function(n) {
                    this.topDelta = n.pageY - this.mouseY0;
                    this.leftDelta = n.pageX - this.mouseX0;
                    this.isHidden || this.updatePosition()
                },
                hide: function() {
                    this.isHidden || (this.isHidden = !0, this.el && this.el.hide())
                },
                show: function() {
                    this.isHidden && (this.isHidden = !1, this.updatePosition(), this.getEl().show())
                }
            }),
            br = s.extend({
                view: null,
                isRTL: null,
                cellHtml: "<td/>",
                constructor: function(n) {
                    this.view = n;
                    this.isRTL = n.opt("isRTL")
                },
                rowHtml: function(n, t) {
                    var i, u, f = this.getHtmlRenderer("cell", n),
                        r = "";
                    for (t = t || 0, i = 0; this.colCnt > i; i++) u = this.getCell(t, i), r += f(u);
                    return r = this.bookendCells(r, n, t), "<tr>" + r + "<\/tr>"
                },
                bookendCells: function(n, t, i) {
                    var r = this.getHtmlRenderer("intro", t)(i || 0),
                        u = this.getHtmlRenderer("outro", t)(i || 0),
                        f = this.isRTL ? u : r,
                        e = this.isRTL ? r : u;
                    return "string" == typeof n ? f + n + e : n.prepend(f).append(e)
                },
                getHtmlRenderer: function(n, t) {
                    var e, r, u, i, f = this.view;
                    return e = n + "Html", t && (r = t + au(n) + "Html"), r && (i = f[r]) ? u = f : r && (i = this[r]) ? u = this : (i = f[e]) ? u = f : (i = this[e]) && (u = this), "function" == typeof i ? function() {
                        return i.apply(u, arguments) || ""
                    } : function() {
                        return i || ""
                    }
                }
            }),
            c = i.Grid = br.extend({
                start: null,
                end: null,
                rowCnt: 0,
                colCnt: 0,
                rowData: null,
                colData: null,
                el: null,
                coordMap: null,
                elsByFill: null,
                externalDragStartProxy: null,
                colHeadFormat: null,
                eventTimeFormat: null,
                displayEventTime: null,
                displayEventEnd: null,
                cellDuration: null,
                largeUnit: null,
                constructor: function() {
                    br.apply(this, arguments);
                    this.coordMap = new pf(this);
                    this.elsByFill = {};
                    this.externalDragStartProxy = h(this, "externalDragStart")
                },
                computeColHeadFormat: function() {},
                computeEventTimeFormat: function() {
                    return this.view.opt("smallTimeFormat")
                },
                computeDisplayEventTime: function() {
                    return !0
                },
                computeDisplayEventEnd: function() {
                    return !0
                },
                setRange: function(n) {
                    var i, r, t = this.view;
                    this.start = n.start.clone();
                    this.end = n.end.clone();
                    this.rowData = [];
                    this.colData = [];
                    this.updateCells();
                    this.colHeadFormat = t.opt("columnFormat") || this.computeColHeadFormat();
                    this.eventTimeFormat = t.opt("eventTimeFormat") || t.opt("timeFormat") || this.computeEventTimeFormat();
                    i = t.opt("displayEventTime");
                    null == i && (i = this.computeDisplayEventTime());
                    r = t.opt("displayEventEnd");
                    null == r && (r = this.computeDisplayEventEnd());
                    this.displayEventTime = i;
                    this.displayEventEnd = r
                },
                updateCells: function() {},
                rangeToSegs: function() {},
                diffDates: function(n, t) {
                    return this.largeUnit ? ki(n, t, this.largeUnit) : bi(n, t)
                },
                getCell: function(t, i) {
                    var r;
                    return null == i && ("number" == typeof t ? (i = t % this.colCnt, t = Math.floor(t / this.colCnt)) : (i = t.col, t = t.row)), r = {
                        row: t,
                        col: i
                    }, n.extend(r, this.getRowData(t), this.getColData(i)), n.extend(r, this.computeCellRange(r)), r
                },
                computeCellRange: function(n) {
                    var t = this.computeCellDate(n);
                    return {
                        start: t,
                        end: t.clone().add(this.cellDuration)
                    }
                },
                computeCellDate: function() {},
                getRowData: function(n) {
                    return this.rowData[n] || {}
                },
                getColData: function(n) {
                    return this.colData[n] || {}
                },
                getRowEl: function() {},
                getColEl: function() {},
                getCellDayEl: function(n) {
                    return this.getColEl(n.col) || this.getRowEl(n.row)
                },
                computeRowCoords: function() {
                    for (var t, i, r = [], n = 0; this.rowCnt > n; n++) t = this.getRowEl(n), i = t.offset().top, r.push({
                        top: i,
                        bottom: i + t.outerHeight()
                    });
                    return r
                },
                computeColCoords: function() {
                    for (var t, i, r = [], n = 0; this.colCnt > n; n++) t = this.getColEl(n), i = t.offset().left, r.push({
                        left: i,
                        right: i + t.outerWidth()
                    });
                    return r
                },
                setElement: function(t) {
                    var i = this;
                    this.el = t;
                    t.on("mousedown", function(t) {
                        n(t.target).is(".fc-event-container *, .fc-more") || n(t.target).closest(".fc-popover").length || i.dayMousedown(t)
                    });
                    this.bindSegHandlers();
                    this.bindGlobalHandlers()
                },
                removeElement: function() {
                    this.unbindGlobalHandlers();
                    this.el.remove()
                },
                renderSkeleton: function() {},
                renderDates: function() {},
                destroyDates: function() {},
                bindGlobalHandlers: function() {
                    n(document).on("dragstart sortstart", this.externalDragStartProxy)
                },
                unbindGlobalHandlers: function() {
                    n(document).off("dragstart sortstart", this.externalDragStartProxy)
                },
                dayMousedown: function(n) {
                    var i, t, u = this,
                        r = this.view,
                        f = r.opt("selectable"),
                        e = new st(this.coordMap, {
                            scroll: r.opt("dragScroll"),
                            dragStart: function() {
                                r.unselect()
                            },
                            cellOver: function(n, r, e) {
                                e && (i = r ? n : null, f && (t = u.computeSelection(e, n), t ? u.renderSelection(t) : nt()))
                            },
                            cellOut: function() {
                                i = null;
                                t = null;
                                u.destroySelection();
                                w()
                            },
                            listenStop: function(n) {
                                i && r.trigger("dayClick", u.getCellDayEl(i), i.start, n);
                                t && r.reportSelection(t, n);
                                w()
                            }
                        });
                    e.mousedown(n)
                },
                renderRangeHelper: function(n, t) {
                    var i = this.fabricateHelperEvent(n, t);
                    this.renderHelper(i, t)
                },
                fabricateHelperEvent: function(n, t) {
                    var i = t ? it(t.event) : {};
                    return i.start = n.start.clone(), i.end = n.end ? n.end.clone() : null, i.allDay = null, this.view.calendar.normalizeEventRange(i), i.className = (i.className || []).concat("fc-helper"), t || (i.editable = !1), i
                },
                renderHelper: function() {},
                destroyHelper: function() {},
                renderSelection: function(n) {
                    this.renderHighlight(n)
                },
                destroySelection: function() {
                    this.destroyHighlight()
                },
                computeSelection: function(n, t) {
                    var i, r = [n.start, n.end, t.start, t.end];
                    return r.sort(vu), i = {
                        start: r[0].clone(),
                        end: r[3].clone()
                    }, this.view.calendar.isSelectionRangeAllowed(i) ? i : null
                },
                renderHighlight: function(n) {
                    this.renderFill("highlight", this.rangeToSegs(n))
                },
                destroyHighlight: function() {
                    this.destroyFill("highlight")
                },
                highlightSegClasses: function() {
                    return ["fc-highlight"]
                },
                renderFill: function() {},
                destroyFill: function(n) {
                    var t = this.elsByFill[n];
                    t && (t.remove(), delete this.elsByFill[n])
                },
                renderFillSegEls: function(t, i) {
                    var r, u = this,
                        f = this[t + "SegEl"],
                        e = "",
                        o = [];
                    if (i.length) {
                        for (r = 0; i.length > r; r++) e += this.fillSegHtml(t, i[r]);
                        n(e).each(function(t, r) {
                            var s = i[t],
                                e = n(r);
                            f && (e = f.call(u, s, e));
                            e && (e = n(e), e.is(u.fillSegTag) && (s.el = e, o.push(s)))
                        })
                    }
                    return o
                },
                fillSegTag: "div",
                fillSegHtml: function(n, t) {
                    var i = this[n + "SegClasses"],
                        r = this[n + "SegCss"],
                        u = i ? i.call(this, t) : [],
                        f = ut(r ? r.call(this, t) : {});
                    return "<" + this.fillSegTag + (u.length ? ' class="' + u.join(" ") + '"' : "") + (f ? ' style="' + f + '"' : "") + " />"
                },
                headHtml: function() {
                    return '<div class="fc-row ' + this.view.widgetHeaderClass + '"><table><thead>' + this.rowHtml("head") + "<\/thead><\/table><\/div>"
                },
                headCellHtml: function(n) {
                    var i = this.view,
                        t = n.start;
                    return '<th class="fc-day-header ' + i.widgetHeaderClass + " fc-" + pr[t.day()] + '">' + r(t.format(this.colHeadFormat)) + "<\/th>"
                },
                bgCellHtml: function(n) {
                    var r = this.view,
                        t = n.start,
                        i = this.getDayClasses(t);
                    return i.unshift("fc-day", r.widgetContentClass), '<td class="' + i.join(" ") + '" data-date="' + t.format("YYYY-MM-DD") + '"><\/td>'
                },
                getDayClasses: function(n) {
                    var i = this.view,
                        r = i.calendar.getNow().stripTime(),
                        t = ["fc-" + pr[n.day()]];
                    return 1 == i.intervalDuration.as("months") && n.month() != i.intervalStart.month() && t.push("fc-other-month"), n.isSame(r, "day") ? t.push("fc-today", i.highlightStateClass) : r > n ? t.push("fc-past") : t.push("fc-future"), t
                }
            });
        c.mixin({
            mousedOverSeg: null,
            isDraggingSeg: !1,
            isResizingSeg: !1,
            isDraggingExternal: !1,
            segs: null,
            renderEvents: function(n) {
                for (var u, f = this.eventsToSegs(n), t = [], i = [], r = 0; f.length > r; r++) u = f[r], wu(u.event) ? t.push(u) : i.push(u);
                t = this.renderBgSegs(t) || t;
                i = this.renderFgSegs(i) || i;
                this.segs = t.concat(i)
            },
            destroyEvents: function() {
                this.triggerSegMouseout();
                this.destroyFgSegs();
                this.destroyBgSegs();
                this.segs = null
            },
            getEventSegs: function() {
                return this.segs || []
            },
            renderFgSegs: function() {},
            destroyFgSegs: function() {},
            renderFgSegEls: function(t, i) {
                var r, e = this.view,
                    u = "",
                    f = [];
                if (t.length) {
                    for (r = 0; t.length > r; r++) u += this.fgSegHtml(t[r], i);
                    n(u).each(function(i, r) {
                        var u = t[i],
                            o = e.resolveEventEl(u.event, n(r));
                        o && (o.data("fc-seg", u), u.el = o, f.push(u))
                    })
                }
                return f
            },
            fgSegHtml: function() {},
            renderBgSegs: function(n) {
                return this.renderFill("bgEvent", n)
            },
            destroyBgSegs: function() {
                this.destroyFill("bgEvent")
            },
            bgEventSegEl: function(n, t) {
                return this.view.resolveEventEl(n.event, t)
            },
            bgEventSegClasses: function(n) {
                var t = n.event,
                    i = t.source || {};
                return ["fc-bgevent"].concat(t.className, i.className || [])
            },
            bgEventSegCss: function(n) {
                var i = this.view,
                    t = n.event,
                    r = t.source || {};
                return {
                    "background-color": t.backgroundColor || t.color || r.backgroundColor || r.color || i.opt("eventBackgroundColor") || i.opt("eventColor")
                }
            },
            businessHoursSegClasses: function() {
                return ["fc-nonbusiness", "fc-bgevent"]
            },
            bindSegHandlers: function() {
                var t = this,
                    i = this.view;
                n.each({
                    mouseenter: function(n, i) {
                        t.triggerSegMouseover(n, i)
                    },
                    mouseleave: function(n, i) {
                        t.triggerSegMouseout(n, i)
                    },
                    click: function(n, t) {
                        return i.trigger("eventClick", this, n.event, t)
                    },
                    mousedown: function(r, u) {
                        n(u.target).is(".fc-resizer") && i.isEventResizable(r.event) ? t.segResizeMousedown(r, u, n(u.target).is(".fc-start-resizer")) : i.isEventDraggable(r.event) && t.segDragMousedown(r, u)
                    }
                }, function(i, r) {
                    t.el.on(i, ".fc-event-container > *", function(i) {
                        var u = n(this).data("fc-seg");
                        if (u && !t.isDraggingSeg && !t.isResizingSeg) return r.call(this, u, i)
                    })
                })
            },
            triggerSegMouseover: function(n, t) {
                this.mousedOverSeg || (this.mousedOverSeg = n, this.view.trigger("eventMouseover", n.el[0], n.event, t))
            },
            triggerSegMouseout: function(n, t) {
                t = t || {};
                this.mousedOverSeg && (n = n || this.mousedOverSeg, this.mousedOverSeg = null, this.view.trigger("eventMouseout", n.el[0], n.event, t))
            },
            segDragMousedown: function(n, t) {
                var r, e = this,
                    i = this.view,
                    s = i.calendar,
                    o = n.el,
                    f = n.event,
                    u = new bf(n.el, {
                        parentEl: i.el,
                        opacity: i.opt("dragOpacity"),
                        revertDuration: i.opt("dragRevertDuration"),
                        zIndex: 2
                    }),
                    h = new st(i.coordMap, {
                        distance: 5,
                        scroll: i.opt("dragScroll"),
                        subjectEl: o,
                        subjectCenter: !0,
                        listenStart: function(n) {
                            u.hide();
                            u.start(n)
                        },
                        dragStart: function(t) {
                            e.triggerSegMouseout(n, t);
                            e.segDragStart(n, t);
                            i.hideEvent(f)
                        },
                        cellOver: function(t, o, h) {
                            n.cell && (h = n.cell);
                            r = e.computeEventDrop(h, t, f);
                            r && !s.isEventRangeAllowed(r, f) && (nt(), r = null);
                            r && i.renderDrag(r, n) ? u.hide() : u.show();
                            o && (r = null)
                        },
                        cellOut: function() {
                            i.destroyDrag();
                            u.show();
                            r = null
                        },
                        cellDone: function() {
                            w()
                        },
                        dragStop: function(t) {
                            u.stop(!r, function() {
                                i.destroyDrag();
                                i.showEvent(f);
                                e.segDragStop(n, t);
                                r && i.reportEventDrop(f, r, this.largeUnit, o, t)
                            })
                        },
                        listenStop: function() {
                            u.stop()
                        }
                    });
                h.mousedown(t)
            },
            segDragStart: function(n, t) {
                this.isDraggingSeg = !0;
                this.view.trigger("eventDragStart", n.el[0], n.event, t, {})
            },
            segDragStop: function(n, t) {
                this.isDraggingSeg = !1;
                this.view.trigger("eventDragStop", n.el[0], n.event, t, {})
            },
            computeEventDrop: function(n, t, i) {
                var u, r, e = this.view.calendar,
                    o = n.start,
                    f = t.start;
                return o.hasTime() === f.hasTime() ? (u = this.diffDates(f, o), i.allDay && at(u) ? (r = {
                    start: i.start.clone(),
                    end: e.getEventEnd(i),
                    allDay: !1
                }, e.normalizeEventRangeTimes(r)) : r = {
                    start: i.start.clone(),
                    end: i.end ? i.end.clone() : null,
                    allDay: i.allDay
                }, r.start.add(u), r.end && r.end.add(u)) : r = {
                    start: f.clone(),
                    end: null,
                    allDay: !f.hasTime()
                }, r
            },
            applyDragOpacity: function(n) {
                var t = this.view.opt("dragOpacity");
                null != t && n.each(function(n, i) {
                    i.style.opacity = t
                })
            },
            externalDragStart: function(t, i) {
                var r, u, f = this.view;
                f.opt("droppable") && (r = n((i ? i.item : null) || t.target), u = f.opt("dropAccept"), (n.isFunction(u) ? u.call(r[0], r) : r.is(u)) && (this.isDraggingExternal || this.listenToExternalDrag(r, t, i)))
            },
            listenToExternalDrag: function(n, t, i) {
                var f, u, r = this,
                    e = gu(n);
                f = new st(this.coordMap, {
                    listenStart: function() {
                        r.isDraggingExternal = !0
                    },
                    cellOver: function(n) {
                        u = r.computeExternalDrop(n, e);
                        u ? r.renderDrag(u) : nt()
                    },
                    cellOut: function() {
                        u = null;
                        r.destroyDrag();
                        w()
                    },
                    dragStop: function() {
                        r.destroyDrag();
                        w();
                        u && r.view.reportExternalDrop(e, u, n, t, i)
                    },
                    listenStop: function() {
                        r.isDraggingExternal = !1
                    }
                });
                f.startDrag(t)
            },
            computeExternalDrop: function(n, t) {
                var i = {
                    start: n.start.clone(),
                    end: null
                };
                return t.startTime && !i.start.hasTime() && i.start.time(t.startTime), t.duration && (i.end = i.start.clone().add(t.duration)), this.view.calendar.isExternalDropRangeAllowed(i, t.eventProps) ? i : null
            },
            renderDrag: function() {},
            destroyDrag: function() {},
            segResizeMousedown: function(n, t, i) {
                var o, r, f = this,
                    e = this.view,
                    s = e.calendar,
                    h = n.el,
                    u = n.event,
                    c = s.getEventEnd(u);
                o = new st(this.coordMap, {
                    distance: 5,
                    scroll: e.opt("dragScroll"),
                    subjectEl: h,
                    dragStart: function(t) {
                        f.triggerSegMouseout(n, t);
                        f.segResizeStart(n, t)
                    },
                    cellOver: function(t, o, h) {
                        r = i ? f.computeEventStartResize(h, t, u) : f.computeEventEndResize(h, t, u);
                        r && (s.isEventRangeAllowed(r, u) ? r.start.isSame(u.start) && r.end.isSame(c) && (r = null) : (nt(), r = null));
                        r && (e.hideEvent(u), f.renderEventResize(r, n))
                    },
                    cellOut: function() {
                        r = null
                    },
                    cellDone: function() {
                        f.destroyEventResize();
                        e.showEvent(u);
                        w()
                    },
                    dragStop: function(t) {
                        f.segResizeStop(n, t);
                        r && e.reportEventResize(u, r, this.largeUnit, h, t)
                    }
                });
                o.mousedown(t)
            },
            segResizeStart: function(n, t) {
                this.isResizingSeg = !0;
                this.view.trigger("eventResizeStart", n.el[0], n.event, t, {})
            },
            segResizeStop: function(n, t) {
                this.isResizingSeg = !1;
                this.view.trigger("eventResizeStop", n.el[0], n.event, t, {})
            },
            computeEventStartResize: function(n, t, i) {
                return this.computeEventResize("start", n, t, i)
            },
            computeEventEndResize: function(n, t, i) {
                return this.computeEventResize("end", n, t, i)
            },
            computeEventResize: function(n, t, i, r) {
                var u, f, e = this.view.calendar,
                    o = this.diffDates(i[n], t[n]);
                return u = {
                    start: r.start.clone(),
                    end: e.getEventEnd(r),
                    allDay: r.allDay
                }, u.allDay && at(o) && (u.allDay = !1, e.normalizeEventRangeTimes(u)), u[n].add(o), u.start.isBefore(u.end) || (f = r.allDay ? e.defaultAllDayEventDuration : e.defaultTimedEventDuration, this.cellDuration && f > this.cellDuration && (f = this.cellDuration), "start" == n ? u.start = u.end.clone().subtract(f) : u.end = u.start.clone().add(f)), u
            },
            renderEventResize: function() {},
            destroyEventResize: function() {},
            getEventTimeText: function(n, t, i) {
                return null == t && (t = this.eventTimeFormat), null == i && (i = this.displayEventEnd), this.displayEventTime && n.start.hasTime() ? i && n.end ? this.view.formatRange(n, t) : n.start.format(t) : ""
            },
            getSegClasses: function(n, t, i) {
                var r = n.event,
                    u = ["fc-event", n.isStart ? "fc-start" : "fc-not-start", n.isEnd ? "fc-end" : "fc-not-end"].concat(r.className, r.source ? r.source.className : []);
                return t && u.push("fc-draggable"), i && u.push("fc-resizable"), u
            },
            getEventSkinCss: function(n) {
                var t = this.view,
                    i = n.source || {},
                    r = n.color,
                    u = i.color,
                    f = t.opt("eventColor");
                return {
                    "background-color": n.backgroundColor || r || i.backgroundColor || u || t.opt("eventBackgroundColor") || f,
                    "border-color": n.borderColor || r || i.borderColor || u || t.opt("eventBorderColor") || f,
                    color: n.textColor || i.textColor || t.opt("eventTextColor")
                }
            },
            eventsToSegs: function(n, t) {
                for (var u = this.eventsToRanges(n), r = [], i = 0; u.length > i; i++) r.push.apply(r, this.eventRangeToSegs(u[i], t));
                return r
            },
            eventsToRanges: function(t) {
                var r = this,
                    u = ku(t),
                    i = [];
                return n.each(u, function(n, t) {
                    t.length && i.push.apply(i, bu(t[0]) ? r.eventsToInverseRanges(t) : r.eventsToNormalRanges(t))
                }), i
            },
            eventsToNormalRanges: function(n) {
                for (var i, r, u, e = this.view.calendar, f = [], t = 0; n.length > t; t++) i = n[t], r = i.start.clone().stripZone(), u = e.getEventEnd(i).stripZone(), f.push({
                    event: i,
                    start: r,
                    end: u,
                    eventStartMS: +r,
                    eventDurationMS: u - r
                });
                return f
            },
            eventsToInverseRanges: function(n) {
                var i, r, e = this.view,
                    h = e.start.clone().stripZone(),
                    o = e.end.clone().stripZone(),
                    u = this.eventsToNormalRanges(n),
                    f = [],
                    s = n[0],
                    t = h;
                for (u.sort(du), i = 0; u.length > i; i++) r = u[i], r.start > t && f.push({
                    event: s,
                    start: t,
                    end: r.start
                }), t = r.end;
                return o > t && f.push({
                    event: s,
                    start: t,
                    end: o
                }), f
            },
            eventRangeToSegs: function(n, t) {
                for (var u, i = t ? t(n) : this.rangeToSegs(n), r = 0; i.length > r; r++) u = i[r], u.event = n.event, u.eventStartMS = n.eventStartMS, u.eventDurationMS = n.eventDurationMS;
                return i
            }
        });
        i.compareSegs = d;
        i.dataAttrPrefix = "";
        g = c.extend({
            numbersVisible: !1,
            bottomCoordPadding: 0,
            breakOnWeeks: null,
            cellDates: null,
            dayToCellOffsets: null,
            rowEls: null,
            dayEls: null,
            helperEls: null,
            constructor: function() {
                c.apply(this, arguments);
                this.cellDuration = t.duration(1, "day")
            },
            renderDates: function(n) {
                for (var t, r, e = this.view, u = this.rowCnt, o = this.colCnt, s = u * o, f = "", i = 0; u > i; i++) f += this.dayRowHtml(i, n);
                for (this.el.html(f), this.rowEls = this.el.find(".fc-row"), this.dayEls = this.el.find(".fc-day"), t = 0; s > t; t++) r = this.getCell(t), e.trigger("dayRender", null, r.start, this.dayEls.eq(t))
            },
            destroyDates: function() {
                this.destroySegPopover()
            },
            renderBusinessHours: function() {
                var n = this.view.calendar.getBusinessHoursEvents(!0),
                    t = this.eventsToSegs(n);
                this.renderFill("businessHours", t, "bgevent")
            },
            dayRowHtml: function(n, t) {
                var r = this.view,
                    i = ["fc-row", "fc-week", r.widgetContentClass];
                return t && i.push("fc-rigid"), '<div class="' + i.join(" ") + '"><div class="fc-bg"><table>' + this.rowHtml("day", n) + '<\/table><\/div><div class="fc-content-skeleton"><table>' + (this.numbersVisible ? "<thead>" + this.rowHtml("number", n) + "<\/thead>" : "") + "<\/table><\/div><\/div>"
            },
            dayCellHtml: function(n) {
                return this.bgCellHtml(n)
            },
            computeColHeadFormat: function() {
                return this.rowCnt > 1 ? "ddd" : this.colCnt > 1 ? this.view.opt("dayOfMonthFormat") : "dddd"
            },
            computeEventTimeFormat: function() {
                return this.view.opt("extraSmallTimeFormat")
            },
            computeDisplayEventEnd: function() {
                return 1 == this.colCnt
            },
            updateCells: function() {
                var t, r, i, n;
                if (this.updateCellDates(), t = this.cellDates, this.breakOnWeeks) {
                    for (r = t[0].day(), n = 1; t.length > n && t[n].day() != r; n++);
                    i = Math.ceil(t.length / n)
                } else i = 1, n = t.length;
                this.rowCnt = i;
                this.colCnt = n
            },
            updateCellDates: function() {
                for (var u = this.view, n = this.start.clone(), r = [], t = -1, i = []; n.isBefore(this.end);) u.isHiddenDay(n) ? i.push(t + .5) : (t++, i.push(t), r.push(n.clone())), n.add(1, "days");
                this.cellDates = r;
                this.dayToCellOffsets = i
            },
            computeCellDate: function(n) {
                var t = this.colCnt,
                    i = n.row * t + (this.isRTL ? t - n.col - 1 : n.col);
                return this.cellDates[i].clone()
            },
            getRowEl: function(n) {
                return this.rowEls.eq(n)
            },
            getColEl: function(n) {
                return this.dayEls.eq(n)
            },
            getCellDayEl: function(n) {
                return this.dayEls.eq(n.row * this.colCnt + n.col)
            },
            computeRowCoords: function() {
                var n = c.prototype.computeRowCoords.call(this);
                return n[n.length - 1].bottom += this.bottomCoordPadding, n
            },
            rangeToSegs: function(n) {
                var o, s, u, f, h, c, l, t, i, r, v = this.isRTL,
                    y = this.rowCnt,
                    e = this.colCnt,
                    a = [];
                for (n = this.view.computeDayRange(n), o = this.dateToCellOffset(n.start), s = this.dateToCellOffset(n.end.subtract(1, "days")), u = 0; y > u; u++) f = u * e, h = f + e - 1, t = Math.max(f, o), i = Math.min(h, s), t = Math.ceil(t), i = Math.floor(i), i >= t && (c = t === o, l = i === s, t -= f, i -= f, r = {
                    row: u,
                    isStart: c,
                    isEnd: l
                }, v ? (r.leftCol = e - i - 1, r.rightCol = e - t - 1) : (r.leftCol = t, r.rightCol = i), a.push(r));
                return a
            },
            dateToCellOffset: function(n) {
                var t = this.dayToCellOffsets,
                    i = n.diff(this.start, "days");
                return 0 > i ? t[0] - 1 : i >= t.length ? t[t.length - 1] + 1 : t[i]
            },
            renderDrag: function(n, t) {
                return this.renderHighlight(this.view.calendar.ensureVisibleEventRange(n)), t && !t.el.closest(this.el).length ? (this.renderRangeHelper(n, t), this.applyDragOpacity(this.helperEls), !0) : void 0
            },
            destroyDrag: function() {
                this.destroyHighlight();
                this.destroyHelper()
            },
            renderEventResize: function(n, t) {
                this.renderHighlight(n);
                this.renderRangeHelper(n, t)
            },
            destroyEventResize: function() {
                this.destroyHighlight();
                this.destroyHelper()
            },
            renderHelper: function(t, i) {
                var u, f = [],
                    r = this.eventsToSegs([t]);
                r = this.renderFgSegEls(r);
                u = this.renderSegRows(r);
                this.rowEls.each(function(t, r) {
                    var o, s = n(r),
                        e = n('<div class="fc-helper-skeleton"><table/><\/div>');
                    o = i && i.row === t ? i.el.position().top : s.find(".fc-content-skeleton tbody").position().top;
                    e.css("top", o).find("table").append(u[t].tbodyEl);
                    s.append(e);
                    f.push(e[0])
                });
                this.helperEls = n(f)
            },
            destroyHelper: function() {
                this.helperEls && (this.helperEls.remove(), this.helperEls = null)
            },
            fillSegTag: "td",
            renderFill: function(t, i, r) {
                var u, f, e, o = [];
                for (i = this.renderFillSegEls(t, i), u = 0; i.length > u; u++) f = i[u], e = this.renderFillRow(t, f, r), this.rowEls.eq(f.row).append(e), o.push(e[0]);
                return this.elsByFill[t] = n(o), i
            },
            renderFillRow: function(t, i, r) {
                var f, u, s = this.colCnt,
                    e = i.leftCol,
                    o = i.rightCol + 1;
                return r = r || t.toLowerCase(), f = n('<div class="fc-' + r + '-skeleton"><table><tr/><\/table><\/div>'), u = f.find("tr"), e > 0 && u.append('<td colspan="' + e + '"/>'), u.append(i.el.attr("colspan", o - e)), s > o && u.append('<td colspan="' + (s - o) + '"/>'), this.bookendCells(u, t), f
            }
        });
        g.mixin({
            rowStructs: null,
            destroyEvents: function() {
                this.destroySegPopover();
                c.prototype.destroyEvents.apply(this, arguments)
            },
            getEventSegs: function() {
                return c.prototype.getEventSegs.call(this).concat(this.popoverSegs || [])
            },
            renderBgSegs: function(t) {
                var i = n.grep(t, function(n) {
                    return n.event.allDay
                });
                return c.prototype.renderBgSegs.call(this, i)
            },
            renderFgSegs: function(t) {
                var i;
                return t = this.renderFgSegEls(t), i = this.rowStructs = this.renderSegRows(t), this.rowEls.each(function(t, r) {
                    n(r).find(".fc-content-skeleton > table").append(i[t].tbodyEl)
                }), t
            },
            destroyFgSegs: function() {
                for (var n, t = this.rowStructs || []; n = t.pop();) n.tbodyEl.remove();
                this.rowStructs = null
            },
            renderSegRows: function(n) {
                for (var r = [], i = this.groupSegRows(n), t = 0; i.length > t; t++) r.push(this.renderSegRow(t, i[t]));
                return r
            },
            fgSegHtml: function(n, t) {
                var u, f, e = this.view,
                    i = n.event,
                    a = e.isEventDraggable(i),
                    s = !t && i.allDay && n.isStart && e.isEventResizableFromStart(i),
                    h = !t && i.allDay && n.isEnd && e.isEventResizableFromEnd(i),
                    c = this.getSegClasses(n, a, s || h),
                    l = ut(this.getEventSkinCss(i)),
                    o = "";
                return c.unshift("fc-day-grid-event", "fc-h-event"), n.isStart && (u = this.getEventTimeText(i), u && (o = '<span class="fc-time">' + r(u) + "<\/span>")), f = '<span class="fc-title">' + (r(i.title || "") || "&nbsp;") + "<\/span>", '<a class="' + c.join(" ") + '"' + (i.url ? ' href="' + r(i.url) + '"' : "") + (l ? ' style="' + l + '"' : "") + '><div class="fc-content">' + (this.isRTL ? f + " " + o : o + " " + f) + "<\/div>" + (s ? '<div class="fc-resizer fc-start-resizer" />' : "") + (h ? '<div class="fc-resizer fc-end-resizer" />' : "") + "<\/a>"
            },
            renderSegRow: function(t, i) {
                function y(t) {
                    for (; t > u;) r = (l[f - 1] || [])[u], r ? r.attr("rowspan", parseInt(r.attr("rowspan") || 1, 10) + 1) : (r = n("<td/>"), o.append(r)), c[f][u] = r, l[f][u] = r, u++
                }
                for (var s, u, o, h, e, r, w = this.colCnt, a = this.buildSegLevels(i), b = Math.max(1, a.length), p = n("<tbody/>"), v = [], c = [], l = [], f = 0; b > f; f++) {
                    if (s = a[f], u = 0, o = n("<tr/>"), v.push([]), c.push([]), l.push([]), s)
                        for (h = 0; s.length > h; h++) {
                            for (e = s[h], y(e.leftCol), r = n('<td class="fc-event-container"/>').append(e.el), e.leftCol != e.rightCol ? r.attr("colspan", e.rightCol - e.leftCol + 1) : l[f][u] = r; e.rightCol >= u;) c[f][u] = r, v[f][u] = e, u++;
                            o.append(r)
                        }
                    y(w);
                    this.bookendCells(o, "eventSkeleton");
                    p.append(o)
                }
                return {
                    row: t,
                    tbodyEl: p,
                    cellMatrix: c,
                    segMatrix: v,
                    segLevels: a,
                    segs: i
                }
            },
            buildSegLevels: function(n) {
                var r, u, t, i = [];
                for (n.sort(d), r = 0; n.length > r; r++) {
                    for (u = n[r], t = 0; i.length > t && nf(u, i[t]); t++);
                    u.level = t;
                    (i[t] || (i[t] = [])).push(u)
                }
                for (t = 0; i.length > t; t++) i[t].sort(tf);
                return i
            },
            groupSegRows: function(n) {
                for (var i = [], t = 0; this.rowCnt > t; t++) i.push([]);
                for (t = 0; n.length > t; t++) i[n[t].row].push(n[t]);
                return i
            }
        });
        g.mixin({
            segPopover: null,
            popoverSegs: null,
            destroySegPopover: function() {
                this.segPopover && this.segPopover.hide()
            },
            limitRows: function(n) {
                for (var i, r = this.rowStructs || [], t = 0; r.length > t; t++) this.unlimitRow(t), i = n ? "number" == typeof n ? n : this.computeRowLevelLimit(t) : !1, i !== !1 && this.limitRow(t, i)
            },
            computeRowLevelLimit: function(t) {
                function e(t, i) {
                    r = Math.max(r, n(i).outerHeight())
                }
                for (var u, r, o = this.rowEls.eq(t), s = o.height(), f = this.rowStructs[t].tbodyEl.children(), i = 0; f.length > i; i++)
                    if (u = f.eq(i).removeClass("fc-limited"), r = 0, u.find("> td > :first-child").each(e), u.position().top + r > s) return i;
                return !1
            },
            limitRow: function(t, i) {
                function tt(e) {
                    for (; e > f;) u = g.getCell(t, f), r = g.getCellSegs(u, i), r.length && (o = w[i - 1][f], y = g.renderMoreLink(u, r), c = n("<div/>").append(y), o.append(c), nt.push(c[0])), f++
                }
                var u, p, w, b, l, e, r, k, a, o, it, d, h, v, c, y, g = this,
                    s = this.rowStructs[t],
                    nt = [],
                    f = 0;
                if (i && s.segLevels.length > i) {
                    for (p = s.segLevels[i - 1], w = s.cellMatrix, b = s.tbodyEl.children().slice(i).addClass("fc-limited").get(), l = 0; p.length > l; l++) {
                        for (e = p[l], tt(e.leftCol), a = [], k = 0; e.rightCol >= f;) u = this.getCell(t, f), r = this.getCellSegs(u, i), a.push(r), k += r.length, f++;
                        if (k) {
                            for (o = w[i - 1][e.leftCol], it = o.attr("rowspan") || 1, d = [], h = 0; a.length > h; h++) v = n('<td class="fc-more-cell"/>').attr("rowspan", it), r = a[h], u = this.getCell(t, e.leftCol + h), y = this.renderMoreLink(u, [e].concat(r)), c = n("<div/>").append(y), v.append(c), d.push(v[0]), nt.push(v[0]);
                            o.addClass("fc-limited").after(n(d));
                            b.push(o[0])
                        }
                    }
                    tt(this.colCnt);
                    s.moreEls = n(nt);
                    s.limitedEls = n(b)
                }
            },
            unlimitRow: function(n) {
                var t = this.rowStructs[n];
                t.moreEls && (t.moreEls.remove(), t.moreEls = null);
                t.limitedEls && (t.limitedEls.removeClass("fc-limited"), t.limitedEls = null)
            },
            renderMoreLink: function(t, i) {
                var r = this,
                    u = this.view;
                return n('<a class="fc-more"/>').text(this.getMoreLinkText(i.length)).on("click", function(f) {
                    var e = u.opt("eventLimitClick"),
                        o = t.start,
                        s = n(this),
                        c = r.getCellDayEl(t),
                        l = r.getCellSegs(t),
                        h = r.resliceDaySegs(l, o),
                        a = r.resliceDaySegs(i, o);
                    "function" == typeof e && (e = u.trigger("eventLimitClick", null, {
                        date: o,
                        dayEl: c,
                        moreEl: s,
                        segs: h,
                        hiddenSegs: a
                    }, f));
                    "popover" === e ? r.showSegPopover(t, s, h) : "string" == typeof e && u.calendar.zoomTo(o, e)
                })
            },
            showSegPopover: function(n, t, i) {
                var e, r, u = this,
                    o = this.view,
                    f = t.parent();
                e = 1 == this.rowCnt ? o.el : this.rowEls.eq(n.row);
                r = {
                    className: "fc-more-popover",
                    content: this.renderSegPopoverContent(n, i),
                    parentEl: this.el,
                    top: e.offset().top,
                    autoHide: !0,
                    viewportConstrain: o.opt("popoverViewportConstrain"),
                    hide: function() {
                        u.segPopover.destroy();
                        u.segPopover = null;
                        u.popoverSegs = null
                    }
                };
                this.isRTL ? r.right = f.offset().left + f.outerWidth() + 1 : r.left = f.offset().left - 1;
                this.segPopover = new yf(r);
                this.segPopover.show()
            },
            renderSegPopoverContent: function(t, i) {
                var u, f = this.view,
                    o = f.opt("theme"),
                    s = t.start.format(f.opt("dayPopoverFormat")),
                    e = n('<div class="fc-header ' + f.widgetHeaderClass + '"><span class="fc-close ' + (o ? "ui-icon ui-icon-closethick" : "fc-icon fc-icon-x") + '"><\/span><span class="fc-title">' + r(s) + '<\/span><div class="fc-clear"/><\/div><div class="fc-body ' + f.widgetContentClass + '"><div class="fc-event-container"><\/div><\/div>'),
                    h = e.find(".fc-event-container");
                for (i = this.renderFgSegEls(i, !0), this.popoverSegs = i, u = 0; i.length > u; u++) i[u].cell = t, h.append(i[u].el);
                return e
            },
            resliceDaySegs: function(t, i) {
                var u = n.map(t, function(n) {
                        return n.event
                    }),
                    r = i.clone().stripTime(),
                    f = r.clone().add(1, "days"),
                    e = {
                        start: r,
                        end: f
                    };
                return t = this.eventsToSegs(u, function(n) {
                    var t = ct(n, e);
                    return t ? [t] : []
                }), t.sort(d), t
            },
            getMoreLinkText: function(n) {
                var t = this.view.opt("eventLimitText");
                return "function" == typeof t ? t(n) : "+" + n + " " + t
            },
            getCellSegs: function(n, t) {
                for (var i, u = this.rowStructs[n.row].segMatrix, r = t || 0, f = []; u.length > r;) i = u[r][n.col], i && f.push(i), r++;
                return f
            }
        });
        ui = c.extend({
            slotDuration: null,
            snapDuration: null,
            minTime: null,
            maxTime: null,
            axisFormat: null,
            dayEls: null,
            slatEls: null,
            slatTops: null,
            helperEl: null,
            businessHourSegs: null,
            constructor: function() {
                c.apply(this, arguments);
                this.processOptions()
            },
            renderDates: function() {
                this.el.html(this.renderHtml());
                this.dayEls = this.el.find(".fc-day");
                this.slatEls = this.el.find(".fc-slats tr")
            },
            renderBusinessHours: function() {
                var n = this.view.calendar.getBusinessHoursEvents();
                this.businessHourSegs = this.renderFill("businessHours", this.eventsToSegs(n), "bgevent")
            },
            renderHtml: function() {
                return '<div class="fc-bg"><table>' + this.rowHtml("slotBg") + '<\/table><\/div><div class="fc-slats"><table>' + this.slatRowHtml() + "<\/table><\/div>"
            },
            slotBgCellHtml: function(n) {
                return this.bgCellHtml(n)
            },
            slatRowHtml: function() {
                for (var n, i, u, f = this.view, o = this.isRTL, s = "", h = 0 == this.slotDuration.asMinutes() % 15, e = t.duration(+this.minTime); this.maxTime > e;) n = this.start.clone().time(e), i = n.minutes(), u = '<td class="fc-axis fc-time ' + f.widgetContentClass + '" ' + f.axisStyleAttr() + ">" + (h && i ? "" : "<span>" + r(n.format(this.axisFormat)) + "<\/span>") + "<\/td>", s += "<tr " + (i ? 'class="fc-minor"' : "") + ">" + (o ? "" : u) + '<td class="' + f.widgetContentClass + '"/>' + (o ? u : "") + "<\/tr>", e.add(this.slotDuration);
                return s
            },
            processOptions: function() {
                var n = this.view,
                    r = n.opt("slotDuration"),
                    i = n.opt("snapDuration");
                r = t.duration(r);
                i = i ? t.duration(i) : r;
                this.slotDuration = r;
                this.snapDuration = i;
                this.cellDuration = i;
                this.minTime = t.duration(n.opt("minTime"));
                this.maxTime = t.duration(n.opt("maxTime"));
                this.axisFormat = n.opt("axisFormat") || n.opt("smallTimeFormat")
            },
            computeColHeadFormat: function() {
                return this.colCnt > 1 ? this.view.opt("dayOfMonthFormat") : "dddd"
            },
            computeEventTimeFormat: function() {
                return this.view.opt("noMeridiemTimeFormat")
            },
            computeDisplayEventEnd: function() {
                return !0
            },
            updateCells: function() {
                for (var i = this.view, t = [], n = this.start.clone(); n.isBefore(this.end);) t.push({
                    day: n.clone()
                }), n.add(1, "day"), n = i.skipHiddenDays(n);
                this.isRTL && t.reverse();
                this.colData = t;
                this.colCnt = t.length;
                this.rowCnt = Math.ceil((this.maxTime - this.minTime) / this.snapDuration)
            },
            computeCellDate: function(n) {
                var t = this.computeSnapTime(n.row);
                return this.view.calendar.rezoneDate(n.day).time(t)
            },
            getColEl: function(n) {
                return this.dayEls.eq(n)
            },
            computeSnapTime: function(n) {
                return t.duration(this.minTime + this.snapDuration * n)
            },
            rangeToSegs: function(n) {
                var i, t, r, u, e = this.colCnt,
                    f = [];
                for (n = {
                        start: n.start.clone().stripZone(),
                        end: n.end.clone().stripZone()
                    }, t = 0; e > t; t++) r = this.colData[t].day, u = {
                    start: r.clone().time(this.minTime),
                    end: r.clone().time(this.maxTime)
                }, i = ct(n, u), i && (i.col = t, f.push(i));
                return f
            },
            updateSize: function(n) {
                this.computeSlatTops();
                n && this.updateSegVerticals()
            },
            computeRowCoords: function() {
                for (var t, r = this.el.offset().top, i = [], n = 0; this.rowCnt > n; n++) t = {
                    top: r + this.computeTimeTop(this.computeSnapTime(n))
                }, n > 0 && (i[n - 1].bottom = t.top), i.push(t);
                return t.bottom = t.top + this.computeTimeTop(this.computeSnapTime(n)), i
            },
            computeDateTop: function(n, i) {
                return this.computeTimeTop(t.duration(n.clone().stripZone() - i.clone().stripTime()))
            },
            computeTimeTop: function(n) {
                var i, u, r, f, t = (n - this.minTime) / this.slotDuration;
                return t = Math.max(0, t), t = Math.min(this.slatEls.length, t), i = Math.floor(t), u = t - i, r = this.slatTops[i], u ? (f = this.slatTops[i + 1], r + (f - r) * u) : r
            },
            computeSlatTops: function() {
                var t, i = [];
                this.slatEls.each(function(r, u) {
                    t = n(u).position().top;
                    i.push(t)
                });
                i.push(t + this.slatEls.last().outerHeight());
                this.slatTops = i
            },
            renderDrag: function(n, t) {
                return t ? (this.renderRangeHelper(n, t), this.applyDragOpacity(this.helperEl), !0) : (this.renderHighlight(this.view.calendar.ensureVisibleEventRange(n)), void 0)
            },
            destroyDrag: function() {
                this.destroyHelper();
                this.destroyHighlight()
            },
            renderEventResize: function(n, t) {
                this.renderRangeHelper(n, t)
            },
            destroyEventResize: function() {
                this.destroyHelper()
            },
            renderHelper: function(t, i) {
                var o, f, e, r, u = this.eventsToSegs([t]);
                for (u = this.renderFgSegEls(u), o = this.renderSegTable(u), f = 0; u.length > f; f++) e = u[f], i && i.col === e.col && (r = i.el, e.el.css({
                    left: r.css("left"),
                    right: r.css("right"),
                    "margin-left": r.css("margin-left"),
                    "margin-right": r.css("margin-right")
                }));
                this.helperEl = n('<div class="fc-helper-skeleton"/>').append(o).appendTo(this.el)
            },
            destroyHelper: function() {
                this.helperEl && (this.helperEl.remove(), this.helperEl = null)
            },
            renderSelection: function(n) {
                this.view.opt("selectHelper") ? this.renderRangeHelper(n) : this.renderHighlight(n)
            },
            destroySelection: function() {
                this.destroyHelper();
                this.destroyHighlight()
            },
            renderFill: function(t, i, r) {
                var h, f, c, u, e, a, v, l, o, s;
                if (i.length) {
                    for (i = this.renderFillSegEls(t, i), h = this.groupSegCols(i), r = r || t.toLowerCase(), f = n('<div class="fc-' + r + '-skeleton"><table><tr/><\/table><\/div>'), c = f.find("tr"), u = 0; h.length > u; u++)
                        if (e = h[u], a = n("<td/>").appendTo(c), e.length)
                            for (v = n('<div class="fc-' + r + '-container"/>').appendTo(a), l = this.colData[u].day, o = 0; e.length > o; o++) s = e[o], v.append(s.el.css({
                                top: this.computeDateTop(s.start, l),
                                bottom: -this.computeDateTop(s.end, l)
                            }));
                    this.bookendCells(c, t);
                    this.el.append(f);
                    this.elsByFill[t] = f
                }
                return i
            }
        });
        ui.mixin({
            eventSkeletonEl: null,
            renderFgSegs: function(t) {
                return t = this.renderFgSegEls(t), this.el.append(this.eventSkeletonEl = n('<div class="fc-content-skeleton"/>').append(this.renderSegTable(t))), t
            },
            destroyFgSegs: function() {
                this.eventSkeletonEl && (this.eventSkeletonEl.remove(), this.eventSkeletonEl = null)
            },
            renderSegTable: function(t) {
                var e, r, i, u, f, o, s = n("<table><tr/><\/table>"),
                    h = s.find("tr");
                for (e = this.groupSegCols(t), this.computeSegVerticals(t), u = 0; e.length > u; u++) {
                    for (f = e[u], rf(f), o = n('<div class="fc-event-container"/>'), r = 0; f.length > r; r++) i = f[r], i.el.css(this.generateSegPositionCss(i)), 30 > i.bottom - i.top && i.el.addClass("fc-short"), o.append(i.el);
                    h.append(n("<td/>").append(o))
                }
                return this.bookendCells(h, "eventSkeleton"), s
            },
            updateSegVerticals: function() {
                var n, t = (this.segs || []).concat(this.businessHourSegs || []);
                for (this.computeSegVerticals(t), n = 0; t.length > n; n++) t[n].el.css(this.generateSegVerticalCss(t[n]))
            },
            computeSegVerticals: function(n) {
                for (var t, i = 0; n.length > i; i++) t = n[i], t.top = this.computeDateTop(t.start, t.start), t.bottom = this.computeDateTop(t.end, t.start)
            },
            fgSegHtml: function(n, t) {
                var u, e, o, f = this.view,
                    i = n.event,
                    l = f.isEventDraggable(i),
                    a = !t && n.isStart && f.isEventResizableFromStart(i),
                    s = !t && n.isEnd && f.isEventResizableFromEnd(i),
                    h = this.getSegClasses(n, l, a || s),
                    c = ut(this.getEventSkinCss(i));
                return h.unshift("fc-time-grid-event", "fc-v-event"), f.isMultiDayEvent(i) ? (n.isStart || n.isEnd) && (u = this.getEventTimeText(n), e = this.getEventTimeText(n, "LT"), o = this.getEventTimeText(n, null, !1)) : (u = this.getEventTimeText(i), e = this.getEventTimeText(i, "LT"), o = this.getEventTimeText(i, null, !1)), '<a class="' + h.join(" ") + '"' + (i.url ? ' href="' + r(i.url) + '"' : "") + (c ? ' style="' + c + '"' : "") + '><div class="fc-content">' + (u ? '<div class="fc-time" data-start="' + r(o) + '" data-full="' + r(e) + '"><span>' + r(u) + "<\/span><\/div>" : "") + (i.title ? '<div class="fc-title">' + r(i.title) + "<\/div>" : "") + '<\/div><div class="fc-bg"/>' + (s ? '<div class="fc-resizer fc-end-resizer" />' : "") + "<\/a>"
            },
            generateSegPositionCss: function(n) {
                var u, f, e = this.view.opt("slotEventOverlap"),
                    i = n.backwardCoord,
                    r = n.forwardCoord,
                    t = this.generateSegVerticalCss(n);
                return e && (r = Math.min(1, i + 2 * (r - i))), this.isRTL ? (u = 1 - r, f = i) : (u = i, f = 1 - r), t.zIndex = n.level + 1, t.left = 100 * u + "%", t.right = 100 * f + "%", e && n.forwardPressure && (t[this.isRTL ? "marginLeft" : "marginRight"] = 20), t
            },
            generateSegVerticalCss: function(n) {
                return {
                    top: n.top,
                    bottom: -n.bottom
                }
            },
            groupSegCols: function(n) {
                for (var i = [], t = 0; this.colCnt > t; t++) i.push([]);
                for (t = 0; n.length > t; t++) i[n[t].col].push(n[t]);
                return i
            }
        });
        p = i.View = s.extend({
            type: null,
            name: null,
            title: null,
            calendar: null,
            options: null,
            coordMap: null,
            el: null,
            isDisplayed: !1,
            isSkeletonRendered: !1,
            isEventsRendered: !1,
            start: null,
            end: null,
            intervalStart: null,
            intervalEnd: null,
            intervalDuration: null,
            intervalUnit: null,
            isSelected: !1,
            scrollerEl: null,
            scrollTop: null,
            widgetHeaderClass: null,
            widgetContentClass: null,
            highlightStateClass: null,
            nextDayThreshold: null,
            isHiddenDayHash: null,
            documentMousedownProxy: null,
            constructor: function(n, i, r, u) {
                this.calendar = n;
                this.type = this.name = i;
                this.options = r;
                this.intervalDuration = u || t.duration(1, "day");
                this.nextDayThreshold = t.duration(this.opt("nextDayThreshold"));
                this.initThemingProps();
                this.initHiddenDays();
                this.documentMousedownProxy = h(this, "documentMousedown");
                this.initialize()
            },
            initialize: function() {},
            opt: function(n) {
                return this.options[n]
            },
            trigger: function(n, t) {
                var i = this.calendar;
                return i.trigger.apply(i, [n, t || this].concat(Array.prototype.slice.call(arguments, 2), [this]))
            },
            setDate: function(n) {
                this.setRange(this.computeRange(n))
            },
            setRange: function(t) {
                n.extend(this, t);
                this.updateTitle()
            },
            computeRange: function(n) {
                var r, u, f = lt(this.intervalDuration),
                    t = n.clone().startOf(f),
                    i = t.clone().add(this.intervalDuration);
                return /year|month|week|day/.test(f) ? (t.stripTime(), i.stripTime()) : (t.hasTime() || (t = this.calendar.rezoneDate(t)), i.hasTime() || (i = this.calendar.rezoneDate(i))), r = t.clone(), r = this.skipHiddenDays(r), u = i.clone(), u = this.skipHiddenDays(u, -1, !0), {
                    intervalUnit: f,
                    intervalStart: t,
                    intervalEnd: i,
                    start: r,
                    end: u
                }
            },
            computePrevDate: function(n) {
                return this.massageCurrentDate(n.clone().startOf(this.intervalUnit).subtract(this.intervalDuration), -1)
            },
            computeNextDate: function(n) {
                return this.massageCurrentDate(n.clone().startOf(this.intervalUnit).add(this.intervalDuration))
            },
            massageCurrentDate: function(n, t) {
                return 1 >= this.intervalDuration.as("days") && this.isHiddenDay(n) && (n = this.skipHiddenDays(n, t), n.startOf("day")), n
            },
            updateTitle: function() {
                this.title = this.computeTitle()
            },
            computeTitle: function() {
                return this.formatRange({
                    start: this.intervalStart,
                    end: this.intervalEnd
                }, this.opt("titleFormat") || this.computeTitleFormat(), this.opt("titleRangeSeparator"))
            },
            computeTitleFormat: function() {
                return "year" == this.intervalUnit ? "YYYY" : "month" == this.intervalUnit ? this.opt("monthYearFormat") : this.intervalDuration.as("days") > 1 ? "ll" : "LL"
            },
            formatRange: function(n, t, i) {
                var r = n.end;
                return r.hasTime() || (r = r.clone().subtract(1)), ur(n.start, r, t, i, this.opt("isRTL"))
            },
            setElement: function(n) {
                this.el = n;
                this.bindGlobalHandlers()
            },
            removeElement: function() {
                this.clear();
                this.isSkeletonRendered && (this.destroySkeleton(), this.isSkeletonRendered = !1);
                this.unbindGlobalHandlers();
                this.el.remove()
            },
            display: function(n) {
                var t = null;
                this.isDisplayed && (t = this.queryScroll());
                this.clear();
                this.setDate(n);
                this.render();
                this.updateSize();
                this.renderBusinessHours();
                this.isDisplayed = !0;
                t = this.computeInitialScroll(t);
                this.forceScroll(t);
                this.triggerRender()
            },
            clear: function() {
                this.isDisplayed && (this.unselect(), this.clearEvents(), this.triggerDestroy(), this.destroyBusinessHours(), this.destroy(), this.isDisplayed = !1)
            },
            render: function() {
                this.isSkeletonRendered || (this.renderSkeleton(), this.isSkeletonRendered = !0);
                this.renderDates()
            },
            destroy: function() {
                this.destroyDates()
            },
            renderSkeleton: function() {},
            destroySkeleton: function() {},
            renderDates: function() {},
            destroyDates: function() {},
            renderBusinessHours: function() {},
            destroyBusinessHours: function() {},
            triggerRender: function() {
                this.trigger("viewRender", this, this, this.el)
            },
            triggerDestroy: function() {
                this.trigger("viewDestroy", this, this, this.el)
            },
            bindGlobalHandlers: function() {
                n(document).on("mousedown", this.documentMousedownProxy)
            },
            unbindGlobalHandlers: function() {
                n(document).off("mousedown", this.documentMousedownProxy)
            },
            initThemingProps: function() {
                var n = this.opt("theme") ? "ui" : "fc";
                this.widgetHeaderClass = n + "-widget-header";
                this.widgetContentClass = n + "-widget-content";
                this.highlightStateClass = n + "-state-highlight"
            },
            updateSize: function(n) {
                var t;
                n && (t = this.queryScroll());
                this.updateHeight();
                this.updateWidth();
                n && this.setScroll(t)
            },
            updateWidth: function() {},
            updateHeight: function() {
                var n = this.calendar;
                this.setHeight(n.getSuggestedViewHeight(), n.isHeightAuto())
            },
            setHeight: function() {},
            computeScrollerHeight: function(n) {
                var t, i, r = this.scrollerEl;
                return t = this.el.add(r), t.css({
                    position: "relative",
                    left: -1
                }), i = this.el.outerHeight() - r.height(), t.css({
                    position: "",
                    left: ""
                }), n - i
            },
            computeInitialScroll: function() {
                return 0
            },
            queryScroll: function() {
                if (this.scrollerEl) return this.scrollerEl.scrollTop()
            },
            setScroll: function(n) {
                if (this.scrollerEl) return this.scrollerEl.scrollTop(n)
            },
            forceScroll: function(n) {
                var t = this;
                this.setScroll(n);
                setTimeout(function() {
                    t.setScroll(n)
                }, 0)
            },
            displayEvents: function(n) {
                var t = this.queryScroll();
                this.clearEvents();
                this.renderEvents(n);
                this.isEventsRendered = !0;
                this.setScroll(t);
                this.triggerEventRender()
            },
            clearEvents: function() {
                this.isEventsRendered && (this.triggerEventDestroy(), this.destroyEvents(), this.isEventsRendered = !1)
            },
            renderEvents: function() {},
            destroyEvents: function() {},
            triggerEventRender: function() {
                this.renderedEventSegEach(function(n) {
                    this.trigger("eventAfterRender", n.event, n.event, n.el)
                });
                this.trigger("eventAfterAllRender")
            },
            triggerEventDestroy: function() {
                this.renderedEventSegEach(function(n) {
                    this.trigger("eventDestroy", n.event, n.event, n.el)
                })
            },
            resolveEventEl: function(t, i) {
                var r = this.trigger("eventRender", t, t, i);
                return r === !1 ? i = null : r && r !== !0 && (i = n(r)), i
            },
            showEvent: function(n) {
                this.renderedEventSegEach(function(n) {
                    n.el.css("visibility", "")
                }, n)
            },
            hideEvent: function(n) {
                this.renderedEventSegEach(function(n) {
                    n.el.css("visibility", "hidden")
                }, n)
            },
            renderedEventSegEach: function(n, t) {
                for (var r = this.getEventSegs(), i = 0; r.length > i; i++) t && r[i].event._id !== t._id || r[i].el && n.call(this, r[i])
            },
            getEventSegs: function() {
                return []
            },
            isEventDraggable: function(n) {
                var t = n.source || {};
                return o(n.startEditable, t.startEditable, this.opt("eventStartEditable"), n.editable, t.editable, this.opt("editable"))
            },
            reportEventDrop: function(n, t, i, r, u) {
                var f = this.calendar,
                    e = f.mutateEvent(n, t, i),
                    o = function() {
                        e.undo();
                        f.reportEventChange()
                    };
                this.triggerEventDrop(n, e.dateDelta, o, r, u);
                f.reportEventChange()
            },
            triggerEventDrop: function(n, t, i, r, u) {
                this.trigger("eventDrop", r[0], n, t, i, u, {})
            },
            reportExternalDrop: function(t, i, r, u, f) {
                var e, o, s = t.eventProps;
                s && (e = n.extend({}, s, i), o = this.calendar.renderEvent(e, t.stick)[0]);
                this.triggerExternalDrop(o, i, r, u, f)
            },
            triggerExternalDrop: function(n, t, i, r, u) {
                this.trigger("drop", i[0], t.start, r, u);
                n && this.trigger("eventReceive", null, n)
            },
            renderDrag: function() {},
            destroyDrag: function() {},
            isEventResizableFromStart: function(n) {
                return this.opt("eventResizableFromStart") && this.isEventResizable(n)
            },
            isEventResizableFromEnd: function(n) {
                return this.isEventResizable(n)
            },
            isEventResizable: function(n) {
                var t = n.source || {};
                return o(n.durationEditable, t.durationEditable, this.opt("eventDurationEditable"), n.editable, t.editable, this.opt("editable"))
            },
            reportEventResize: function(n, t, i, r, u) {
                var f = this.calendar,
                    e = f.mutateEvent(n, t, i),
                    o = function() {
                        e.undo();
                        f.reportEventChange()
                    };
                this.triggerEventResize(n, e.durationDelta, o, r, u);
                f.reportEventChange()
            },
            triggerEventResize: function(n, t, i, r, u) {
                this.trigger("eventResize", r[0], n, t, i, u, {})
            },
            select: function(n, t) {
                this.unselect(t);
                this.renderSelection(n);
                this.reportSelection(n, t)
            },
            renderSelection: function() {},
            reportSelection: function(n, t) {
                this.isSelected = !0;
                this.trigger("select", null, n.start, n.end, t)
            },
            unselect: function(n) {
                this.isSelected && (this.isSelected = !1, this.destroySelection(), this.trigger("unselect", null, n))
            },
            destroySelection: function() {},
            documentMousedown: function(t) {
                var i;
                this.isSelected && this.opt("unselectAuto") && wi(t) && (i = this.opt("unselectCancel"), i && n(t.target).closest(i).length || this.unselect(t))
            },
            initHiddenDays: function() {
                var t, i = this.opt("hiddenDays") || [],
                    r = [],
                    u = 0;
                for (this.opt("weekends") === !1 && i.push(0, 6), t = 0; 7 > t; t++)(r[t] = -1 !== n.inArray(t, i)) || u++;
                if (!u) throw "invalid hiddenDays";
                this.isHiddenDayHash = r
            },
            isHiddenDay: function(n) {
                return t.isMoment(n) && (n = n.day()), this.isHiddenDayHash[n]
            },
            skipHiddenDays: function(n, t, i) {
                var r = n.clone();
                for (t = t || 1; this.isHiddenDayHash[(r.day() + (i ? t : 0) + 7) % 7];) r.add(t, "days");
                return r
            },
            computeDayRange: function(n) {
                var r, u = n.start.clone().stripTime(),
                    i = n.end,
                    t = null;
                return i && (t = i.clone().stripTime(), r = +i.time(), r && r >= this.nextDayThreshold && t.add(1, "days")), (!i || u >= t) && (t = u.clone().add(1, "days")), {
                    start: u,
                    end: t
                }
            },
            isMultiDayEvent: function(n) {
                var t = this.computeDayRange(n);
                return t.end.diff(t.start, "days") > 1
            }
        });
        e = i.Calendar = i.CalendarBase = s.extend({
            dirDefaults: null,
            langDefaults: null,
            overrides: null,
            options: null,
            viewSpecCache: null,
            view: null,
            header: null,
            constructor: sf,
            initOptions: function(n) {
                var i, t, u, r;
                n = gr(n);
                i = n.lang;
                t = y[i];
                t || (i = e.defaults.lang, t = y[i] || {});
                u = o(n.isRTL, t.isRTL, e.defaults.isRTL);
                r = u ? e.rtlDefaults : {};
                this.dirDefaults = r;
                this.langDefaults = t;
                this.overrides = n;
                this.options = k(e.defaults, r, t, n);
                ar(this.options);
                this.viewSpecCache = {}
            },
            getViewSpec: function(n) {
                var t = this.viewSpecCache;
                return t[n] || (t[n] = this.buildViewSpec(n))
            },
            getUnitViewSpec: function(t) {
                var r, u, f;
                if (-1 != n.inArray(t, ii))
                    for (r = this.header.getViewsWithButtons(), n.each(i.views, function(n) {
                            r.push(n)
                        }), u = 0; r.length > u; u++)
                        if (f = this.getViewSpec(r[u]), f && f.singleUnit == t) return f
            },
            buildViewSpec: function(n) {
                for (var f, u, e, i, o, r, l = this.overrides.views || {}, h = [], c = [], s = n; s && !f;) u = a[s] || {}, e = l[s] || {}, i = i || e.duration || u.duration, s = e.type || u.type, "function" == typeof u ? (f = u, h.unshift(f.defaults || {})) : h.unshift(u), c.unshift(e);
                if (f) return (r = {
                    "class": f,
                    type: n
                }, i && (i = t.duration(i), i.valueOf() || (i = null)), i && (r.duration = i, o = lt(i), 1 === i.as(o) && (r.singleUnit = o, c.unshift(l[o] || {}))), r.defaults = k.apply(null, h), r.overrides = k.apply(null, c), this.buildViewSpecOptions(r), this.buildViewSpecButtonText(r, n), r)
            },
            buildViewSpecOptions: function(n) {
                n.options = k(e.defaults, n.defaults, this.dirDefaults, this.langDefaults, this.overrides, n.overrides);
                ar(n.options)
            },
            buildViewSpecButtonText: function(n, t) {
                function i(i) {
                    var r = i.buttonText || {};
                    return r[t] || (n.singleUnit ? r[n.singleUnit] : null)
                }
                n.buttonTextOverride = i(this.overrides) || n.overrides.buttonText;
                n.buttonTextDefault = i(this.langDefaults) || i(this.dirDefaults) || n.defaults.buttonText || i(e.defaults) || (n.duration ? this.humanizeDuration(n.duration) : null) || t
            },
            instantiateView: function(n) {
                var t = this.getViewSpec(n);
                return new t["class"](this, n, t.options, t.duration)
            },
            isValidViewType: function(n) {
                return Boolean(this.getViewSpec(n))
            }
        });
        e.defaults = {
            titleRangeSeparator: " — ",
            monthYearFormat: "MMMM YYYY",
            defaultTimedEventDuration: "02:00:00",
            defaultAllDayEventDuration: {
                days: 1
            },
            forceEventDuration: !1,
            nextDayThreshold: "09:00:00",
            defaultView: "month",
            aspectRatio: 1.35,
            header: {
                left: "title",
                center: "",
                right: "today prev,next"
            },
            weekends: !0,
            weekNumbers: !1,
            weekNumberTitle: "W",
            weekNumberCalculation: "local",
            lazyFetching: !0,
            startParam: "start",
            endParam: "end",
            timezoneParam: "timezone",
            timezone: !1,
            isRTL: !1,
            buttonText: {
                prev: "prev",
                next: "next",
                prevYear: "prev year",
                nextYear: "next year",
                year: "year",
                today: "today",
                month: "month",
                week: "week",
                day: "day"
            },
            buttonIcons: {
                prev: "left-single-arrow",
                next: "right-single-arrow",
                prevYear: "left-double-arrow",
                nextYear: "right-double-arrow"
            },
            theme: !1,
            themeButtonIcons: {
                prev: "circle-triangle-w",
                next: "circle-triangle-e",
                prevYear: "seek-prev",
                nextYear: "seek-next"
            },
            dragOpacity: .75,
            dragRevertDuration: 500,
            dragScroll: !0,
            unselectAuto: !0,
            dropAccept: "*",
            eventLimit: !1,
            eventLimitText: "more",
            eventLimitClick: "popover",
            dayPopoverFormat: "LL",
            handleWindowResize: !0,
            windowResizeDelay: 200
        };
        e.englishDefaults = {
            dayPopoverFormat: "dddd, MMMM D"
        };
        e.rtlDefaults = {
            header: {
                left: "next,prev today",
                center: "",
                right: "title"
            },
            buttonIcons: {
                prev: "right-single-arrow",
                next: "left-single-arrow",
                prevYear: "right-double-arrow",
                nextYear: "left-double-arrow"
            },
            themeButtonIcons: {
                prev: "circle-triangle-e",
                next: "circle-triangle-w",
                nextYear: "seek-prev",
                prevYear: "seek-next"
            }
        };
        y = i.langs = {};
        i.datepickerLang = function(t, i, r) {
            var u = y[t] || (y[t] = {});
            u.isRTL = r.isRTL;
            u.weekNumberTitle = r.weekHeader;
            n.each(kf, function(n, t) {
                u[n] = t(r)
            });
            n.datepicker && (n.datepicker.regional[i] = n.datepicker.regional[t] = r, n.datepicker.regional.en = n.datepicker.regional[""], n.datepicker.setDefaults(r))
        };
        i.lang = function(t, i) {
            var r, u;
            r = y[t] || (y[t] = {});
            i && (r = y[t] = k(r, i));
            u = vr(t);
            n.each(df, function(n, t) {
                null == r[n] && (r[n] = t(u, r))
            });
            e.defaults.lang = t
        };
        var kf = {
                buttonText: function(n) {
                    return {
                        prev: yt(n.prevText),
                        next: yt(n.nextText),
                        today: yt(n.currentText)
                    }
                },
                monthYearFormat: function(n) {
                    return n.showMonthAfterYear ? "YYYY[" + n.yearSuffix + "] MMMM" : "MMMM YYYY[" + n.yearSuffix + "]"
                }
            },
            df = {
                dayOfMonthFormat: function(n, t) {
                    var i = n.longDateFormat("l");
                    return i = i.replace(/^Y+[^\w\s]*|[^\w\s]*Y+$/g, ""), t.isRTL ? i += " ddd" : i = "ddd " + i, i
                },
                mediumTimeFormat: function(n) {
                    return n.longDateFormat("LT").replace(/\s*a$/i, "a")
                },
                smallTimeFormat: function(n) {
                    return n.longDateFormat("LT").replace(":mm", "(:mm)").replace(/(\Wmm)$/, "($1)").replace(/\s*a$/i, "a")
                },
                extraSmallTimeFormat: function(n) {
                    return n.longDateFormat("LT").replace(":mm", "(:mm)").replace(/(\Wmm)$/, "($1)").replace(/\s*a$/i, "t")
                },
                hourFormat: function(n) {
                    return n.longDateFormat("LT").replace(":mm", "").replace(/(\Wmm)$/, "").replace(/\s*a$/i, "a")
                },
                noMeridiemTimeFormat: function(n) {
                    return n.longDateFormat("LT").replace(/\s*a$/i, "")
                }
            },
            gf = {
                smallDayDateFormat: function(n) {
                    return n.isRTL ? "D dd" : "dd D"
                },
                weekFormat: function(n) {
                    return n.isRTL ? "w[ " + n.weekNumberTitle + "]" : "[" + n.weekNumberTitle + " ]w"
                },
                smallWeekFormat: function(n) {
                    return n.isRTL ? "w[" + n.weekNumberTitle + "]" : "[" + n.weekNumberTitle + "]w"
                }
            };
        i.lang("en", e.englishDefaults);
        i.sourceNormalizers = [];
        i.sourceFetchers = [];
        kr = {
            dataType: "json",
            cache: !1
        };
        dr = 1;
        e.prototype.getPeerEvents = function(n) {
            for (var i, r = this.getEventCache(), u = [], t = 0; r.length > t; t++) i = r[t], n && n._id === i._id || u.push(i);
            return u
        };
        fi = a.basic = p.extend({
            dayGrid: null,
            dayNumbersVisible: !1,
            weekNumbersVisible: !1,
            weekNumberWidth: null,
            headRowEl: null,
            initialize: function() {
                this.dayGrid = new g(this);
                this.coordMap = this.dayGrid.coordMap
            },
            setRange: function(n) {
                p.prototype.setRange.call(this, n);
                this.dayGrid.breakOnWeeks = /year|month|week/.test(this.intervalUnit);
                this.dayGrid.setRange(n)
            },
            computeRange: function(n) {
                var t = p.prototype.computeRange.call(this, n);
                return /year|month/.test(t.intervalUnit) && (t.start.startOf("week"), t.start = this.skipHiddenDays(t.start), t.end.weekday() && (t.end.add(1, "week").startOf("week"), t.end = this.skipHiddenDays(t.end, -1, !0))), t
            },
            render: function() {
                this.dayNumbersVisible = this.dayGrid.rowCnt > 1;
                this.weekNumbersVisible = this.opt("weekNumbers");
                this.dayGrid.numbersVisible = this.dayNumbersVisible || this.weekNumbersVisible;
                this.el.addClass("fc-basic-view").html(this.renderHtml());
                this.headRowEl = this.el.find("thead .fc-row");
                this.scrollerEl = this.el.find(".fc-day-grid-container");
                this.dayGrid.coordMap.containerEl = this.scrollerEl;
                this.dayGrid.setElement(this.el.find(".fc-day-grid"));
                this.dayGrid.renderDates(this.hasRigidRows())
            },
            destroy: function() {
                this.dayGrid.destroyDates();
                this.dayGrid.removeElement()
            },
            renderBusinessHours: function() {
                this.dayGrid.renderBusinessHours()
            },
            renderHtml: function() {
                return '<table><thead class="fc-head"><tr><td class="' + this.widgetHeaderClass + '">' + this.dayGrid.headHtml() + '<\/td><\/tr><\/thead><tbody class="fc-body"><tr><td class="' + this.widgetContentClass + '"><div class="fc-day-grid-container"><div class="fc-day-grid"/><\/div><\/td><\/tr><\/tbody><\/table>'
            },
            headIntroHtml: function() {
                if (this.weekNumbersVisible) return '<th class="fc-week-number ' + this.widgetHeaderClass + '" ' + this.weekNumberStyleAttr() + "><span>" + r(this.opt("weekNumberTitle")) + "<\/span><\/th>"
            },
            numberIntroHtml: function(n) {
                if (this.weekNumbersVisible) return '<td class="fc-week-number" ' + this.weekNumberStyleAttr() + "><span>" + this.dayGrid.getCell(n, 0).start.format("w") + "<\/span><\/td>"
            },
            dayIntroHtml: function() {
                if (this.weekNumbersVisible) return '<td class="fc-week-number ' + this.widgetContentClass + '" ' + this.weekNumberStyleAttr() + "><\/td>"
            },
            introHtml: function() {
                if (this.weekNumbersVisible) return '<td class="fc-week-number" ' + this.weekNumberStyleAttr() + "><\/td>"
            },
            numberCellHtml: function(n) {
                var t, i = n.start;
                return this.dayNumbersVisible ? (t = this.dayGrid.getDayClasses(i), t.unshift("fc-day-number"), '<td class="' + t.join(" ") + '" data-date="' + i.format() + '">' + i.date() + "<\/td>") : "<td/>"
            },
            weekNumberStyleAttr: function() {
                return null !== this.weekNumberWidth ? 'style="width:' + this.weekNumberWidth + 'px"' : ""
            },
            hasRigidRows: function() {
                var n = this.opt("eventLimit");
                return n && "number" != typeof n
            },
            updateWidth: function() {
                this.weekNumbersVisible && (this.weekNumberWidth = li(this.el.find(".fc-week-number")))
            },
            setHeight: function(n, t) {
                var r, i = this.opt("eventLimit");
                ht(this.scrollerEl);
                si(this.headRowEl);
                this.dayGrid.destroySegPopover();
                i && "number" == typeof i && this.dayGrid.limitRows(i);
                r = this.computeScrollerHeight(n);
                this.setGridHeight(r, t);
                i && "number" != typeof i && this.dayGrid.limitRows(i);
                !t && ai(this.scrollerEl, r) && (oi(this.headRowEl, tt(this.scrollerEl)), r = this.computeScrollerHeight(n), this.scrollerEl.height(r))
            },
            setGridHeight: function(n, t) {
                t ? ci(this.dayGrid.rowEls) : hi(this.dayGrid.rowEls, n, !0)
            },
            renderEvents: function(n) {
                this.dayGrid.renderEvents(n);
                this.updateHeight()
            },
            getEventSegs: function() {
                return this.dayGrid.getEventSegs()
            },
            destroyEvents: function() {
                this.dayGrid.destroyEvents()
            },
            renderDrag: function(n, t) {
                return this.dayGrid.renderDrag(n, t)
            },
            destroyDrag: function() {
                this.dayGrid.destroyDrag()
            },
            renderSelection: function(n) {
                this.dayGrid.renderSelection(n)
            },
            destroySelection: function() {
                this.dayGrid.destroySelection()
            }
        });
        ei = a.month = fi.extend({
            computeRange: function(n) {
                var i, t = fi.prototype.computeRange.call(this, n);
                return this.isFixedWeeks() && (i = Math.ceil(t.end.diff(t.start, "weeks", !0)), t.end.add(6 - i, "weeks")), t
            },
            setGridHeight: function(n, t) {
                t = t || "variable" === this.opt("weekMode");
                t && (n *= this.rowCnt / 6);
                hi(this.dayGrid.rowEls, n, !t)
            },
            isFixedWeeks: function() {
                var n = this.opt("weekMode");
                return n ? "fixed" === n : this.opt("fixedWeekCount")
            }
        });
        ei.duration = {
            months: 1
        };
        ei.defaults = {
            fixedWeekCount: !0
        };
        a.basicWeek = {
            type: "basic",
            duration: {
                weeks: 1
            }
        };
        a.basicDay = {
            type: "basic",
            duration: {
                days: 1
            }
        };
        var ne = 5,
            te = a.agenda = p.extend({
                timeGrid: null,
                dayGrid: null,
                axisWidth: null,
                noScrollRowEls: null,
                bottomRuleEl: null,
                bottomRuleHeight: null,
                initialize: function() {
                    this.timeGrid = new ui(this);
                    this.opt("allDaySlot") ? (this.dayGrid = new g(this), this.coordMap = new wf([this.dayGrid.coordMap, this.timeGrid.coordMap])) : this.coordMap = this.timeGrid.coordMap
                },
                setRange: function(n) {
                    p.prototype.setRange.call(this, n);
                    this.timeGrid.setRange(n);
                    this.dayGrid && this.dayGrid.setRange(n)
                },
                render: function() {
                    this.el.addClass("fc-agenda-view").html(this.renderHtml());
                    this.scrollerEl = this.el.find(".fc-time-grid-container");
                    this.timeGrid.coordMap.containerEl = this.scrollerEl;
                    this.timeGrid.setElement(this.el.find(".fc-time-grid"));
                    this.timeGrid.renderDates();
                    this.bottomRuleEl = n('<hr class="fc-divider ' + this.widgetHeaderClass + '"/>').appendTo(this.timeGrid.el);
                    this.dayGrid && (this.dayGrid.setElement(this.el.find(".fc-day-grid")), this.dayGrid.renderDates(), this.dayGrid.bottomCoordPadding = this.dayGrid.el.next("hr").outerHeight());
                    this.noScrollRowEls = this.el.find(".fc-row:not(.fc-scroller *)")
                },
                destroy: function() {
                    this.timeGrid.destroyDates();
                    this.timeGrid.removeElement();
                    this.dayGrid && (this.dayGrid.destroyDates(), this.dayGrid.removeElement())
                },
                renderBusinessHours: function() {
                    this.timeGrid.renderBusinessHours();
                    this.dayGrid && this.dayGrid.renderBusinessHours()
                },
                renderHtml: function() {
                    return '<table><thead class="fc-head"><tr><td class="' + this.widgetHeaderClass + '">' + this.timeGrid.headHtml() + '<\/td><\/tr><\/thead><tbody class="fc-body"><tr><td class="' + this.widgetContentClass + '">' + (this.dayGrid ? '<div class="fc-day-grid"/><hr class="fc-divider ' + this.widgetHeaderClass + '"/>' : "") + '<div class="fc-time-grid-container"><div class="fc-time-grid"/><\/div><\/td><\/tr><\/tbody><\/table>'
                },
                headIntroHtml: function() {
                    var n, t;
                    return this.opt("weekNumbers") ? (n = this.timeGrid.getCell(0).start, t = n.format(this.opt("smallWeekFormat")), '<th class="fc-axis fc-week-number ' + this.widgetHeaderClass + '" ' + this.axisStyleAttr() + "><span>" + r(t) + "<\/span><\/th>") : '<th class="fc-axis ' + this.widgetHeaderClass + '" ' + this.axisStyleAttr() + "><\/th>"
                },
                dayIntroHtml: function() {
                    return '<td class="fc-axis ' + this.widgetContentClass + '" ' + this.axisStyleAttr() + "><span>" + (this.opt("allDayHtml") || r(this.opt("allDayText"))) + "<\/span><\/td>"
                },
                slotBgIntroHtml: function() {
                    return '<td class="fc-axis ' + this.widgetContentClass + '" ' + this.axisStyleAttr() + "><\/td>"
                },
                introHtml: function() {
                    return '<td class="fc-axis" ' + this.axisStyleAttr() + "><\/td>"
                },
                axisStyleAttr: function() {
                    return null !== this.axisWidth ? 'style="width:' + this.axisWidth + 'px"' : ""
                },
                updateSize: function(n) {
                    this.timeGrid.updateSize(n);
                    p.prototype.updateSize.call(this, n)
                },
                updateWidth: function() {
                    this.axisWidth = li(this.el.find(".fc-axis"))
                },
                setHeight: function(n, t) {
                    var i, r;
                    null === this.bottomRuleHeight && (this.bottomRuleHeight = this.bottomRuleEl.outerHeight());
                    this.bottomRuleEl.hide();
                    this.scrollerEl.css("overflow", "");
                    ht(this.scrollerEl);
                    si(this.noScrollRowEls);
                    this.dayGrid && (this.dayGrid.destroySegPopover(), i = this.opt("eventLimit"), i && "number" != typeof i && (i = ne), i && this.dayGrid.limitRows(i));
                    t || (r = this.computeScrollerHeight(n), ai(this.scrollerEl, r) ? (oi(this.noScrollRowEls, tt(this.scrollerEl)), r = this.computeScrollerHeight(n), this.scrollerEl.height(r)) : (this.scrollerEl.height(r).css("overflow", "hidden"), this.bottomRuleEl.show()))
                },
                computeInitialScroll: function() {
                    var i = t.duration(this.opt("scrollTime")),
                        n = this.timeGrid.computeTimeTop(i);
                    return n = Math.ceil(n), n && n++, n
                },
                renderEvents: function(n) {
                    for (var u, i = [], r = [], f = [], t = 0; n.length > t; t++) n[t].allDay ? i.push(n[t]) : r.push(n[t]);
                    u = this.timeGrid.renderEvents(r);
                    this.dayGrid && (f = this.dayGrid.renderEvents(i));
                    this.updateHeight()
                },
                getEventSegs: function() {
                    return this.timeGrid.getEventSegs().concat(this.dayGrid ? this.dayGrid.getEventSegs() : [])
                },
                destroyEvents: function() {
                    this.timeGrid.destroyEvents();
                    this.dayGrid && this.dayGrid.destroyEvents()
                },
                renderDrag: function(n, t) {
                    return n.start.hasTime() ? this.timeGrid.renderDrag(n, t) : this.dayGrid ? this.dayGrid.renderDrag(n, t) : void 0
                },
                destroyDrag: function() {
                    this.timeGrid.destroyDrag();
                    this.dayGrid && this.dayGrid.destroyDrag()
                },
                renderSelection: function(n) {
                    n.start.hasTime() || n.end.hasTime() ? this.timeGrid.renderSelection(n) : this.dayGrid && this.dayGrid.renderSelection(n)
                },
                destroySelection: function() {
                    this.timeGrid.destroySelection();
                    this.dayGrid && this.dayGrid.destroySelection()
                }
            });
        return te.defaults = {
            allDaySlot: !0,
            allDayText: "all-day",
            scrollTime: "06:00:00",
            slotDuration: "00:30:00",
            minTime: "00:00:00",
            maxTime: "24:00:00",
            slotEventOverlap: !0
        }, a.agendaWeek = {
            type: "agenda",
            duration: {
                weeks: 1
            }
        }, a.agendaDay = {
            type: "agenda",
            duration: {
                days: 1
            }
        }, i
    })