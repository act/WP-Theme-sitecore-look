define([],
	function () {
	    function cleanupForms() {
	        $("form").find("input[name^='DataItemModel']").each(function () {
	            $(this).attr("name", $(this).attr("name").replace("DataItemModel.", ""));
	        });
	        $("form").find("select[name^='DataItemModel']").each(function () {
	            $(this).attr("name", $(this).attr("name").replace("DataItemModel.", ""));
	        });
	    }

        return {
            init: cleanupForms
        }
	}
	);