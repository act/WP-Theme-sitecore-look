define([],
	function () {
	    function include(module) {
	        require([module], function (mod) {
	            if (mod != undefined && mod.init != undefined) {
	                mod.init();
	            }
	        });
	    }

	    return {
	        init: function (modules) {
	            if (modules == undefined || modules.split == undefined) {
	                return;
	            }

	            var moduleArray = modules.split(';');
	            for (var i = 0; i < moduleArray.length; i++) {
	                include(moduleArray[i]);
	            }
	        }

	    }

	}
	);