define(['includeModule'],
	function (includeModule) {
	    $(function () {
	        var modules = $(".require-js");
	        for (var i = 0; i < modules.length; i++) {
	            includeModule.init($(modules[i]).val());
	        }
	    });
	}
	);