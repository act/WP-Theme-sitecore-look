<?php
/**
 * The page template for our theme
 *
 */



  get_header();
  
  
  
//Get the page ID containing the sideboxes (can be a parent page)
$id_page_boxes=get_the_ID(); //by default, we'll display the sideboxes of current page. But this id will be updated to parent page if inherit has been selected
if (get_field('boxinherit') == 1) 
{
	$ancestors = get_ancestors( get_the_ID(), 'page' ); //get list of parent pages
	foreach ($ancestors as $ancestor) //find the closest parent with a left box title
	{
		if (get_field('boxinherit',$ancestor) == 0) 
		{ 
			$id_page_boxes=$ancestor; //get id of parent page
			break;
		}
	}
}

?>


<div class="row bg-light-grey">
    <div class="wrapper paddingtop">
      
        <div class="row-medium masonry-area">
			<?php if ( get_page_template_slug( get_the_ID() ) == "page-templates/nosidebar.php"){
				   echo "<div class=\"pane base12 t-base6 pane-around clearfix\">";
				}
				else echo "<div class=\"pane base8 t-base6 pane-around clearfix\">";
			?>          
				<article class="bg-white pad-around">
				<?php /* $attachments = new Attachments( 'attachments' ); ?>
				<?php if( $attachments->exist() ) : ?>
					<div class="white-slick bottom-nav-slick slick-small image-carousel row-margin">
					<?php while( $attachments->get() ) : 
							if ($attachments->type()=="image") :?>
						
								<img src='<?php echo $attachments->src( 'full' ); ?>' class='img-scale' alt='<?php echo $attachments->field( 'title' ); ?>' />
							<?php 
							endif;
						endwhile; ?>
					</div>
				<?php endif; */ ?>
				<?php
				while (have_posts()) :
					the_post();
					the_post_thumbnail( array(200,200)  );
					the_content();

				endwhile; 	
				if (get_field('include_page')!="") {include_external_page (get_field('include_page')); }
							
				
				?>
				</article>
				
			</div>
			
		<?php if (get_field('box1content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box1content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box2content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box2content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box3content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box3content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
		<?php if (get_field('box4content',$id_page_boxes) != "") { ?>
			<div class="pane base4 t-base6 pane-around">            
				<article class="bg-white pad-around">
						<?php the_field('box4content',$id_page_boxes); ?>
				</article>
			</div>
		<?php } ?>
			<?php
				get_sidebar();
			?>
        </div>


    </div>
</div>
<?php 
	get_footer();
 ?>
