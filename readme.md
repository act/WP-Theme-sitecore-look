# WP theme Sitecore look

The development of this Wordpress theme has been part of the broader "Wordpress for Research" project, in Spring 2017.
The theme is not fully compliant with the Wordpress standards as it doesn't properly deal with blogs and comments. Not all page types are correctly handled.
This theme is only intended to be used for a website similar to what would have been done in Sitecore.
For proper blogging, please use another dedicated theme!


### Prerequisites

Wordpress 4.8 and above

```
Example: https://research.reading.ac.uk/theme-environment/
```

### Installing

tbc


## Deployment

Copy the files into the Wordpress subdirectory: ..../wp-content/themes/
Then, activate the theme in wordpress

## Built With

The structure and scripts used in this theme are copied from the original Active-Edition template.

## Contributing

UoR internal contribution only

## Versioning

tbc

## Authors

University of Reading / Academic Computing Team / Eric Mathieu

## License

All developments are the property of the University of Reading.
No copy authorized.
